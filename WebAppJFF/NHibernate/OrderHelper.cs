﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using System.ComponentModel;
using NHibernate.Criterion;

namespace MPS.NHibernate {
  public static class OrderHelper {
    public static Order[] FromString(string properties) {
      string[] parts = properties.Split(',');
      Order[] result = new Order[parts.Length];
      for (int i = 0; i < parts.Length; i++) {
        string property = parts[i].Trim();
        if (property.EndsWith(" DESC", StringComparison.InvariantCultureIgnoreCase))
          result[i] = FromListDirection(property.Substring(0, property.Length - " DESC".Length).Trim(), ListSortDirection.Descending);
        else if (property.EndsWith(" ASC", StringComparison.InvariantCultureIgnoreCase))
          result[i] = FromListDirection(property.Substring(0, property.Length - " ASC".Length).Trim(), ListSortDirection.Ascending);
        else
          result[i] = FromListDirection(property, ListSortDirection.Ascending);
      }
      return result;
    }

    public static Order FromListDirection(string propertyName, ListSortDirection direction) {
      switch (direction) {
        case ListSortDirection.Ascending:
          return Order.Asc(propertyName);
        case ListSortDirection.Descending:
          return Order.Desc(propertyName);
        default:
          throw new ArgumentOutOfRangeException("direction");
      }
    }

    public static bool IsOrderConstraints(ref Order order, out string[] classes) {
      classes = null;
      string property = order.ToString();
      string propertyName;
      ListSortDirection direction = ListSortDirection.Ascending;
      if (property.EndsWith(" DESC", StringComparison.InvariantCultureIgnoreCase)) {
        propertyName = property.Substring(0, property.Length - " DESC".Length).Trim();
        direction = ListSortDirection.Descending;
      } else if (property.EndsWith(" ASC", StringComparison.InvariantCultureIgnoreCase))
        propertyName = property.Substring(0, property.Length - " ASC".Length).Trim();
      else
        propertyName = property;
      if (!propertyName.Contains("."))
        return false;
      string[] partes = propertyName.Split('.');
      order = FromListDirection(partes[partes.Length - 1], direction);
      classes = partes.Take(partes.Length - 1).ToArray();
      return true;
    }
  }
}
