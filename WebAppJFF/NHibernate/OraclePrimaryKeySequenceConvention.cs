﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace MPS.NHibernate {
  public class OraclePrimaryKeySequenceConvention : IIdConvention {

    public void Apply(IIdentityInstance instance) {
      if(instance.EntityType.Name.Length > 26)
        instance.GeneratedBy.Sequence(string.Format("Seq_{0}", instance.EntityType.Name.Substring(0,26)));
      else
        instance.GeneratedBy.Sequence(string.Format("Seq_{0}", instance.EntityType.Name));
    }
  }
}
