﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPS.NHibernate {
  public class SessionTransactionScope : IDisposable {
    public SessionTransactionScope() {
      if (!SessionHelper.HasCurrent) {
        ownsSession = true;
        SessionHelper.OpenSession();
      }
      session = SessionHelper.Current;
      if (session.Transaction.IsActive)
        transaction = session.Transaction;
      else {
        ownsTransaction = true;
        transaction = session.BeginTransaction();
      }
    }

    private bool ownsSession;
    private ISession session;
    private bool ownsTransaction;
    private ITransaction transaction;
    private bool complete;

    public void Complete() {
      if (disposed)
        throw new ObjectDisposedException("O escopo de sessão já foi liberado.");
      if (complete)
        throw new InvalidOperationException("O escopo de sessão já foi completado.");
      if (session.Transaction != transaction)
        throw new InvalidOperationException("A transação da sessão do NHibernate foi alterada.");
      if (!session.Transaction.IsActive)
        throw new InvalidOperationException("A transação da sessão do NHibernate não está ativa.");
      session.Flush();
      if (ownsTransaction)
        transaction.Commit();
      complete = true;
    }

    private bool disposed;

    public void Dispose() {
      if (!disposed) {
        disposed = true;
        if (transaction != null) {
          if (ownsTransaction) {
            if (!complete)
              transaction.Rollback();
            transaction.Dispose();
          }
          transaction = null;
        }
        if (session != null) {
          if (ownsSession)
            session.Dispose();
          session = null;
        }
      }
    }
  }
}
