﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MPS.NHibernate.Log {
  public class LogPropertyCollection {
    private object[] state;
    private string[] propertyNames;

    public object this[string property] {
      get {
        int indice = Array.FindIndex(propertyNames, n => n == property);
        return state[indice];
        ;
      }
    }

    public LogPropertyCollection(object[] state, string[] propertyNames) {
      this.state = state;
      this.propertyNames = propertyNames;
    }
  }
}
