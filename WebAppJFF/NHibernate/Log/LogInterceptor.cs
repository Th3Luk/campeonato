﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using System.Web;
using NHibernate.Collection;
using System.Collections;
using NHibernate.Persister.Collection;

namespace MPS.NHibernate.Log {
  public class LogInterceptor : IInterceptor {

    public LogInterceptor() {

      EntidadesParaLogarInsercao = new List<IEntidadeComLog>();
    }

    ICollection<IEntidadeComLog> EntidadesParaLogarInsercao;

    #region IInterceptor Members

    public void AfterTransactionBegin(ITransaction tx) {

    }

    public void AfterTransactionCompletion(ITransaction tx) {
    }

    public void BeforeTransactionCompletion(ITransaction tx) {
    }

    public int[] FindDirty(object entity, object id, object[] currentState, object[] previousState, string[] propertyNames, global::NHibernate.Type.IType[] types) {
      return null;
    }

    public object GetEntity(string entityName, object id) {
      return null;
    }

    public string GetEntityName(object entity) {
      return null;
    }

    public object Instantiate(string entityName, EntityMode entityMode, object id) {
      return null;
    }

    public bool? IsTransient(object entity) {
      return null;
    }


    private String getPropertyName(AbstractPersistentCollection collection) {
      String propertyName = collection.Role;
      if (propertyName != null) {
        // Hibernate role is <className>.<propertyName>
        int lastDot = propertyName.LastIndexOf('.');
        if (lastDot != -1)
          propertyName = propertyName.Substring(lastDot + 1);

      } else if (collection.Owner != null) {
        ISession session = SessionHelper.Current;
        ICollectionPersister collectionPersister = session.GetSessionImplementation().PersistenceContext.GetCollectionEntry(collection).CurrentPersister;
        String role = collectionPersister.CollectionMetadata.Role;
        propertyName = role.Substring(role.LastIndexOf('.') != -1 ? role.LastIndexOf('.') + 1 : 0);

      }
      return propertyName;
    }

    private void onCollectionCreateRemove(object collection, object key, bool isCreate) {
      AbstractPersistentCollection c = collection as AbstractPersistentCollection;
      if (c == null) {
        return;
      }
      Object entidadeComLog = c.Owner;
      if (entidadeComLog is IEntidadeComLog) {
        IEnumerable listaAtual = (IEnumerable)collection;
        String propertyName = getPropertyName(c);
        for (int i = 0; i < listaAtual.Cast<object>().Count(); i++) {
          object elemento = listaAtual.Cast<object>().ElementAt(i);
          IEntidadeParaLog log = ((IEntidadeComLog)entidadeComLog).ObterEntidadeLogColecao(propertyName, (int)key, elemento);
          if (log != null) {
            if (isCreate)
              GerarLog(log, OperacaoLog.Inclusao);
            else
              GerarLog(log, OperacaoLog.Exclusao);
          } else {
            //a entidade não implementa log a coleção
            break;
          }
        }
      }
    }

    public void OnCollectionRecreate(object collection, object key) {
      onCollectionCreateRemove(collection, key, true);

    }

    public void OnCollectionRemove(object collection, object key) {
      onCollectionCreateRemove(collection, key, false);
    }

    public void OnCollectionUpdate(object collection, object key) {
      AbstractPersistentCollection c = collection as AbstractPersistentCollection;
      if ((c == null) || (!c.IsDirty)) {
        return;
      }
      Object entidadeComLog = c.Owner;
      if (entidadeComLog is IEntidadeComLog) {
        IEnumerable listaAtual = (IEnumerable)collection;
        IEnumerable listaAnterior = c.StoredSnapshot as IEnumerable;
        string nomePropriedade = getPropertyName(c);
        for (int i = 0; i < listaAtual.Cast<object>().Count(); i++) {
          object elemento = listaAtual.Cast<object>().ElementAt(i);
          IEntidadeParaLog log = ((IEntidadeComLog)entidadeComLog).ObterEntidadeLogColecao(nomePropriedade, (int)key, elemento);
          if (log != null) {
            if (c.NeedsInserting(elemento, i, NHibernateUtil.Object)) {
              GerarLog(log, OperacaoLog.Inclusao);
            } else if (c.NeedsUpdating(elemento, i, NHibernateUtil.Object)) {
              GerarLog(log, OperacaoLog.Atualizacao);
            }
          } else {
            //a entidade não implementa log a coleção
            break;
          }
        }
        if (listaAnterior != null || c.StoredSnapshot != null) {
          listaAnterior = c.StoredSnapshot as IEnumerable; //StoredSnapshot retorna null de vez enquando então força a leitura novamente
          foreach (var elemento in listaAnterior) {
            if (!listaAtual.Cast<object>().Contains(elemento)) {
              IEntidadeParaLog log = ((IEntidadeComLog)entidadeComLog).ObterEntidadeLogColecao(nomePropriedade, (int)key, elemento);
              if (log != null) {
                GerarLog(log, OperacaoLog.Exclusao);
              } else {
                //a entidade não implementa log para a coleção
                break;
              }
            }
          }
        }
      }

    }

    public void OnDelete(object entity, object id, object[] state, string[] propertyNames, global::NHibernate.Type.IType[] types) {
      IEntidadeComLog entidade = entity as IEntidadeComLog;
      //testa se a entidade deve fazer log
      if (entidade != null)
        GerarLog(entidade.ObterEntidadeExclusaoLog(new LogPropertyCollection(state, propertyNames), (int)id), OperacaoLog.Exclusao);
    }

    public bool OnFlushDirty(object entity, object id, object[] currentState, object[] previousState, string[] propertyNames, global::NHibernate.Type.IType[] types) {
      IEntidadeComLog entidade = entity as IEntidadeComLog;
      //testa se a entidade deve fazer log
      if (entidade != null)
        GerarLog(entidade.ObterEntidadeLog(), OperacaoLog.Atualizacao);
      return true;
    }

    public bool OnLoad(object entity, object id, object[] state, string[] propertyNames, global::NHibernate.Type.IType[] types) {
      return true;
    }

    public global::NHibernate.SqlCommand.SqlString OnPrepareStatement(global::NHibernate.SqlCommand.SqlString sql) {
      return sql;
    }

    public bool OnSave(object entity, object id, object[] state, string[] propertyNames, global::NHibernate.Type.IType[] types) {
      //OBS: os objetos que são inseridos devem ser guardados em uma lista para serem logados
      //após o flush com o banco, pois só então saberemos o seu Id (gerado pelo Identity do banco)
      IEntidadeParaLog entidadeParaLog = entity as IEntidadeParaLog;
      //como as entidades para log também passam por esse método, deve-se testar se não é uma entidade
      //para log que está sendo salva, que obviamente, não precisa ser logada
      if (entidadeParaLog == null) {
        IEntidadeComLog entidadeComLog = entity as IEntidadeComLog;
        if (entidadeComLog != null)
          EntidadesParaLogarInsercao.Add(entidadeComLog);
      }
      return true;
    }

    public void PostFlush(System.Collections.ICollection entities) {
      bool gerouLog = false;
      foreach (IEntidadeComLog e in EntidadesParaLogarInsercao) {
        GerarLog(e.ObterEntidadeLog(), OperacaoLog.Inclusao);
        gerouLog = true;
      }
      EntidadesParaLogarInsercao.Clear();
      if (gerouLog)
        SessionHelper.Current.Flush();
    }

    public void PreFlush(System.Collections.ICollection entities) {

    }

    public void SetSession(ISession session) {

    }

    #endregion

    private void GerarLog(IEntidadeParaLog entidadeLog, OperacaoLog operacao) {
      entidadeLog.DataOperacao = DateTime.Now;
      entidadeLog.Operacao = (char)operacao;
      if ((HttpContext.Current != null) && !String.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
        entidadeLog.UsuarioOperacao = HttpContext.Current.User.Identity.Name;
      else
        entidadeLog.UsuarioOperacao = "Integração Sistema";
      SalvarEntidadeLog(entidadeLog);
    }

    private void SalvarEntidadeLog(IEntidadeParaLog log) {
        ISession session = SessionHelper.Current;
        session.Save(log);      
    }

    public enum OperacaoLog {
      Inclusao = 'I',
      Atualizacao = 'A',
      Exclusao = 'E'
    }
  }
}
