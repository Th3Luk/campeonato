﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MPS.NHibernate.Log {
  public interface IEntidadeParaLog {

    DateTime DataOperacao { get; set; }
    char Operacao { get; set; }
    int IdOriginal { get; set; }
    string UsuarioOperacao { get; set; }
  }
}
