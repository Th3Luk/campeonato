﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MPS.NHibernate.Log {
  public interface IEntidadeComLog {
    IEntidadeParaLog ObterEntidadeLog();
    IEntidadeParaLog ObterEntidadeExclusaoLog(LogPropertyCollection props, int idOriginal);
    IEntidadeParaLog ObterEntidadeLogColecao(string propriedade, int idOriginal, object entidade);
  }
}
