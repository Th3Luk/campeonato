﻿using System;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;
using System.Linq;


namespace MPS.NHibernate {
  public class ForeignKeyNameConvention : IReferenceConvention, IHasManyConvention, IJoinedSubclassConvention, ICollectionConvention, IHasManyToManyConvention {

    private string AbreviarPalavrasConhecidas(string texto) {
      return texto.Replace("Funcionario", "Func").Replace("Arquivo", "Arq").Replace("Documento", "Doc")
                  .Replace("LocalFisico", "Local").Replace("Movimento", "Mov").Replace("Questionario", "Ques")
                  .Replace("Protocolo", "Prot").Replace("Processo", "Proc").Replace("Expediente", "Exp")
                  .Replace("Requerimento", "Req").Replace("Destinatario", "Des").Replace("Distribuicao", "Dist")
                  .Replace("Informativo", "Inf").Replace("Juntada", "Junt").Replace("Lembrete", "Lemb")
                  .Replace("Automatico", "Aut").Replace("Assinatura", "Ass").Replace("Resposta", "Resp")
                  .Replace("Situacao", "Sit").Replace("Usuario", "Usu").Replace("Versao", "Ver")
                  .Replace("PalavrasChave", "PalChav").Replace("Pessoa", "Pes").Replace("Destino", "Des")
                  .Replace("TipoTramite", "TipTram").Replace("Pergunta", "Perg").Replace("Externo", "Ext")
                  .Replace("Visibilidade", "Visib").Replace("Encerramento", "Encer").Replace("Abertura", "Aber")
                  .Replace("Numero", "Num").Replace("Publicac", "Pub").Replace("Provisorio", "Prov").Replace("Data", "Dt")
                  .Replace("Autorizado", "Aut").Replace("Arquivamento", "Arq").Replace("GrupoLocal", "GrLocal")
                  .Replace("Julgamento", "Julg").Replace("Complemento", "Comp").Replace("Temporario", "Temp")
                  .Replace("Qualificacao", "Qualif").Replace("Intimacao", "Int").Replace("Vinculos", "Vinc")
                  .Replace("Relacionamento", "Rel").Replace("Hierarquia", "Hier").Replace("Conferencia", "Conf")
                  .Replace("FluxoEtapa", "FlxEta");
    }

    private string GetKeyConstraint(string propriedade, Type entityType) {
      //cara as classes de log não criar FK
      if (entityType.GetInterfaces().Where(x => x.Name.Equals("IEntidadeParaLog")).Any())
        return "none";
      else
        return GetKeyConstraint(propriedade, entityType.Name);      
    }

    private string GetKeyConstraint(string propriedade, string entityType) {
      entityType = AbreviarPalavrasConhecidas(entityType);
      propriedade = AbreviarPalavrasConhecidas(propriedade);
      string constraint = String.Format("{0}_{1}_FK", entityType, propriedade);
      if (constraint.Length > 30) {
        string aux = constraint;
        while (aux.Length > 30) {
          entityType = entityType.Remove(entityType.Length - 1, 1);
          propriedade = propriedade.Remove(propriedade.Length - 1, 1);
          aux = String.Format("{0}_{1}_FK", entityType, propriedade);
        }
        constraint = aux;
      }
      return constraint;
    }

    private string GetKeyIndex(string propriedade, Type entityType) {
      string entityTypeName = AbreviarPalavrasConhecidas(entityType.Name);
      propriedade = AbreviarPalavrasConhecidas(propriedade);
      string indexName = String.Format("{0}_{1}_IX", entityTypeName, propriedade);
      if (indexName.Length > 30) {
        string aux = indexName;
        while (aux.Length > 30) {
          entityTypeName = entityTypeName.Remove(entityTypeName.Length - 1, 1);
          propriedade = propriedade.Remove(propriedade.Length - 1, 1);
          aux = String.Format("{0}_{1}_IX", entityTypeName, propriedade);
        }
        indexName = aux;
      }
      return indexName;
    }

    public void Apply(IOneToManyCollectionInstance instance) {
      instance.Key.ForeignKey(GetKeyConstraint(instance.Member.Name, instance.EntityType));
    }

    public void Apply(IManyToOneInstance instance) {
      instance.ForeignKey(GetKeyConstraint(instance.Property.Name, instance.EntityType));
      var colunaMap = instance.Columns.FirstOrDefault();
      if (colunaMap != null) {
        //caso a propriedade for mapeada com UNIQUE o banco de dados criar um index unique automaticamente
        if (!colunaMap.Unique)
          instance.Index(GetKeyIndex(instance.Property.Name, instance.EntityType));
      }
    }

    public void Apply(ICollectionInstance instance) {
      instance.Key.ForeignKey(GetKeyConstraint(instance.Member.Name, instance.EntityType));
    }

    public void Apply(IJoinedSubclassInstance instance) {
      var type = instance.EntityType;
      var baseType = type.BaseType;
      if (baseType == null) return;
      instance.Key.ForeignKey(GetKeyConstraint(type.Name, baseType));
    }

    public void Apply(IManyToManyCollectionInstance instance) {
     instance.Key.ForeignKey(GetKeyConstraint(instance.Member.Name, instance.TableName));
     instance.Relationship.ForeignKey(GetKeyConstraint(instance.Relationship.StringIdentifierForModel, instance.TableName));
    }

  }
}
