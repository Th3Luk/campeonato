﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace MPS.NHibernate {

  /// <summary>
  /// Ensures that all of our strings are stored as varchar instead of nvarchar.
  /// </summary>
  public class PropertyConvention : IPropertyConvention {

    public void Apply(IPropertyInstance instance) {
      if (instance.Property.PropertyType == typeof(string))
        instance.CustomType("AnsiString");
      if (instance.Property.PropertyType == typeof(bool))
        instance.Not.Nullable();
      if (instance.Property.PropertyType == typeof(char)) 
        instance.CustomSqlType("char(1)");            
    }

  }
}
