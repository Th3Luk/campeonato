using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Web;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System.Linq;
using System.Text;
using System.Data;
using NHibernate.Dialect;
using MPS.NHibernate.Log;

namespace MPS.NHibernate {
  public static class SessionHelper {

    #region Utilitários de SchemaExport
    public static void ValidarNomesTabelasColunas(Configuration config) {
      StringBuilder erros = new StringBuilder();
      foreach (var item in config.ClassMappings) {
        if (item.Table.Name.Length > 30)
          erros.AppendLine("tabela : " + item.Table.Name);
        foreach (var coluna in item.IdentityTable.ColumnIterator.Where(x => x.Name.Length > 30)) {
          erros.AppendLine("coluna : " + coluna.Name + " Tabela : " + item.Table.Name);
        }
      }
      foreach (var item in config.CollectionMappings.Where(x => x.CollectionTable != null)) {
        if (item.CollectionTable.Name.Length > 30)
          erros.AppendLine("tabela : " + item.CollectionTable.Name);
      }
      if (erros.Length > 0)
        throw new InvalidOperationException(erros.ToString());
    }

    public static void ExportSchema(Configuration config, string fileName, string delimiter) {
      bool outputToConsole = false;
      bool applyToDb = false;
      bool dropOnly = false;
      SchemaExport se = new SchemaExport(config);
      se.SetDelimiter(delimiter);
      se.SetOutputFile(fileName);
      se.Execute(outputToConsole, applyToDb, dropOnly);
    }
    #endregion

    #region Propriedades Privadas
    private const string SESSION_KEY = "MPS.NHibernate.Session";
    private static ISessionFactory sessionFactory;
    #endregion


    public static Func<Configuration> BuildConfiguration { get; set; }

    public static Configuration GetConfiguration() {
      Func<Configuration> buildConfiguration = BuildConfiguration;
      if (buildConfiguration == null)
        throw new InvalidOperationException("Configuration Builder is not configured.");
      Configuration result = buildConfiguration();
      //SchemaMetadataUpdater.QuoteTableAndColumns(result);
      return result;
    }

    private static ISessionFactory GetSessionFactory() {
      if (sessionFactory == null) {
        Configuration configuration = GetConfiguration();
        if (configuration == null)
          throw new InvalidOperationException("Invalid session factory configuration.");
        sessionFactory = configuration.BuildSessionFactory();
      }
      return sessionFactory;
    }

    private static ISession GetSession() {
      ISession resultado;
      if (HttpContext.Current == null)
        resultado = (ISession)CallContext.GetData(SESSION_KEY);
      else
        resultado = (ISession)HttpContext.Current.Items[SESSION_KEY];
      return resultado;
    }

    private static void SetSession(ISession session) {
      if (session != null) {
        if (HttpContext.Current != null)
          HttpContext.Current.Items[SESSION_KEY] = session;
        else
          CallContext.SetData(SESSION_KEY, session);
      } else {
        if (HttpContext.Current != null)
          HttpContext.Current.Items.Remove(SESSION_KEY);
        else
          CallContext.FreeNamedDataSlot(SESSION_KEY);
      }
    }

    public static bool IsDialectCurrent(Type dialect) {
      if (HasCurrent)
        return Current.GetSessionImplementation().Factory.Dialect.GetType() == dialect;
      else
        return false;
    }
    
    public static void OpenSession() {
      ISession sessao = GetSession();
      if (sessao == null) {
        LogInterceptor interceptor = new LogInterceptor();
        sessao = GetSessionFactory().OpenSession(interceptor);
        sessao.FlushMode = FlushMode.Commit;
        SetSession(sessao);
      }
    }

    public static void CloseSession() {
      ISession sessao = GetSession();
      SetSession(null);
      if (sessao != null)
        sessao.Close();
    }

    public static bool HasCurrent {
      get {
        return GetSession() != null;
      }
    }

    public static ISession Current {
      get {
        ISession resultado = GetSession();
        if (resultado == null)
          throw new InvalidOperationException("There is no current session.");
        return resultado;
      }
    }

    public static void RunInNewSession(Action action) {
      if (action == null)
        throw new ArgumentNullException("action");
      var currentSession = GetSession(); // salva a sessao atual
      try {
        SetSession(null); // desliga a sessao atual para poder abrir uma nova
        OpenSession(); // abre a sessao nova temporaria
        try {
          action(); // executa o método passado
        } finally {
          CloseSession(); // fecha a sessao nova temporaria
        }
      } finally {
        SetSession(currentSession); // devolve a sessao original
      }
    }

    public static object InicializarEntidadeProxy(object entidadeProxy) {
      NHibernateUtil.Initialize(entidadeProxy);
      return entidadeProxy;
    }

    public static Type TipoEntidadeProxy(object entidadeProxy) {
      return NHibernateUtil.GetClass(entidadeProxy);
    }

    public static bool IsProxy(object entidade) {
      return NHibernateUtil.IsInitialized(entidade);
    }
  }
}
