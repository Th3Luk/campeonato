﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppJFF.Classes.Base;
using NHibernate.Criterion;

namespace WebAppJFF.DAO.Base
{
  public interface IBaseDAO<T>
    where T : BaseEntidade {
    void Inserir(T item);
    T ObterPorId(int id);
    IList<T> SelecionarTodos();
    void Selecionar(int startRowIndex, int maximumRows, string sortOrder, out int total, out IList<T> result);
    IList<T> SelecionarPorIds(int[] ids);
    int ObterTotal(Action<DetachedCriteria> configurar);
    int ObterTotal();
    void Atualizar(T item);
    void Excluir(int id);
    void LimparSession();
  }
}