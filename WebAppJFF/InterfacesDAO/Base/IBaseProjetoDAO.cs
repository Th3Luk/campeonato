﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppJFF.Classes;
using WebAppJFF.Classes.Base;

namespace WebAppJFF.DAO.Base
{
  public interface IBaseProjetoDAO<T> : IBaseDAO<T>
    where T : BaseEntidade {
    BaseEntidade Base { get; set; }
  }
}