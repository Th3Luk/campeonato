﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppJFF.Classes.Base;

namespace WebAppJFF.DAO.Base
{
  public interface IBaseAtivoDAO<T> : IBaseDAO<T>
    where T : BaseEntidade {
    IList<T> SelecionarAtivos();
    void SelecionarAtivos(int startRowIndex, int maximumRows, string sortOrder, out int total, out IList<T> result);
  }
}