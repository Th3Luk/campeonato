﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppJFF.Classes;
using WebAppJFF.DAO.Base;

namespace WebAppJFF.InterfacesDAO
{
  public interface ITimeDAO : IBaseDescricaoDAO<Time>
  {
  }
}