﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MPS.Runtime.Dependency;
using Servicos.Base;
using Classes;
using Interfaces.DAL;
using Interfaces.Servicos;


namespace Servicos
{
  public class TimeServico : BaseDescricaoServico<Time, ITimeDAO>, ITimeServico
  {
    public void SelecionarPorParametros(int? idPerfilJogador, int idJogo, string nome, int startRowIndex, int maximumRows, string sortOrder, out int Total, out IList<Time> resultado)
    {
      ITimeDAO iDAO = Resolver.GetImplementationOf<ITimeDAO>();
      iDAO.SelecionarPorParametros(idPerfilJogador, idJogo, nome, startRowIndex, maximumRows, sortOrder, out Total, out resultado);
    }
  }
}