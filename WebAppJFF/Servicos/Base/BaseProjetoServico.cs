﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MPS.Runtime.Dependency;
using Classes.Base;
using Interfaces.Base;


namespace Servicos.Base
{
  public abstract class BaseProjetoServico<T, I> :BaseServico<T, I>, IBaseProjetoServico<T>
    where T : BaseEntidade
    where I : IBaseProjetoDAO<T> {

    public BaseEntidade baseEnt { get; set; }

    protected override I ObterInstanciaDAOEspecifico() {
      I dao = Resolver.GetImplementationOf<I>();
      dao.Base = this.baseEnt;
      return dao;
    }
  }
}