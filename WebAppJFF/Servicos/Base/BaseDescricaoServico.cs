﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes.Base;
using MPS.Runtime.Dependency;
using Interfaces.Base;

namespace Servicos.Base
{
  public class BaseDescricaoServico<T, I> : BaseAtivoServico<T, I>, IBaseDescricaoServico<T>
    where T : BaseNomeEntidade
    where I : IBaseDescricaoDAO<T> {

    #region IBaseDescricaoServico<T> Members

    public IList<T> SelecionarPorDescricao(string descricao) {
      I iDAO = Resolver.GetImplementationOf<I>();
      var ret = iDAO.SelecionarPorDescricao(descricao);
      ConfigurarTotal(ret.Count);
      return ret;
    }

    public virtual IList<T> SelecionarPorDescricaoAutoComplete(string descricao) {
      I iDAO = Resolver.GetImplementationOf<I>();
      var ret = iDAO.SelecionarPorDescricaoAutoComplete(descricao);
      ConfigurarTotal(ret.Count);
      return ret;
    }

    public IList<T> SelecionarAtivosPorEdicao(int edicao)
    {
      I iDAO = Resolver.GetImplementationOf<I>();
      var ret = iDAO.SelecionarAtivosPorEdicao(edicao);
      ConfigurarTotal(ret.Count);
      return ret;
    }

    public virtual IList<T> SelecionarAtivosPorDescricaoAutoComplete(string descricao) {
      I iDAO = Resolver.GetImplementationOf<I>();
      var ret = iDAO.SelecionarAtivosPorDescricaoAutoComplete(descricao);
      ConfigurarTotal(ret.Count);
      return ret;
    }

    public T ObterPorNome(string nome)
    {
        I iDAO = Resolver.GetImplementationOf<I>();
        return iDAO.ObterPorNome(nome);
    }


    #endregion

  }
}