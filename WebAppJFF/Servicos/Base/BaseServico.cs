﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes.Base;
using MPS.Runtime.Dependency;
using Classes;
using Interfaces.Base;

namespace Servicos.Base
{
  public abstract class BaseServico<T, I> : IBaseServico<T>
    where T : BaseEntidade
    where I : IBaseDAO<T> {
    private int total;

    public void ConfigurarTotal(int total) {
      this.total = total;
    }

    public int ObterTotal() {
      return this.total;
    }

    #region IBaseServico<T> Members

    public virtual void Inserir(T item) {
      I iDAO = Resolver.GetImplementationOf<I>();
      //item.Validate();
      iDAO.Inserir(item);
    }

    public T ObterPorId(int id) {
      I iDAO = Resolver.GetImplementationOf<I>();
      return iDAO.ObterPorId(id);
    }

    public IList<T> SelecionarTodos() {
      I iDAO = Resolver.GetImplementationOf<I>();
      var ret = iDAO.SelecionarTodos();
      ConfigurarTotal(ret.Count);
      return ret;
    }

    public IList<T> Selecionar(int startRowIndex, int maximumRows, string sortOrder) {
      IList<T> result;
      int total;
      I iDAO = Resolver.GetImplementationOf<I>();
      iDAO.Selecionar(startRowIndex, maximumRows, sortOrder, out total, out result);
      ConfigurarTotal(total);
      return result;
    }

    public IList<T> SelecionarPorIds(int[] ids) {
      I iDAO = Resolver.GetImplementationOf<I>();
      return iDAO.SelecionarPorIds(ids);
    }

    public virtual void Atualizar(T item) {
      I iDAO = Resolver.GetImplementationOf<I>();
      //item.Validate();
      iDAO.Atualizar(item);
    }

    public virtual void Excluir(T item) {
      I iDAO = Resolver.GetImplementationOf<I>();
      iDAO.Excluir(item.Id);
    }

    protected virtual I ObterInstanciaDAOEspecifico() {
      return Resolver.GetImplementationOf<I>();
    }

    public void LimparSession() {
      ObterInstanciaDAOEspecifico().LimparSession();
    }

    #endregion
  }
}