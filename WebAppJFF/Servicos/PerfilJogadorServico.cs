﻿using MPS.Runtime.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;
using Interfaces.DAL;
using Interfaces.Servicos;
using Servicos.Base;

namespace Servicos
{
  public class PerfilJogadorServico : BaseDescricaoServico<PerfilJogador, IPerfilJogadorDAO>, IPerfilJogadorServico
  {
    public PerfilJogador SelecionarPorParametros(int idUsuario)
    {
      IPerfilJogadorDAO iDAO = Resolver.GetImplementationOf<IPerfilJogadorDAO>();
      var ret = iDAO.SelecionarPorParametros(idUsuario);
      return ret;
    }
  }
}