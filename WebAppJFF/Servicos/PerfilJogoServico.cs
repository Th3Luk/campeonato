﻿using MPS.Runtime.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;
using Interfaces.DAL;
using Interfaces.Servicos;
using Servicos.Base;

namespace Servicos
{
  public class PerfilJogoServico : BaseDescricaoServico<PerfilJogo, IPerfilJogoDAO>, IPerfilJogoServico
  {
    public PerfilJogo SelecionarPorParametros(int idJogador, int idJogo)
    {
      IPerfilJogoDAO iDAO = Resolver.GetImplementationOf<IPerfilJogoDAO>();
      var ret = iDAO.SelecionarPorParametros(idJogador, idJogo);
      return ret;
    }

    public void SelecionarParametrosPaginacao(string login, int? idFaculdade, int? idPosicao, int? idJogo, int? idTime, bool somenteSemTime, int startRowIndex, int maximumRows, string sortOrder, out int Total, out IList<PerfilJogo> resultado)
    {
      ObterInstanciaDAOEspecifico().SelecionarParametrosPaginacao(login, idFaculdade, idPosicao, idJogo, idTime, somenteSemTime, startRowIndex, maximumRows, sortOrder, out Total, out resultado);
    }
  }
}