﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Classes;
using Interfaces.DAL;
using Interfaces.Servicos;
using Servicos.Base;

namespace Servicos
{
  public class ConviteServico : BaseServico<ConviteJogTime, IConviteDAO>, IConviteServico
  {
    public void SelecionarParametrosPaginacao(int? idJogador, int? idTime, int startRowIndex, int maximumRows, string sortOrder, out int Total, out IList<ConviteJogTime> resultado)
    {
      ObterInstanciaDAOEspecifico().SelecionarParametrosPaginacao(idJogador, idTime, startRowIndex, maximumRows, sortOrder, out Total, out resultado);
    }
  }
}
