﻿using MPS.Runtime.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;
using Interfaces;
using Interfaces.DAL;
using Interfaces.Servicos;
using Servicos.Base;

namespace Servicos
{
  public class UsuarioServico : BaseDescricaoServico<Usuario, IUsuarioDAO>, IUsuarioServico
  {
    public Usuario ObterPorLogin(string login)
    {
      IUsuarioDAO daoUsuario = Resolver.GetImplementationOf<IUsuarioDAO>();
      Usuario usuario = daoUsuario.ObterPorLogin(login);
      return usuario;
    }

    public bool ValidarLogin(Usuario usuario, string senha, out string mensagem)
    {
      Criptografia criptografia;
      try
      {
        criptografia = new Criptografia('S');
      }
      catch (Exception)
      {
        mensagem = "Erro criptografia";
        return false;
      }

      byte[] bytes = new byte[senha.Length * sizeof(char)];

      System.Buffer.BlockCopy(senha.ToCharArray(), 0, bytes, 0, bytes.Length);
      byte[] conteudoCriptografado = criptografia.MontarCriptografia(bytes);

      int sizeSenha = usuario.Senha.Length;

      if (conteudoCriptografado.Length < sizeSenha ||
          (sizeSenha < MAX_SENHA && conteudoCriptografado.Length > sizeSenha))
      {
        mensagem = "Senha inválida";
        return false;
      }
      for (int i = 0; i < sizeSenha; i++)
        if (usuario.Senha[i] != conteudoCriptografado[i])
        {
          mensagem = "Senha inválida";
          return false;
        }
      mensagem = "Sucesso";
      return true;
    }

    private const int MAX_SENHA = 100;

    public bool VerificarAutenticidadeSenha(Usuario usuario, string senha)
    {
      Criptografia criptografia = null;
      try
      {
        criptografia = new Criptografia('S');
        
      }
      catch (Exception)
      {
        return false;
      }

      byte[] bytes = new byte[senha.Length * sizeof(char)];

      System.Buffer.BlockCopy(senha.ToCharArray(), 0, bytes, 0, bytes.Length);
      byte[] conteudoCriptografado = criptografia.MontarCriptografia(bytes);

      int sizeSenha = usuario.Senha.Length;

      if (conteudoCriptografado.Length < sizeSenha ||
          (sizeSenha < MAX_SENHA && conteudoCriptografado.Length > sizeSenha))
        return false;
      for (int i = 0; i < sizeSenha; i++)
        if (usuario.Senha[i] != conteudoCriptografado[i])
          return false;

      return true;
    }

    public byte[] RetornaSenhaEncript(string senha)
    {
      Criptografia criptografia = null;
      try
      {
        criptografia = new Criptografia('S');
        byte[] bytes = new byte[senha.Length * sizeof(char)];
        System.Buffer.BlockCopy(senha.ToCharArray(), 0, bytes, 0, bytes.Length);
        return criptografia.MontarCriptografia(bytes);
      }
      catch (Exception)
      {
        throw;
      }
    }
  }
}