﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MPS.Runtime.Dependency;
using Classes;
using Interfaces;
using Servicos;
using System.Web.Security;
using WebAppJFF.Utilitarios;
using MPS.Web.UI.WebControls;
using Interfaces.Servicos;

namespace WebAppJFF.Paginas.Cadastro {
  public partial class CadastroLogin : BaseAcessoUsuarioPagina {
    protected void Page_Load(object sender, EventArgs e) {
      if (!IsPostBack) {
        if (UsuarioLogado != null) {
          CadastroPerfil.PerfilJogador = UsuarioLogado.PerfilJogador;
          AcessoPanel.Visible = false;
        } else {
          AcessoPanel.Visible = true;
        }
      }
    }

    protected void LoginTextBox_TextChanged(object sender, EventArgs e) {
      LoginPadrao.Visible = false;
      LoginErro.Visible = false;
      LoginSucesso.Visible = false;
      string login = (sender as TextBox).Text;
      IUsuarioServico servico = Resolver.GetImplementationOf<IUsuarioServico>();
      Usuario usuario = servico.ObterPorLogin(login);
      if (login == "") {
        LoginTextBox.Text = "";
        LoginPadrao.Visible = true;
        LoginPadrao.Focus();
      } else if (usuario != null) {
        LoginErro.Visible = true;
        ErroLogin.Text = "Login " + login + " já está em uso.";
        LoginError.Focus();
      } else {
        LoginSucesso.Visible = true;
        LoginSucessoTextBox.Text = login;
        SenhaTxt.Focus();
      }
    }

    protected void SalvarButton_Click(object sender, EventArgs e) {
      try {

        //UpdatePanel1.Update();
        IPerfilJogadorServico servicoPerfil = Resolver.GetImplementationOf<IPerfilJogadorServico>();
        CadastroPerfil.PreencherPerfilJogador();
        if (CadastroPerfil.PerfilJogador.Id == 0)
          servicoPerfil.Inserir(CadastroPerfil.PerfilJogador);
        else
          servicoPerfil.Atualizar(CadastroPerfil.PerfilJogador);

        IUsuarioServico servico = Resolver.GetImplementationOf<IUsuarioServico>();
        Usuario usuario = null;
        if (UsuarioLogado == null)
          usuario = new Usuario();
        else
          usuario = UsuarioLogado;
        usuario.Ativo = true;
        usuario.DataCadastro = DateTime.Now;
        usuario.Admin = false;

        usuario.PerfilJogador = CadastroPerfil.PerfilJogador;
        if (usuario.Id == 0) {
          usuario.Login = LoginSucessoTextBox.Text;
          usuario.Senha = servico.RetornaSenhaEncript(SenhaTxt.Text);
          servico.Inserir(usuario);
          FormsAuthentication.SetAuthCookie(usuario.Login + "/user", false);
          string pagina = ResolveClientUrl("Perfil.aspx");
          SecureQueryString qs = new SecureQueryString();
          LinkButton nomeLink = (sender) as LinkButton;
          qs["DadosCadastrados"] = "salvo";
          pagina += "?dados=";
          Response.Redirect(pagina + qs.ToString(), true);
        } else {
          servico.Atualizar(usuario);
          ErrorSummary.AddError(this, "Seus dados foram atualizados com sucesso.");
          GEDValidationSummary1.CssClass = "alert alert-success";
        }
      } catch (Exception ex) {
        ErrorSummary.AddError(this, "Erro ao cadastrar dados. " + ex.Message);
        GEDValidationSummary1.CssClass = "alert alert-danger";
      }
    }
  }
}