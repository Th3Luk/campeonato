﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MPS.Web.UI.WebControls;
using MPS.Web.UI;
using Classes;
using Interfaces.Servicos;
using MPS.Runtime.Dependency;
using WebAppJFF.Utilitarios;
using WebAppJFF.Paginas;

namespace WebAppJFF.Paginas.Cadastro {
  public partial class Perfil : BaseAcessoUsuarioPagina
  {

    public List<ItemJogadorTime> JogadoresTime
    {
      get
      {
        if (ViewState["JogadoresTime"] == null)
          return new List<ItemJogadorTime>();
        return (List<ItemJogadorTime>)ViewState["JogadoresTime"];
      }
      set
      {
        ViewState["JogadoresTime"] = value;
      }
    }

    public JogoEnum JogoTela
    {
      get
      {
        if (ViewState["Jogo"] == null)
          ViewState["Jogo"] = JogoEnum.Dota;
        return (JogoEnum)ViewState["Jogo"];
      }
      set
      {
        ViewState["Jogo"] = value;
      }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {

        if (Request["dados"] != null)
        {
          SecureQueryString qs = new SecureQueryString(Request["Dados"]);
          if (!string.IsNullOrEmpty(qs["DadosCadastrados"]))
          {
            ErrorSummary.AddError(this, "Seus dados cadastrais foram salvos com sucesso. Informe seus dados de jogador.");
            ValidationSummary1.CssClass = "alert alert-success";
          }
        }
        TimeImagem.ImageUrl = UrlHandler;
      }
      AcessoNormal();
      BuscaJogadorGridControl.SelecionarComPaginacao = SelecionarPessoas;
      GridJogadores.SelecionarComPaginacao = SelecionarJogadores;
      GridConvites.SelecionarComPaginacao = SelecionarConvites;
      if (!IsPostBack)
      {
        if (UsuarioLogado != null)
          PreencherCamposPerfil();
        AtivaButton();
        CarregaCampos();
      }
    }

    protected void LolButton_Click(object sender, EventArgs e)
    {
      JogoTela = JogoEnum.Lol;
      AtivaButton();

    }

    protected void CSButton_Click(object sender, EventArgs e)
    {
      JogoTela = JogoEnum.Cs;
      AtivaButton();
    }

    protected void DotaButton_Click(object sender, EventArgs e)
    {
      JogoTela = JogoEnum.Dota;
      AtivaButton();
    }

    private void AtivaButton()
    {
      DotaButton.CssClass = "btn btn-default btn-lg btn-block";
      CSButton.CssClass = "btn btn-default btn-lg btn-block";
      LolButton.CssClass = "btn btn-default btn-lg btn-block";
      LegendaDota.Visible = false;
      LegendaLol.Visible = false;
      LegendaCs.Visible = false;
      LegendaTimeDota.Visible = false;
      LegendaTimeLol.Visible = false;
      LegendaTimeCS.Visible = false;
      switch (JogoTela)
      {
        case JogoEnum.Dota:
          DotaButton.CssClass = "btn btn-primary btn-lg btn-block";
          LegendaDota.Visible = true;
          LegendaTimeDota.Visible = true;
          break;
        case JogoEnum.Cs:
          CSButton.CssClass = "btn btn-primary btn-lg btn-block";
          LegendaTimeCS.Visible = true;
          LegendaCs.Visible = true;
          break;
        case JogoEnum.Lol:
          LolButton.CssClass = "btn btn-primary btn-lg btn-block";
          LegendaLol.Visible = true;
          LegendaTimeLol.Visible = true;
          break;
      }
      CarregaCampos();
    }

    private void CarregaCampos()
    {
      JogadoresTime = null;
      PreencherPosicoes();
      PreencherDropDownFaculdadePesquisa();
      PreencherCamposPerfil();
      AutuacaoUpdatePanel.Update();
    }

    private void PreencherCamposPerfil()
    {
      PerfilJogador perfil = UsuarioLogado.PerfilJogador;
      PerfilJogadorHidden.Value = perfil.Id.ToString();
      //CadastroPerfil.PerfilJogador = perfil;
      PreencherCamposJogador(perfil);
    }

    private void PreencherCamposJogador(PerfilJogador perfilJogador)
    {
      IPerfilJogoServico perfilServico = Resolver.GetImplementationOf<IPerfilJogoServico>();
      PerfilJogo perfilJogo = perfilServico.SelecionarPorParametros(perfilJogador.Id, (int)JogoTela);
      if (perfilJogo != null)
      {
        LoginTextBox.Text = perfilJogo.Nome;
        if (perfilJogo.IdPosicao != 0)
          PosicaoDropDown.SelectedValue = perfilJogo.IdPosicao.ToString();
        DescricaoTextBox.Text = perfilJogo.Descricao;
        TempoGameTextBox.Text = perfilJogo.TempoJogo.ToString();
        PerfilJogoHidden.Value = perfilJogo.Id.ToString();
        if (perfilJogo.Time != null)
        {
          PreencherCamposTime(perfilJogo.Time);
        }
        else
        {
          GridConvites.CarregarDados(0, null, null);
          LimparCamposTime();
        }
      }
      else
      {
        LimparCamposPerfilJogo();
        LimparCamposTime();
      }
    }

    private void LimparCamposTime()
    {
      ButtonAddTime.Visible = true;
      CapitaoTextBox.Text = "";
      capitaoHidden.Value = "0";
      TimeHidden.Value = "0";
      NomeTimeTextBox.Text = "";
      TagTextBox.Text = "";
      CadastrarTime.Visible = true;
      CriarTimePanel.Visible = true;
      JogadoresPanel.Visible = false;
    }

    private void PreencherCamposTime(Time time)
    {
      if (time != null)
      {
        ButtonAddTime.Visible = false;
        CadastrarTime.Visible = true;
        CriarTimePanel.Visible = false;
        JogadoresPanel.Visible = true;
        CapitaoTextBox.Text = "Capitão: " + time.Capitao.Nome;
        capitaoHidden.Value = time.Capitao.Id.ToString();
        TimeHidden.Value = time.Id.ToString();
        NomeTimeTextBox.Text = time.Nome;
        TagTextBox.Text = time.Tag;
        RecuperarJogadoresTime(time);
        if (PerfilJogoHidden.Value != capitaoHidden.Value)
          BloqueiaEdicaoTime();
        UpdatePanel1.Update();
        RequiredFieldValidator2.Enabled = true;
        RequiredFieldValidator1.Enabled = true;
        RequiredFieldValidator6.Enabled = false;
        RequiredFieldValidator5.Enabled = false;
      }
      else
      {
        CadastrarTime.Visible = false;
        CriarTimePanel.Visible = true;
        JogadoresPanel.Visible = false;
        RequiredFieldValidator2.Enabled = false;
        RequiredFieldValidator1.Enabled = false;
        RequiredFieldValidator6.Enabled = true;
        RequiredFieldValidator5.Enabled = true;
      }

    }

    private void PreencherPosicoes()
    {
      switch (JogoTela)
      {
        case JogoEnum.Dota:
          if (PosicaoDropDown != null)
          {
            PosicaoDropDown.Items.Clear();
            PosicaoPesquisaDropDown.Items.Clear();
            foreach (PosicaoDota tipo in Enum.GetValues(typeof(PosicaoDota)))
            {
              PosicaoDropDown.Items.Add(new ListItem(EnumHelper.ObterDescricaoEnum(tipo), ((int)tipo).ToString()));
              PosicaoPesquisaDropDown.Items.Add(new ListItem(EnumHelper.ObterDescricaoEnum(tipo), ((int)tipo).ToString()));
            }

          }
          break;
        case JogoEnum.Cs:
          if (PosicaoDropDown != null)
          {
            PosicaoDropDown.Items.Clear();
            PosicaoPesquisaDropDown.Items.Clear();
            foreach (PosicaoCs tipo in Enum.GetValues(typeof(PosicaoCs)))
            {
              PosicaoDropDown.Items.Add(new ListItem(EnumHelper.ObterDescricaoEnum(tipo), ((int)tipo).ToString()));
              PosicaoPesquisaDropDown.Items.Add(new ListItem(EnumHelper.ObterDescricaoEnum(tipo), ((int)tipo).ToString()));
            }
          }
          break;
        case JogoEnum.Lol:
          if (PosicaoDropDown != null)
          {
            PosicaoDropDown.Items.Clear();
            PosicaoPesquisaDropDown.Items.Clear();
            foreach (PosicaoLol tipo in Enum.GetValues(typeof(PosicaoLol)))
            {
              PosicaoDropDown.Items.Add(new ListItem(EnumHelper.ObterDescricaoEnum(tipo), ((int)tipo).ToString()));
              PosicaoPesquisaDropDown.Items.Add(new ListItem(EnumHelper.ObterDescricaoEnum(tipo), ((int)tipo).ToString()));
            }
          }
          break;
      }
      PosicaoDropDown.Items.Insert(0, new ListItem("Selecionar", ""));
      PosicaoPesquisaDropDown.Items.Insert(0, new ListItem("Selecionar", ""));
    }

    protected void SalvarButton_Click(object sender, EventArgs e)
    {
      try
      {
        IPerfilJogadorServico perfilServico = Resolver.GetImplementationOf<IPerfilJogadorServico>();
        //Salvar Dados Jogador
        PerfilJogo perfil = SalvarPerfilJogo(perfilServico.ObterPorId(Int32.Parse(PerfilJogadorHidden.Value)));

        //Salvar Dados Time
        if (TimeHidden.Value != "0")
          SalvarTime(perfil);

        ErrorSummary.AddError(this, "Dados cadastrados com sucesso");
        ValidationSummary1.CssClass = "alert alert-success";

        AbaAtivaHF.Value = "0";
        IdArquivoCriado.Value = "";
      }
      catch (Exception ex)
      {
        ErrorSummary.AddError(this, "Erro ao cadastrar dados");
        ValidationSummary1.CssClass = "alert alert-danger";
      }

    }

    private PerfilJogo SalvarPerfilJogo(PerfilJogador perfilJogador)
    {
      try
      {
        IPerfilJogoServico perfilServico = Resolver.GetImplementationOf<IPerfilJogoServico>();
        PerfilJogo perfil = perfilServico.SelecionarPorParametros(UsuarioLogado.PerfilJogador.Id, (int)JogoTela);
        if (perfil == null)
          perfil = new PerfilJogo();
        perfil.PerfilJogador = perfilJogador;
        perfil.IdJogo = (int)JogoTela;
        if (PosicaoDropDown.SelectedValue != "")
          perfil.IdPosicao = Int32.Parse(PosicaoDropDown.SelectedValue);
        perfil.Nome = LoginTextBox.Text;
        if (TempoGameTextBox.Text != "")
          perfil.TempoJogo = Int32.Parse(TempoGameTextBox.Text);
        perfil.Descricao = DescricaoTextBox.Text;
        if (perfil.Id == 0)
          perfilServico.Inserir(perfil);
        else
          perfilServico.Atualizar(perfil);

        PerfilJogoHidden.Value = perfil.Id.ToString();
        return perfil;
      }
      catch (Exception ex)
      {
        throw new Exception("Erro ao cadastrar perfil jogo. " + ex);
      }
    }

    private bool validarDadosTime(Time time)
    {
      if (time == null) {
        ITimeServico timeServico = Resolver.GetImplementationOf<ITimeServico>();
        int total = 0;
        IList<Time> timeVerifica;
        timeServico.SelecionarPorParametros(null, (int)JogoTela, NomeTimeTextBox.Text, 0, Int32.MaxValue, "", out total,out timeVerifica);
        if (timeVerifica.Any(x => x.Nome.ToUpper() == NomeTimeTextBox.Text.ToUpper())) {
          ErrorSummary.AddError(this, "Já existe um time com o nome: " + NomeTimeTextBox.Text);
          ValidationSummary1.CssClass = "alert alert-danger";
          return false;
        }        
      }

      return ValidarImagem(time);
    }

    private bool ValidarImagem(Time time)
    {
      if (time != null)
        return true;
      IArqTempServico servico = Resolver.GetImplementationOf<IArqTempServico>();
      if (IdArquivoCriado.Value == "")
      {
        ErrorSummary.AddError(this, "Logo do time é obrigatório");
        ValidationSummary1.CssClass = "alert alert-danger";
        return false;
      }
      return true;
      //try
      //{
      //  FileUpload fileUpload = (FileUpload1) as FileUpload;
      //  if (fileUpload != null)
      //  {
      //    if (fileUpload.HasFile)
      //    {
      //      string fileName = Server.HtmlEncode(fileUpload.FileName);
      //      // Get the extension of the uploaded file.
      //      string extension = System.IO.Path.GetExtension(fileName);

      //      // Allow only files with .doc or .xls extensions
      //      // to be uploaded.
      //      if ((extension != ".jpg") && (extension != ".png") && (extension != ".jpeg") && (extension != ".gif"))
      //        throw new Exception("Formato de arquivo Inválido");

      //      if ((fileUpload.PostedFile.ContentLength > 1 * 500000))
      //        //1000000 = 1 megabyte
      //        throw new Exception("O Arquivo é muito grande. É permitido anexar no máximo 500 kbytes para imagens.");
      //      return true;
      //    }
      //    else
      //    {
      //      if (TimeHidden.Value == "0")
      //        throw new Exception("Logo é obrigatório.");
      //    }
      //  }
      //  return false;
      //}
      //catch (Exception exc)
      //{
      //  throw exc;
      //}
    }

    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);
      string script =
            "$('#" + FileUpload1.ClientID + "').fileupload({" +
              "replaceFileInput: false," +
              "dataType: 'json'," +
              "url: '" + ResolveUrl("~/Ajax/FileUploadHandler.ashx") + "'," +
              "done: function (e, data) {" +
                "$('#" + IdArquivoCriado.ClientID + "').val(data.result.Id);" +
                "$('#" + EnviarArquivoButton.ClientID + "').click();" +
              "}," +
              "progressall: function (e, data) {" +
                "var progress = parseInt(data.loaded / data.total * 100, 10);" +
                "$('#" + ProgressPanel.ClientID + "').show();" +
                "$('#" + ProgressPanel.ClientID + " .progress-bar').css(" +
                    "'width'," +
                    "progress + '%'" +
                ");" +
              "}" +
          "});";
      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "transformaFileUpload" + this.ClientID, script, true);
    }

    private void SalvarTime(PerfilJogo perfilJogo)
    {

      try
      {
        if (validarDadosTime(perfilJogo.Time))
        {
          ITimeServico timeServico = Resolver.GetImplementationOf<ITimeServico>();
          IPerfilJogoServico perfilServico = Resolver.GetImplementationOf<IPerfilJogoServico>();
          Time time;
          if (perfilJogo.Time == null)
          {
            time = new Time();
            time.Capitao = perfilJogo;
          }
          else
          {
            time = perfilJogo.Time;
            if (PerfilJogoHidden.Value != time.Capitao.Id.ToString())
              return;
          }
          IArqTempServico servico = Resolver.GetImplementationOf<IArqTempServico>();
          ArqTemp arq = null; 
          if (IdArquivoCriado.Value != "")
            arq = servico.ObterPorId(Int32.Parse(IdArquivoCriado.Value));
          if (arq != null)
          {
            time.Logo = arq.Imagem;
            servico.Excluir(arq);
            IdArquivoCriado.Value = "";
          }
          time.Nome = NomeTimeTextBox.Text;
          time.Tag = TagTextBox.Text;
          capitaoHidden.Value = time.Capitao.Id.ToString();
          TimeHidden.Value = time.Id.ToString();
          if (time.Id == 0)
          {
            //Limpar convites
            IConviteServico conviteServico = Resolver.GetImplementationOf<IConviteServico>();
            IList<ConviteJogTime> convites;
            int total;
            conviteServico.SelecionarParametrosPaginacao(perfilJogo.Id, null, 0, int.MaxValue, "", out total, out convites);
            if (total > 0)
            {
              foreach (ConviteJogTime convite in convites)
              {
                conviteServico.Excluir(convite);
              }
            }
            //Incluir time
            timeServico.Inserir(time);
            if (capitaoHidden.Value == perfilJogo.Id.ToString())
            {
              perfilJogo.Time = time;
              perfilServico.Atualizar(perfilJogo);
            }
          }
          else
            timeServico.Atualizar(time);
          TimeHidden.Value = time.Id.ToString();
        }
      }
      catch (Exception ex)
      {
        ErrorSummary.AddError(this, "Erro ao Cadastrar time. " + ex.Message);
        ValidationSummary1.CssClass = "alert alert-danger";
      }

    }

    private void LimparCamposPerfilJogo()
    {
      LoginTextBox.Text = "";
      PosicaoDropDown.SelectedIndex = 0;
      DescricaoTextBox.Text = "";
      TempoGameTextBox.Text = "";
      PerfilJogoHidden.Value = "0";
      GridConvites.CarregarDados(0, null, null);
    }

    #region AbaTime

    private void BloqueiaEdicaoTime()
    {
      CapitaoTextBox.Enabled = false;
      NomeTimeTextBox.Enabled = false;
      TagTextBox.Enabled = false;
      ConvidarButton.Visible = false;
      LogoTimePanel.Visible = false;
    }

    protected void ButtonAddTime_Click(object sender, EventArgs e)
    {
      if (validarDadosTime(null))
      {
        IPerfilJogadorServico perfilServico = Resolver.GetImplementationOf<IPerfilJogadorServico>();
        //Salvar Dados Jogador
        PerfilJogo perfil = SalvarPerfilJogo(perfilServico.ObterPorId(Int32.Parse(PerfilJogadorHidden.Value)));
        CapitaoTextBox.Text = "Capitão: " + perfil.Nome;
        capitaoHidden.Value = perfil.Id.ToString();
        SalvarTime(perfil);
        IncluirJogador(perfil, false);
        ButtonAddTime.Visible = false;
        CadastrarTime.Visible = true;
        CriarTimePanel.Visible = false;
        JogadoresPanel.Visible = true;
        TimeImagem.ImageUrl = UrlHandler;
        AutuacaoUpdatePanel.Update();

      }
    }

    protected void PesquisarJogadoresButton_Click(object sender, EventArgs e)
    {
      BuscaJogadorGridControl.Visible = true;
      BuscaJogadorGridControl.CarregarDados(0, null, null);
    }

    protected void AdicionarJogadorButton_Click(object sender, EventArgs e)
    {
      try
      {
        Button jogLink = (sender) as Button;
        int idJogador = int.Parse(jogLink.CommandArgument.ToString());
        IPerfilJogoServico pServico = Resolver.GetImplementationOf<IPerfilJogoServico>();
        PerfilJogo perfilJogo = pServico.ObterPorId(idJogador);

        List<ItemJogadorTime> jogadoresTime = this.JogadoresTime;
        if (jogadoresTime.Count(n => n.id == perfilJogo.Id) > 0)
          throw new Exception("Este jogador já está cadastrado.");

        IncluirJogador(perfilJogo, true);

        FiltrosPesqDetalhadaModal.FecharModal();
      }
      catch (Exception ex)
      {
        ErrorSummary.AddError(this, ex.Message);
        ValidationSummary1.CssClass = "alert alert-danger";
        UpdatePanel3.Update();
      }
    }

    private void LimparCamposPesquisa()
    {
      LoginPesquisaTextBox.Text = "";
      FaculdadePesquisaDropDown.SelectedIndex = 0;
      PosicaoPesquisaDropDown.SelectedIndex = 0;
      BuscaJogadorGridControl.Visible = false;
    }

    public object[] SelecionarPessoas(int startRow, int pageSize, string sortOrder, out int total)
    {
      IPerfilJogoServico pServico = Resolver.GetImplementationOf<IPerfilJogoServico>();
      IList<PerfilJogo> resultado;

      int? idFaculdade = null;
      if (FaculdadePesquisaDropDown.SelectedValue != "")
        idFaculdade = Int32.Parse(FaculdadePesquisaDropDown.SelectedValue);

      int? idPosicao = null;
      if (PosicaoPesquisaDropDown.SelectedValue != "")
        idPosicao = Int32.Parse(PosicaoPesquisaDropDown.SelectedValue);

      pServico.SelecionarParametrosPaginacao(LoginPesquisaTextBox.Text, idFaculdade, idPosicao, (int)JogoTela, null, true, startRow, pageSize, sortOrder, out total, out resultado);
      return resultado.ToArray();
    }

    private void IncluirJogador(PerfilJogo perfilJogo, bool convite)
    {
      List<ItemJogadorTime> jogadoresTime = this.JogadoresTime;

      jogadoresTime.Add(new ItemJogadorTime()
      {
        id = perfilJogo.Id,
        Login = perfilJogo.Nome,
        Posicao = RetornaPosicao(perfilJogo.IdPosicao),
        Nome = perfilJogo.PerfilJogador.Nome,
        Convite = convite,
        DescrConvite = convite ? "Convidado" : "Confirmado"
      });
      this.JogadoresTime = jogadoresTime;
      LimparCamposPesquisa();
      GridJogadores.CarregarDados(0, null, null);
      //Se o time já está criado então salva os jogadores
      ITimeServico timeServico = Resolver.GetImplementationOf<ITimeServico>();
      if (TimeHidden.Value != "0")
        PreencherJogadoresTime(timeServico.ObterPorId(Int32.Parse(TimeHidden.Value)));
      UpdatePanel1.Update();
    }

    private void PreencherDropDownFaculdadePesquisa()
    {
      if ((FaculdadePesquisaDropDown != null) && (FaculdadePesquisaDropDown.Items.Count == 0))
      {
        FaculdadePesquisaDropDown.Items.Clear();
        foreach (FaculdadeEnum tipo in Enum.GetValues(typeof(FaculdadeEnum)))
        {
          FaculdadePesquisaDropDown.Items.Add(new ListItem(EnumHelper.ObterDescricaoEnum(tipo), ((int)tipo).ToString()));
        }
        FaculdadePesquisaDropDown.Items.Insert(0, new ListItem("Selecionar", ""));
      }
    }

    public string RetornaPosicao(object valor)
    {
      switch (JogoTela)
      {
        case JogoEnum.Cs:
          return EnumHelper.ObterDescricaoEnum((PosicaoCs)(valor));
        case JogoEnum.Dota:
          return EnumHelper.ObterDescricaoEnum((PosicaoDota)(valor));
        case JogoEnum.Lol:
          return EnumHelper.ObterDescricaoEnum((PosicaoLol)(valor));
        default:
          return "";
      }
    }

    public void GridJogadores_AposItemCriado(object sender, RepeaterItemEventArgs e)
    {
      ItemJogadorTime item = (e.Item.DataItem as ItemJogadorTime);
      LinkButton excluirBtn = (e.Item.FindControl("ExcluirJogadorButton") as LinkButton);
      if (item.id.ToString() == capitaoHidden.Value || PerfilJogoHidden.Value != capitaoHidden.Value)
        excluirBtn.Visible = false;
      else
        excluirBtn.CommandArgument = item.id.ToString();
    }
    protected void ExcluirJogadorButton_Click(object sender, EventArgs e)
    {
      LinkButton jogLink = (sender) as LinkButton;
      int idJogador = int.Parse(jogLink.CommandArgument.ToString());
      ItemJogadorTime item = this.JogadoresTime.Where(n => n.id == idJogador).FirstOrDefault();
      JogadoresTime.Remove(item);
      GridJogadores.CarregarDados(0, null, null);
      ITimeServico timeServico = Resolver.GetImplementationOf<ITimeServico>();
      if (TimeHidden.Value != "0")
        PreencherJogadoresTime(timeServico.ObterPorId(Int32.Parse(TimeHidden.Value)));
    }

    protected void RecuperarJogadoresTime(Time time)
    {
      List<ItemJogadorTime> jogadoresPagina = new List<ItemJogadorTime>();
      IPerfilJogoServico perfilJogoServico = Resolver.GetImplementationOf<IPerfilJogoServico>();
      IList<PerfilJogo> jogadores;
      int total = 0;
      //Recuperar jogadores
      perfilJogoServico.SelecionarParametrosPaginacao("", null, null, null, time.Id, false, 0, Int32.MaxValue, "", out total, out jogadores);
      foreach (PerfilJogo perfil in jogadores)
      {
        jogadoresPagina.Add(new ItemJogadorTime()
        {
          id = perfil.Id,
          Login = perfil.Nome,
          Posicao = RetornaPosicao(perfil.IdPosicao),
          Nome = perfil.PerfilJogador.Nome,
          Convite = false,
          DescrConvite = "Confirmado"
        });
      }

      //Recuperar convites
      IConviteServico conviteServico = Resolver.GetImplementationOf<IConviteServico>();
      IList<ConviteJogTime> convites;
      conviteServico.SelecionarParametrosPaginacao(null, time.Id, 0, Int32.MaxValue, "", out total, out convites);
      foreach (ConviteJogTime convite in convites)
      {
        jogadoresPagina.Add(new ItemJogadorTime()
        {
          id = convite.Jogador.Id,
          Login = convite.Jogador.Nome,
          Posicao = RetornaPosicao(convite.Jogador.IdPosicao),
          Nome = convite.Jogador.PerfilJogador.Nome,
          Convite = true,
          DescrConvite = "Convidado"
        });
      }

      this.JogadoresTime = jogadoresPagina;
      GridJogadores.CarregarDados(0, null, null);
    }

    protected object[] SelecionarJogadores(int startRow, int pageSize, string sortOrder, out int total)
    {
      total = JogadoresTime.Count();
      if ((sortOrder != null) && (sortOrder != ""))
      {
        if (sortOrder.Contains("DESC"))
          JogadoresTime = JogadoresTime.OrderByDescending(t => t.GetType().GetProperty(sortOrder.Replace(" DESC", "")).GetValue(t, null)).ToList();
        else
          JogadoresTime = JogadoresTime.OrderBy(t => t.GetType().GetProperty(sortOrder).GetValue(t, null)).ToList();
      }

      if (total > pageSize)
      {
        if ((total - startRow) < pageSize)
          pageSize = (total - startRow);
        ItemJogadorTime[] copia = new ItemJogadorTime[pageSize];
        JogadoresTime.CopyTo(startRow, copia, 0, pageSize);
        return copia;
      }
      else
        return JogadoresTime.ToArray();
    }

    private void PreencherJogadoresTime(Time time)
    {
      IPerfilJogoServico perfilJogoServico = Resolver.GetImplementationOf<IPerfilJogoServico>();
      IConviteServico conviteServico = Resolver.GetImplementationOf<IConviteServico>();
      IList<ConviteJogTime> convites;
      IList<PerfilJogo> jogadores;
      int total = 0;
      perfilJogoServico.SelecionarParametrosPaginacao("", null, null, null, time.Id, false, 0, Int32.MaxValue, "", out total, out jogadores);
      //Remover jogadores do time
      List<PerfilJogo> jogadoresRemover = new List<PerfilJogo>();
      if (jogadores.Count > 0)
      {
        foreach (PerfilJogo jogador in jogadores)
          if (!this.JogadoresTime.Any(n => n.id == jogador.Id && !n.Convite))
            jogadoresRemover.Add(jogador);
        foreach (PerfilJogo jogRemover in jogadoresRemover)
        {
          jogRemover.Time = null;
          perfilJogoServico.Atualizar(jogRemover);
        }
      }

      conviteServico.SelecionarParametrosPaginacao(null, time.Id, 0, Int32.MaxValue, "", out total, out convites);
      //Remover convites do time
      List<ConviteJogTime> convitesRemover = new List<ConviteJogTime>();
      if (convites.Count > 0)
      {
        foreach (ConviteJogTime convite in convites)
          if (!this.JogadoresTime.Any(n => n.id == convite.Id && n.Convite))
            convitesRemover.Add(convite);
        foreach (ConviteJogTime convRemover in convitesRemover)
        {
          conviteServico.Excluir(convRemover);
        }
      }

      //Adiciona convites
      foreach (ItemJogadorTime item in JogadoresTime)
      {
        if ((item.Convite) && (!convites.Any(n => item.id == n.Id)))
        {

          PerfilJogo perfil = perfilJogoServico.ObterPorId(item.id);
          ConviteJogTime convite = new ConviteJogTime();
          convite.Time = time;
          convite.Jogador = perfil;
          convite.DataConvite = DateTime.Now;
          conviteServico.Inserir(convite);
        }
      }
    }

    private string UrlHandler
    {
      get
      {
        string url = "";
        Time time = null;
        if (UsuarioLogado != null)
        {
          IPerfilJogoServico perfilServico = Resolver.GetImplementationOf<IPerfilJogoServico>();
          PerfilJogo perfilJogo = perfilServico.SelecionarPorParametros(UsuarioLogado.PerfilJogador.Id, (int)JogoTela);
          if (perfilJogo != null && perfilJogo.Time != null)
          {
            ITimeServico Servico = Resolver.GetImplementationOf<ITimeServico>();
            time = Servico.ObterPorId(perfilJogo.Time.Id);
          }
        }
        string teste = "";
        if (time != null)
          teste = time.Id.ToString();
        else
          teste = "0";

        url = ResolveUrl("/Ajax/CarimboImagemHandler.ashx") + "?idTime=" + teste; 
        return url;
      }
    }

    #endregion

    #region Convite

    protected object[] SelecionarConvites(int startRow, int pageSize, string sortOrder, out int total)
    {
      IConviteServico cServico = Resolver.GetImplementationOf<IConviteServico>();
      IList<ConviteJogTime> resultado = new List<ConviteJogTime>();
      total = 0;
      IPerfilJogoServico pjServico = Resolver.GetImplementationOf<IPerfilJogoServico>();
      if (PerfilJogoHidden.Value != "0")
      {
        PerfilJogo perfilJogo = pjServico.ObterPorId(Int32.Parse(PerfilJogoHidden.Value));

        cServico.SelecionarParametrosPaginacao(perfilJogo.Id, null, startRow, pageSize, sortOrder, out total, out resultado);
        return resultado.ToArray();
      }
      else
        return resultado.ToArray();
    }


    protected void ConfirmarConviteButton_Click(object sender, EventArgs e)
    {
      IPerfilJogoServico pjServico = Resolver.GetImplementationOf<IPerfilJogoServico>();
      IConviteServico cServico = Resolver.GetImplementationOf<IConviteServico>();
      LinkButton jogLink = (sender) as LinkButton;
      int idconvite = int.Parse(jogLink.CommandArgument.ToString());
      ConviteJogTime convite = cServico.ObterPorId(idconvite);
      convite.Jogador.Time = convite.Time;
      pjServico.Atualizar(convite.Jogador);

      IList<ConviteJogTime> convites;
      int total;
      cServico.SelecionarParametrosPaginacao(convite.Jogador.Id, null, 0, Int32.MaxValue, "", out total, out convites);

      foreach (ConviteJogTime conviteDel in convites)
        cServico.Excluir(conviteDel);
      PreencherCamposTime(convite.Time);
      AutuacaoUpdatePanel.Update();
    }

    #endregion

    protected void EnviarArquivoButton_Click(object sender, EventArgs e)
    {
      ValidarImagem(null);
    }
  }


}