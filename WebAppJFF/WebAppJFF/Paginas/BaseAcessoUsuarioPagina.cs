﻿using MPS.Runtime.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Classes;
using Interfaces.Servicos;

namespace WebAppJFF.Paginas
{
  public partial class BaseAcessoUsuarioPagina : System.Web.UI.Page {


    public void AcessoAdm()
    {
      if (Request.IsAuthenticated)
      {
        var ident = Page.User.Identity as FormsIdentity;
        string[] teste = ident.Name.ToString().Split('/');
        bool adm = teste[1] == "adm";
        if (!adm)
          Response.Redirect(FormsAuthentication.LoginUrl, true);
      }
      else
        Response.Redirect(FormsAuthentication.LoginUrl, true);

    }

    public void AcessoNormal()
    {
      if (!Request.IsAuthenticated)
      {
        Response.Redirect(FormsAuthentication.LoginUrl, true);
      }  
    }

    public void UsuarioJaLogado()
    {
      if (Request.IsAuthenticated)
      {
        Response.Redirect(FormsAuthentication.DefaultUrl, true);
      }
    }


    private Usuario usuarioLogado;
    public Usuario UsuarioLogado
    {
      get
      {
        if (Request.IsAuthenticated)
          {
            var ident = Page.User.Identity as FormsIdentity;
            string[] login = ident.Name.ToString().Split('/');
            IUsuarioServico servico = Resolver.GetImplementationOf<IUsuarioServico>();
            usuarioLogado = servico.ObterPorLogin(login[0]);
            if (usuarioLogado == null)
            {
              FormsAuthentication.SignOut();
              Response.Redirect(FormsAuthentication.LoginUrl, true);
            }
          }
        return usuarioLogado;
      }
    }
  }
}