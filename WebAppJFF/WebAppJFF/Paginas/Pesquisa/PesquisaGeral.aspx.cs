﻿using Classes;
using Interfaces.Servicos;
using MPS.Runtime.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppJFF.Paginas.Pesquisa {
  public partial class PesquisaGeral : BaseAcessoUsuarioPagina {

    protected void Page_Load(object sender, EventArgs e) {
      GridTimes.SelecionarComPaginacao = SelecionarTimes;
      PreencherDropDownJogo();
    }

    protected void PesquisarTimeButton_Click(object sender, EventArgs e) {
      
    }

    private void PreencherDropDownJogo() {
      if ((JogoDropDown != null) && (JogoDropDown.Items.Count == 0)) {
        JogoDropDown.Items.Clear();
        foreach (JogoEnum tipo in Enum.GetValues(typeof(JogoEnum))) {
          JogoDropDown.Items.Add(new ListItem(EnumHelper.ObterDescricaoEnum(tipo), ((int)tipo).ToString()));
        }
        JogoDropDown.Items.Insert(0, new ListItem("Selecionar", ""));
      }
    }

    public object[] SelecionarTimes(int startRow, int pageSize, string sortOrder, out int total) {
      ITimeServico tServico = Resolver.GetImplementationOf<ITimeServico>();
      IList<Time> resultado;

      int idJogo = 0;
      if (JogoDropDown.SelectedValue != "")
        idJogo = Int32.Parse(JogoDropDown.SelectedValue);

      tServico.SelecionarPorParametros(null, idJogo, DescricaoTextBox.Text, startRow, pageSize, sortOrder, out total, out resultado);
      return resultado.ToArray();
    }

    protected void PesquisaButton_Click(object sender, EventArgs e) {
      if (TimesCheckbox.Checked)
        GridTimes.CarregarDados(0, null, null);
    }

    protected void PesquisarTimeButton_Click1(object sender, EventArgs e) {

    }

    protected void PesquisarTimeButton_Click2(object sender, EventArgs e) {

    }
  }
}