﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteCampeonato.Master" AutoEventWireup="true" CodeBehind="PesquisaGeral.aspx.cs" Inherits="WebAppJFF.Paginas.Pesquisa.PesquisaGeral" %>

<%@ Register Src="~/Controle/GridControl.ascx" TagName="GridControl" TagPrefix="camp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <div class="panel-group-margin">
    <%--Com espaço superior--%>
  </div>
  <br />
    <br />
  <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Conditional">
    <ContentTemplate>
      <camp:GEDValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="" />
      <camp:GEDValidationSummary ID="GEDValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="" ValidationGroup="PesquisaValidation" />
      <div class="row">
        <div class="col-md-12 col-xs-12">

          <div class="panel panel-primary">
            <div class="panel-heading">Filtros Pesquisa</div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">

                    <label id="Label3" class="control-label">Descrição</label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="DescricaoTextBox" placeholder="Informe a Descrição"></asp:TextBox>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label id="Label1" class="control-label">Jogo</label>
                    <sup runat="server" id="Sup3" visible="true" class="icone-obrigatorio text-danger" title="Campo obrigatório">
                      <small>
                        <span class="ionicons ion-ios-medical fa-fw" aria-hidden="true"></span>
                      </small>
                    </sup>
                    <asp:DropDownList runat="server" CssClass="form-control" ID="JogoDropDown"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="PesquisaValidation"
                      ErrorMessage="Campo 'Jogo' é obrigatório." ControlToValidate="JogoDropDown"
                      EnableClientScript="true" runat="server" Display="Dynamic" ForeColor="" CssClass="text-danger" Enabled="true" Visible="true">
                    </asp:RequiredFieldValidator>
                  </div>
                </div>
              </div>

              <label class="checkbox-inline">
                <input type="checkbox" id="TimesCheckbox" runat="server" value="option1">
                Pesquisar por Times
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" id="JogadoresCheckbox" value="option2">
                Pesquisar por Jogadores
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" id="CampeonatosCheckbox" value="option3">
                Pesquisar por Campeonatos
              </label>
            </div>
          </div>

          <br />
          <asp:Button runat="server" ID="PesquisaButton" OnClick="PesquisaButton_Click" CssClass="btn btn-primary btn-lg btn-block" Text="Pesquisa" CausesValidation="true" ValidationGroup="PesquisaValidation"></asp:Button>
          <br />
          <br />
          <div class="panel panel-success">
            <div class="panel-heading">Times</div>
            <div class="panel-body">
              <camp:GridControl ID="GridTimes" runat="server" EnableBotaoImprimir="False" ScriptDetalhes="obterJogadoresTemplate">
                <HeaderTemplate>
                  <th data-sortexpression="Nome">Nome</th>
                  <th data-sortexpression="Tag">Tag</th>
                  <th data-sortexpression="Capitao.Nome">Capitão</th>
                  <th>Campeonatos</th>
                </HeaderTemplate>
                <RowTemplate>
                  <tr data-key="<%# Eval("id") %>">
                    <td>
                      <asp:Button ID="PesquisarTimeButton" runat="server" Text='<%# Eval("Nome")%>' CommandArgument='<%# Eval("Id")%>' OnClick="PesquisarTimeButton_Click2" CssClass="btn-link" /></td>
                    <td><%# Eval("Tag")%></td>
                    <td><%# Eval("Capitao.Nome")%></td>
                    <td><%--Fazer controle pra buscar Imagem dos camps jogados--%></td>
                  </tr>
                </RowTemplate>
              </camp:GridControl>
            </div>
          </div>
        </div>
      </div>    
    </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>
