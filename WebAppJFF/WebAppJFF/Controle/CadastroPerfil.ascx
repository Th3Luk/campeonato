﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CadastroPerfil.ascx.cs" Inherits="WebAppJFF.Controle.CadastroPerfil" %>
<asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
  <ContentTemplate>
    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-2">
      <asp:Image runat="server" ID="Jogadorimagem" CssClass="img-thumbnail img-responsive" ImageUrl="~/img/Axe.png"
        onerror="this.onload = null; this.src='../../img/blank-avatar.png';" />
        </div>
        <div class="col-xs-10">
          <div class="form-group">
            <label id="NomeLabel" class="control-label">Nome</label>
            <sup runat="server" id="Sup2" visible="true" class="icone-obrigatorio text-danger" title="Campo obrigatório">
              <small>
                <span class="ionicons ion-ios-medical fa-fw" aria-hidden="true"></span>
              </small>
            </sup>
            <asp:TextBox runat="server" CssClass="form-control" ID="NomeTextBox" placeholder="Digite seu Nome"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="CadValidation"
              ErrorMessage="Campo 'Nome' é obrigatório." ControlToValidate="NomeTextBox"
              EnableClientScript="true" runat="server" Display="Dynamic" ForeColor="" CssClass="text-danger" Enabled="false" Visible="true">
            </asp:RequiredFieldValidator>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <sup runat="server" id="Sup1" visible="true" class="icone-obrigatorio text-danger" title="Campo obrigatório">
              <small>
                <span class="ionicons ion-ios-medical fa-fw" aria-hidden="true"></span>
              </small>
            </sup>
            <asp:TextBox runat="server" CssClass="form-control" ID="EmailTextBox" placeholder="Digite seu Email"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="CadValidation"
              ErrorMessage="Campo 'Email' é obrigatório." ControlToValidate="EmailTextBox"
              EnableClientScript="true" runat="server" Display="Dynamic" ForeColor="" CssClass="text-danger" Enabled="false" Visible="true">
            </asp:RequiredFieldValidator>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-7">
          <div class="form-group">
            <label id="Label1" class="control-label">Faculdade</label>
            <sup runat="server" id="Sup3" visible="true" class="icone-obrigatorio text-danger" title="Campo obrigatório">
              <small>
                <span class="ionicons ion-ios-medical fa-fw" aria-hidden="true"></span>
              </small>
            </sup>
            <asp:DropDownList runat="server" CssClass="form-control" ID="FaculdadeDropDown"></asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="CadValidation"
              ErrorMessage="Campo 'Faculdade' é obrigatório." ControlToValidate="FaculdadeDropDown"
              EnableClientScript="true" runat="server" Display="Dynamic" ForeColor="" CssClass="text-danger" Enabled="false" Visible="true">
            </asp:RequiredFieldValidator>
          </div>

        </div>
        <div class="col-xs-5">
          <div class="form-group">
            <label id="Label10" class="control-label">RA</label>
            <sup runat="server" id="Sup4" visible="true" class="icone-obrigatorio text-danger" title="Campo obrigatório">
              <small>
                <span class="ionicons ion-ios-medical fa-fw" aria-hidden="true"></span>
              </small>
            </sup>
            <asp:TextBox runat="server" CssClass="form-control" ID="RATextoBox" placeholder="Digite seu RA"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
              ErrorMessage="Campo 'RA' é obrigatório." ControlToValidate="RATextoBox"
              EnableClientScript="true" runat="server" Display="Dynamic" ForeColor="" CssClass="text-danger" Enabled="false" Visible="true">
            </asp:RequiredFieldValidator>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label id="Label2" class="control-label">Twitch</label>
        <asp:TextBox runat="server" CssClass="form-control" ID="TwitchTextBox" placeholder="Digite sua Twitch"></asp:TextBox>
      </div>
      <fieldset class="group">
        <legend class="control-label">Escolher Imagem de Perfil</legend>
        <asp:FileUpload ID="ArquivoUpLoad" runat="server" onchange="javascript: ArquivoUpload_Changed(this);" />
        <asp:Panel runat="server" ID="PanelUploadMsg" EnableViewState="false" CssClass="alert alert-warning" Style="display: none;">
          <asp:Label runat="server" ID="LabelUploadMsg" CssClass="control-label" />
        </asp:Panel>
        <hr />
      </fieldset>


    </div>
  </ContentTemplate>
</asp:UpdatePanel>
