﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppJFF.Controle {

    public class MyTemplateContainer : Control, INamingContainer { }

    /**
     * Componente para Bootstrap Modal
     * Este componente funciona com placeholders
     * Fonte: http://stackoverflow.com/questions/11658053/how-to-add-a-templating-to-a-usercontrol
     * Fonte: https://msdn.microsoft.com/en-us/library/36574bf6%28v=vs.140%29.aspx
     * Fonte: http://www.codeproject.com/Articles/397646/A-Beginners-Tutorial-for-Understanding-Templated-U
     */
    public partial class BootsModalControl : System.Web.UI.UserControl {

        #region Header Template

        // PlaceHolder para o Header
        [TemplateContainer(typeof(MyTemplateContainer))]
        [TemplateInstance(TemplateInstance.Single)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public ITemplate HeaderTemplate { get; set; }

        // Binding Header Template
        private void BindHeader() {
            this.MyHeaderPlace.Controls.Clear();

            if (this.HeaderTemplate != null) {
                var container = new MyTemplateContainer();

                this.HeaderTemplate.InstantiateIn(container);
                this.MyHeaderPlace.Controls.Add(container);
            } else {
                this.MyHeaderPlace.Controls.Add(new LiteralControl("No header template defined"));
            }
        }

        #endregion

        #region Content Template

        // PlaceHolder para o Content
        [TemplateContainer(typeof(MyTemplateContainer))]
        [TemplateInstance(TemplateInstance.Single)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public ITemplate ContentTemplate { get; set; }

        // Binding Content Template
        private void BindContent() {
            this.MyContentPlace.Controls.Clear();

            if (this.ContentTemplate != null) {
                var container = new MyTemplateContainer();

                this.ContentTemplate.InstantiateIn(container);
                this.MyContentPlace.Controls.Add(container);
            } else {
                this.MyContentPlace.Controls.Add(new LiteralControl("No content template defined"));
            }
        }

        #endregion

        #region Footer Template

        // PlaceHolder para o Footer
        [TemplateContainer(typeof(MyTemplateContainer))]
        [TemplateInstance(TemplateInstance.Single)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public ITemplate FooterTemplate { get; set; }

        // Binding Content Template
        private void BindFooter() {
            this.MyFooterPlace.Controls.Clear();

            if (this.FooterTemplate != null) {
                var container = new MyTemplateContainer();

                this.FooterTemplate.InstantiateIn(container);

                this.MyFooterPlace.Controls.Add(new LiteralControl("<div class=\"modal-footer\">"));
                this.MyFooterPlace.Controls.Add(container);
                this.MyFooterPlace.Controls.Add(new LiteralControl("</div>"));
            }
        }

        #endregion

        // O ClientID do componente, sera o ClientID da Modal
        public override string ClientID {
            get {
                // base.ClientID
                return Modal.ClientID;
            }
        }

        // Classes CSS que devem ser adicionadas a modal
        private string cssClass;
        public string CssClass {
            get {
                if (String.IsNullOrEmpty(this.cssClass))
                    return "";

                return this.cssClass;
            }
            set {
                this.cssClass = value;
            }
        }

        protected void Page_Init(object sender, EventArgs e) {
            BindHeader();
            BindContent();
            BindFooter();
        }

        #region Manipulação da Modal
        public void AbrirModal() {
            var script = "$('#" + this.ClientID + "').modal('show');";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), this.ClientID + "_OpenModal", script, true);
        }

        public void FecharModal() {
            var script = "$('#" + this.ClientID + "').modal('hide');";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), this.ClientID + "_CloseModal", script, true);
        }
        #endregion

    }
}