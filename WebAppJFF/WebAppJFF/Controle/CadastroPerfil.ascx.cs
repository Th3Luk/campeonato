﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using MPS.Runtime.Dependency;
using Classes;
using Interfaces;
using Interfaces.Servicos;
using MPS.Web.UI.WebControls;

namespace WebAppJFF.Controle {
  public partial class CadastroPerfil : BaseAcessoUsuarioControle {

    private byte[] imagem {
      get {
        if (ViewState["imagem"] != null)
          return ViewState["imagem"] as byte[];
        return null;
      }
      set {
        ViewState["imagem"] = value;
      }
    }

    PerfilJogador perfilJogador;
    public PerfilJogador PerfilJogador {
      get {
        if (UsuarioLogado != null && perfilJogador == null)
          perfilJogador = UsuarioLogado.PerfilJogador;
        else if (perfilJogador == null)
          perfilJogador = new PerfilJogador();

        return perfilJogador;
      }
      set {
        perfilJogador = value;
        PreencherDropDownFaculdade();
        SetarCampos(value);
      }
    }

    public string ValidationGroup {
      set {
        RequiredFieldValidator1.ValidationGroup = value;
        RequiredFieldValidator1.Enabled = true;
        RequiredFieldValidator2.ValidationGroup = value;
        RequiredFieldValidator2.Enabled = true;
        RequiredFieldValidator3.ValidationGroup = value;
        RequiredFieldValidator3.Enabled = true;
        RequiredFieldValidator4.ValidationGroup = value;
        RequiredFieldValidator4.Enabled = true;
      }
    }

    private void SetarCampos(PerfilJogador perfilJogador) {
      NomeTextBox.Text = perfilJogador.Nome;
      EmailTextBox.Text = perfilJogador.Email;
      FaculdadeDropDown.SelectedValue = perfilJogador.IdFaculdade.ToString();
      RATextoBox.Text = perfilJogador.RA;
      TwitchTextBox.Text = perfilJogador.Twitch;

    }

    private void PreencherDropDownFaculdade() {
      if ((FaculdadeDropDown != null) && (FaculdadeDropDown.Items.Count == 0)) {
        FaculdadeDropDown.Items.Clear();
        foreach (FaculdadeEnum tipo in Enum.GetValues(typeof(FaculdadeEnum))) {
          FaculdadeDropDown.Items.Add(new ListItem(EnumHelper.ObterDescricaoEnum(tipo), ((int)tipo).ToString()));
        }
        FaculdadeDropDown.Items.Insert(0, new ListItem("Selecionar", ""));
      }
    }

    protected void Page_Load(object sender, EventArgs e) {
      if (!IsPostBack)
        PreencherDropDownFaculdade();
      Jogadorimagem.ImageUrl = UrlHandler;
    }

    public void PreencherPerfilJogador() {
      try {

        PerfilJogador.Ativo = true;
        PerfilJogador.Email = EmailTextBox.Text;
        PerfilJogador.Nome = NomeTextBox.Text;
        PerfilJogador.RA = RATextoBox.Text;
        PerfilJogador.Twitch = TwitchTextBox.Text;
        PerfilJogador.IdFaculdade = Int32.Parse(FaculdadeDropDown.SelectedValue);
        if (ValidarImagem())
          PerfilJogador.Imagem = ArquivoUpLoad.FileBytes;
      } catch (Exception exc) {
        throw exc;
      }
    }

    private bool ValidarImagem() {
      try {
        FileUpload fileUpload = ArquivoUpLoad as FileUpload;
        if (fileUpload != null) {
          if (fileUpload.HasFile) {
            string fileName = Server.HtmlEncode(fileUpload.FileName);
            // Get the extension of the uploaded file.
            string extension = System.IO.Path.GetExtension(fileName);

            // Allow only files with .doc or .xls extensions
            // to be uploaded.
            if ((extension != ".jpg") && (extension != ".png") && (extension != ".jpeg") && (extension != ".gif"))
              throw new Exception("Formato de arquivo Inválido");

            if ((fileUpload.PostedFile.ContentLength > 1 * 500000))
              //1000000 = 1 megabyte
              throw new Exception("O Arquivo é muito grande. É permitido anexar no máximo 500 kbytes para imagens.");
            return true;
          }
        }
        return false;
      } catch (Exception exc) {
        throw exc;
      }
    }

    private string UrlHandler {
      get {
        string url = "";
        if (PerfilJogador.Id != 0)
          url = ResolveUrl("~/Ajax/Handler1.ashx") + "?idUsuario=" + PerfilJogador.Id.ToString();
        return url;
      }
    }
  }
}