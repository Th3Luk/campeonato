﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NoticiaControle.ascx.cs" Inherits="WebAppJFF.Controle.NoticiaControle" %>
<div class="divNoticia">
    <asp:Image runat="server" ID="imgImportante" ImageUrl="~/img/importante.png" CssClass="imagemImportante" ToolTip="Noticia importante!" />
    <asp:Label runat="server" ID="lblTituloNoticia" CssClass="labelTituloNoticia"></asp:Label>
    <asp:Label runat="server" ID="lblDataNoticia" CssClass="lblDataNoticia"></asp:Label>
    <br />
    <div class="divTextoNoticia">
        <asp:Label runat="server" ID="lblTextoNoticia" CssClass="textoNoticia"></asp:Label>
    </div>
    <br />
</div>
