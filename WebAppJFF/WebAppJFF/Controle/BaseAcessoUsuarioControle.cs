﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using MPS.Runtime.Dependency;
using Classes;
using Interfaces;
using Interfaces.Servicos;

namespace WebAppJFF.Controle {
  public partial class BaseAcessoUsuarioControle : System.Web.UI.UserControl {
    private Usuario usuarioLogado;
    public Usuario UsuarioLogado
    {
      get
      {
        if (Request.IsAuthenticated)
        {
          var ident = Page.User.Identity as FormsIdentity;
          string[] login = ident.Name.ToString().Split('/');
          IUsuarioServico servico = Resolver.GetImplementationOf<IUsuarioServico>();
          usuarioLogado = servico.ObterPorLogin(login[0]);
          if (usuarioLogado == null)
          {
            FormsAuthentication.SignOut();
            Response.Redirect(FormsAuthentication.LoginUrl, true);
          }
        }
        return usuarioLogado;
      }
    }
  }
}