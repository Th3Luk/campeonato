﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppJFF.Controle {
  public partial class NoticiaResumidaControle : System.Web.UI.UserControl {

    public string TituloNoticia {
      get {
        return (string)ViewState["TituloNoticia"];
      }
      set {
        if (value.Length > 20) {
          ViewState["TituloNoticia"] = value.Substring(0, 20) + "...";
          lblTituloNoticia.Text = value.Substring(0, 20) + "...";
        } else {
          ViewState["TituloNoticia"] = value;
          lblTituloNoticia.Text = value;
        }
      }
    }

    public bool Importante {
      get {
        return (bool)ViewState["Importante"];
      }
      set {
        ViewState["Importante"] = value;
        if (value == false)
          imgImportante.Visible = false;
      }
    }

    public string TextoNoticia {
      get {
        return (string)ViewState["TextoNoticia"];
      }
      set {
        if (value.Length > 35) {
          ViewState["TextoNoticia"] = value.Substring(0, 35) + "...";
          lblTextoNoticia.Text = value.Substring(0, 35) + "...";
        } else {
          ViewState["TextoNoticia"] = value;
          lblTextoNoticia.Text = value;
        }
      }
    }

    protected void Page_Load(object sender, EventArgs e) {

    }
  }
}