﻿// Check the existence of an element in jQuery
// if ($(selector).exists()) { // Do something }
// Fonte: http://stackoverflow.com/a/31047/1003020
jQuery.fn.exists = function () {
    return this.length > 0;
}

// Return the element id
//var el_id = $(selector).id();
jQuery.fn.id = function () {
    return this.get(0).id;
}

$(document).ready(function () {
    // Recupera a altura da tela do usuario
    var window_height = $(window).height();

    // Verifica se o acesso esta sendo feito por dispositivo mobile
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $('body').addClass('mobile');
    }
});

// Codigo para controle de multiplas modais oriundo da fonte abaixo
// Fonte: http://miles-by-motorcycle.com/static/bootstrap-modal/index.html
$(document).ready(function () {
    if ($.isFunction($.fn.modal))
        $(document)
            // Going to show
            .on('show.bs.modal', '.modal', function (event) {
                var modal = $(this); // The actual modal
                var button = $(event.relatedTarget); // Button that triggered the modal

                // Fix para a posiçao da modal
                if ($(document).height() > $(window).height())
                    $('body').addClass('modal-open-noscroll'); // no-scroll
                else
                    $('body').removeClass('modal-open-noscroll');

                // Colocamos as modais no final do main-page-form
                modal.appendTo(".main-page-form");
                // Fonte: http://stackoverflow.com/questions/29837294/jquery-appendto-bug/29837354#29837354
                event.stopPropagation(); // stop the event from bubbling further up
            })
            // Is visible already
            .on('shown.bs.modal', '.modal', function (event) {
                var modal = $(this); // The actual modal
                var button = $(event.relatedTarget); // Button that triggered the modal

                // Fix para a posiçao da modal
                if ($('body').hasClass('modal-open-noscroll'))
                    $('body').css('padding-right', '17px'); // Veja: http://davidwalsh.name/detect-scrollbar-width

                // Keep track of the number of open modals
                if (typeof ($('body').data('fv_open_modals')) == 'undefined')
                    $('body').data('fv_open_modals', 0);

                // if the z-index of this modal has been set, ignore.
                if ($(this).hasClass('fv-modal-stack'))
                    return;
                $(this).addClass('fv-modal-stack');
                // Increment the number of opened modals
                $('body').data('fv_open_modals', $('body').data('fv_open_modals') + 1);


                // Ajuste de tela quando a tab Documentos abrir - Usado pelo Splitter
                if ($('.pdf-viewer-splitter', modal).exists()) {
                    $('.pdf-viewer-splitter', modal).trigger('resize');
                }
            })
            // Going to hide
            .on('hide.bs.modal', '.modal', function (event) {
                var modal = $(this); // The actual modal
                var button = $(event.relatedTarget); // Button that triggered the modal

                // Colocamos as modais no seus locais originais
                $('#' + modal.id() + '_modalplaceholder').after(modal);
            })
            // Is hidden already
            .on('hidden.bs.modal', '.modal', function (event) {
                var modal = $(this); // The actual modal
                var button = $(event.relatedTarget); // Button that triggered the modal

                modal.removeClass('fv-modal-stack');
                // Decrement the number of opened modals
                $('body').data('fv_open_modals', $('body').data('fv_open_modals') - 1);

                // If exists a opened modal
                if ($('body').data('fv_open_modals') > 0) {
                    if (!$('body').hasClass('modal-open')) {
                        $('body').addClass('modal-open');

                        // Fix para a posiçao da modal
                        if ($('body').hasClass('modal-open-noscroll'))
                            $('body').css('padding-right', '17px'); // Veja: http://davidwalsh.name/detect-scrollbar-width
                    }
                } else {
                    // Fix para a posiçao da modal
                    $('body').removeClass('modal-open-noscroll');
                }
            });
});

// Copy to Clipboard
$(function () {
    // Prompt para copiar texto
    function copyToClipboard(text) {
        window.prompt('Para copiar, aperte Ctrl+C e Enter', text);
    }

    // Ao clickar no numero do processo, o numero eh copiado para a area de transferencia do usuario
    $(document).on('click', '.numprocesso', function () {
        copyToClipboard($(this).html());
    });
});

// Seleciona o texto de um input ao clica-lo
// Fonte: http://stackoverflow.com/a/4067488/1003020
$(function () {
    /* Seleciona o texto do campo pagina atual */
    $(document).on('click', '.select-all-text', function () {
        var txt = $(this).get(0);
        txt.setSelectionRange(0, txt.value.length);
    });
});

// Hide Banner
$(function () {
    // Inicializa o javascript de esconder o cabecalho
    var hideOnStart = false;
    if (HideElement !== undefined)
        HideElement('bannerTop', hideOnStart);
});

// Função que ativa todos os Popovers sempre que uma requisição for realizada
function CarregarPopover() {
    $('[data-toggle="popover"]').popover({
        trigger: 'focus hover',
        placement: 'top',
        html: 'true'
    });
}
$(function () {
    //Fonte: https://msdn.microsoft.com/en-us/library/bb383810.aspx
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(CarregarPopover); // metodo que serve pro load e request
});

// Libera a tela
function hideCarregando() {
    $.unblockUI();
}
// Bloqueia a tela
function showCarregando() {
    $.blockUI({
        css: { backgroundColor: 'transparent', border: 'none' },
        message: '<img id="imgLoading" alt="Carregando..." src="/img/loading.gif" />'
    });
}

//idGrid = grid que vai receber o detalhe
//callback = funcao que obtem o html a ser exibido como detalhe
function carregarDetalhes(idGrid, callback) {
    //se a grid foi inicializada, nao precisa adicionar a primeira coluna e o evento de click
    if ($('#' + idGrid).attr('data-grid-details') == 'iniciada')
        return true;

    //adiciona a informacao de que a grid foi inicializada
    $('#' + idGrid).attr('data-grid-details', 'iniciada');

    //adiciona a primeira coluna com o botao que exibe os detalhes
    var myTD = '<td class="toggle-details"><span class="fa fa-angle-double-right fa-fw toggle-details-icon"></span></td>';
    $('#' + idGrid + ' thead tr:first-child').prepend('<th></th>');
    $('#' + idGrid + ' tbody tr').prepend(myTD);

    //adiciona o evento de click no botao de detalhe que foi adicionado
    $('#' + idGrid + ' tbody tr').on('click', '.toggle-details', function () {
        var $btn = $('.toggle-details-icon', $(this));
        var $row = $(this).parent();
        var colspan = $('td', $row).length;

        //se o detalhe da linha foi carregado, nao precisa carregar novamente
        //apenas mostra ou esconde a linha
        if ($row.attr('data-detalhe-situacao') == 'carregado') {
            if ($row.next().is(':visible'))
                $row.next().hide();
            else
                $row.next().show();
        } else {
            //executa a funcao callback para obter o html a ser exibido
            var idObjeto = $row.attr('data-key');
            //var conteudo = eval(callback + '(' + idObjeto + ');');
            var conteudo = callback(idObjeto);

            $row.after('<tr class="row-detail"><td colspan="' + colspan + '"><div class="row-detail-container">' + conteudo + '</div></td></tr><tr style="display:none;"></tr>');
            $row.attr('data-detalhe-situacao', 'carregado');

            // Carregamos as Popovers do template, caso exista alguma
            CarregarPopover();
        }

        if ($btn.hasClass('fa-angle-double-right')) {
            $row.addClass('row-details-open');
            $btn.switchClass('fa-angle-double-right', 'fa-angle-double-down');
        } else {
            $row.removeClass('row-details-open');
            $btn.switchClass('fa-angle-double-down', 'fa-angle-double-right');
        }
    });
}

//metodo para obter o html do template
//urlTemplate: url do html que contem o template
//idTemplate: id do template
function carregarTemplate(urlTemplate, idTemplate) {
    //cria uma div para 'pendurar' o template, caso a div ainda nao exista
    if (!$('#template-container').exists())
        $('body').append('<div id="template-container"></div>');
    //busca o template, caso ele ainda nao tenha sido carregado
    if (!$('#' + idTemplate).exists()) {
        var template = $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: urlTemplate,
            cache: false,
            async: false
        });
        $('#template-container').append(template.responseText);
    }
    return $('#processo-template').html();
}

/**
 * Dropdown de Painel de Controle
 * Esta dropdown nao fecha quando seu conteudo eh clicado, ao 
 * contrario das Dropdowns normais do Bootstrap
 * Fonte: http://stackoverflow.com/a/25196101/1003020
 */
$(function () {
    if ($('li.dropdown.menu-dashboard').exists()) {
        $('li.dropdown.menu-dashboard a').on('click', function (event) {
            $(this).parent().toggleClass('open');
        });
        $('body').on('click', function (e) {
            if (!$('li.dropdown.menu-dashboard').is(e.target)
                && $('li.dropdown.menu-dashboard').has(e.target).length === 0
                && $('.open').has(e.target).length === 0
            ) {
                $('li.dropdown.menu-dashboard').removeClass('open');
            }
        });
    }
});

/**
 * Menu Principal Unico
 * Funcionalidade para deixar o menu-primario selecionado
 * enquanto o usuario navega pelo menu-secundario equivalente
 */
$(function () {
    $('.menu-principal-unico .menu-primario ul > li').hover(function () {

        $('.menu-principal-unico .menu-secundario ul').hide();
        $('.menu-principal-unico .menu-primario ul .open').removeClass('open');

        var submenu = $(this).attr('class');
        $('.menu-principal-unico .menu-secundario .' + submenu).show();
        $('.menu-principal-unico .menu-secundario .' + submenu + ' ul').show();

        $(this).addClass('open');
    }, function () {
        var submenu = $(this).attr('class');
        $('.menu-principal-unico .menu-secundario .' + submenu).show();
        $('.menu-principal-unico .menu-secundario .' + submenu + ' ul').show();
    });
});

/**
 * Calcula posição esq/dir do menu principal
 * Se um menu vai estrapolar a largura da tela a direita, abre o menu na esquerda
 */
$(function () {
    $('.menu-principal ul.dropdown-menu [data-toggle=dropdown]').on('mouseover', function (event) {
        var menu = $(this).parent().find('ul:first');
        if (!menu.hasClass('calculated')) {
            var menupos = menu.offset();
            if ((menupos.left + menu.width()) + 30 > $(window).width()) {
                var newpos = -menu.width();
            } else {
                var newpos = $(this).parent().width();
            }
            menu.css({ left: newpos });
            menu.addClass('calculated');
        }
    });
});

/**
 * :::::::::::::: CODIGOS EM AVALIACAO ::::::::::::::
 */
$(function () {
    /**
     * Menu que gruda no topo no scroll
     * Fonte: http://stackoverflow.com/a/22759948/1003020
     */
    //var navbarOffset = $('.navbar').offset().top; // Temos q considerar o $('#bannerTop') tbem!
    //$(window).scroll(function () {
    //    if ($(window).scrollTop() >= navbarOffset) {
    //        $('#content').css('padding-top', $('.navbar').height());
    //        $('.navbar').addClass('navbar-fixed-top');
    //    } else {
    //        $('#content').css('padding-top', 0);
    //        $('.navbar').removeClass('navbar-fixed-top');
    //    }
    //});
});

/**
 * :::::::::::::: PRECISAM DE REVISAO ::::::::::::::
 */

// Esconde a layer de modal visivel
function hideCarregandoModal() {
    $('#backgroundOverJanelaModal').removeClass('show').addClass('hidden');
}
// Mantem a layer de modal visivel
function showCarregandoModal() {
    $('#backgroundOverJanelaModal').removeClass('hidden').addClass('show');
}

$("form").ready(function () {
    hideCarregando();
});

$("form").submit(function () {
    if ((typeof (Page_IsValid) == 'undefined') || (Page_IsValid))
        showCarregando();
});

/*
* Scripts para Mascaras Numericas
*/
function ativarMascaraNumerica(campo, config) {
    campo.maskMoney({
        thousands: '.',
        decimal: ',',
        symbolStay: true,
        allowZero: true,

        symbol: config.simbolo,
        precision: config.precisaoDecimal,
        allowNegative: config.permitirNegativo
    });
    campo.off('focusout')
        .on('focusout', { campo: campo, config: config }, function () {
            var vlrNumerico = parseFloat(campo.val().replace(config.simbolo, '').replace(/\./g, '').replace(',', '.'));
            if ((config.valorMaximo != null) && (vlrNumerico > config.valorMaximo)) {
                alert('O valor informado é maior que o máximo permitido.\nUse um valor inferior a ' + config.valorMaximo);
            } else if ((config.valorMinimo != null) && (vlrNumerico < config.valorMinimo)) {
                alert('O valor informado é menor que o mínimo permitido.\nUse um valor superior a ' + config.valorMinimo);
            } else if ((config.precisaoInteiro != null) && (parseInt(vlrNumerico).toString().length > config.precisaoInteiro)) {
                alert('A quantidade de dígitos inteiros excede o máximo permitido.\nUse um número com no máximo ' + config.precisaoInteiro + ' dígitos.');
            }
        });
}
function ativarMascaraPersonalizada(campo, mask) {
    campo.mask(mask, { placeholder: ' ' });
}

// Mostra e esconde a tela de carregamento no inicio e no final de uma requisicao
$(function () {
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    function BeginRequestHandler(sender, args) {
        showCarregando();
    }
    function EndRequestHandler(sender, args) {
        hideCarregando();
    }
});

// Revisar esse bloco - @author: vinicius.garca
function showHideDetalhesExcecao() {
    var div = document.getElementById('divDetalhesExcecao');
    if (div != undefined) {
        if (div.style.display == 'none')
            div.style.display = 'block';
        else
            div.style.display = 'none';
    }
}
function abrirPopup(page, width, height) {
    var busca = document.getElementById('BuscaAjuda').value;
    OpenWin = this.open(page + '?busca=' + busca, "CtrlWindow", "toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=yes,dependent=no,directories=no,width=" + width + ",height=" + height + ",x=50,y=50");
}

//var campoAjuda = document.getElementById('BuscaAjuda');
//campoAjuda.onkeypress = function (e) {
//    e = e ? e : window.event;
//    if (e.keyCode == 13) {
//        var serverPath = '<%=serverPath%>';
//        javascript: abrirPopup(serverPath, '900', '600');
//        return false;
//    }
//}
