﻿function obterDocumentoTemplate(id) {
    var retorno = '';
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: '/Ajax/DocumentoAjax.asmx/obterPorId',
        data: { 'id': id },
        success: function (result) {
          retorno = DefineCamposTemplate(result);
        },
        error: function (xmlRequest) {
            retorno = '<div class="alert alert-danger" role="alert">Não foi possível obter os detalhes do registro.</div>';
        },
        async: false
    });
    return retorno;
}

function DefineCamposTemplate(result) {
  var htmlTemplate = carregarTemplate('/assets/templates/processo-template.html', 'processo-template');

  //obtem a linha q vai ser iterada
  $linha = $('.linha-interessado', $(htmlTemplate)).parent();
  var i;
  var linhaHtml = '';
  for (i = 0; i < result.d.Interessados.length; i++) {
    linhaHtml = linhaHtml + $linha.html().replace(/{{Qualificacao}}/g, result.d.Interessados[i].Qualificacao);
    linhaHtml = linhaHtml.replace(/{{Nome}}/g, result.d.Interessados[i].Nome);
  }

  //Controlar visibilidade indicativos            
  var indicativosHtml = $('.Urgente', $(htmlTemplate)).parent().html();
  if (!result.d.Urgente) {
    indicativosHtml = indicativosHtml.replace("Urgente", "hidden");
  }
  if (!result.d.Sigiloso) {
    indicativosHtml = indicativosHtml.replace("Sigiloso", "hidden");
  }
  if (!result.d.Virtual) {
    indicativosHtml = indicativosHtml.replace("Virtual", "hidden");
  }

  if (result.d.Lembretes == "")
    indicativosHtml = indicativosHtml.replace("Lembretes", "hidden");
  else
    indicativosHtml = indicativosHtml.replace("DataContentLembretes", result.d.Lembretes);

  if (result.d.Prazos == "")
    indicativosHtml = indicativosHtml.replace("Prazo", "hidden");
  else
    indicativosHtml = indicativosHtml.replace("DataContentPrazos", result.d.Prazos);

  if (result.d.Impedimentos == "")
    indicativosHtml = indicativosHtml.replace("Impedimentos", "hidden");
  else
    indicativosHtml = indicativosHtml.replace("DataContentImpeditivos", result.d.Impedimentos);

  if (result.d.Avisos == "")
    indicativosHtml = indicativosHtml.replace("Avisos", "hidden");
  else
    indicativosHtml = indicativosHtml.replace("DataContentAvisos", result.d.Avisos);


  var subst1 = $('.Urgente', $(htmlTemplate)).parent().html();
  htmlTemplate = htmlTemplate.replace(subst1, indicativosHtml);

  //substitui a linha template pela linha gerada
  var subst = $('.linha-interessado', $(htmlTemplate)).parent().html();
  htmlTemplate = htmlTemplate.replace(subst, linhaHtml);

  //usar "/string/g" para fazer replace all. Senao faz apenas 1 vez
  htmlTemplate = htmlTemplate.replace(/{{Numero}}/g, result.d.Numero);
  htmlTemplate = htmlTemplate.replace(/{{Id}}/g, result.d.Id);
  htmlTemplate = htmlTemplate.replace(/{{Assunto}}/g, result.d.Assunto);
  htmlTemplate = htmlTemplate.replace(/{{Observacao}}/g, result.d.Observacao);
  htmlTemplate = htmlTemplate.replace(/{{DataAutuacao}}/g, result.d.DataAutuacao);
  htmlTemplate = htmlTemplate.replace(/{{LocalAtual}}/g, result.d.LocalAtual);
  htmlTemplate = htmlTemplate.replace(/{{Situacao}}/g, result.d.Situacao);
  return htmlTemplate;
}

