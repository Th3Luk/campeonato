(function ($) {
  $(function () {
    /**
         * Bootstrap Wizard
		 * Add Wizard functionalities to Bootstrap Tab
		 * 
		 * @version 1.0
		 * @date 2015.03.03
		 * @author Vinicius Garcia
		 * @requires jquery 1.10 or higher
		 */
    BootstrapWizard = function (target, abaAtivaHF) {
      var list = $('.tab-list', target);
      var pager = $('.tab-content .wizard', target);

      // Total de itens na lista de abas
      var total = list.find('li').length;

      // Fun�ao que retorna o index da primeira tab ativa
      getFirstActiveIndex = function () {
        return list.find('li:not(.disabled):first').index(list.selector + ' li');
      }

      // Fun�ao que retorna o index da ultima tab ativa
      getLastActiveIndex = function () {
        return list.find('li:not(.disabled):last').index(list.selector + ' li');
      }

      //se existe valor no HF seta ele
      if (abaAtivaHF.val().length > 0) {
        tab = list.find('li:eq(' + parseInt(abaAtivaHF.val()) + ')');
        $('a', tab).tab('show');
      } else {
        // Seleciona a primeira aba, caso n�o exista nenhuma ativa
        if (!$('.active', target).exists()) {
          $('li:not(.disabled):first a', target).tab('show');
          $('.previous .btn', pager).prop('disabled', true);
          $('.next .btn', pager).prop('disabled', false);
        }
      }

      $('a[data-toggle="tab"]', target).on('shown.bs.tab', function (e) {
        var newtab = $(e.target); // newly activated tab
        var oldtab = $(e.relatedTarget); // previous active tab

        // Index da pagina que esta sendo exibida atualmente
        var index = newtab.parent().index(list.selector + ' li');
        abaAtivaHF.val(index);
        var primeirapagina = getFirstActiveIndex();
        $('.previous .btn', pager).prop('disabled', (index == primeirapagina));

        var ultimapagina = getLastActiveIndex();
        $('.next .btn', pager).prop('disabled', (index == ultimapagina));
      })

      // Evento disparado antes de exibir uma nova aba
      $('a[data-toggle="tab"]', target).on('show.bs.tab', function (e) {
        var newtab = $(e.target); // newly activated tab
        var oldtab = $(e.relatedTarget); // previous active tab

        // Se estiver desabilitado, nao exibir
        if (newtab.parent().hasClass('disabled'))
          return false;
      });

      // Botao PREVIOUS
      $('.previous .btn', pager).on('click', function (e) {
        var activetab = list.find('li.active');
        var index = activetab.index(list.selector + ' li');

        // Verificamos se nao estamos na primeira aba
        var primeirapagina = 0;
        if (index > primeirapagina) {
          var previoustab = null;
          var previousindex = index;
          do {
            // Procuramos a aba ativa anterior
            previoustab = list.find('li:eq(' + --previousindex + ')');
          } while ((previoustab.hasClass('disabled') != false) && (previousindex > primeirapagina));

          // Retrocedemos para a aba anterior se ela estiver ativa e seu index estiver na lista
          if ((previoustab.hasClass('disabled') == false) || (previousindex > primeirapagina))
            $('a', previoustab).tab('show');
        }
      });

      // Botao NEXT
      $('.next .btn', pager).on('click', function (e) {
        var activetab = list.find('li.active');
        var index = activetab.index(list.selector + ' li');

        // Verificamos se nao estamos na ultima aba
        var ultimapagina = total - 1;
        if (index < ultimapagina) {
          var nexttab = null;
          var nextindex = index;
          do {
            // Procuramos a proxima aba ativa
            nexttab = list.find('li:eq(' + ++nextindex + ')');
          } while ((nexttab.hasClass('disabled') != false) && (nextindex < ultimapagina));

          // Avan�amos para a proxima aba se ela estiver ativa e seu index estiver na lista
          if ((nexttab.hasClass('disabled') == false) || (nextindex < ultimapagina))
            $('a', nexttab).tab('show');
        }
      });
    }
  });
})(jQuery);