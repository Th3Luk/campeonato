/**
 * .: BotaoAtalhos.js :.
 * Inicializaçao das funcionalidades do botao de atalhos, ao lado do menu
 * do sistema GED. Esta eh uma feature especialista do GED, precisara de 
 * adaptaçoes para ser utilizada em outros sistemas.
 * - Observar estrutura de html para este botao em GED.Master
 * 
 * - Dependente de jQuery 1.9+
 * - Compativel com FF, Chrome, IE 9, IE 7, IE 8 ( Sem efeito de sombra em 
 *   IE 7 e IE 8 )
 * 
 * -> Bugs conhecidos:
 * - Ateh o momento, nenhum! :)
 * 
 * @author: Vinicius Garcia
 * @date: 2013-05-21
 */
$(function () {
	// Ativa o menu de atalhos
	$('.botaoAtalhos').click(function () {
		$('.menuAtalhos').toggle();
	});
	// Esconde o menu de atalhos qndo o mouse nao estiver em cima dele
	$('.botaoAtalhos, .menuAtalhos, .menuAtalhosContent')
		.mouseover(function () {
			$('.menuAtalhos').show();
		}).mouseout(function () {
			$('.menuAtalhos').hide();
		});
});