﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppJFF.Controle {
  public partial class PromocaoControle : System.Web.UI.UserControl {

    public string ImagemPromocao {
      get {
        return (string)ViewState["ImagemPromocao"];
      }
      set {
        ViewState["ImagemPromocao"] = value;
        imgPromocao.ImageUrl = value;
      }
    }

    public string Descricao {
      get {
        return (string)ViewState["Descricao"];
      }
      set {
        ViewState["Descricao"] = value;
        lblDescricao.Text = value;
      }
    }

    protected void Page_Load(object sender, EventArgs e) {

    }
  }
}