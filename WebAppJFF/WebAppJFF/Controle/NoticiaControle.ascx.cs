﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppJFF.Controle {
  public partial class NoticiaControle : System.Web.UI.UserControl {

    public string TituloNoticia {
      get {
        return (string)ViewState["TituloNoticia"];
      }
      set {
        ViewState["TituloNoticia"] = value;
        lblTituloNoticia.Text = value;
      }
    }

    public string DataNoticia {
      get {
        return (string)ViewState["DataNoticia"];
      }
      set {
        ViewState["DataNoticia"] = value;
        lblDataNoticia.Text = value;
      }
    }

    public bool Importante {
      get {
        return (bool)ViewState["Importante"];
      }
      set {
        ViewState["Importante"] = value;
        if (value == false)
          imgImportante.Visible = false;
      }
    }

    public string TextoNoticia {
      get {
        return (string)ViewState["TextoNoticia"];
      }
      set {
        ViewState["TextoNoticia"] = value;
        lblTextoNoticia.Text = value;
      }
    }

    protected void Page_Load(object sender, EventArgs e) {

    }
  }
}