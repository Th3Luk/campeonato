﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BootsModalControl.ascx.cs" Inherits="WebAppJFF.Controle.BootsModalControl" %>

<%-- Placeholder que marca o local original da modal criada --%>
<span id="<%= Modal.ClientID %>_modalplaceholder" class="hidden"></span>

<div class="modal" runat="server" ID="Modal" tabindex="-1">
    <div class="modal-dialog <%= CssClass.ToString() %>">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <asp:PlaceHolder runat="server" ID="MyHeaderPlace" />
                </h4>
            </div>
            <div class="modal-body container-fluid">
                <asp:PlaceHolder runat="server" ID="MyContentPlace" />
            </div>
            
            <asp:PlaceHolder runat="server" ID="MyFooterPlace" />
            
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
