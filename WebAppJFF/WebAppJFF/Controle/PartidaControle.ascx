﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PartidaControle.ascx.cs" Inherits="WebAppJFF.Controle.PartidaControle" %>

<%@ Register TagPrefix="uc1" TagName="Time" Src="~/Controle/TimeControle.ascx" %>
<div class="divPartidaControle">
    <uc1:Time ID="Time1" runat="server" />
    <asp:Image runat="server" ImageUrl="~/img/VS.png" Width="50px" Height="50px" CssClass="imagemVS" />
    <uc1:Time ID="Time2" runat="server" />
    <table style="width: 100%; clear:both;">
        <tr>
            <td style="width: 33%;">&nbsp;</td>
            <td style="text-align: center; width: 33%;"><asp:Label runat="server" ID="lblDataHora" CssClass="labelDataHora"></asp:Label> </td>
            <td style="width: 33%;"> 
                <asp:ImageButton runat="server" ID="btnVerDetalhes" Text="Ver detalhes" CssClass="botaoVerDetalhes" ImageUrl="~/img/FundoBotao.png" OnClick="btnVerDetalhes_Click" Visible="false" />
                <asp:DropDownList runat="server" ID="ddlPartidas" CssClass="dropdownVerDetalhes" AutoPostBack="true" OnSelectedIndexChanged="ddlPartidas_SelectedIndexChanged" Visible="false">
                    <asp:ListItem Text="Partida 1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Partida 2"></asp:ListItem>
                    <asp:ListItem Text="Partida 3"></asp:ListItem>
                    <asp:ListItem Text="Partida 4"></asp:ListItem>
                    <asp:ListItem Text="Partida 5"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</div>
