﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppJFF.Controle {
  public partial class TimeControle : System.Web.UI.UserControl {

    public string NomeTime {
      get {
        return (string)ViewState["NomeTime"];
      }
      set {
        ViewState["NomeTime"] = value;
        lblNomeTime.Text = value;
      }
    }
    public string NomeCapitao {
      get {
        return (string)ViewState["NomeCapitao"];
      }
      set {
        ViewState["NomeCapitao"] = value;
        lblNomeCapitao.Text = value;
      }
    }
    public string LinkSteamCapitao {
      get {
        return (string)ViewState["LinkSteamCapitao"];
      }
      set {
        ViewState["LinkSteamCapitao"] = value;
        if (!value.StartsWith("http://"))
          linkCapitao.HRef = "http://" + value;
        else
          linkCapitao.HRef = value;
      }
    }
    
    protected void Page_Load(object sender, EventArgs e) {
      
    }

  }
}