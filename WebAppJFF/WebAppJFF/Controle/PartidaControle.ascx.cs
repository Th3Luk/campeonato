﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppJFF.Controle {
  public partial class PartidaControle : System.Web.UI.UserControl {

    public string IDPartida {
      get {
        return (string)ViewState["IDPartida"];
      }
      set {
        ViewState["IDPartida"] = value;
      }
    }
    public string IDPartida2 {
      get {
        return (string)ViewState["IDPartida2"];
      }
      set {
        ViewState["IDPartida2"] = value;
      }
    }
    public string IDPartida3 {
      get {
        return (string)ViewState["IDPartida3"];
      }
      set {
        ViewState["IDPartida3"] = value;
      }
    }
    public string IDPartida4 {
      get {
        return (string)ViewState["IDPartida4"];
      }
      set {
        ViewState["IDPartida4"] = value;
      }
    }
    public string IDPartida5 {
      get {
        return (string)ViewState["IDPartida5"];
      }
      set {
        ViewState["IDPartida5"] = value;
      }
    }

    public string DataHora {
      get {
        return (string)ViewState["DataHora"];
      }
      set {
        ViewState["DataHora"] = value;
        lblDataHora.Text = value;
      }
    }

    public string NomeTime1 {
      get {
        return (string)ViewState["NomeTime"];
      }
      set {
        ViewState["NomeTime"] = value;
        Time1.NomeTime = value;
      }
    }
    public string NomeCapitao1 {
      get {
        return (string)ViewState["NomeCapitao"];
      }
      set {
        ViewState["NomeCapitao"] = value;
        Time1.NomeCapitao = value;
      }
    }
    public string LinkSteamCapitao1 {
      get {
        return (string)ViewState["LinkSteamCapitao1"];
      }
      set {
        ViewState["LinkSteamCapitao1"] = value;
        Time1.LinkSteamCapitao = value;
      }
    }
    
    public string NomeTime2 {
      get {
        return (string)ViewState["NomeTime"];
      }
      set {
        ViewState["NomeTime"] = value;
        Time2.NomeTime = value;
      }
    }
    public string NomeCapitao2 {
      get {
        return (string)ViewState["NomeCapitao"];
      }
      set {
        ViewState["NomeCapitao"] = value;
        Time2.NomeCapitao = value;
      }
    }
    public string LinkSteamCapitao2 {
      get {
        return (string)ViewState["LinkSteamCapitao2"];
      }
      set {
        ViewState["LinkSteamCapitao2"] = value;
        Time2.LinkSteamCapitao = value;
      }
    }
    
    protected void Page_Load(object sender, EventArgs e) {
        CarregarPartidas();
    }

    private void CarregarPartidas() {
      ddlPartidas.Items.Clear();
      ddlPartidas.Items.Add("Escolha a partida...");

      if (IDPartida != null && IDPartida != "") {
        ListItem item = new ListItem();
        item.Text = "Partida 1";
        item.Value = Convert.ToString(IDPartida);
        ddlPartidas.Items.Add(item);
        ddlPartidas.SelectedItem.Value = "Partida 1";
        btnVerDetalhes.Visible = true;
      }

      if (IDPartida2 != null && IDPartida2 != "") {
        ListItem item2 = new ListItem();
        item2.Text = "Partida 2";
        item2.Value = Convert.ToString(IDPartida2);
        ddlPartidas.Items.Add(item2);

        btnVerDetalhes.Visible = false;
        ddlPartidas.Visible = true;
      }

      if (IDPartida3 != null && IDPartida3 != "") {
        ListItem item3 = new ListItem();
        item3.Text = "Partida 3";
        item3.Value = Convert.ToString(IDPartida3);
        ddlPartidas.Items.Add(item3);
      }

      if (IDPartida4 != null && IDPartida4 != "") {
        ListItem item4 = new ListItem();
        item4.Text = "Partida 4";
        item4.Value = Convert.ToString(IDPartida4);
        ddlPartidas.Items.Add(item4);
      }
      if (IDPartida5 != null && IDPartida5 != "") {
        ListItem item5 = new ListItem();
        item5.Text = "Partida 5";
        item5.Value = Convert.ToString(IDPartida5);
        ddlPartidas.Items.Add(item5);
      }
    }

    protected void btnVerDetalhes_Click(object sender, ImageClickEventArgs e) {
      Response.Redirect("ResultadoPartida.aspx?IDPartida=" + IDPartida);
    }

    protected void ddlPartidas_SelectedIndexChanged(object sender, EventArgs e) {
      if (ddlPartidas.SelectedIndex > 0) {
        if (ddlPartidas.SelectedValue != null) {
          Response.Redirect("ResultadoPartida.aspx?IDPartida=" + ddlPartidas.SelectedValue);
        }
      }      
    }

  }
}