﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Text;
using AjaxControlToolkit;

namespace WebAppJFF.Controle {
  public partial class Calendario : System.Web.UI.UserControl {
    #region Atributos

    #endregion

    #region Propriedades

    #region Controle de Label
    // Define se o label deve ou nao ser exibido
    public bool ExibirLabel {
      get {
        return ControleLabel.Visible;
      }
      set {
        ControleLabel.Visible = value;
      }
    }
    // Modifica o texto do label do controle
    public string ControleLabelText {
      get {
        return ControleLabel.Text;
      }
      set {
        ControleLabel.Text = value;
      }
    }

    // Verifica se o icone Obrigatorio deve ou nao ser exibido
    private void AjustarIconeObrigatorio() {
      ObrigatorioIcon.Visible = false;
      if (this.ExibirLabel == true) {
        if (this.Obrigatorio == true) {
          ObrigatorioIcon.Visible = true;
        }
      }
    }
    #endregion

    public bool Enabled {
      get {
        return this.DataTxt.Enabled;
      }
      set {
        this.DataTxt.Enabled = this.CalendarExtender2.Enabled = value;
      }
    }

    public bool EnableDataHora {
      get;
      set;
    }

    public string Text {
      get { return this.DataTxt.Text; }
      set { this.DataTxt.Text = value; }
    }

    public DateTime? Data {
      get {
        DateTime data;
        if (EnableDataHora) {
          if (DateTime.TryParseExact(DataTxt.Text, "dd/MM/yyyy HH:mm", new CultureInfo("pt-BR", false), DateTimeStyles.None, out data))
            return data;
        } else {
          if (DateTime.TryParseExact(DataTxt.Text, "dd/MM/yyyy", new CultureInfo("pt-BR", false), DateTimeStyles.None, out data))
            return data;
        }
        return null;
      }
      set {
        if (value != null) {
          if (EnableDataHora)
            DataTxt.Text = value.Value.ToString("dd/MM/yyyy HH:mm");
          else
            DataTxt.Text = value.Value.ToString("dd/MM/yyyy");
          //GeraScriptCorrigirData(DataTxt.Text);
        }
      }
    }

    public bool Obrigatorio {
      get { return this.DataRequiredFieldValidator.Enabled; }
      set {
        this.DataRequiredFieldValidator.Visible = this.DataRequiredFieldValidator.Enabled = value;
      }
    }
    public string ErrorMessageObrigatorio {
      get { return this.DataRequiredFieldValidator.ErrorMessage; }
      set { this.DataRequiredFieldValidator.ErrorMessage = value; }
    }
    public string OnFocus {
      set { this.DataTxt.Attributes.Add("onFocus", value); }
    }
    public string OnBlur {
      set { this.DataTxt.Attributes.Add("onBlur", value); }
    }

    public string OnBlurTextBox { get; set; }

    public void Validate() {
      DataRequiredFieldValidator.Validate();
    }

    public string UpdatePanelId {
      get {
        object obj = ViewState["UpdatePanelId"];
        if (obj != null) {
          return (string)obj;
        }
        return string.Empty;
      }
      set {
        ViewState["UpdatePanelId"] = value;
      }
    }

    private bool UpdatePanelCompatibility {
      get {
        return !string.IsNullOrEmpty(UpdatePanelId);
      }
    }

    public string ClientIdDataTextBox {
      get {
        return DataTxt.ClientID;
      }
    }

    #endregion

    #region Eventos
    protected void Page_Load(object sender, EventArgs e) {
      //DataTxt.ID = this.ID + "Txt";
      //mensagemLabel.ID = this.ID + "Label";
      DataRequiredFieldValidator.ControlToValidate = DataTxt.ID;
      DataRequiredFieldValidator.ErrorMessage = "Campo '" + ControleLabel.Text + "' é obrigatório.";
      CalendarExtender2.TargetControlID = DataTxt.ID;
      MaskedEditExtender1.TargetControlID = DataTxt.ID;
      DataTxt.MaxLength = 10;
      if (EnableDataHora) {
        MaskedEditExtender1.Mask = "99/99/9999 99:99";
        MaskedEditExtender1.MaskType = MaskedEditType.DateTime;
        DataTxt.MaxLength = 16;
        CalendarExtender2.Format = "dd/MM/yyyy HH:mm";
      }
    }

    protected override void OnPreRender(EventArgs e) {
      base.OnPreRender(e);
      AjustarIconeObrigatorio();
      string msgLabelClientID = mensagemLabel.ClientID;
      StringBuilder funcaoValidarEnterKey = new StringBuilder();
      StringBuilder funcaoValidarData = new StringBuilder();
      funcaoValidarData.Append("javascript:validarData('" + DataTxt.ClientID + "', '" + msgLabelClientID + "');");
      funcaoValidarEnterKey.Append("javascript:validarEnterKey('" + DataTxt.ClientID + "', '" + msgLabelClientID + "',event);return false;");
      DataTxt.Attributes.Add("onchange", funcaoValidarData.ToString() + "return false;");
      DataTxt.Attributes.Add("onblur", funcaoValidarData.ToString() + OnBlurTextBox + "return false;");
      DataTxt.Attributes.Add("onKeyPress", funcaoValidarEnterKey.ToString());

      StringBuilder funcaoOnFocus = new StringBuilder();
      funcaoOnFocus.Append("javascript:onFocus('" + msgLabelClientID + "'); return false;");
      DataTxt.Attributes.Add("onfocus", funcaoOnFocus.ToString());

      Control panel = this.Page;
      if (UpdatePanelCompatibility)
        panel = FindUpdatePanel(UpdatePanelId);
      ScriptManager.RegisterClientScriptBlock(panel, panel.GetType(), "ScriptsControle" + this.ID, scriptsControle, true);
    }

    protected override void Render(HtmlTextWriter writer) {
      base.Render(writer);
      //código necessário para corrigir o "bug" onde o CalendarExtender zera a hora depois de um postback
      string data = this.Request.Form.Get(DataTxt.UniqueID);
      if (string.IsNullOrEmpty(data))
        data = DataTxt.Text;
      GeraScriptCorrigirData(data);
    }

    private void GeraScriptCorrigirData(string data) {
      if (!string.IsNullOrEmpty(data)) {
        StringBuilder sb = new StringBuilder();
        sb.Append("corrigeData('" + data + "','" + DataTxt.ClientID + "');");
        if (UpdatePanelCompatibility) {
          Control updatePanel = FindUpdatePanel(UpdatePanelId);
          ScriptManager.RegisterClientScriptBlock(updatePanel, updatePanel.GetType(), "ScriptArrumarDataCalendario" + this.ID, sb.ToString(), true);
        }
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ScriptArrumarDataCalendarioPagina" + this.ID, sb.ToString(), true);
      }
    }

    private Control FindUpdatePanel(string id) {
      Control container = this;
      while (container != null) {
        if (container.ID == id)
          return container;
        container = container.Parent;
      }
      throw new Exception(string.Format("Não encontrado o UpdatePanel: {0}.", id));
    }

    private string scriptsControle = @"
     function corrigeData(data, idTextBox) {
        var textBox = document.getElementById(idTextBox);
        if (textBox != null) {
          if (textBox.value.match('00:00') == null) {
            setTimeout(""corrigeData('"" + data + ""','"" + idTextBox + ""')"", 200);
          }
          else
            textBox.value = data;
        }
      }

      function onFocus(msgLabelStr) {
        var msgLabel = document.getElementById(msgLabelStr);
        msgLabel.value = '';
        msgLabel.style.display = 'none';
      }

      function validarData(dataTxtStr, msgLabelStr) {
        var dataTxt = document.getElementById(dataTxtStr);
        var msgLabel = document.getElementById(msgLabelStr);
        var str = dataTxt.value;

        if (str != '') {
          var dataArray = str.split(' ');
          var dataSemHora = dataArray[0];
          var dataSeparada = dataSemHora.split('/');
          var diaStr = dataSeparada[0];
          var mesStr = dataSeparada[1];
          var anoStr = dataSeparada[2];

          if (dataArray[1] != null) {
            var tempo = dataArray[1].split(':');
            var hora = tempo[0];
            var min = tempo[1];
            if (hora > 23 || min > 59) {
              //se a hora foi informada verifica max hora e minutos
              dataTxt.value = '';
              msgLabel.style.display = '';
            }
          }
          if (anoStr < 1900 || anoStr > 9999) {
            dataTxt.value = '';
            msgLabel.style.display = '';
          } else {
            //Verificando o intervalo permitido para os valores dos meses e dias
            if (diaStr < 1 || diaStr > 31 || mesStr < 1 || mesStr > 12) {
              dataTxt.value = '';
              msgLabel.style.display = '';
            } else {
              // caso seja mês 2 verifica se o ano é bissexto
              if (mesStr == 2) {
                //se for bissexto
                if (anoStr % 4 == 0) {
                  // Se for bissexto pode o dia ser no máximo 29
                  if (diaStr > 29) {
                    dataTxt.value = '';
                    msgLabel.style.display = '';
                  }
                  // se não for bisexto o dia pode ser no máximo 28
                } else if (diaStr > 28) {
                  dataTxt.value = '';
                  msgLabel.style.display = '';
                }
                //caso seja mes 4, 6, 9 ou 11 o dia pode ser no máximo 30
              } else if ((mesStr == 4 || mesStr == 6 || mesStr == 9 || mesStr == 11) && diaStr > 30) {
                dataTxt.value = '';
                msgLabel.style.display = '';
              }
            }
          }
        }
        else { //caso str == "")
          dataTxt.value = '';
        }
      }

      function validarEnterKey(dataTxtStr, msgLabelStr, event) {
        var tecla = (event.keyCode) ? event.keyCode : event.which;
        if (tecla == 13) // ENTER
           validarData(dataTxtStr, msgLabelStr);
        if (tecla == 9) //TAB
           $this.next().focus(); //Avança o focus() para o prox obj
      }
";

    #endregion

    #region métodos

    public override void Focus() {
      DataTxt.Focus();
    }

    #endregion
  }
}