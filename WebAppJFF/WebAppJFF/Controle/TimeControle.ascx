﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimeControle.ascx.cs" Inherits="WebAppJFF.Controle.TimeControle" %>
<div class="divControle">
    <span class="spanTime">
        <asp:Label runat="server" ID="lblNomeTime" CssClass="labelTime"></asp:Label>
        <br />
        <asp:Label runat="server" ID="Label1" CssClass="labelCapitao" Text="Capitão: "></asp:Label>
        <asp:Label runat="server" ID="lblNomeCapitao" CssClass="labelCapitao"></asp:Label>
    </span>
        <a runat="server" id="linkCapitao" class="linkCapitao" target="_blank">
            <asp:Image runat="server" ID="imgLogoSteam" ImageUrl="~/img/logoSteam.png" CssClass="logoSteamCapitao"/>
        </a>
</div>
