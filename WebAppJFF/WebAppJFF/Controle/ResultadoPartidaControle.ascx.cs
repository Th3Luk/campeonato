﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppJFF.Controle {
  public partial class ResultadoPartidaControle : System.Web.UI.UserControl {

    public string IDPartida {
      get {
        return (string)ViewState["IDPartida"];
      }
      set {
        ViewState["IDPartida"] = value;
        lblIdPartida.Text = value;
      }
    }

    public string EquipeVencedora {
      get {
        return (string)ViewState["EquipeVencedora"];
      }
      set {
        ViewState["EquipeVencedora"] = value;
        lblEquipeVencedora.Text = value;
      }
    }

    public string HorarioPartida {
      get {
        return (string)ViewState["HorarioPartida"];
      }
      set {
        ViewState["HorarioPartida"] = value;
        lblHorarioPartida.Text = value;
      }
    }

    public string NomeTime1 {
      get {
        return (string)ViewState["NomeTime1"];
      }
      set {
        ViewState["NomeTime1"] = value;
        lblTime1.Text = value;
      }
    }

    public string UrlLogoTime1 {
      get {
        return (string)ViewState["UrlLogoTime1"];
      }
      set {
        ViewState["UrlLogoTime1"] = value;
        imgLogoTime1.ImageUrl = value;
      }
    }

    public string UrlRADHeroiJogador1 {
      get {
        return (string)ViewState["UrlRADHeroiJogador1"];
      }
      set {
        ViewState["UrlRADHeroiJogador1"] = value;
        imgRADHeroiJogador1.ImageUrl = value;
      }
    }

    public string RADNomeJogador1 {
      get {
        return (string)ViewState["RADNomeJogador1"];
      }
      set {
        ViewState["RADNomeJogador1"] = value;
        lblRADNomeJogador1.Text = value;
      }
    }

    public string UrlRADFotoJogador1 {
      get {
        return (string)ViewState["UrlRADFotoJogador1"];
      }
      set {
        ViewState["UrlRADFotoJogador1"] = value;
        imgRADFotoJogador1.ImageUrl = value;
      }
    }

    public string RADNivelJogador1 {
      get {
        return (string)ViewState["RADNivelJogador1"];
      }
      set {
        ViewState["RADNivelJogador1"] = value;
        lblRADNivelJogador1.Text = value;
      }
    }

    public string RADKillsJogador1 {
      get {
        return (string)ViewState["RADKillsJogador1"];
      }
      set {
        ViewState["RADKillsJogador1"] = value;
        lblRADKillsJogador1.Text = value;
      }
    }

    public string RADDeathsJogador1 {
      get {
        return (string)ViewState["RADDeathsJogador1"];
      }
      set {
        ViewState["RADDeathsJogador1"] = value;
        lblRADDeathsJogador1.Text = value;
      }
    }

    public string RADAssistenciaJogador1 {
      get {
        return (string)ViewState["RADAssistenciaJogador1"];
      }
      set {
        ViewState["RADAssistenciaJogador1"] = value;
        lblRADAssistenciaJogador1.Text = value;
      }
    }

    public string RADLastHitJogador1 {
      get {
        return (string)ViewState["RADLastHitJogador1"];
      }
      set {
        ViewState["RADLastHitJogador1"] = value;
        lblRADLastHitJogador1.Text = value;
      }
    }

    public string RADDenyJogador1 {
      get {
        return (string)ViewState["RADDenyJogador1"];
      }
      set {
        ViewState["RADDenyJogador1"] = value;
        lblRADDenyJogador1.Text = value;
      }
    }

    public string RADOuroPorMinutoJogador1 {
      get {
        return (string)ViewState["RADOuroPorMinutoJogador1"];
      }
      set {
        ViewState["RADOuroPorMinutoJogador1"] = value;
        lblRADOuroPorMinutoJogador1.Text = value;
      }
    }

    public string RADOuroTotalJogador1 {
      get {
        return (string)ViewState["RADOuroTotalJogador1"];
      }
      set {
        ViewState["RADOuroTotalJogador1"] = value;
        lblRADOuroTotalJogador1.Text = value;
      }
    }

    public string RADXPPorMinutoJogador1 {
      get {
        return (string)ViewState["RADXPPorMinutoJogador1"];
      }
      set {
        ViewState["RADXPPorMinutoJogador1"] = value;
        lblRADXPPorMinutoJogador1.Text = value;
      }
    }

    public string UrlRADItem1Jogador1 {
      get {
        return (string)ViewState["UrlRADItem1Jogador1"];
      }
      set {
        ViewState["UrlRADItem1Jogador1"] = value;
        imgRADItem1Jogador1.ImageUrl = value;
      }
    }

    public string UrlRADItem2Jogador1 {
      get {
        return (string)ViewState["UrlRADItem2Jogador1"];
      }
      set {
        ViewState["UrlRADItem1Jogador2"] = value;
        imgRADItem2Jogador1.ImageUrl = value;
      }
    }

    public string UrlRADItem3Jogador1 {
      get {
        return (string)ViewState["UrlRADItem3Jogador1"];
      }
      set {
        ViewState["UrlRADItem3Jogador1"] = value;
        imgRADItem3Jogador1.ImageUrl = value;
      }
    }

    public string UrlRADItem4Jogador1 {
      get {
        return (string)ViewState["UrlRADItem4Jogador1"];
      }
      set {
        ViewState["UrlRADItem4Jogador1"] = value;
        imgRADItem4Jogador1.ImageUrl = value;
      }
    }

    public string UrlRADItem5Jogador1 {
      get {
        return (string)ViewState["UrlRADItem5Jogador1"];
      }
      set {
        ViewState["UrlRADItem5Jogador1"] = value;
        imgRADItem5Jogador1.ImageUrl = value;
      }
    }

    public string UrlRADItem6Jogador1 {
      get {
        return (string)ViewState["UrlRADItem6Jogador1"];
      }
      set {
        ViewState["UrlRADItem6Jogador1"] = value;
        imgRADItem6Jogador1.ImageUrl = value;
      }
    }

    public string UrlRADHeroiJogador2 {
      get {
        return (string)ViewState["UrlRADHeroiJogador2"];
      }
      set {
        ViewState["UrlRADHeroiJogador2"] = value;
        imgRADHeroiJogador2.ImageUrl = value;
      }
    }

    public string RADNomeJogador2 {
      get {
        return (string)ViewState["RADNomeJogador2"];
      }
      set {
        ViewState["RADNomeJogador2"] = value;
        lblRADNomeJogador2.Text = value;
      }
    }

    public string UrlRADFotoJogador2 {
      get {
        return (string)ViewState["UrlRADFotoJogador2"];
      }
      set {
        ViewState["UrlRADFotoJogador2"] = value;
        imgRADFotoJogador2.ImageUrl = value;
      }
    }

    public string RADNivelJogador2 {
      get {
        return (string)ViewState["RADNivelJogador2"];
      }
      set {
        ViewState["RADNivelJogador2"] = value;
        lblRADNivelJogador2.Text = value;
      }
    }

    public string RADKillsJogador2 {
      get {
        return (string)ViewState["RADKillsJogador2"];
      }
      set {
        ViewState["RADKillsJogador2"] = value;
        lblRADKillsJogador2.Text = value;
      }
    }

    public string RADDeathsJogador2 {
      get {
        return (string)ViewState["RADDeathsJogador2"];
      }
      set {
        ViewState["RADDeathsJogador2"] = value;
        lblRADDeathsJogador2.Text = value;
      }
    }

    public string RADAssistenciaJogador2 {
      get {
        return (string)ViewState["RADAssistenciaJogador2"];
      }
      set {
        ViewState["RADAssistenciaJogador2"] = value;
        lblRADAssistenciaJogador2.Text = value;
      }
    }

    public string RADLastHitJogador2 {
      get {
        return (string)ViewState["RADLastHitJogador2"];
      }
      set {
        ViewState["RADLastHitJogador2"] = value;
        lblRADLastHitJogador2.Text = value;
      }
    }

    public string RADDenyJogador2 {
      get {
        return (string)ViewState["RADDenyJogador2"];
      }
      set {
        ViewState["RADDenyJogador2"] = value;
        lblRADDenyJogador2.Text = value;
      }
    }

    public string RADOuroPorMinutoJogador2 {
      get {
        return (string)ViewState["RADOuroPorMinutoJogador2"];
      }
      set {
        ViewState["RADOuroPorMinutoJogador2"] = value;
        lblRADOuroPorMinutoJogador2.Text = value;
      }
    }

    public string RADOuroTotalJogador2 {
      get {
        return (string)ViewState["RADOuroTotalJogador2"];
      }
      set {
        ViewState["RADOuroTotalJogador2"] = value;
        lblRADOuroTotalJogador2.Text = value;
      }
    }

    public string RADXPPorMinutoJogador2 {
      get {
        return (string)ViewState["RADXPPorMinutoJogador2"];
      }
      set {
        ViewState["RADXPPorMinutoJogador2"] = value;
        lblRADXPPorMinutoJogador2.Text = value;
      }
    }

    public string UrlRADItem1Jogador2 {
      get {
        return (string)ViewState["UrlRADItem1Jogador2"];
      }
      set {
        ViewState["UrlRADItem1Jogador2"] = value;
        imgRADItem1Jogador2.ImageUrl = value;
      }
    }

    public string UrlRADItem2Jogador2 {
      get {
        return (string)ViewState["UrlRADItem2Jogador2"];
      }
      set {
        ViewState["UrlRADItem1Jogador2"] = value;
        imgRADItem2Jogador2.ImageUrl = value;
      }
    }

    public string UrlRADItem3Jogador2 {
      get {
        return (string)ViewState["UrlRADItem3Jogador2"];
      }
      set {
        ViewState["UrlRADItem3Jogador2"] = value;
        imgRADItem3Jogador2.ImageUrl = value;
      }
    }

    public string UrlRADItem4Jogador2 {
      get {
        return (string)ViewState["UrlRADItem4Jogador2"];
      }
      set {
        ViewState["UrlRADItem4Jogador2"] = value;
        imgRADItem4Jogador2.ImageUrl = value;
      }
    }

    public string UrlRADItem5Jogador2 {
      get {
        return (string)ViewState["UrlRADItem5Jogador2"];
      }
      set {
        ViewState["UrlRADItem5Jogador2"] = value;
        imgRADItem5Jogador2.ImageUrl = value;
      }
    }

    public string UrlRADItem6Jogador2 {
      get {
        return (string)ViewState["UrlRADItem6Jogador2"];
      }
      set {
        ViewState["UrlRADItem6Jogador2"] = value;
        imgRADItem6Jogador2.ImageUrl = value;
      }
    }

    public string UrlRADHeroiJogador3 {
      get {
        return (string)ViewState["UrlRADHeroiJogador3"];
      }
      set {
        ViewState["UrlRADHeroiJogador3"] = value;
        imgRADHeroiJogador3.ImageUrl = value;
      }
    }

    public string RADNomeJogador3 {
      get {
        return (string)ViewState["RADNomeJogador3"];
      }
      set {
        ViewState["RADNomeJogador3"] = value;
        lblRADNomeJogador3.Text = value;
      }
    }

    public string UrlRADFotoJogador3 {
      get {
        return (string)ViewState["UrlRADFotoJogador3"];
      }
      set {
        ViewState["UrlRADFotoJogador3"] = value;
        imgRADFotoJogador3.ImageUrl = value;
      }
    }

    public string RADNivelJogador3 {
      get {
        return (string)ViewState["RADNivelJogador3"];
      }
      set {
        ViewState["RADNivelJogador3"] = value;
        lblRADNivelJogador3.Text = value;
      }
    }

    public string RADKillsJogador3 {
      get {
        return (string)ViewState["RADKillsJogador3"];
      }
      set {
        ViewState["RADKillsJogador3"] = value;
        lblRADKillsJogador3.Text = value;
      }
    }

    public string RADDeathsJogador3 {
      get {
        return (string)ViewState["RADDeathsJogador3"];
      }
      set {
        ViewState["RADDeathsJogador3"] = value;
        lblRADDeathsJogador3.Text = value;
      }
    }

    public string RADAssistenciaJogador3 {
      get {
        return (string)ViewState["RADAssistenciaJogador3"];
      }
      set {
        ViewState["RADAssistenciaJogador3"] = value;
        lblRADAssistenciaJogador3.Text = value;
      }
    }

    public string RADLastHitJogador3 {
      get {
        return (string)ViewState["RADLastHitJogador3"];
      }
      set {
        ViewState["RADLastHitJogador3"] = value;
        lblRADLastHitJogador3.Text = value;
      }
    }

    public string RADDenyJogador3 {
      get {
        return (string)ViewState["RADDenyJogador3"];
      }
      set {
        ViewState["RADDenyJogador3"] = value;
        lblRADDenyJogador3.Text = value;
      }
    }

    public string RADOuroPorMinutoJogador3 {
      get {
        return (string)ViewState["RADOuroPorMinutoJogador3"];
      }
      set {
        ViewState["RADOuroPorMinutoJogador3"] = value;
        lblRADOuroPorMinutoJogador3.Text = value;
      }
    }

    public string RADOuroTotalJogador3 {
      get {
        return (string)ViewState["RADOuroTotalJogador3"];
      }
      set {
        ViewState["RADOuroTotalJogador3"] = value;
        lblRADOuroTotalJogador3.Text = value;
      }
    }

    public string RADXPPorMinutoJogador3 {
      get {
        return (string)ViewState["RADXPPorMinutoJogador3"];
      }
      set {
        ViewState["RADXPPorMinutoJogador3"] = value;
        lblRADXPPorMinutoJogador3.Text = value;
      }
    }

    public string UrlRADItem1Jogador3 {
      get {
        return (string)ViewState["UrlRADItem1Jogador3"];
      }
      set {
        ViewState["UrlRADItem1Jogador3"] = value;
        imgRADItem1Jogador3.ImageUrl = value;
      }
    }

    public string UrlRADItem2Jogador3 {
      get {
        return (string)ViewState["UrlRADItem2Jogador3"];
      }
      set {
        ViewState["UrlRADItem1Jogador2"] = value;
        imgRADItem2Jogador3.ImageUrl = value;
      }
    }

    public string UrlRADItem3Jogador3 {
      get {
        return (string)ViewState["UrlRADItem3Jogador3"];
      }
      set {
        ViewState["UrlRADItem3Jogador3"] = value;
        imgRADItem3Jogador3.ImageUrl = value;
      }
    }

    public string UrlRADItem4Jogador3 {
      get {
        return (string)ViewState["UrlRADItem4Jogador3"];
      }
      set {
        ViewState["UrlRADItem4Jogador3"] = value;
        imgRADItem4Jogador3.ImageUrl = value;
      }
    }

    public string UrlRADItem5Jogador3 {
      get {
        return (string)ViewState["UrlRADItem5Jogador3"];
      }
      set {
        ViewState["UrlRADItem5Jogador3"] = value;
        imgRADItem5Jogador3.ImageUrl = value;
      }
    }

    public string UrlRADItem6Jogador3 {
      get {
        return (string)ViewState["UrlRADItem6Jogador3"];
      }
      set {
        ViewState["UrlRADItem6Jogador3"] = value;
        imgRADItem6Jogador3.ImageUrl = value;
      }
    }

    public string UrlRADHeroiJogador4 {
      get {
        return (string)ViewState["UrlRADHeroiJogador4"];
      }
      set {
        ViewState["UrlRADHeroiJogador4"] = value;
        imgRADHeroiJogador4.ImageUrl = value;
      }
    }

    public string RADNomeJogador4 {
      get {
        return (string)ViewState["RADNomeJogador4"];
      }
      set {
        ViewState["RADNomeJogador4"] = value;
        lblRADNomeJogador4.Text = value;
      }
    }

    public string UrlRADFotoJogador4 {
      get {
        return (string)ViewState["UrlRADFotoJogador4"];
      }
      set {
        ViewState["UrlRADFotoJogador4"] = value;
        imgRADFotoJogador4.ImageUrl = value;
      }
    }

    public string RADNivelJogador4 {
      get {
        return (string)ViewState["RADNivelJogador4"];
      }
      set {
        ViewState["RADNivelJogador4"] = value;
        lblRADNivelJogador4.Text = value;
      }
    }

    public string RADKillsJogador4 {
      get {
        return (string)ViewState["RADKillsJogador4"];
      }
      set {
        ViewState["RADKillsJogador4"] = value;
        lblRADKillsJogador4.Text = value;
      }
    }

    public string RADDeathsJogador4 {
      get {
        return (string)ViewState["RADDeathsJogador4"];
      }
      set {
        ViewState["RADDeathsJogador4"] = value;
        lblRADDeathsJogador4.Text = value;
      }
    }

    public string RADAssistenciaJogador4 {
      get {
        return (string)ViewState["RADAssistenciaJogador4"];
      }
      set {
        ViewState["RADAssistenciaJogador4"] = value;
        lblRADAssistenciaJogador4.Text = value;
      }
    }

    public string RADLastHitJogador4 {
      get {
        return (string)ViewState["RADLastHitJogador4"];
      }
      set {
        ViewState["RADLastHitJogador4"] = value;
        lblRADLastHitJogador4.Text = value;
      }
    }

    public string RADDenyJogador4 {
      get {
        return (string)ViewState["RADDenyJogador4"];
      }
      set {
        ViewState["RADDenyJogador4"] = value;
        lblRADDenyJogador4.Text = value;
      }
    }

    public string RADOuroPorMinutoJogador4 {
      get {
        return (string)ViewState["RADOuroPorMinutoJogador4"];
      }
      set {
        ViewState["RADOuroPorMinutoJogador4"] = value;
        lblRADOuroPorMinutoJogador4.Text = value;
      }
    }

    public string RADOuroTotalJogador4 {
      get {
        return (string)ViewState["RADOuroTotalJogador4"];
      }
      set {
        ViewState["RADOuroTotalJogador4"] = value;
        lblRADOuroTotalJogador4.Text = value;
      }
    }

    public string RADXPPorMinutoJogador4 {
      get {
        return (string)ViewState["RADXPPorMinutoJogador4"];
      }
      set {
        ViewState["RADXPPorMinutoJogador4"] = value;
        lblRADXPPorMinutoJogador4.Text = value;
      }
    }

    public string UrlRADItem1Jogador4 {
      get {
        return (string)ViewState["UrlRADItem1Jogador4"];
      }
      set {
        ViewState["UrlRADItem1Jogador4"] = value;
        imgRADItem1Jogador4.ImageUrl = value;
      }
    }

    public string UrlRADItem2Jogador4 {
      get {
        return (string)ViewState["UrlRADItem2Jogador4"];
      }
      set {
        ViewState["UrlRADItem1Jogador2"] = value;
        imgRADItem2Jogador4.ImageUrl = value;
      }
    }

    public string UrlRADItem3Jogador4 {
      get {
        return (string)ViewState["UrlRADItem3Jogador4"];
      }
      set {
        ViewState["UrlRADItem3Jogador4"] = value;
        imgRADItem3Jogador4.ImageUrl = value;
      }
    }

    public string UrlRADItem4Jogador4 {
      get {
        return (string)ViewState["UrlRADItem4Jogador4"];
      }
      set {
        ViewState["UrlRADItem4Jogador4"] = value;
        imgRADItem4Jogador4.ImageUrl = value;
      }
    }

    public string UrlRADItem5Jogador4 {
      get {
        return (string)ViewState["UrlRADItem5Jogador4"];
      }
      set {
        ViewState["UrlRADItem5Jogador4"] = value;
        imgRADItem5Jogador4.ImageUrl = value;
      }
    }

    public string UrlRADItem6Jogador4 {
      get {
        return (string)ViewState["UrlRADItem6Jogador4"];
      }
      set {
        ViewState["UrlRADItem6Jogador4"] = value;
        imgRADItem6Jogador4.ImageUrl = value;
      }
    }

    public string UrlRADHeroiJogador5 {
      get {
        return (string)ViewState["UrlRADHeroiJogador5"];
      }
      set {
        ViewState["UrlRADHeroiJogador5"] = value;
        imgRADHeroiJogador5.ImageUrl = value;
      }
    }

    public string RADNomeJogador5 {
      get {
        return (string)ViewState["RADNomeJogador5"];
      }
      set {
        ViewState["RADNomeJogador5"] = value;
        lblRADNomeJogador5.Text = value;
      }
    }

    public string UrlRADFotoJogador5 {
      get {
        return (string)ViewState["UrlRADFotoJogador5"];
      }
      set {
        ViewState["UrlRADFotoJogador5"] = value;
        imgRADFotoJogador5.ImageUrl = value;
      }
    }

    public string RADNivelJogador5 {
      get {
        return (string)ViewState["RADNivelJogador5"];
      }
      set {
        ViewState["RADNivelJogador5"] = value;
        lblRADNivelJogador5.Text = value;
      }
    }

    public string RADKillsJogador5 {
      get {
        return (string)ViewState["RADKillsJogador5"];
      }
      set {
        ViewState["RADKillsJogador5"] = value;
        lblRADKillsJogador5.Text = value;
      }
    }

    public string RADDeathsJogador5 {
      get {
        return (string)ViewState["RADDeathsJogador5"];
      }
      set {
        ViewState["RADDeathsJogador5"] = value;
        lblRADDeathsJogador5.Text = value;
      }
    }

    public string RADAssistenciaJogador5 {
      get {
        return (string)ViewState["RADAssistenciaJogador5"];
      }
      set {
        ViewState["RADAssistenciaJogador5"] = value;
        lblRADAssistenciaJogador5.Text = value;
      }
    }

    public string RADLastHitJogador5 {
      get {
        return (string)ViewState["RADLastHitJogador5"];
      }
      set {
        ViewState["RADLastHitJogador5"] = value;
        lblRADLastHitJogador5.Text = value;
      }
    }

    public string RADDenyJogador5 {
      get {
        return (string)ViewState["RADDenyJogador5"];
      }
      set {
        ViewState["RADDenyJogador5"] = value;
        lblRADDenyJogador5.Text = value;
      }
    }

    public string RADOuroPorMinutoJogador5 {
      get {
        return (string)ViewState["RADOuroPorMinutoJogador5"];
      }
      set {
        ViewState["RADOuroPorMinutoJogador5"] = value;
        lblRADOuroPorMinutoJogador5.Text = value;
      }
    }

    public string RADOuroTotalJogador5 {
      get {
        return (string)ViewState["RADOuroTotalJogador5"];
      }
      set {
        ViewState["RADOuroTotalJogador5"] = value;
        lblRADOuroTotalJogador5.Text = value;
      }
    }

    public string RADXPPorMinutoJogador5 {
      get {
        return (string)ViewState["RADXPPorMinutoJogador5"];
      }
      set {
        ViewState["RADXPPorMinutoJogador5"] = value;
        lblRADXPPorMinutoJogador5.Text = value;
      }
    }

    public string UrlRADItem1Jogador5 {
      get {
        return (string)ViewState["UrlRADItem1Jogador5"];
      }
      set {
        ViewState["UrlRADItem1Jogador5"] = value;
        imgRADItem1Jogador5.ImageUrl = value;
      }
    }

    public string UrlRADItem2Jogador5 {
      get {
        return (string)ViewState["UrlRADItem2Jogador5"];
      }
      set {
        ViewState["UrlRADItem1Jogador2"] = value;
        imgRADItem2Jogador5.ImageUrl = value;
      }
    }

    public string UrlRADItem3Jogador5 {
      get {
        return (string)ViewState["UrlRADItem3Jogador5"];
      }
      set {
        ViewState["UrlRADItem3Jogador5"] = value;
        imgRADItem3Jogador5.ImageUrl = value;
      }
    }

    public string UrlRADItem4Jogador5 {
      get {
        return (string)ViewState["UrlRADItem4Jogador5"];
      }
      set {
        ViewState["UrlRADItem4Jogador5"] = value;
        imgRADItem4Jogador5.ImageUrl = value;
      }
    }

    public string UrlRADItem5Jogador5 {
      get {
        return (string)ViewState["UrlRADItem5Jogador5"];
      }
      set {
        ViewState["UrlRADItem5Jogador5"] = value;
        imgRADItem5Jogador5.ImageUrl = value;
      }
    }

    public string UrlRADItem6Jogador5 {
      get {
        return (string)ViewState["UrlRADItem6Jogador5"];
      }
      set {
        ViewState["UrlRADItem6Jogador5"] = value;
        imgRADItem6Jogador5.ImageUrl = value;
      }
    }

    public string RADNivelTotal {
      get {
        return (string)ViewState["RADNivelTotal"];
      }
      set {
        ViewState["RADNivelTotal"] = value;
        lblRADNivelTotal.Text = value;
      }
    }

    public string RADKillsJogadorTotal {
      get {
        return (string)ViewState["RADKillsJogadorTotal"];
      }
      set {
        ViewState["RADKillsJogadorTotal"] = value;
        lblRADKillsJogadorTotal.Text = value;
      }
    }

    public string RADDeathsJogadorTotal {
      get {
        return (string)ViewState["RADDeathsJogadorTotal"];
      }
      set {
        ViewState["RADDeathsJogadorTotal"] = value;
        lblRADDeathsJogadorTotal.Text = value;
      }
    }

    public string RADAssistenciaJogadorTotal {
      get {
        return (string)ViewState["RADAssistenciaJogadorTotal"];
      }
      set {
        ViewState["RADAssistenciaJogadorTotal"] = value;
        lblRADAssistenciaJogadorTotal.Text = value;
      }
    }

    public string RADLastHitJogadorTotal {
      get {
        return (string)ViewState["RADLastHitJogadorTotal"];
      }
      set {
        ViewState["RADLastHitJogadorTotal"] = value;
        lblRADLastHitJogadorTotal.Text = value;
      }
    }

    public string RADDenyJogadorTotal {
      get {
        return (string)ViewState["RADDenyJogadorTotal"];
      }
      set {
        ViewState["RADDenyJogadorTotal"] = value;
        lblRADDenyJogadorTotal.Text = value;
      }
    }

    public string RADOuroPorMinutoJogadorTotal {
      get {
        return (string)ViewState["RADOuroPorMinutoJogadorTotal"];
      }
      set {
        ViewState["RADOuroPorMinutoJogadorTotal"] = value;
        lblRADOuroPorMinutoJogadorTotal.Text = value;
      }
    }

    public string RADOuroTotalJogadorTotal {
      get {
        return (string)ViewState["RADOuroTotalJogadorTotal"];
      }
      set {
        ViewState["RADOuroTotalJogadorTotal"] = value;
        lblRADOuroTotalJogadorTotal.Text = value;
      }
    }

    public string RADXPPorMinutoJogadorTotal {
      get {
        return (string)ViewState["RADXPPorMinutoJogadorTotal"];
      }
      set {
        ViewState["RADXPPorMinutoJogadorTotal"] = value;
        lblRADXPPorMinutoJogadorTotal.Text = value;
      }
    }

    public string NomeTime2 {
      get {
        return (string)ViewState["NomeTime2"];
      }
      set {
        ViewState["NomeTime2"] = value;
        lblTime2.Text = value;
      }
    }

    public string UrlLogoTime2 {
      get {
        return (string)ViewState["UrlLogoTime2"];
      }
      set {
        ViewState["UrlLogoTime2"] = value;
        imgLogoTime2.ImageUrl = value;
      }
    }

    public string UrlTEMHeroiJogador1 {
      get {
        return (string)ViewState["UrlTEMHeroiJogador1"];
      }
      set {
        ViewState["UrlTEMHeroiJogador1"] = value;
        imgTEMHeroiJogador1.ImageUrl = value;
      }
    }

    public string TEMNomeJogador1 {
      get {
        return (string)ViewState["TEMNomeJogador1"];
      }
      set {
        ViewState["TEMNomeJogador1"] = value;
        lblTEMNomeJogador1.Text = value;
      }
    }

    public string UrlTEMFotoJogador1 {
      get {
        return (string)ViewState["UrlTEMFotoJogador1"];
      }
      set {
        ViewState["UrlTEMFotoJogador1"] = value;
        imgTEMFotoJogador1.ImageUrl = value;
      }
    }

    public string TEMNivelJogador1 {
      get {
        return (string)ViewState["TEMNivelJogador1"];
      }
      set {
        ViewState["TEMNivelJogador1"] = value;
        lblTEMNivelJogador1.Text = value;
      }
    }

    public string TEMKillsJogador1 {
      get {
        return (string)ViewState["TEMKillsJogador1"];
      }
      set {
        ViewState["TEMKillsJogador1"] = value;
        lblTEMKillsJogador1.Text = value;
      }
    }

    public string TEMDeathsJogador1 {
      get {
        return (string)ViewState["TEMDeathsJogador1"];
      }
      set {
        ViewState["TEMDeathsJogador1"] = value;
        lblTEMDeathsJogador1.Text = value;
      }
    }

    public string TEMAssistenciaJogador1 {
      get {
        return (string)ViewState["TEMAssistenciaJogador1"];
      }
      set {
        ViewState["TEMAssistenciaJogador1"] = value;
        lblTEMAssistenciaJogador1.Text = value;
      }
    }

    public string TEMLastHitJogador1 {
      get {
        return (string)ViewState["TEMLastHitJogador1"];
      }
      set {
        ViewState["TEMLastHitJogador1"] = value;
        lblTEMLastHitJogador1.Text = value;
      }
    }

    public string TEMDenyJogador1 {
      get {
        return (string)ViewState["TEMDenyJogador1"];
      }
      set {
        ViewState["TEMDenyJogador1"] = value;
        lblTEMDenyJogador1.Text = value;
      }
    }

    public string TEMOuroPorMinutoJogador1 {
      get {
        return (string)ViewState["TEMOuroPorMinutoJogador1"];
      }
      set {
        ViewState["TEMOuroPorMinutoJogador1"] = value;
        lblTEMOuroPorMinutoJogador1.Text = value;
      }
    }

    public string TEMOuroTotalJogador1 {
      get {
        return (string)ViewState["TEMOuroTotalJogador1"];
      }
      set {
        ViewState["TEMOuroTotalJogador1"] = value;
        lblTEMOuroTotalJogador1.Text = value;
      }
    }

    public string TEMXPPorMinutoJogador1 {
      get {
        return (string)ViewState["TEMXPPorMinutoJogador1"];
      }
      set {
        ViewState["TEMXPPorMinutoJogador1"] = value;
        lblTEMXPPorMinutoJogador1.Text = value;
      }
    }

    public string UrlTEMItem1Jogador1 {
      get {
        return (string)ViewState["UrlTEMItem1Jogador1"];
      }
      set {
        ViewState["UrlTEMItem1Jogador1"] = value;
        imgTEMItem1Jogador1.ImageUrl = value;
      }
    }

    public string UrlTEMItem2Jogador1 {
      get {
        return (string)ViewState["UrlTEMItem2Jogador1"];
      }
      set {
        ViewState["UrlTEMItem1Jogador2"] = value;
        imgTEMItem2Jogador1.ImageUrl = value;
      }
    }

    public string UrlTEMItem3Jogador1 {
      get {
        return (string)ViewState["UrlTEMItem3Jogador1"];
      }
      set {
        ViewState["UrlTEMItem3Jogador1"] = value;
        imgTEMItem3Jogador1.ImageUrl = value;
      }
    }

    public string UrlTEMItem4Jogador1 {
      get {
        return (string)ViewState["UrlTEMItem4Jogador1"];
      }
      set {
        ViewState["UrlTEMItem4Jogador1"] = value;
        imgTEMItem4Jogador1.ImageUrl = value;
      }
    }

    public string UrlTEMItem5Jogador1 {
      get {
        return (string)ViewState["UrlTEMItem5Jogador1"];
      }
      set {
        ViewState["UrlTEMItem5Jogador1"] = value;
        imgTEMItem5Jogador1.ImageUrl = value;
      }
    }

    public string UrlTEMItem6Jogador1 {
      get {
        return (string)ViewState["UrlTEMItem6Jogador1"];
      }
      set {
        ViewState["UrlTEMItem6Jogador1"] = value;
        imgTEMItem6Jogador1.ImageUrl = value;
      }
    }

    public string UrlTEMHeroiJogador2 {
      get {
        return (string)ViewState["UrlTEMHeroiJogador2"];
      }
      set {
        ViewState["UrlTEMHeroiJogador2"] = value;
        imgTEMHeroiJogador2.ImageUrl = value;
      }
    }

    public string TEMNomeJogador2 {
      get {
        return (string)ViewState["TEMNomeJogador2"];
      }
      set {
        ViewState["TEMNomeJogador2"] = value;
        lblTEMNomeJogador2.Text = value;
      }
    }

    public string UrlTEMFotoJogador2 {
      get {
        return (string)ViewState["UrlTEMFotoJogador2"];
      }
      set {
        ViewState["UrlTEMFotoJogador2"] = value;
        imgTEMFotoJogador2.ImageUrl = value;
      }
    }

    public string TEMNivelJogador2 {
      get {
        return (string)ViewState["TEMNivelJogador2"];
      }
      set {
        ViewState["TEMNivelJogador2"] = value;
        lblTEMNivelJogador2.Text = value;
      }
    }

    public string TEMKillsJogador2 {
      get {
        return (string)ViewState["TEMKillsJogador2"];
      }
      set {
        ViewState["TEMKillsJogador2"] = value;
        lblTEMKillsJogador2.Text = value;
      }
    }

    public string TEMDeathsJogador2 {
      get {
        return (string)ViewState["TEMDeathsJogador2"];
      }
      set {
        ViewState["TEMDeathsJogador2"] = value;
        lblTEMDeathsJogador2.Text = value;
      }
    }

    public string TEMAssistenciaJogador2 {
      get {
        return (string)ViewState["TEMAssistenciaJogador2"];
      }
      set {
        ViewState["TEMAssistenciaJogador2"] = value;
        lblTEMAssistenciaJogador2.Text = value;
      }
    }

    public string TEMLastHitJogador2 {
      get {
        return (string)ViewState["TEMLastHitJogador2"];
      }
      set {
        ViewState["TEMLastHitJogador2"] = value;
        lblTEMLastHitJogador2.Text = value;
      }
    }

    public string TEMDenyJogador2 {
      get {
        return (string)ViewState["TEMDenyJogador2"];
      }
      set {
        ViewState["TEMDenyJogador2"] = value;
        lblTEMDenyJogador2.Text = value;
      }
    }

    public string TEMOuroPorMinutoJogador2 {
      get {
        return (string)ViewState["TEMOuroPorMinutoJogador2"];
      }
      set {
        ViewState["TEMOuroPorMinutoJogador2"] = value;
        lblTEMOuroPorMinutoJogador2.Text = value;
      }
    }

    public string TEMOuroTotalJogador2 {
      get {
        return (string)ViewState["TEMOuroTotalJogador2"];
      }
      set {
        ViewState["TEMOuroTotalJogador2"] = value;
        lblTEMOuroTotalJogador2.Text = value;
      }
    }

    public string TEMXPPorMinutoJogador2 {
      get {
        return (string)ViewState["TEMXPPorMinutoJogador2"];
      }
      set {
        ViewState["TEMXPPorMinutoJogador2"] = value;
        lblTEMXPPorMinutoJogador2.Text = value;
      }
    }

    public string UrlTEMItem1Jogador2 {
      get {
        return (string)ViewState["UrlTEMItem1Jogador2"];
      }
      set {
        ViewState["UrlTEMItem1Jogador2"] = value;
        imgTEMItem1Jogador2.ImageUrl = value;
      }
    }

    public string UrlTEMItem2Jogador2 {
      get {
        return (string)ViewState["UrlTEMItem2Jogador2"];
      }
      set {
        ViewState["UrlTEMItem1Jogador2"] = value;
        imgTEMItem2Jogador2.ImageUrl = value;
      }
    }

    public string UrlTEMItem3Jogador2 {
      get {
        return (string)ViewState["UrlTEMItem3Jogador2"];
      }
      set {
        ViewState["UrlTEMItem3Jogador2"] = value;
        imgTEMItem3Jogador2.ImageUrl = value;
      }
    }

    public string UrlTEMItem4Jogador2 {
      get {
        return (string)ViewState["UrlTEMItem4Jogador2"];
      }
      set {
        ViewState["UrlTEMItem4Jogador2"] = value;
        imgTEMItem4Jogador2.ImageUrl = value;
      }
    }

    public string UrlTEMItem5Jogador2 {
      get {
        return (string)ViewState["UrlTEMItem5Jogador2"];
      }
      set {
        ViewState["UrlTEMItem5Jogador2"] = value;
        imgTEMItem5Jogador2.ImageUrl = value;
      }
    }

    public string UrlTEMItem6Jogador2 {
      get {
        return (string)ViewState["UrlTEMItem6Jogador2"];
      }
      set {
        ViewState["UrlTEMItem6Jogador2"] = value;
        imgTEMItem6Jogador2.ImageUrl = value;
      }
    }

    public string UrlTEMHeroiJogador3 {
      get {
        return (string)ViewState["UrlTEMHeroiJogador3"];
      }
      set {
        ViewState["UrlTEMHeroiJogador3"] = value;
        imgTEMHeroiJogador3.ImageUrl = value;
      }
    }

    public string TEMNomeJogador3 {
      get {
        return (string)ViewState["TEMNomeJogador3"];
      }
      set {
        ViewState["TEMNomeJogador3"] = value;
        lblTEMNomeJogador3.Text = value;
      }
    }

    public string UrlTEMFotoJogador3 {
      get {
        return (string)ViewState["UrlTEMFotoJogador3"];
      }
      set {
        ViewState["UrlTEMFotoJogador3"] = value;
        imgTEMFotoJogador3.ImageUrl = value;
      }
    }

    public string TEMNivelJogador3 {
      get {
        return (string)ViewState["TEMNivelJogador3"];
      }
      set {
        ViewState["TEMNivelJogador3"] = value;
        lblTEMNivelJogador3.Text = value;
      }
    }

    public string TEMKillsJogador3 {
      get {
        return (string)ViewState["TEMKillsJogador3"];
      }
      set {
        ViewState["TEMKillsJogador3"] = value;
        lblTEMKillsJogador3.Text = value;
      }
    }

    public string TEMDeathsJogador3 {
      get {
        return (string)ViewState["TEMDeathsJogador3"];
      }
      set {
        ViewState["TEMDeathsJogador3"] = value;
        lblTEMDeathsJogador3.Text = value;
      }
    }

    public string TEMAssistenciaJogador3 {
      get {
        return (string)ViewState["TEMAssistenciaJogador3"];
      }
      set {
        ViewState["TEMAssistenciaJogador3"] = value;
        lblTEMAssistenciaJogador3.Text = value;
      }
    }

    public string TEMLastHitJogador3 {
      get {
        return (string)ViewState["TEMLastHitJogador3"];
      }
      set {
        ViewState["TEMLastHitJogador3"] = value;
        lblTEMLastHitJogador3.Text = value;
      }
    }

    public string TEMDenyJogador3 {
      get {
        return (string)ViewState["TEMDenyJogador3"];
      }
      set {
        ViewState["TEMDenyJogador3"] = value;
        lblTEMDenyJogador3.Text = value;
      }
    }

    public string TEMOuroPorMinutoJogador3 {
      get {
        return (string)ViewState["TEMOuroPorMinutoJogador3"];
      }
      set {
        ViewState["TEMOuroPorMinutoJogador3"] = value;
        lblTEMOuroPorMinutoJogador3.Text = value;
      }
    }

    public string TEMOuroTotalJogador3 {
      get {
        return (string)ViewState["TEMOuroTotalJogador3"];
      }
      set {
        ViewState["TEMOuroTotalJogador3"] = value;
        lblTEMOuroTotalJogador3.Text = value;
      }
    }

    public string TEMXPPorMinutoJogador3 {
      get {
        return (string)ViewState["TEMXPPorMinutoJogador3"];
      }
      set {
        ViewState["TEMXPPorMinutoJogador3"] = value;
        lblTEMXPPorMinutoJogador3.Text = value;
      }
    }

    public string UrlTEMItem1Jogador3 {
      get {
        return (string)ViewState["UrlTEMItem1Jogador3"];
      }
      set {
        ViewState["UrlTEMItem1Jogador3"] = value;
        imgTEMItem1Jogador3.ImageUrl = value;
      }
    }

    public string UrlTEMItem2Jogador3 {
      get {
        return (string)ViewState["UrlTEMItem2Jogador3"];
      }
      set {
        ViewState["UrlTEMItem1Jogador2"] = value;
        imgTEMItem2Jogador3.ImageUrl = value;
      }
    }

    public string UrlTEMItem3Jogador3 {
      get {
        return (string)ViewState["UrlTEMItem3Jogador3"];
      }
      set {
        ViewState["UrlTEMItem3Jogador3"] = value;
        imgTEMItem3Jogador3.ImageUrl = value;
      }
    }

    public string UrlTEMItem4Jogador3 {
      get {
        return (string)ViewState["UrlTEMItem4Jogador3"];
      }
      set {
        ViewState["UrlTEMItem4Jogador3"] = value;
        imgTEMItem4Jogador3.ImageUrl = value;
      }
    }

    public string UrlTEMItem5Jogador3 {
      get {
        return (string)ViewState["UrlTEMItem5Jogador3"];
      }
      set {
        ViewState["UrlTEMItem5Jogador3"] = value;
        imgTEMItem5Jogador3.ImageUrl = value;
      }
    }

    public string UrlTEMItem6Jogador3 {
      get {
        return (string)ViewState["UrlTEMItem6Jogador3"];
      }
      set {
        ViewState["UrlTEMItem6Jogador3"] = value;
        imgTEMItem6Jogador3.ImageUrl = value;
      }
    }

    public string UrlTEMHeroiJogador4 {
      get {
        return (string)ViewState["UrlTEMHeroiJogador4"];
      }
      set {
        ViewState["UrlTEMHeroiJogador4"] = value;
        imgTEMHeroiJogador4.ImageUrl = value;
      }
    }

    public string TEMNomeJogador4 {
      get {
        return (string)ViewState["TEMNomeJogador4"];
      }
      set {
        ViewState["TEMNomeJogador4"] = value;
        lblTEMNomeJogador4.Text = value;
      }
    }

    public string UrlTEMFotoJogador4 {
      get {
        return (string)ViewState["UrlTEMFotoJogador4"];
      }
      set {
        ViewState["UrlTEMFotoJogador4"] = value;
        imgTEMFotoJogador4.ImageUrl = value;
      }
    }

    public string TEMNivelJogador4 {
      get {
        return (string)ViewState["TEMNivelJogador4"];
      }
      set {
        ViewState["TEMNivelJogador4"] = value;
        lblTEMNivelJogador4.Text = value;
      }
    }

    public string TEMKillsJogador4 {
      get {
        return (string)ViewState["TEMKillsJogador4"];
      }
      set {
        ViewState["TEMKillsJogador4"] = value;
        lblTEMKillsJogador4.Text = value;
      }
    }

    public string TEMDeathsJogador4 {
      get {
        return (string)ViewState["TEMDeathsJogador4"];
      }
      set {
        ViewState["TEMDeathsJogador4"] = value;
        lblTEMDeathsJogador4.Text = value;
      }
    }

    public string TEMAssistenciaJogador4 {
      get {
        return (string)ViewState["TEMAssistenciaJogador4"];
      }
      set {
        ViewState["TEMAssistenciaJogador4"] = value;
        lblTEMAssistenciaJogador4.Text = value;
      }
    }

    public string TEMLastHitJogador4 {
      get {
        return (string)ViewState["TEMLastHitJogador4"];
      }
      set {
        ViewState["TEMLastHitJogador4"] = value;
        lblTEMLastHitJogador4.Text = value;
      }
    }

    public string TEMDenyJogador4 {
      get {
        return (string)ViewState["TEMDenyJogador4"];
      }
      set {
        ViewState["TEMDenyJogador4"] = value;
        lblTEMDenyJogador4.Text = value;
      }
    }

    public string TEMOuroPorMinutoJogador4 {
      get {
        return (string)ViewState["TEMOuroPorMinutoJogador4"];
      }
      set {
        ViewState["TEMOuroPorMinutoJogador4"] = value;
        lblTEMOuroPorMinutoJogador4.Text = value;
      }
    }

    public string TEMOuroTotalJogador4 {
      get {
        return (string)ViewState["TEMOuroTotalJogador4"];
      }
      set {
        ViewState["TEMOuroTotalJogador4"] = value;
        lblTEMOuroTotalJogador4.Text = value;
      }
    }

    public string TEMXPPorMinutoJogador4 {
      get {
        return (string)ViewState["TEMXPPorMinutoJogador4"];
      }
      set {
        ViewState["TEMXPPorMinutoJogador4"] = value;
        lblTEMXPPorMinutoJogador4.Text = value;
      }
    }

    public string UrlTEMItem1Jogador4 {
      get {
        return (string)ViewState["UrlTEMItem1Jogador4"];
      }
      set {
        ViewState["UrlTEMItem1Jogador4"] = value;
        imgTEMItem1Jogador4.ImageUrl = value;
      }
    }

    public string UrlTEMItem2Jogador4 {
      get {
        return (string)ViewState["UrlTEMItem2Jogador4"];
      }
      set {
        ViewState["UrlTEMItem1Jogador2"] = value;
        imgTEMItem2Jogador4.ImageUrl = value;
      }
    }

    public string UrlTEMItem3Jogador4 {
      get {
        return (string)ViewState["UrlTEMItem3Jogador4"];
      }
      set {
        ViewState["UrlTEMItem3Jogador4"] = value;
        imgTEMItem3Jogador4.ImageUrl = value;
      }
    }

    public string UrlTEMItem4Jogador4 {
      get {
        return (string)ViewState["UrlTEMItem4Jogador4"];
      }
      set {
        ViewState["UrlTEMItem4Jogador4"] = value;
        imgTEMItem4Jogador4.ImageUrl = value;
      }
    }

    public string UrlTEMItem5Jogador4 {
      get {
        return (string)ViewState["UrlTEMItem5Jogador4"];
      }
      set {
        ViewState["UrlTEMItem5Jogador4"] = value;
        imgTEMItem5Jogador4.ImageUrl = value;
      }
    }

    public string UrlTEMItem6Jogador4 {
      get {
        return (string)ViewState["UrlTEMItem6Jogador4"];
      }
      set {
        ViewState["UrlTEMItem6Jogador4"] = value;
        imgTEMItem6Jogador4.ImageUrl = value;
      }
    }

    public string UrlTEMHeroiJogador5 {
      get {
        return (string)ViewState["UrlTEMHeroiJogador5"];
      }
      set {
        ViewState["UrlTEMHeroiJogador5"] = value;
        imgTEMHeroiJogador5.ImageUrl = value;
      }
    }

    public string TEMNomeJogador5 {
      get {
        return (string)ViewState["TEMNomeJogador5"];
      }
      set {
        ViewState["TEMNomeJogador5"] = value;
        lblTEMNomeJogador5.Text = value;
      }
    }

    public string UrlTEMFotoJogador5 {
      get {
        return (string)ViewState["UrlTEMFotoJogador5"];
      }
      set {
        ViewState["UrlTEMFotoJogador5"] = value;
        imgTEMFotoJogador5.ImageUrl = value;
      }
    }

    public string TEMNivelJogador5 {
      get {
        return (string)ViewState["TEMNivelJogador5"];
      }
      set {
        ViewState["TEMNivelJogador5"] = value;
        lblTEMNivelJogador5.Text = value;
      }
    }

    public string TEMKillsJogador5 {
      get {
        return (string)ViewState["TEMKillsJogador5"];
      }
      set {
        ViewState["TEMKillsJogador5"] = value;
        lblTEMKillsJogador5.Text = value;
      }
    }

    public string TEMDeathsJogador5 {
      get {
        return (string)ViewState["TEMDeathsJogador5"];
      }
      set {
        ViewState["TEMDeathsJogador5"] = value;
        lblTEMDeathsJogador5.Text = value;
      }
    }

    public string TEMAssistenciaJogador5 {
      get {
        return (string)ViewState["TEMAssistenciaJogador5"];
      }
      set {
        ViewState["TEMAssistenciaJogador5"] = value;
        lblTEMAssistenciaJogador5.Text = value;
      }
    }

    public string TEMLastHitJogador5 {
      get {
        return (string)ViewState["TEMLastHitJogador5"];
      }
      set {
        ViewState["TEMLastHitJogador5"] = value;
        lblTEMLastHitJogador5.Text = value;
      }
    }

    public string TEMDenyJogador5 {
      get {
        return (string)ViewState["TEMDenyJogador5"];
      }
      set {
        ViewState["TEMDenyJogador5"] = value;
        lblTEMDenyJogador5.Text = value;
      }
    }

    public string TEMOuroPorMinutoJogador5 {
      get {
        return (string)ViewState["TEMOuroPorMinutoJogador5"];
      }
      set {
        ViewState["TEMOuroPorMinutoJogador5"] = value;
        lblTEMOuroPorMinutoJogador5.Text = value;
      }
    }

    public string TEMOuroTotalJogador5 {
      get {
        return (string)ViewState["TEMOuroTotalJogador5"];
      }
      set {
        ViewState["TEMOuroTotalJogador5"] = value;
        lblTEMOuroTotalJogador5.Text = value;
      }
    }

    public string TEMXPPorMinutoJogador5 {
      get {
        return (string)ViewState["TEMXPPorMinutoJogador5"];
      }
      set {
        ViewState["TEMXPPorMinutoJogador5"] = value;
        lblTEMXPPorMinutoJogador5.Text = value;
      }
    }

    public string UrlTEMItem1Jogador5 {
      get {
        return (string)ViewState["UrlTEMItem1Jogador5"];
      }
      set {
        ViewState["UrlTEMItem1Jogador5"] = value;
        imgTEMItem1Jogador5.ImageUrl = value;
      }
    }

    public string UrlTEMItem2Jogador5 {
      get {
        return (string)ViewState["UrlTEMItem2Jogador5"];
      }
      set {
        ViewState["UrlTEMItem1Jogador2"] = value;
        imgTEMItem2Jogador5.ImageUrl = value;
      }
    }

    public string UrlTEMItem3Jogador5 {
      get {
        return (string)ViewState["UrlTEMItem3Jogador5"];
      }
      set {
        ViewState["UrlTEMItem3Jogador5"] = value;
        imgTEMItem3Jogador5.ImageUrl = value;
      }
    }

    public string UrlTEMItem4Jogador5 {
      get {
        return (string)ViewState["UrlTEMItem4Jogador5"];
      }
      set {
        ViewState["UrlTEMItem4Jogador5"] = value;
        imgTEMItem4Jogador5.ImageUrl = value;
      }
    }

    public string UrlTEMItem5Jogador5 {
      get {
        return (string)ViewState["UrlTEMItem5Jogador5"];
      }
      set {
        ViewState["UrlTEMItem5Jogador5"] = value;
        imgTEMItem5Jogador5.ImageUrl = value;
      }
    }

    public string UrlTEMItem6Jogador5 {
      get {
        return (string)ViewState["UrlTEMItem6Jogador5"];
      }
      set {
        ViewState["UrlTEMItem6Jogador5"] = value;
        imgTEMItem6Jogador5.ImageUrl = value;
      }
    }

    public string TEMNivelTotal {
      get {
        return (string)ViewState["TEMNivelTotal"];
      }
      set {
        ViewState["TEMNivelTotal"] = value;
        lblTEMNivelTotal.Text = value;
      }
    }

    public string TEMKillsJogadorTotal {
      get {
        return (string)ViewState["TEMKillsJogadorTotal"];
      }
      set {
        ViewState["TEMKillsJogadorTotal"] = value;
        lblTEMKillsJogadorTotal.Text = value;
      }
    }

    public string TEMDeathsJogadorTotal {
      get {
        return (string)ViewState["TEMDeathsJogadorTotal"];
      }
      set {
        ViewState["TEMDeathsJogadorTotal"] = value;
        lblTEMDeathsJogadorTotal.Text = value;
      }
    }

    public string TEMAssistenciaJogadorTotal {
      get {
        return (string)ViewState["TEMAssistenciaJogadorTotal"];
      }
      set {
        ViewState["TEMAssistenciaJogadorTotal"] = value;
        lblTEMAssistenciaJogadorTotal.Text = value;
      }
    }

    public string TEMLastHitJogadorTotal {
      get {
        return (string)ViewState["TEMLastHitJogadorTotal"];
      }
      set {
        ViewState["TEMLastHitJogadorTotal"] = value;
        lblTEMLastHitJogadorTotal.Text = value;
      }
    }

    public string TEMDenyJogadorTotal {
      get {
        return (string)ViewState["TEMDenyJogadorTotal"];
      }
      set {
        ViewState["TEMDenyJogadorTotal"] = value;
        lblTEMDenyJogadorTotal.Text = value;
      }
    }

    public string TEMOuroPorMinutoJogadorTotal {
      get {
        return (string)ViewState["TEMOuroPorMinutoJogadorTotal"];
      }
      set {
        ViewState["TEMOuroPorMinutoJogadorTotal"] = value;
        lblTEMOuroPorMinutoJogadorTotal.Text = value;
      }
    }

    public string TEMOuroTotalJogadorTotal {
      get {
        return (string)ViewState["TEMOuroTotalJogadorTotal"];
      }
      set {
        ViewState["TEMOuroTotalJogadorTotal"] = value;
        lblTEMOuroTotalJogadorTotal.Text = value;
      }
    }

    public string TEMXPPorMinutoJogadorTotal {
      get {
        return (string)ViewState["TEMXPPorMinutoJogadorTotal"];
      }
      set {
        ViewState["TEMXPPorMinutoJogadorTotal"] = value;
        lblTEMXPPorMinutoJogadorTotal.Text = value;
      }
    }

    public virtual string FotoJogadorMaiorkill {
      get { return (string)ViewState["FotoJogadorMaiorkill"]; }
      set {
        ViewState["FotoJogadorMaiorkill"] = value;
        imgFotoJogadorMaiorkill.ImageUrl = value;
      }
    }

    public virtual string FotoHeroiJogadorMaiorkill {
      get { return (string)ViewState["FotoHeroiJogadorMaiorkill"]; }
      set {
        ViewState["FotoHeroiJogadorMaiorkill"] = value;
        imgFotoHeroiJogadorMaiorkill.ImageUrl = value;
      }
    }

    public virtual string JogadorMaiorkill {
      get { return (string)ViewState["JogadorMaiorkill"]; }
      set {
        ViewState["JogadorMaiorkill"] = value;
        lblJogadorMaiorkill.Text = value;
      }
    }

    public virtual int ValorMaiorkill {
      get { return (int)ViewState["ValorMaiorkill"]; }
      set {
        ViewState["ValorMaiorkill"] = value;
        lblValorMaiorkill.Text = value.ToString();
      }
    }

      public virtual string FotoJogadorMenorkill {
      get { return (string)ViewState["FotoJogadorMenorkill"]; }
      set {
        ViewState["FotoJogadorMenorkill"] = value;
        imgFotoJogadorMenorkill.ImageUrl = value;
      }
    }

    public virtual string FotoHeroiJogadorMenorkill {
      get { return (string)ViewState["FotoHeroiJogadorMenorkill"]; }
      set {
        ViewState["FotoHeroiJogadorMenorkill"] = value;
        imgFotoHeroiJogadorMenorkill.ImageUrl = value;
      }
    }

    public virtual string JogadorMenorkill {
      get { return (string)ViewState["JogadorMenorkill"]; }
      set {
        ViewState["JogadorMenorkill"] = value;
        lblJogadorMenorkill.Text = value;
      }
    }

    public virtual int ValorMenorkill {
      get { return (int)ViewState["ValorMenorkill"]; }
      set {
        ViewState["ValorMenorkill"] = value;
        lblValorMenorkill.Text = value.ToString();
      }
    }

    public virtual string FotoJogadorMenosMorreu {
      get { return (string)ViewState["FotoJogadorMenosMorreu"]; }
      set {
        ViewState["FotoJogadorMenosMorreu"] = value;
        imgFotoJogadorMenosMorreu.ImageUrl = value;
      }
    }

    public virtual string FotoHeroiJogadorMenosMorreu {
      get { return (string)ViewState["FotoHeroiJogadorMenosMorreu"]; }
      set {
        ViewState["FotoHeroiJogadorMenosMorreu"] = value;
        imgFotoHeroiJogadorMenosMorreu.ImageUrl = value;
      }
    }

    public virtual string JogadorMenosMorreu {
      get { return (string)ViewState["JogadorMenosMorreu"]; }
      set {
        ViewState["JogadorMenosMorreu"] = value;
        lblJogadorMenosMorreu.Text = value;
      }
    }

    public virtual int ValorMenosMorte {
      get { return (int)ViewState["ValorMenosMorte"]; }
      set {
        ViewState["ValorMenosMorte"] = value;
        lblValorMenosMorte.Text = value.ToString();
      }
    }

    public virtual string FotoJogadorMaisMorreu {
      get { return (string)ViewState["FotoJogadorMaisMorreu"]; }
      set {
        ViewState["FotoJogadorMaisMorreu"] = value;
        imgFotoJogadorMaisMorreu.ImageUrl = value;
      }
    }

    public virtual string FotoHeroiJogadorMaisMorreu {
      get { return (string)ViewState["FotoHeroiJogadorMaisMorreu"]; }
      set {
        ViewState["FotoHeroiJogadorMaisMorreu"] = value;
        imgFotoHeroiJogadorMaisMorreu.ImageUrl = value;
      }
    }

    public virtual string JogadorMaisMorreu {
      get { return (string)ViewState["JogadorMaisMorreu"]; }
      set {
        ViewState["JogadorMaisMorreu"] = value;
        lblJogadorMaisMorreu.Text = value;
      }
    }

    public virtual int ValorMaisMorte {
      get { return (int)ViewState["ValorMaisMorte"]; }
      set {
        ViewState["ValorMaisMorte"] = value;
        lblValorMaisMorte.Text = value.ToString();
      }
    }

    public virtual string FotoJogadorMaiorAssist {
      get { return (string)ViewState["FotoJogadorMaiorAssist"]; }
      set {
        ViewState["FotoJogadorMaiorAssist"] = value;
        imgFotoJogadorMaiorAssist.ImageUrl = value;
      }
    }

    public virtual string FotoHeroiJogadorMaiorAssist {
      get { return (string)ViewState["FotoHeroiJogadorMaiorAssist"]; }
      set {
        ViewState["FotoHeroiJogadorMaiorAssist"] = value;
        imgFotoHeroiJogadorMaiorAssist.ImageUrl = value;
      }
    }

    public virtual string JogadorMaiorAssist {
      get { return (string)ViewState["JogadorMaiorAssist"]; }
      set {
        ViewState["JogadorMaiorAssist"] = value;
        lblJogadorMaiorAssist.Text = value;
      }
    }

    public virtual int ValorMaiorAssist {
      get { return (int)ViewState["ValorMaiorAssist"]; }
      set {
        ViewState["ValorMaiorAssist"] = value;
        lblValorMaiorAssist.Text = value.ToString();
      }
    }

    public virtual string FotoJogadorMenorAssist {
      get { return (string)ViewState["FotoJogadorMenorAssist"]; }
      set {
        ViewState["FotoJogadorMenorAssist"] = value;
        imgFotoJogadorMenorAssist.ImageUrl = value;
      }
    }

    public virtual string FotoHeroiJogadorMenorAssist {
      get { return (string)ViewState["FotoHeroiJogadorMenorAssist"]; }
      set {
        ViewState["FotoHeroiJogadorMenorAssist"] = value;
        imgFotoHeroiJogadorMenorAssist.ImageUrl = value;
      }
    }

    public virtual string JogadorMenorAssist {
      get { return (string)ViewState["JogadorMenorAssist"]; }
      set {
        ViewState["JogadorMenorAssist"] = value;
        lblJogadorMenorAssist.Text = value;
      }
    }

    public virtual int ValorMenorAssist {
      get { return (int)ViewState["ValorMenorAssist"]; }
      set {
        ViewState["ValorMenorAssist"] = value;
        lblValorMenorAssist.Text = value.ToString();
      }
    }

    public virtual string FotoJogadorMaiorLastHit {
      get { return (string)ViewState["FotoJogadorMaiorLastHit"]; }
      set {
        ViewState["FotoJogadorMaiorLastHit"] = value;
        imgFotoJogadorMaiorLastHit.ImageUrl = value;
      }
    }

    public virtual string FotoHeroiJogadorMaiorLastHit {
      get { return (string)ViewState["FotoHeroiJogadorMaiorLastHit"]; }
      set {
        ViewState["FotoHeroiJogadorMaiorLastHit"] = value;
        imgFotoHeroiJogadorMaiorLastHit.ImageUrl = value;
      }
    }

    public virtual string JogadorMaiorLastHit {
      get { return (string)ViewState["JogadorMaiorLastHit"]; }
      set {
        ViewState["JogadorMaiorLastHit"] = value;
        lblJogadorMaiorLastHit.Text = value;
      }
    }

    public virtual int ValorMaiorLastHit {
      get { return (int)ViewState["ValorMaiorLastHit"]; }
      set {
        ViewState["ValorMaiorLastHit"] = value;
        lblValorMaiorLastHit.Text = value.ToString();
      }
    }

    public virtual string FotoJogadorMenorLastHit {
      get { return (string)ViewState["FotoJogadorMenorLastHit"]; }
      set {
        ViewState["FotoJogadorMenorLastHit"] = value;
        imgFotoJogadorMenorLastHit.ImageUrl = value;
      }
    }

    public virtual string FotoHeroiJogadorMenorLastHit {
      get { return (string)ViewState["FotoHeroiJogadorMenorLastHit"]; }
      set {
        ViewState["FotoHeroiJogadorMenorLastHit"] = value;
        imgFotoHeroiJogadorMenorLastHit.ImageUrl = value;
      }
    }

    public virtual string JogadorMenorLastHit {
      get { return (string)ViewState["JogadorMenorLastHit"]; }
      set {
        ViewState["JogadorMenorLastHit"] = value;
        lblJogadorMenorLastHit.Text = value;
      }
    }

    public virtual int ValorMenorLastHit {
      get { return (int)ViewState["ValorMenorLastHit"]; }
      set {
        ViewState["ValorMenorLastHit"] = value;
        lblValorMenorLastHit.Text = value.ToString();
      }
    }

    public virtual string FotoJogadorMaiorDeny {
      get { return (string)ViewState["FotoJogadorMaiorDeny"]; }
      set {
        ViewState["FotoJogadorMaiorDeny"] = value;
        imgFotoJogadorMaiorDeny.ImageUrl = value;
      }
    }

    public virtual string FotoHeroiJogadorMaiorDeny {
      get { return (string)ViewState["FotoHeroiJogadorMaiorDeny"]; }
      set {
        ViewState["FotoHeroiJogadorMaiorDeny"] = value;
        imgFotoHeroiJogadorMaiorDeny.ImageUrl = value;
      }
    }

    public virtual string JogadorMaiorDeny {
      get { return (string)ViewState["JogadorMaiorDeny"]; }
      set {
        ViewState["JogadorMaiorDeny"] = value;
        lblJogadorMaiorDeny.Text = value;
      }
    }

    public virtual int ValorMaiorDeny {
      get { return (int)ViewState["ValorMaiorDeny"]; }
      set {
        ViewState["ValorMaiorDeny"] = value;
        lblValorMaiorDeny.Text = value.ToString();
      }
    }

    public virtual string FotoJogadorMenorDeny {
      get { return (string)ViewState["FotoJogadorMenorDeny"]; }
      set {
        ViewState["FotoJogadorMenorDeny"] = value;
        imgFotoJogadorMenorDeny.ImageUrl = value;
      }
    }

    public virtual string FotoHeroiJogadorMenorDeny {
      get { return (string)ViewState["FotoHeroiJogadorMenorDeny"]; }
      set {
        ViewState["FotoHeroiJogadorMenorDeny"] = value;
        imgFotoHeroiJogadorMenorDeny.ImageUrl = value;
      }
    }

    public virtual string JogadorMenorDeny {
      get { return (string)ViewState["JogadorMenorDeny"]; }
      set {
        ViewState["JogadorMenorDeny"] = value;
        lblJogadorMenorDeny.Text = value;
      }
    }

    public virtual int ValorMenorDeny {
      get { return (int)ViewState["ValorMenorDeny"]; }
      set {
        ViewState["ValorMenorDeny"] = value;
        lblValorMenorDeny.Text = value.ToString();
      }
    }

    public virtual string FotoJogadorMaiorXpMin {
      get { return (string)ViewState["FotoJogadorMaiorXpMin"]; }
      set {
        ViewState["FotoJogadorMaiorXpMin"] = value;
        imgFotoJogadorMaiorXpMin.ImageUrl = value;
      }
    }

    public virtual string FotoHeroiJogadorMaiorXpMin {
      get { return (string)ViewState["FotoHeroiJogadorMaiorXpMin"]; }
      set {
        ViewState["FotoHeroiJogadorMaiorXpMin"] = value;
        imgFotoHeroiJogadorMaiorXpMin.ImageUrl = value;
      }
    }

    public virtual string JogadorMaiorXpMin {
      get { return (string)ViewState["JogadorMaiorXpMin"]; }
      set {
        ViewState["JogadorMaiorXpMin"] = value;
        lblJogadorMaiorXpMin.Text = value;
      }
    }

    public virtual int ValorMaiorXpMin {
      get { return (int)ViewState["ValorMaiorXpMin"]; }
      set {
        ViewState["ValorMaiorXpMin"] = value;
        lblValorMaiorXpMin.Text = value.ToString();
      }
    }

    public virtual string FotoJogadorMenorXpMin {
      get { return (string)ViewState["FotoJogadorMenorXpMin"]; }
      set {
        ViewState["FotoJogadorMenorXpMin"] = value;
        imgFotoJogadorMenorXpMin.ImageUrl = value;
      }
    }

    public virtual string FotoHeroiJogadorMenorXpMin {
      get { return (string)ViewState["FotoHeroiJogadorMenorXpMin"]; }
      set {
        ViewState["FotoHeroiJogadorMenorXpMin"] = value;
        imgFotoHeroiJogadorMenorXpMin.ImageUrl = value;
      }
    }

    public virtual string JogadorMenorXpMin {
      get { return (string)ViewState["JogadorMenorXpMin"]; }
      set {
        ViewState["JogadorMenorXpMin"] = value;
        lblJogadorMenorXpMin.Text = value;
      }
    }

    public virtual int ValorMenorXpMin {
      get { return (int)ViewState["ValorMenorXpMin"]; }
      set {
        ViewState["ValorMenorXpMin"] = value;
        lblValorMenorXpMin.Text = value.ToString();
      }
    }

    public virtual string FotoJogadorMaisRico {
      get { return (string)ViewState["FotoJogadorMaisRico"]; }
      set {
        ViewState["FotoJogadorMaisRico"] = value;
        imgFotoJogadorMaisRico.ImageUrl = value;
      }
    }

    public virtual string FotoHeroiJogadorMaisRico {
      get { return (string)ViewState["FotoHeroiJogadorMaisRico"]; }
      set {
        ViewState["FotoHeroiJogadorMaisRico"] = value;
        imgFotoHeroiJogadorMaisRico.ImageUrl = value;
      }
    }

    public virtual string JogadorMaisRico {
      get { return (string)ViewState["JogadorMaisRico"]; }
      set {
        ViewState["JogadorMaisRico"] = value;
        lblJogadorMaisRico.Text = value;
      }
    }

    public virtual int ValorMaisRico {
      get { return (int)ViewState["ValorMaisRico"]; }
      set {
        ViewState["ValorMaisRico"] = value;
        lblValorMaisRico.Text = value.ToString();
      }
    }

    public virtual string FotoJogadorMaisPobre {
      get { return (string)ViewState["FotoJogadorMaisPobre"]; }
      set {
        ViewState["FotoJogadorMaisPobre"] = value;
        imgFotoJogadorMaisPobre.ImageUrl = value;
      }
    }

    public virtual string FotoHeroiJogadorMaisPobre {
      get { return (string)ViewState["FotoHeroiJogadorMaisPobre"]; }
      set {
        ViewState["FotoHeroiJogadorMaisPobre"] = value;
        imgFotoHeroiJogadorMaisPobre.ImageUrl = value;
      }
    }

    public virtual string JogadorMaisPobre {
      get { return (string)ViewState["JogadorMaisPobre"]; }
      set {
        ViewState["JogadorMaisPobre"] = value;
        lblJogadorMaisPobre.Text = value;
      }
    }

    public virtual int ValorMaisPobre {
      get { return (int)ViewState["ValorMaisPobre"]; }
      set {
        ViewState["ValorMaisPobre"] = value;
        lblValorMaisPobre.Text = value.ToString();
      }
    }

    public virtual string FotoJogadorMaisDanoCausadoEmHeroi {
      get { return (string)ViewState["FotoJogadorMaisDanoCausadoEmHeroi"]; }
      set {
        ViewState["FotoJogadorMaisDanoCausadoEmHeroi"] = value;
        imgFotoJogadorMaisDanoCausadoEmHeroi.ImageUrl = value;
      }
    }

    public virtual string FotoHeroiJogadorMaisDanoCausadoEmHeroi {
      get { return (string)ViewState["FotoHeroiJogadorMaisDanoCausadoEmHeroi"]; }
      set {
        ViewState["FotoHeroiJogadorMaisDanoCausadoEmHeroi"] = value;
        imgFotoHeroiJogadorMaisDanoCausadoEmHeroi.ImageUrl = value;
      }
    }

    public virtual string JogadorMaisDanoCausadoEmHeroi {
      get { return (string)ViewState["JogadorMaisDanoCausadoEmHeroi"]; }
      set {
        ViewState["JogadorMaisDanoCausadoEmHeroi"] = value;
        lblJogadorMaisDanoCausadoEmHeroi.Text = value;
      }
    }

    public virtual int ValorMaisDanoCausadoEmHeroi {
      get { return (int)ViewState["ValorMaisDanoCausadoEmHeroi"]; }
      set {
        ViewState["ValorMaisDanoCausadoEmHeroi"] = value;
        lblValorMaisDanoCausadoEmHeroi.Text = value.ToString();
      }
    }

    public virtual string FotoJogadorMaisDanoCausadoEmTorre {
      get { return (string)ViewState["FotoJogadorMaisDanoCausadoEmTorre"]; }
      set {
        ViewState["FotoJogadorMaisDanoCausadoEmTorre"] = value;
        imgFotoJogadorMaisDanoCausadoEmTorre.ImageUrl = value;
      }
    }

    public virtual string FotoHeroiJogadorMaisDanoCausadoEmTorre {
      get { return (string)ViewState["FotoHeroiJogadorMaisDanoCausadoEmTorre"]; }
      set {
        ViewState["FotoHeroiJogadorMaisDanoCausadoEmTorre"] = value;
        imgFotoHeroiJogadorMaisDanoCausadoEmTorre.ImageUrl = value;
      }
    }

    public virtual string JogadorMaisDanoCausadoEmTorre {
      get { return (string)ViewState["JogadorMaisDanoCausadoEmTorre"]; }
      set {
        ViewState["JogadorMaisDanoCausadoEmTorre"] = value;
        lblJogadorMaisDanoCausadoEmTorre.Text = value;
      }
    }

    public virtual int ValorMaisDanoCausadoEmTorre {
      get { return (int)ViewState["ValorMaisDanoCausadoEmTorre"]; }
      set {
        ViewState["ValorMaisDanoCausadoEmTorre"] = value;
        lblValorMaisDanoCausadoEmTorre.Text = value.ToString();
      }
    }

    public virtual string FotoHeroiMelhorJogador
    {
        get { return (string)ViewState["FotoHeroiMelhorJogador"]; }
        set
        {
            ViewState["FotoHeroiMelhorJogador"] = value;
            imgFotoHeroiMelhorJogador.ImageUrl = value;
        }
    }

    public virtual string MelhorJogador
    {
        get { return (string)ViewState["MelhorJogador"]; }
        set
        {
            ViewState["MelhorJogador"] = value;
            lblMelhorJogador.Text = value;
        }
    }

    public virtual string FotoMelhorJogador
    {
        get { return (string)ViewState["FotoMelhorJogador"]; }
        set
        {
            ViewState["FotoMelhorJogador"] = value;
            imgFotoMelhorJogador.ImageUrl = value;
        }
    }
    
    public virtual string FotoHeroiPiorJogador
    {
        get { return (string)ViewState["FotoHeroiPiorJogador"]; }
        set
        {
            ViewState["FotoHeroiPiorJogador"] = value;
            imgFotoHeroiPiorJogador.ImageUrl = value;
        }
    }

    public virtual string PiorJogador
    {
        get { return (string)ViewState["PiorJogador"]; }
        set
        {
            ViewState["PiorJogador"] = value;
            lblPiorJogador.Text = value;
        }
    }

    public virtual string FotoPiorJogador
    {
        get { return (string)ViewState["FotoPiorJogador"]; }
        set
        {
            ViewState["FotoPiorJogador"] = value;
            imgFotoPiorJogador.ImageUrl = value;
        }
    }


    protected void Page_Load(object sender, EventArgs e) {
      
    }

  }
}