﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppJFF.Controle
{
  public partial class GridControl : System.Web.UI.UserControl {

    protected override void OnLoad(EventArgs e) {
      base.OnLoad(e);
      if (IsPostBack) {
        if (Request["__EVENTTARGET"] == OrdenarGrid.ClientID) {
          Ordenar(Request["__EVENTARGUMENT"]);
        }
      }
    }

    private string mensagemGridVazia = "Não existem registros.";

    public string MensagemGridVazia {
      get { return mensagemGridVazia; }
      set { mensagemGridVazia = value; }
    }

    public string ScriptDetalhes { get; set; }

    public delegate void AposItemCriadoEventHandler(object sender, RepeaterItemEventArgs e);

    [Description("GridControl_OnAposItemCriado"), BrowsableAttribute(true)]
    public event AposItemCriadoEventHandler AposItemCriado;

    public Func<string, SortDirection?, string> MontarLinkImpressaoPesquisa;

    public bool EnableBotaoImprimir { get; set; }

    #region TempoPesquisa
    private void BeginSearchTime() {
      DateTime time = DateTime.Now;
      ViewState["BeginTime"] = time;
    }

    private void EndSearchTime() {
      DateTime beginTime = (DateTime)ViewState["BeginTime"];
      TempoPesquisa = (DateTime.Now - beginTime).TotalSeconds.ToString("F3");
    }

    private string TempoPesquisa {
      get {
        return (ViewState["TempoPesquisa"] != null) ? ViewState["TempoPesquisa"].ToString() : "0";
      }
      set {
        ViewState["TempoPesquisa"] = value;
      }
    }
    #endregion

    #region Propriedades do metodo de selecao
    public delegate object[] SelecionarComPaginacaoFunc(int startRow, int pageSize, string sortOrder, out int total);

    public SelecionarComPaginacaoFunc SelecionarComPaginacao;

    protected int TotalRegistros {
      get {
        if (ViewState["TotalRegistros"] != null)
          return (int)ViewState["TotalRegistros"];
        else
          return 0;
      }
      set { ViewState["TotalRegistros"] = value; }
    }

    protected int PaginaAtual {
      get {
        if (ViewState["PaginaAtual"] != null)
          return (int)ViewState["PaginaAtual"];
        else
          return 0;
      }
      set { ViewState["PaginaAtual"] = value; }
    }

    protected int TamanhoPagina {
      get {
        if (ViewState["TamanhoPagina"] != null)
          return (int)ViewState["TamanhoPagina"];
        else
          return 10;
      }
      set { ViewState["TamanhoPagina"] = value; }
    }

    protected string SortExpression {
      get {
        if (ViewState["SortExpression"] != null)
          return (string)ViewState["SortExpression"];
        else
          return "";
      }
      set { ViewState["SortExpression"] = value; }
    }

    protected SortDirection SortDirecao {
      get {
        if (ViewState["SortDirecao"] != null)
          return (SortDirection)ViewState["SortDirecao"];
        else
          return SortDirection.Ascending;
      }
      set { ViewState["SortDirecao"] = value; }
    }
    #endregion

    protected object DataSource {
      get { return this.BodyRepeater.DataSource; }
      set { this.BodyRepeater.DataSource = value; }
    }

    public void LimparResultado() {
      GridPanel.Visible = false;
      VazioPanel.Visible = true;
    }

    #region Metodos para o Repeater
    [TemplateContainer(typeof(RowContainer))]
    [PersistenceMode(PersistenceMode.InnerProperty)]
    public ITemplate HeaderTemplate { get; set; }

    protected void HeadPlaceHolderPreRender(object sender, EventArgs e) {
      var container = new RowContainer();
      HeaderTemplate.InstantiateIn(container);
      HeadPlaceHolder.Controls.Clear();
      HeadPlaceHolder.Controls.Add(container);
    }

    [TemplateContainer(typeof(RepeaterItem))]
    [PersistenceMode(PersistenceMode.InnerProperty)]
    public ITemplate RowTemplate { get; set; }


    public class RowContainer : Control, INamingContainer {
    }
    #endregion

    public void CarregarDados(int pagina, string sortExpression, SortDirection? sortDirection) {
      SortExpression = sortExpression;
      BeginSearchTime();
      if (sortExpression != null && sortExpression != String.Empty) {
        if (sortDirection != null) {
          SortDirecao = sortDirection.Value;
          if (sortDirection.Value == SortDirection.Descending) {
            if (sortExpression.Contains(",")) {
              string[] sort = sortExpression.Split(',');
              sort[0] += " DESC";
              sortExpression = string.Join(",", sort);
            } else
              sortExpression += " DESC";
          }
        } else
          SortDirecao = SortDirection.Ascending;
      }
      PaginaAtual = pagina;
      int total;
      EndSearchTime();
      DataSource = SelecionarComPaginacao(pagina * TamanhoPagina, TamanhoPagina, sortExpression, out total);
      TotalRegistros = total;
      if (total == 0) {
        GridPanel.Visible = false;
        VazioPanel.Visible = true;
      } else {
        GridPanel.Visible = true;
        VazioPanel.Visible = false;
        DataBind();
        if (EnableBotaoImprimir && MontarLinkImpressaoPesquisa != null) {
          ImprimirLink.NavigateUrl = MontarLinkImpressaoPesquisa(sortExpression, sortDirection);
          ExportarExelLink.NavigateUrl = ImprimirLink.NavigateUrl + "&exportCSV=true";
          ImprimirPanel.Visible = this.Visible;
        } else
          ImprimirPanel.Visible = false;
      }

    }

    #region eventos da paginação manual

    protected void FirstBtn_OnClick(object sender, EventArgs e) {
      CarregarDadosComSortExpression(0);
    }
    protected void LastBtn_OnClick(object sender, EventArgs e) {
      int pageIndex = (TotalRegistros / TamanhoPagina) - 1;
      if (TotalRegistros % TamanhoPagina != 0)
        pageIndex++;
      CarregarDadosComSortExpression(pageIndex);
    }
    protected void PreviousBtn_OnClick(object sender, EventArgs e) {
      int pageIndex = PaginaAtual;
      CarregarDadosComSortExpression(pageIndex - 1);
    }
    protected void NextBtn_OnClick(object sender, EventArgs e) {
      int pageIndex = PaginaAtual;
      CarregarDadosComSortExpression(pageIndex + 1);
    }

    private void CarregarDadosComSortExpression(int pagina) {
      CarregarDados(pagina, SortExpression, SortDirecao);
    }

    #endregion

    #region Metodos para ir a uma pagina especifica

    public string InformacoesConsulta {
      get;
      set;
    }

    protected void IrParaTextBox_TextChanged(object sender, EventArgs e) {
      int pageIndex;
      int totalPaginas = TotalRegistros / TamanhoPagina;
      if (TotalRegistros % TamanhoPagina != 0)
        totalPaginas++;
      if (Int32.TryParse(IrParaTextBox.Text, out pageIndex)) {
        if (pageIndex > 0 && pageIndex <= totalPaginas)
          CarregarDadosComSortExpression(pageIndex - 1);
      }
      IrParaTextBox.Text = "";
    }

    #endregion

    protected override void OnPreRender(EventArgs e) {
      base.OnPreRender(e);
      if (SelecionarComPaginacao != null && TotalRegistros > 0) {
        if (TotalRegistros > TamanhoPagina) {
          ExternalPagingPanel.Visible = this.Visible;
          int totalRegistros = TotalRegistros;
          int pageIndex = PaginaAtual;
          IrParaTextBox.Text = (pageIndex + 1).ToString();
          int totalPaginas = (totalRegistros / TamanhoPagina); ;
          if (totalRegistros % TamanhoPagina != 0)
            totalPaginas++;
          TotalPaginasLabel.Text = totalPaginas.ToString();
          FirstBtn.Visible = PreviousBtn.Visible = pageIndex != 0;
          NextBtn.Visible = LastBtn.Visible = totalPaginas != (pageIndex + 1);

          InformacoesConsulta = "<ul class='list-unstyled'>";
          InformacoesConsulta += "<li>Atualizado em " + DateTime.Now.ToString() + ".</li>";
          InformacoesConsulta += "<li>Retornados " + totalRegistros.ToString() + " registros.</li>";
          InformacoesConsulta += "<li>Executado em " + TempoPesquisa.ToString() + " segundos.</li>";
          InformacoesConsulta += "</ul>";

        } else
          ExternalPagingPanel.Visible = false;
      } else {
        ExternalPagingPanel.Visible = false;
      }
      RegistrarScripts();
    }

    protected void RegistrarScripts() {
      if (!string.IsNullOrEmpty(ScriptDetalhes)) {
        string scriptDetalhes = "carregarDetalhes('" + this.ClientID + "', " + ScriptDetalhes + ");";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "detalhesGrid" + this.ClientID, scriptDetalhes, true);
      }
      string scriptOrdenacao = @"
      $('[data-sortexpression]', '#' + '" + this.ClientID + @"' + ' thead tr').addClass('order-link');
        $('#' + '" + this.ClientID + @"' + ' thead tr').on('click', '[data-sortexpression]', function () {
            var sortExpression = $(this).data('sortexpression');
            __doPostBack('" + OrdenarGrid.ClientID + @"', sortExpression);
        });";
      ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ordenacaoGrid" + this.ClientID, scriptOrdenacao, true);
      if (SortDirecao == SortDirection.Ascending) {
        string scriptColunaOrdanada = "$('[data-sortexpression=\"" + SortExpression + "\"]', '#' + '" + this.ClientID + "' + ' thead tr').addClass('order-asc');";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "colunaOrdenadaGrid" + this.ClientID, scriptColunaOrdanada, true);
      } else {
        string scriptColunaOrdanada = "$('[data-sortexpression=\"" + SortExpression + "\"]', '#' + '" + this.ClientID + "' + ' thead tr').addClass('order-desc');";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "colunaOrdenadaGrid" + this.ClientID, scriptColunaOrdanada, true);
      }
    }

    protected void Ordenar(string sortExpression) {
      //inverte o sort direction se estiver ordenando a mesma coluna
      if (sortExpression == SortExpression) {
        SortDirecao = SortDirecao == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending;
      } else {
        SortDirecao = SortDirection.Ascending;
      }
      CarregarDados(PaginaAtual, sortExpression, SortDirecao);
    }

    protected void BodyRepeater_Init(object sender, EventArgs e) {
      BodyRepeater.ItemTemplate = RowTemplate;
    }

    protected void BodyRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e) {
      if (AposItemCriado != null && e.Item.DataItem != null) {
        AposItemCriado(sender, e);
      }
    }




  }
}
