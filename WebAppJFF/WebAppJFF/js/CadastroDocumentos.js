﻿function focus(valorInicialInput) {
  var input = $('INPUT', '#spanFocoInicial');
  if (input != null && input.length > 0) {
    try {
      if (input[0].value == '') {
        input[0].value = valorInicialInput; 
        input[0].focus();
      }
    }
    catch (err) {
    }
  }
  else {
    input = $('SELECT', '#spanFocoInicial');
    if (input != null && input.length > 0) {
      try {
        input[0].focus();
      }
      catch (err) {
      }
    }
  }
}