
Masked Input plugin for jQuery

- Fonte:
	http://digitalbush.com/projects/masked-input-plugin/

- Mascaras
	a - Represents an alpha character (A-Z,a-z)
	9 - Represents a numeric character (0-9)
	* - Represents an alphanumeric character (A-Z,a-z,0-9)

- Exemplos:

	jQuery(function($){
	   $("#date").mask("99/99/9999");
	   $("#phone").mask("(999) 999-9999");
	   $("#tin").mask("99-9999999");
	   $("#ssn").mask("999-99-9999");
	});
	jQuery(function($){
	   $("#product").mask("99/99/9999",{placeholder:" "});
	});
	jQuery(function($){
	   $("#product").mask("99/99/9999",{completed:function(){alert("You typed the following: "+this.val());}});
	});
	jQuery(function($){
	   $.mask.definitions['~']='[+-]';
	   $("#eyescript").mask("~9.99 ~9.99 999");
	});
	jQuery(function($){
	   $("#phone").mask("(999) 999-9999? x99999");
	});
	jQuery(function($){
	   $("#phone").mask("#hhhhhh");
	});

	


