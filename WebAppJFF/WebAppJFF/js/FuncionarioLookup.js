﻿
var descriptionTextBoxClientID;
var animacaoAjaxClientID;
var keyTextBoxClientID;
var errosDivClientID;
var eventoAposSelecionarFuncionario = false;

function AtualizaClientID(descriptionTextBox, keyTextBox, animacaoAjax, errosDiv) {
  descriptionTextBoxClientID = descriptionTextBox;
  keyTextBoxClientID = keyTextBox;
  animacaoAjaxClientID = animacaoAjax;
  errosDivClientID = errosDiv;
}

function onCompleteBusca(response) {
  MostraAnimacao(false);
  var descriptionTextBox = document.getElementById(descriptionTextBoxClientID);
  descriptionTextBox.value = response;
  if (FuncionarioChangedScript != null && FuncionarioChangedScript != "")
    eval(FuncionarioChangedScript);
}
function onErrorBusca(response) {
  MostraAnimacao(false);
  ImprimeErro(response._message, true);
}
function onTimeOutBusca() {
  MostraAnimacao(false);
  ImprimeErro("Erro de timeout buscando o funcionário", true);
}
function MostraAnimacao(mostrar) {
  var animacaoAJAX = document.getElementById(animacaoAjaxClientID);
  if (mostrar == true)
    animacaoAJAX.style.display = "";
  else
    animacaoAJAX.style.display = "none";
}
function ImprimeErro(erro, mostrar) {
  var errosDiv = document.getElementById(errosDivClientID);
  if (errosDiv != null) {
    if (mostrar == true) {
      errosDiv.style.display = "";
      errosDiv.innerHTML = erro;
      var descriptionTextBox = document.getElementById(descriptionTextBoxClientID);
      descriptionTextBox.value = "";
      var keyTextBox = document.getElementById(keyTextBoxClientID);
      keyTextBox.value = "";
      keyTextBox.focus();
    }
    else
      errosDiv.style.display = "none";
  }
}