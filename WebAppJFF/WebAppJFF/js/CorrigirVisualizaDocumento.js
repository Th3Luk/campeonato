﻿
function corrigeVisualizaDocumento() {
  var minimizado = true;
  var _pchaveMinimizado = false;

  $('#redimencionar').click(function () {
    if (minimizado) {
      $('#frame').attr('style', 'width:96%');
      $('#iframe').attr('style', 'width:96%');
      minimizado = false;
    } else {
      if (ieAntigo()) {
        $('#frame').attr('style', 'width:96%');
        $('#iframe').attr('style', 'width:96%');
      } else {
        $('#frame').attr('style', 'width:71%');
        $('#iframe').attr('style', 'width:71%');
      }
      minimizado = true;
    }
  });

  $(function () {
    $(".palavraChavPainel").css({
      position: 'absolute',
      visibility: 'hidden'
    });
    $('.iframe').css('height', ($('.iframe').height() - $('.palavraChavePainel').outerHeight(true)));
    $(".palavraChavPainel").css({
      position: 'relative',
      visibility: 'visible'
    });

    $('#togglePalavrasChave').click(function () {
      var diffSize
      if (_pchaveMinimizado) {
        diffSize = $('.palavraChavePainel').outerHeight(true);
        $('#iconTogglePalavraChave').attr('class', 'ui-icon ui-icon-triangle-1-s');
        $('._palavrasChave').css('display', '');
        diffSize = $('.palavraChavePainel').outerHeight(true) - diffSize;
        $('.iframe').css('height', ($('.iframe').height() - diffSize));
        _pchaveMinimizado = false;
      } else {
        diffSize = $('.palavraChavePainel').outerHeight(true);
        $('#iconTogglePalavraChave').attr('class', 'ui-icon ui-icon-triangle-1-e');
        $('._palavrasChave').css('display', 'none');
        diffSize = diffSize - $('.palavraChavePainel').outerHeight(true);
        $('.iframe').css('height', ($('.iframe').height() + diffSize));
        _pchaveMinimizado = true;
      }
    });

  });

  function getInternetExplorerVersion() {
    var rv = -1; // Return value assumes failure.
    if (navigator.appName == 'Microsoft Internet Explorer') {
      var ua = navigator.userAgent;
      var re = new RegExp('MSIE ([0-9]{1,}[\.0-9]{0,})');
      if (re.exec(ua) != null)
        rv = parseFloat(RegExp.$1);
    }
    return rv;
  }

  function ieAntigo() {
    var ver = getInternetExplorerVersion();
    if (ver > -1) {
      if (ver >= 8) {
        return false;
      } else {
        return true;
      }
    }
  }

  if (ieAntigo()) {
    $('#frame').attr('style', 'width:96%');
  }
}