﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;

namespace WebAppJFF.Mapa
{
  public class PerfilJogadorMap : ClassMap<PerfilJogador>
  {
    public PerfilJogadorMap()
    {
      Id(x => x.Id);
      Map(x => x.IdFaculdade);
      Map(x => x.Ativo);
      Map(x => x.Email);
      Map(x => x.Imagem);
      Map(x => x.Nome);
      Map(x => x.RA);
      Map(x => x.Twitch);
    }
  }
}