﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;

namespace WebAppJFF.Mapa
{
  public class TimeMap : ClassMap<Time>
  {
    public TimeMap()    
    {
      Id(x => x.Id);
      Map(x => x.Ativo);
      References<PerfilJogo>(x => x.Capitao, "IdPerfilJogo").Not.LazyLoad();
      Map(x => x.Logo);
      Map(x => x.Nome);
      Map(x => x.Tag);
      References<PerfilJogo>(x => x.Criador, "IdCriador");
    }
  }
}