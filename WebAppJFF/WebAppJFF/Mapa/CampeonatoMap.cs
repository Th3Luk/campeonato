﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;

namespace WebAppJFF.Mapa
{
  public class CampeonatoMap : ClassMap<Campeonato> 
  {
    public CampeonatoMap()
    {
      Id(x => x.Id);
      Map(x => x.Nome);
      Map(x => x.Ativo);
      Map(x => x.DataFinalCamp);
      Map(x => x.DataInicioCamp);
      Map(x => x.DataInscricaoFinal);
      Map(x => x.DataInscricaoInicio);
      Map(x => x.DescricaoResumida).Length(4000);
      Map(x => x.IdJogo);
      Map(x => x.Logo);
      Map(x => x.NumeroMaximoTimes);
      Map(x => x.ValorInscricao);
      Map(x => x.ValorPremio);
      References<Usuario>(x => x.Adm, "IdUsuario");
    }
  }
}