﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;

namespace WebAppJFF.Mapa
{
  public class InscricaoMap : ClassMap<Inscricao> 
  {
    public InscricaoMap()
    {
      Id(x => x.Id);
      Map(x => x.Ativo);
      Map(x => x.DataConfirmacao);
      Map(x => x.DataInscricao);
      References<Time>(x => x.Time, "IdTime");
      References<Campeonato>(x => x.Campeonato, "IdCampeonato");
      References<Usuario>(x => x.UsuarioAdm, "IdUsuario");
    }
  }
}