﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;

namespace WebAppJFF.Mapa
{
  public class ArqTempMap : ClassMap<ArqTemp> 
  {
    public ArqTempMap()
    {
      Id(x => x.Id);
      Map(x => x.Imagem);
    }
  }
}