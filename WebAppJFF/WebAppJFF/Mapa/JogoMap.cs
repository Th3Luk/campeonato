﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;

namespace WebAppJFF.Mapa
{
  public class JogoMap : ClassMap<Jogo>
  {
    public JogoMap()
    {
      Id(x => x.Id);
      Map(x => x.Nome);
      Map(x => x.Ativo);
      Map(x => x.Sigla);
      Map(x => x.Logo);
    }
  }
}