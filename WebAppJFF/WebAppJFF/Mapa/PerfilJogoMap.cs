﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;

namespace WebAppJFF.Mapa
{
  public class PerfilJogoMap : ClassMap<PerfilJogo>
  {
    public PerfilJogoMap()
    {
      Id(x => x.Id);
      References<PerfilJogador>(x => x.PerfilJogador, "IdPerfilJogador").Not.LazyLoad().Not.Nullable();
      Map(x => x.IdJogo);
      Map(x => x.Nome);
      Map(x => x.Descricao);
      Map(x => x.TempoJogo);
      Map(x => x.IdPosicao);
      References<Time>(x => x.Time, "IdTime").Not.LazyLoad().Nullable();
    }
  }
}