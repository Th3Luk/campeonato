﻿using FluentNHibernate.Mapping;
using Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate.Type;

namespace WebAppJFF.Mapa
{
  public class UsuarioMap : ClassMap<Usuario>
  {
    public UsuarioMap()    
    {
      Id(x => x.Id);
      Map(x => x.Ativo);
      Map(x => x.DataCadastro);
      Map(x => x.Admin);
      Map(x => x.Login).Unique().Not.Nullable();
      Map(x => x.Senha).CustomType<BinaryBlobType>().Length(100);
      References<PerfilJogador>(x => x.PerfilJogador, "IdPerfilJogador").Not.LazyLoad().Not.Nullable();
    }
  }
}