﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;

namespace WebAppJFF.Mapa
{
  public class ConviteMap : ClassMap<ConviteJogTime>
  {
    public ConviteMap()
    {
      Id(x => x.Id);
      References<PerfilJogo>(x => x.Jogador, "IdPerfilJogo").Not.LazyLoad().Not.Nullable();
      References<Time>(x => x.Time, "IdTime").Not.LazyLoad().Nullable();
      Map(x => x.DataConvite);      
    }
  }
}