﻿using MPS.Runtime.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppJFF.Classes;
using WebAppJFF.Classes.Campeonato;
using WebAppJFF.Interfaces.Campeonato;
using WebAppJFF.InterfacesDAO.Campeonato;
using WebAppJFF.Servicos.Base;

namespace WebAppJFF.Servicos.Campeonato
{
  public class UsuarioServico : BaseDescricaoServico<Usuario, IUsuarioDAO>, IUsuarioServico
  {
    public Usuario ObterPorLogin(string login)
    {
      IUsuarioDAO daoUsuario = Resolver.GetImplementationOf<IUsuarioDAO>();
      Usuario usuario = daoUsuario.ObterPorLogin(login);
      return usuario;
    }

    private const int MAX_SENHA = 100;

    public bool VerificarAutenticidadeSenha(Usuario usuario, string senha)
    {
      Criptografia criptografia = null;
      try
      {
        criptografia = new Criptografia(usuario.TipoCriptografiaSenha);
        
      }
      catch (Exception)
      {
        return false;
      }

      byte[] bytes = new byte[senha.Length * sizeof(char)];

      System.Buffer.BlockCopy(senha.ToCharArray(), 0, bytes, 0, bytes.Length);
      byte[] conteudoCriptografado = criptografia.MontarCriptografia(bytes);

      int sizeSenha = usuario.Senha.Length;

      if (conteudoCriptografado.Length < sizeSenha ||
          (sizeSenha < MAX_SENHA && conteudoCriptografado.Length > sizeSenha))
        return false;
      for (int i = 0; i < sizeSenha; i++)
        if (usuario.Senha[i] != conteudoCriptografado[i])
          return false;

      return true;
    }

    #region Métodos de persistência
    public override void Inserir(Usuario item)
    {
      IUsuarioDAO iDAO = Resolver.GetImplementationOf<IUsuarioDAO>();
      Usuario usuarioValidate = new Usuario();
      // Garantir que o número da Matrícula seja único
      usuarioValidate = ObterPorLogin(item.Login);
      if (usuarioValidate != null)
        throw new Exception("Login já esta em uso.");
      Criptografia criptografia = new Criptografia('S');
      criptografia.MontarCriptografia(item.Senha);
      iDAO.Inserir(item);

    }
    #endregion
  }
}