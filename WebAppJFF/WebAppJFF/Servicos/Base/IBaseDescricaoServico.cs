﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppJFF.Classes.Base;

namespace WebAppJFF.Servicos.Base
{
  public interface IBaseDescricaoServico<T> : IBaseAtivoServico<T> where T : BaseAtivoEntidade {
    IList<T> SelecionarPorDescricao(string descricao);
    IList<T> SelecionarPorDescricaoAutoComplete(string descricao);
    IList<T> SelecionarAtivosPorEdicao(int edicao);
    IList<T> SelecionarAtivosPorDescricaoAutoComplete(string descricao);
    T ObterPorNome(string nome);
  }
}