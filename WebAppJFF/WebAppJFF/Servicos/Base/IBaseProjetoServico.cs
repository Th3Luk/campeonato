﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppJFF.Classes;
using WebAppJFF.Classes.Base;

namespace WebAppJFF.Servicos.Base
{
  public interface IBaseProjetoServico<T> : IBaseServico<T> where T : BaseEntidade {
    BaseEntidade baseEnt { get; set; }
  }
}