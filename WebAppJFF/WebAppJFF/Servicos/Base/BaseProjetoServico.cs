﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppJFF.Classes.Base;
using WebAppJFF.DAO.Base;
using WebAppJFF.Classes;
using MPS.Runtime.Dependency;

namespace WebAppJFF.Servicos.Base
{
  public abstract class BaseProjetoServico<T, I> :BaseServico<T, I>, IBaseProjetoServico<T>
    where T : BaseEntidade
    where I : IBaseProjetoDAO<T> {

    public BaseEntidade baseEnt { get; set; }

    protected override I ObterInstanciaDAOEspecifico() {
      I dao = Resolver.GetImplementationOf<I>();
      dao.Base = this.baseEnt;
      return dao;
    }
  }
}