﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppJFF.Classes.Base;

namespace WebAppJFF.Servicos.Base
{
  public interface IBaseServico<T> where T : BaseEntidade {
    void Inserir(T item);
    T ObterPorId(int id);
    IList<T> Selecionar(int startRowIndex, int maximumRows, string sortOrder);
    IList<T> SelecionarPorIds(int[] ids);
    int ObterTotal();
    IList<T> SelecionarTodos();
    void Atualizar(T item);
    void Excluir(T item);
    void LimparSession();
  }
}