﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppJFF.Classes.Base;
using WebAppJFF.DAO.Base;
using MPS.Runtime.Dependency;

namespace WebAppJFF.Servicos.Base
{
  public class BaseAtivoServico<T, I> : BaseServico<T, I>, IBaseAtivoServico<T>
    where T : BaseAtivoEntidade
    where I : IBaseAtivoDAO<T> {
    #region IBaseAtivoServico<T> Members

    public IList<T> SelecionarAtivos() {
      I iBaseAtivoDAO = Resolver.GetImplementationOf<I>();
      var ret = iBaseAtivoDAO.SelecionarAtivos();
      ConfigurarTotal(ret.Count);
      return ret;
    }
    public IList<T> SelecionarAtivos(int startRowIndex, int maximumRows, string sortOrder) {
      IList<T> result;
      int total;

      I iBaseAtivoDAO = Resolver.GetImplementationOf<I>();
      iBaseAtivoDAO.SelecionarAtivos(startRowIndex, maximumRows, sortOrder,out total,out result);
      ConfigurarTotal(total);
      return result;
    }
    #endregion
  }
}