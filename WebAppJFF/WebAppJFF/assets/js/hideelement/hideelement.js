/**
 * .: HideElement(target_id) :.
 * Funçao que esconde um elemento com animaçao de slideUp e slideDown
 * Cria uma barra propria abaixo d elemento alvo, que serve de trigger
 * para a animaçao.
 * - Dependente de jQuery 1.9+, jQuery.Cookie 1.3+
 * - Compativel com FF, Chrome, IE 9, IE 8, IE 7
 * 
 * -> Bugs conhecidos:
 * - Nenhum ateh agora! =)
 * 
 * @author: Vinicius Garcia
 * @date: 2013-05-09
 */
HideElement = function(target_id, hideOnStart) {
	var container = 'conteudo'; // Container que esta acima do rodape

	var me_id = 'hideHeader-' + target_id;
	var indicador_up = '<span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>'; // '&and;';  // Setinha pra cima
	var indicador_down = '<span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>'; // '&or;'; // Setinha pra baixo
	// Verifica se este elemento deve iniciar escondido ou exibido (default: false - ou seja, nao esconder o elemento no inicio)
	if (hideOnStart === undefined)
		hideOnStart = false;

	// Se exisitir um cookie determinando que o alvo deve iniciar escondido, o escondemos
	if (($.cookie('hideHeader')) && ($.cookie('hideHeader') == 1))
		hideOnStart = true;

	// Guarda as dimensoes originais do elemento alvo...
	$('#' + target_id).attr('data-w', $('#' + target_id).width());
	$('#' + target_id).attr('data-h', $('#' + target_id).height());

	// Guarda as dimensoes originais do elemento container
	$('#' + container).attr('data-w', $('#' + container).width());
	$('#' + container).attr('data-h', $('#' + container).height());

	// Esconder o elemento, caso seja pedido
	if (hideOnStart)
		$('#' + target_id).hide();

	// Se o manipulador nao existe, criamos ele anexo ao alvo
	if ($('#' + me_id).length == 0)
	$('#' + target_id).after($('<div/>', {
	  id: me_id
	}));

	$('#' + me_id)
		.html(($('#' + target_id).is(':hidden') ? indicador_down : indicador_up))
		.css('max-width:', $('#' + target_id).attr('data-w')) // Keep same width of target
		.mousedown(function () { return false; }) // Prevent text selection
		.addClass('hideHeader')
		.click(function () {
			// Se o alvo esta escondido...
			if ($('#' + target_id).is(':hidden')) {
				// Corrige a altura da div de conteudo
				if (parseInt($('#' + container).attr('data-h')) <= $('#' + container).css('min-height'))
					$('#' + container).animate({ height: $('#' + container).height() - parseInt($('#' + target_id).attr('data-h')) }, 'slow');
				// entao, mostrar alvo...
				$('#' + target_id).slideDown('slow', function () {
					$('#' + me_id).html(indicador_up); // Mostra icone de esconder
					// Remove a preferencia de esconder o alvo...
					if ($.cookie('hideHeader'))
						$.removeCookie('hideHeader', { path: '/' });
				});
			} else {
				// Corrige a altura da div de conteudo
				if (parseInt($('#' + container).attr('data-h')) <= $('#' + container).css('min-height'))
					$('#' + container).animate({ height: $('#' + container).height() + parseInt($('#' + target_id).attr('data-h')) }, 'slow');
				// senao, esconder alvo...
				$('#' + target_id).slideUp('slow', function () {
					$('#' + me_id).html(indicador_down); // Mostra icone de exibir
					// Salva a preferencia de esconder o alvo...
					if (!$.cookie('hideHeader'))
						$.cookie('hideHeader', 1, { path: '/' });
				});
			}
		}
	);
	
	// CSS para a barra que esconde o elemento
	$('.hideHeader').css({
		'font-size'        : '8px',
		'cursor'           : 'pointer',
		'text-align'       : 'center',
		'background-color' : '#DEE1DD',
		/* Prevent Text Selection */
		'-ms-user-select'     : 'none', /* MS IE 10 */
		'-webkit-user-select' : 'none', /* webkit (safari, chrome) browsers */
		'-moz-user-select'    : 'none', /* mozilla browsers */
		'-khtml-user-select'  : 'none'  /* webkit (konqueror) browsers */
	}).hover(function() {
			$(this).css({
				'text-decoration'  : 'underline',
				'background-color' : '#C4CDC1'
			});
		},function() {
			$(this).css({
				'text-decoration'  : 'none',
				'background-color' : '#DEE1DD'
			});
	});
}
