﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppJFF.Utilitarios
{
  [Serializable]
  public class ItemJogadorTime
  {
    public int id { get; set; }
    public string Login { get; set; }
    public string Posicao { get; set; }
    public string Nome { get; set; }
    public bool Convite { get; set; }
    public string DescrConvite { get; set; }
  }
}