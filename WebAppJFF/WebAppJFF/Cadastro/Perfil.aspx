﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteBootStrap/SiteCampeonato.Master" AutoEventWireup="true" CodeBehind="Perfil.aspx.cs" Inherits="WebAppJFF.SiteBootStrap.Perfil" %>

<%@ Register Src="~/Controle/CadastroPerfil.ascx" TagName="CadastroPerfil" TagPrefix="camp" %>
<%@ Register Src="~/Controle/GridControl.ascx" TagName="GridControl" TagPrefix="camp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

  <!-- Scripts e Estilos do Wizard -->
  <script>
    // Inicializar Wizard
    function inicializarWizard() {
      if ($.isFunction(BootstrapWizard))
        BootstrapWizard($('#<%= Wizard2.ClientID %>'), $('#<%= AbaAtivaHF.ClientID %>'));
    }
    $(document).ready(function () {
      var prm = Sys.WebForms.PageRequestManager.getInstance();
      prm.add_pageLoaded(inicializarWizard); // metodo que serve pro load e request
    });
  </script>


  <div class="panel-group-margin">
    <%--Com espaço superior--%>
  </div>
  <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Conditional">
    <ContentTemplate>
      <br />
      <br />
      <asp:Button runat="server" ID="DotaButton" OnClick="DotaButton_Click" CssClass="btn btn-primary btn-lg btn-block" Text="Dota 2"></asp:Button>
      <br />
      <asp:Button runat="server" ID="LolButton" OnClick="LolButton_Click" CssClass="btn btn-default btn-lg btn-block" Text="League of Legends"></asp:Button>
      <br />
      <asp:Button runat="server" ID="CSButton" OnClick="CSButton_Click" CssClass="btn btn-default btn-lg btn-block" Text="Counter Strike GO"></asp:Button>
      <br />
      <br />
      <br />
    </ContentTemplate>
  </asp:UpdatePanel>
  <asp:UpdatePanel runat="server" ID="AutuacaoUpdatePanel" UpdateMode="Conditional">

    
    <ContentTemplate>
      <camp:GEDValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="" />
      <camp:GEDValidationSummary ID="GEDValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="" ValidationGroup="CadastroPerfilValidation" />
      <camp:GEDValidationSummary ID="GEDValidationSummary2" runat="server" CssClass="alert alert-danger" ForeColor="" ValidationGroup="CadastroTimeValidation" />
      <asp:HiddenField ID="TimeHidden" runat="server" />
      <asp:Panel ID="Panel1" runat="server" CssClass="row margin-bottom-row" Visible="true">
        <div class="col-sm-12">
          <asp:HiddenField ID="AbaAtivaHF" runat="server" />
          <asp:Panel runat="server" ID="Wizard2" class="tabs-controller row wizard-processo">
            <!-- Lista de Abas -->
            <div class="col-md-2 col-sm-12">
              <ul role="tablist" class="tab-list nav nav-pills nav-stacked">
                <li class="active"><a data-toggle="tab" href="#<%= Wizard2.ClientID %>-infcomplementares">Dados jogador</a></li>
                <li class=""><a data-toggle="tab" href="#<%= Wizard2.ClientID %>-interessados">Time</a></li>
              </ul>
            </div>
            <!-- Conteudo das Abas -->
            <div class="tab-content col-md-10 col-sm-12">
              <!-- Paginador de Abas -->
              <!-- Abas de Conteudo -->
              <div role="tabpanel" class="tab-pane active" id="<%= Wizard2.ClientID %>-infcomplementares">
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <asp:UpdatePanel ID="UpdatePanelInfComplementar" UpdateMode="Conditional" runat="server">
                      <ContentTemplate>
                        <div class="panel panel-primary">
                          <div id="LegendaDota" runat="server" visible="false" class="panel-heading">Perfil Dota 2</div>
                          <div id="LegendaLol" runat="server" visible="false" class="panel-heading">Perfil League of Legends</div>
                          <div id="LegendaCs" runat="server" visible="false" class="panel-heading">Perfil CS GO</div>
                          <div class="panel-body">
                            <fieldset>
                              <asp:HiddenField ID="PerfilJogadorHidden" runat="server" />
                              <asp:HiddenField ID="PerfilJogoHidden" runat="server" />
                              <div class="form-group">
                                <label id="Label1" class="control-label">Login no jogo</label>
                                <sup runat="server" id="Sup1" visible="true" class="icone-obrigatorio text-danger" title="Campo obrigatório">
                                  <small>
                                    <span class="ionicons ion-ios-medical fa-fw" aria-hidden="true"></span>
                                  </small>
                                </sup>
                                <asp:TextBox runat="server" CssClass="form-control" ID="LoginTextBox" placeholder="Digite seu login no ingame"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                  ErrorMessage="Campo 'Login no jogo' é obrigatório." ControlToValidate="LoginTextBox" ValidationGroup="CadastroPerfilValidation"
                                  EnableClientScript="true" runat="server" Display="Dynamic" ForeColor="" CssClass="text-danger" Enabled="true" Visible="true">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                  ErrorMessage="Campo 'Login no jogo' é obrigatório." ControlToValidate="LoginTextBox" ValidationGroup="CadastroTimeValidation"
                                  EnableClientScript="true" runat="server" Display="Dynamic" ForeColor="" CssClass="text-danger" Enabled="true" Visible="true">
                                </asp:RequiredFieldValidator>
                              </div>
                              <asp:Panel runat="server" ID="PanelDota">
                                <div class="form-group">
                                  <label id="NomeLabel" class="control-label">Posição</label>
                                  <asp:DropDownList runat="server" CssClass="form-control" ID="PosicaoDropDown"></asp:DropDownList>

                                </div>
                                <div class="form-group">
                                  <label id="Label2" class="control-label">Descrição</label>
                                  <asp:TextBox runat="server" CssClass="form-control" ID="DescricaoTextBox" TextMode="MultiLine" placeholder="Informe suas características no jogo."></asp:TextBox>
                                </div>
                                <div class="form-group">
                                  <label id="Label3" class="control-label">Tempo de jogo</label>
                                  <asp:TextBox runat="server" CssClass="form-control" ID="TempoGameTextBox" TextMode="Number" placeholder="Quantos anos tem de experiência?."></asp:TextBox>
                                </div>
                              </asp:Panel>
                            </fieldset>
                          </div>
                        </div>
                      </ContentTemplate>
                    </asp:UpdatePanel>
                  </div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="<%= Wizard2.ClientID %>-interessados">
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <asp:Panel runat="server" ID="CriarTimePanel">
                      <div class="panel panel-info">
                        <div class="panel-heading">Convites</div>
                        <div class="panel-body">
                          <camp:GridControl ID="GridConvites" runat="server" EnableBotaoImprimir="False">
                            <HeaderTemplate>
                              <th class="minimal-width">Ações</th>
                              <th data-sortexpression="Time.Nome">Time</th>
                              <th data-sortexpression="DataConvite">Data do convite</th>
                            </HeaderTemplate>
                            <RowTemplate>
                              <tr data-key="<%# Eval("id") %>">
                                <td>
                                  <asp:LinkButton ID="ConfirmarConviteButton" runat="server" Text="Confirmar" OnClick="ConfirmarConviteButton_Click" CommandArgument='<%# Eval("Id") %>' /></td>
                                <td><%# Eval("Time.Nome")  %></td>
                                <td><%# Eval("DataConvite")%></td>
                              </tr>
                            </RowTemplate>
                          </camp:GridControl>
                        </div>
                      </div>
                    </asp:Panel>
                    <div class="panel panel-primary">
                      <div class="panel-heading" id="LegendaTimeDota" runat="server" visible="false">Time Dota 2</div>
                      <div class="panel-heading" id="LegendaTimeLol" runat="server" visible="false">Time League of Legends</div>
                      <div class="panel-heading" id="LegendaTimeCS" runat="server" visible="false">Time Counter Strike GO</div>
                      <div class="panel-body">

                        <asp:Panel runat="server" ID="CadastrarTime" Visible="true">
                          <div class="row">
                            <div class="col-xs-10">
                              <div class="form-group">
                                <asp:Label runat="server" CssClass="form-control-static" ID="CapitaoTextBox" Text="TEste" Enabled="true"></asp:Label>

                              </div>
                              <asp:HiddenField ID="capitaoHidden" runat="server" />
                              <div class="form-group">
                                <label id="Label6" class="control-label">Nome Time</label>
                                <sup runat="server" id="Sup2" visible="true" class="icone-obrigatorio text-danger" title="Campo obrigatório">
                                  <small>
                                    <span class="ionicons ion-ios-medical fa-fw" aria-hidden="true"></span>
                                  </small>
                                </sup>
                                <asp:TextBox runat="server" CssClass="form-control" ID="NomeTimeTextBox" Enabled="true" placeholder="Informe o nome do time."></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                  ErrorMessage="Campo 'Nome Time' é obrigatório." ControlToValidate="NomeTimeTextBox" ValidationGroup="CadastroPerfilValidation"
                                  EnableClientScript="true" runat="server" Display="Dynamic" ForeColor="" CssClass="text-danger" Enabled="false" Visible="true">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                                  ErrorMessage="Campo 'Nome Time' é obrigatório." ControlToValidate="NomeTimeTextBox" ValidationGroup="CadastroTimeValidation"
                                  EnableClientScript="true" runat="server" Display="Dynamic" ForeColor="" CssClass="text-danger" Enabled="true" Visible="true">
                                </asp:RequiredFieldValidator>
                              </div>
                              <div class="form-group">
                                <label id="Label7" class="control-label">TAG</label>
                                <sup runat="server" id="Sup3" visible="true" class="icone-obrigatorio text-danger" title="Campo obrigatório">
                                  <small>
                                    <span class="ionicons ion-ios-medical fa-fw" aria-hidden="true"></span>
                                  </small>
                                </sup>
                                <asp:TextBox runat="server" CssClass="form-control" ID="TagTextBox" Enabled="true" placeholder="Informe tag do time."></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                  ErrorMessage="Campo 'TAG' é obrigatório." ControlToValidate="TagTextBox" ValidationGroup="CadastroPerfilValidation"
                                  EnableClientScript="true" runat="server" Display="Dynamic" ForeColor="" CssClass="text-danger" Enabled="false" Visible="true">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                  ErrorMessage="Campo 'TAG' é obrigatório." ControlToValidate="TagTextBox" ValidationGroup="CadastroTimeValidation"
                                  EnableClientScript="true" runat="server" Display="Dynamic" ForeColor="" CssClass="text-danger" Enabled="true" Visible="true">
                                </asp:RequiredFieldValidator>
                              </div>
                            </div>
                            <div class="col-xs-2">
                                    <asp:Image runat="server" ID="TimeImagem" CssClass="img-thumbnail img-responsive" ImageUrl="~/img/Axe.png"
        onerror="this.onload = null; this.src='../../img/blank-avatar.png';" />
                            </div>
                          </div>
                          <asp:Panel ID="LogoTimePanel" runat="server">
                            <fieldset class="group">
                              <legend class="control-label">Escolher Logo do Time</legend>
                              <input type="file" id="FileUpload1" runat="server" name="file" />
                              <asp:HiddenField ID="IdArquivoCriado" runat="server" />
                              <asp:Panel ID="ProgressPanel" runat="server" CssClass="progress progress-striped active" Style="display: none;">
                                <div class="progress-bar progress-bar-success"></div>
                              </asp:Panel>
                              <asp:Label ID="ArquivoUpLoadValidationLabel" runat="server" EnableViewState="false" Visible="false" Text="*" CssClass="text-danger" />
                              <span id="EnviarArquivoButtonSpan" style="display: none">
                                <asp:Button ID="EnviarArquivoButton" runat="server" CausesValidation="false" OnClick="EnviarArquivoButton_Click" CssClass="btn btn-default" />
                              </span>
                            </fieldset>
                          </asp:Panel>
                          <br />
                          <asp:Button runat="server" ID="ButtonAddTime" OnClick="ButtonAddTime_Click" CssClass="btn btn-primary btn-lg btn-block" Text="Criar Time" CausesValidation="true" ValidationGroup="CadastroTimeValidation"></asp:Button>
                          <br />
                        </asp:Panel>
                        <asp:Panel runat="server" ID="JogadoresPanel" Visible="false">
                          <div runat="server" id="ConvidarButton" class="text-right margin-bottom-row" visible="true">
                            <button type="button" id="PesqDetalhadabutton" visible="true" class="btn btn-primary" data-toggle="modal" data-target="#<%= FiltrosPesqDetalhadaModal.ClientID %>">
                              <asp:Label ID="PesqDetalhadaLabel" runat="server" Visible="true" Text="Convidar Jogadores"></asp:Label>
                            </button>
                          </div>
                          <br />
                          <br />
                          <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                              <div class="panel panel-info">
                                <div class="panel-heading">Jogadores</div>
                                <div class="panel-body">
                                  <camp:GridControl ID="GridJogadores" runat="server" EnableBotaoImprimir="False" OnAposItemCriado="GridJogadores_AposItemCriado">
                                    <HeaderTemplate>
                                      <th class="minimal-width">Ações</th>
                                      <th data-sortexpression="Login">Login</th>
                                      <th data-sortexpression="Posicao">Posição</th>
                                      <th data-sortexpression="Posicao">Nome</th>
                                      <th data-sortexpression="Convite">Convite</th>
                                    </HeaderTemplate>
                                    <RowTemplate>
                                      <tr data-key="<%# Eval("id") %>">
                                        <td>
                                          <asp:LinkButton ID="ExcluirJogadorButton" runat="server" Text="Expulsar" OnClick="ExcluirJogadorButton_Click" /></td>
                                        <td><%# Eval("Login")  %></td>
                                        <td><%# Eval("Posicao")%></td>
                                        <td><%# Eval("Nome")%></td>
                                        <td><%# Eval("DescrConvite")%></td>
                                      </tr>
                                    </RowTemplate>
                                  </camp:GridControl>
                                </div>
                              </div>
                            </ContentTemplate>
                          </asp:UpdatePanel>
                        </asp:Panel>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Paginador de Abas -->
              <div class="text-right margin-bottom-row">
                <asp:Button runat="server" OnClick="SalvarButton_Click" ID="SalvarButton" type="submit" class="btn btn-primary btn-lg" ToolTip="Salvar as informações." Text="Salvar" CausesValidation="true" ValidationGroup="CadastroPerfilValidation"></asp:Button>
              </div>
              <br />
              <hr />
            </div>
          </asp:Panel>
        </div>
      </asp:Panel>
    </ContentTemplate>
  </asp:UpdatePanel>



  <camp:BootsModalControl runat="server" ID="FiltrosPesqDetalhadaModal" CssClass="">
    <HeaderTemplate>Pesquisa de jogadores</HeaderTemplate>
    <ContentTemplate>
      <asp:UpdatePanel runat="server" ID="UpdatePanel3" UpdateMode="Conditional">
        <ContentTemplate>

          <div class="row">
            <div class="col-xs-12">
              <camp:GEDValidationSummary ID="GEDValidationSummary3" runat="server" CssClass="alert alert-danger" ForeColor="" />
              <div class="form-group">
                <label id="Label9" class="control-label">Login no jogo</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="LoginPesquisaTextBox" placeholder="Digite o login do jogador"></asp:TextBox>
              </div>
              <div class="form-group">
                <label id="Label5" class="control-label">Faculdade</label>
                <asp:DropDownList runat="server" CssClass="form-control" ID="FaculdadePesquisaDropDown"></asp:DropDownList>
              </div>
              <div class="form-group">
                <label id="Label8" class="control-label">Posição</label>
                <asp:DropDownList runat="server" CssClass="form-control" ID="PosicaoPesquisaDropDown"></asp:DropDownList>
              </div>
              <div class="form-group">
                <asp:Button runat="server" ID="PesquisarJogadoresButton" OnClick="PesquisarJogadoresButton_Click" CssClass="btn btn-primary btn-lg btn-block" Text="Pesquisar" CausesValidation="true" ValidationGroup="CadValidation"></asp:Button>
              </div>

              <camp:GridControl ID="BuscaJogadorGridControl" runat="server" EnableBotaoImprimir="False">
                <HeaderTemplate>
                  <th class="minimal-width">Ações</th>
                  <th data-sortexpression="Nome">Login</th>
                  <th data-sortexpression="IdPosicao">Posição</th>
                </HeaderTemplate>
                <RowTemplate>
                  <tr data-key="<%# Eval("Id") %>">
                    <td>
                      <asp:Button ID="AdicionarJogadorButton" runat="server" Text="Convidar" CommandArgument='<%# Eval("Id") %>' OnClick="AdicionarJogadorButton_Click" CssClass="btn-link" /></td>
                    <td><%# Eval("Nome")  %></td>
                    <td><%# RetornaPosicao(Eval("IdPosicao"))%></td>
                  </tr>
                </RowTemplate>
              </camp:GridControl>
            </div>
          </div>
        </ContentTemplate>
      </asp:UpdatePanel>
    </ContentTemplate>
  </camp:BootsModalControl>
</asp:Content>
