﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteBootStrap/SiteCampeonato.Master" AutoEventWireup="true" CodeBehind="CadastroCampeonato.aspx.cs" Inherits="WebAppJFF.SiteBootStrap.Cadastro.CadastroCampeonato" %>

<%@ Register Src="~/Controle/Calendario.ascx" TagName="Calendario" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <div class="panel-group-margin">
    <%--Com espaço superior--%>
  </div>
  <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
      <hr />

      <h2 class="text-center">Cadastro Campeonato</h2>

      <camp:GEDValidationSummary ID="GEDValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="" />
      <camp:GEDValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="" ValidationGroup="CadValidation" />

      <div class="col-xs-12">
        <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
          <ContentTemplate>
            <div class="panel panel-primary form-group-zero-margin-bottom" runat="server" id="Div1">
              <div class="panel-heading">Dados Perfil</div>
              <div class="panel-body">

                <div class="row">

                  <div class="col-xs-2">
                    <asp:Image runat="server" ID="Ticketimagem" CssClass="img-thumbnail img-responsive" ImageUrl="~/img/blank-ticket.png"
                      onerror="this.onload = null; this.src='../../img/blank-ticket.png';" />
                  </div>

                  <div class="col-xs-10">

                    <div class="form-group">
                      <label id="NomeLabel" class="control-label">Nome</label>
                      <sup runat="server" id="Sup2" visible="true" class="icone-obrigatorio text-danger" title="Campo obrigatório">
                        <small>
                          <span class="ionicons ion-ios-medical fa-fw" aria-hidden="true"></span>
                        </small>
                      </sup>
                      <asp:TextBox runat="server" CssClass="form-control" ID="NomeTextBox" placeholder="Digite seu Nome"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="CadValidation"
                        ErrorMessage="Campo 'Nome' é obrigatório." ControlToValidate="NomeTextBox"
                        EnableClientScript="true" runat="server" Display="Dynamic" ForeColor="" CssClass="text-danger" Enabled="false" Visible="true">
                      </asp:RequiredFieldValidator>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Descrição Resumida</label>
                      <sup runat="server" id="Sup1" visible="true" class="icone-obrigatorio text-danger" title="Campo obrigatório">
                        <small>
                          <span class="ionicons ion-ios-medical fa-fw" aria-hidden="true"></span>
                        </small>
                      </sup>
                      <asp:TextBox runat="server" CssClass="form-control" ID="DescricaoResumidaTextBox" placeholder="Descrição resumida"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="CadValidation"
                        ErrorMessage="Campo 'Descrição Resumida' é obrigatório." ControlToValidate="DescricaoResumidaTextBox"
                        EnableClientScript="true" runat="server" Display="Dynamic" ForeColor="" CssClass="text-danger" Enabled="false" Visible="true">
                      </asp:RequiredFieldValidator>
                    </div>

                    <div class="form-group form-group-zero-margin-bottom">
                      <asp:Label ID="Label7" runat="server" CssClass="control-label" Text="Data campeonato"></asp:Label>
                      <div class="row">
                        <div class="col-xs-4">
                          <uc1:Calendario ID="DataInicioCampeonato" runat="server" Enabled="true" EnableDataHora="false" Obrigatorio="false" ExibirLabel="true" ControleLabelText="Inicio" />
                        </div>
                        <div class="col-xs-1 separador-calendarios">a</div>
                        <div class="col-xs-4">
                          <uc1:Calendario ID="DataFimCampeonato" runat="server" Enabled="true" EnableDataHora="false" Obrigatorio="false" ExibirLabel="true" ControleLabelText="Fim" />
                        </div>
                      </div>
                    </div>

                    <div class="form-group form-group-zero-margin-bottom">
                      <asp:Label ID="Label1" runat="server" CssClass="control-label" Text="Data inscrições"></asp:Label>
                      <div class="row">
                        <div class="col-xs-4">
                          <uc1:Calendario ID="DataInicioInscricoes" runat="server" Enabled="true" EnableDataHora="false" Obrigatorio="false" ExibirLabel="true" ControleLabelText="Inicio" />
                        </div>
                        <div class="col-xs-1 separador-calendarios">a</div>
                        <div class="col-xs-4">
                          <uc1:Calendario ID="DataFimInscricoes" runat="server" Enabled="true" EnableDataHora="false" Obrigatorio="false" ExibirLabel="true" ControleLabelText="Fim" />
                        </div>
                      </div>
                    </div>

                    <div class="row">

                      <div class="col-xs-4">
                        <div class="form-group">
                          <label id="Label3" class="control-label">Número Times</label>
                          <asp:TextBox runat="server" CssClass="form-control" ID="TempoGameTextBox" TextMode="Number" placeholder="Número de times participantes"></asp:TextBox>
                        </div>
                      </div>

                      <div class="col-xs-4">
                        <div class="form-group">
                          <label id="Label2" class="control-label">Valor Inscrição</label>
                          <asp:TextBox runat="server" CssClass="form-control" ID="TextBox1" placeholder="Valor da inscrição"></asp:TextBox>
                        </div>
                      </div>

                      <div class="col-xs-4">
                        <div class="form-group">
                          <label id="Label4" class="control-label">Valor Premio</label>
                          <asp:TextBox runat="server" CssClass="form-control" ID="TextBox2" placeholder="Valor do premio"></asp:TextBox>
                        </div>
                      </div>

                    </div>

                    <fieldset class="group form-group-zero-margin-bottom">
                      <legend class="control-label">Escolher Imagem do Ticket do Campeonato</legend>
                      <asp:FileUpload ID="ArquivoUpLoad" runat="server" onchange="javascript: ArquivoUpload_Changed(this);" />
                      <asp:Panel runat="server" ID="PanelUploadMsg" EnableViewState="false" CssClass="alert alert-warning" Style="display: none;">
                        <asp:Label runat="server" ID="LabelUploadMsg" CssClass="control-label" />
                      </asp:Panel>
                      <hr />
                    </fieldset>

                  </div>

                </div>

              </div>
            </div>
            <br />
          </ContentTemplate>
        </asp:UpdatePanel>

        <asp:Button runat="server" ID="SalvarButton" OnClick="SalvarButton_Click" CssClass="btn btn-primary btn-lg btn-block" Text="Salvar" CausesValidation="true" ValidationGroup="CadValidation"></asp:Button>

        <br />

      </div>
    </ContentTemplate>
  </asp:UpdatePanel>



</asp:Content>
