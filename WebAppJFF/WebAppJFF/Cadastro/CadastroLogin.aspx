﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteBootStrap/SiteCampeonato.Master" AutoEventWireup="true" CodeBehind="CadastroLogin.aspx.cs" Inherits="WebAppJFF.SiteBootStrap.Cadastro.CadastroLogin" %>

<%@ Register Src="~/Controle/CadastroPerfil.ascx" TagName="CadastroPerfil" TagPrefix="camp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <div class="panel-group-margin">
    <%--Com espaço superior--%>
  </div>

  <hr />

  <h2 class="text-center">Cadastro Usuário</h2>

  <camp:GEDValidationSummary ID="GEDValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="" />
  <camp:GEDValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="" ValidationGroup="CadValidation" />

  <div class="col-xs-12">

    <div class="panel panel-primary" runat="server" id="Div1">
      <div class="panel-heading">Dados Perfil</div>
      <div class="panel-body">
        <camp:CadastroPerfil runat="server" ID="CadastroPerfil" ValidationGroup="CadValidation"></camp:CadastroPerfil>
      </div>
    </div>
    <br />

    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
      <ContentTemplate>
        <div class="panel panel-primary" runat="server" id="AcessoPanel">
          <div class="panel-heading">Dados Acesso</div>
          <div class="panel-body">

            <%--Controle do campo login--%>
            <div class="col-xs-7">

              <asp:Panel CssClass="form-group has-success has-feedback" runat="server" Visible="false" ID="LoginSucesso">
                <label id="Label1" class="control-label">Login</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="LoginSucessoTextBox" placeholder="Escolha seu login" OnTextChanged="LoginTextBox_TextChanged" AutoPostBack="True"></asp:TextBox>
                <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
                <span id="inputSuccess2Status" class="sr-only">(success)</span>
              </asp:Panel>

              <asp:Panel class="form-group has-error has-feedback" runat="server" Visible="false" ID="LoginErro">
                <label class="control-label" for="inputError2">Login</label>
                <sup runat="server" id="Sup2" visible="true" class="icone-obrigatorio text-danger" title="Campo obrigatório">
                  <small>
                    <span class="ionicons ion-ios-medical fa-fw" aria-hidden="true"></span>
                  </small>
                </sup>
                <asp:TextBox runat="server" CssClass="form-control" ID="LoginError" placeholder="Escolha seu login" OnTextChanged="LoginTextBox_TextChanged" AutoPostBack="True"></asp:TextBox>
                <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                <span id="inputError2Status" class="sr-only">(error)</span>
                <div class="alert alert-danger" role="alert">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  <span class="sr-only">Error:</span>
                  <asp:Label Text="Login já esta em uso." runat="server" ID="ErroLogin"></asp:Label>
                </div>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="CadValidation"
                  ErrorMessage="Campo 'Login' é obrigatório." ControlToValidate="LoginError"
                  EnableClientScript="true" runat="server" Display="Dynamic" ForeColor="" CssClass="text-danger" Enabled="true" Visible="true">
                </asp:RequiredFieldValidator>
              </asp:Panel>

              <asp:Panel CssClass="form-group" runat="server" ID="LoginPadrao">
                <label id="Label2" class="control-label">Login</label>
                <sup runat="server" id="Sup1" visible="true" class="icone-obrigatorio text-danger" title="Campo obrigatório">
                  <small>
                    <span class="ionicons ion-ios-medical fa-fw" aria-hidden="true"></span>
                  </small>
                </sup>
                <asp:TextBox runat="server" CssClass="form-control" ID="LoginTextBox" placeholder="Escolha seu login" OnTextChanged="LoginTextBox_TextChanged" AutoPostBack="True"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="CadValidation"
                  ErrorMessage="Campo 'Login' é obrigatório." ControlToValidate="LoginTextBox"
                  EnableClientScript="true" runat="server" Display="Dynamic" ForeColor="" CssClass="text-danger" Enabled="true" Visible="true">
                </asp:RequiredFieldValidator>
              </asp:Panel>

            </div>

            <%--Controle do campo senha--%>
            <div class="col-xs-5">
              <div class="form-group">
                <label id="Label10" class="control-label">Senha</label>
                <sup runat="server" id="Sup3" visible="true" class="icone-obrigatorio text-danger" title="Campo obrigatório">
                  <small>
                    <span class="ionicons ion-ios-medical fa-fw" aria-hidden="true"></span>
                  </small>
                </sup>
                <asp:TextBox runat="server" ID="SenhaTxt" CssClass="form-control" TextMode="Password" AutoCompleteType="Disabled" autocomplete="off" TabIndex="2"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="CadValidation"
                  ErrorMessage="Campo 'Senha' é obrigatório." ControlToValidate="SenhaTxt"
                  EnableClientScript="true" runat="server" Display="Dynamic" ForeColor="" CssClass="text-danger" Enabled="true" Visible="true">
                </asp:RequiredFieldValidator>
              </div>
            </div>

          </div>
        </div>
        <br />
      </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Button runat="server" ID="SalvarButton" OnClick="SalvarButton_Click" CssClass="btn btn-primary btn-lg btn-block" Text="Salvar" CausesValidation="true" ValidationGroup="CadValidation"></asp:Button>

    <br />

  </div>



</asp:Content>
