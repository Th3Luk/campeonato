﻿using Interfaces.Servicos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
using Interfaces;
using MPS.Web.UI.WebControls;
using MPS.Web.UI;
using MPS.Runtime.Dependency;

namespace WebAppJFF
{
  public partial class SiteCampeonato : System.Web.UI.MasterPage
  {

    private string mensagemErroLogin = "Usuário desconhecido ou senha incorreta.";

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        ValidaUsuario();
        LabelSiteMap.Visible = SiteMapPath1.Controls.Count > 0;
      }
    }

    private void ValidaUsuario()
    {
      if (Request.IsAuthenticated)
      {
        var ident = Page.User.Identity as FormsIdentity;
        string[] teste = ident.Name.ToString().Split('/');
        string login = teste[0];
        LoginValidoPanel.Visible = true;
        UsuarioButton.Text = login;
        LoginPanel.Visible = false;
        MenuPerfil.Visible = true;
        MenuCadastro.Visible = false;
        MenuCadastrado.Visible = true;
      }
      else
      {
        LoginValidoPanel.Visible = false;
        UsuarioButton.Text = "";
        LoginPanel.Visible = true;
        MenuPerfil.Visible = false;
        MenuCadastro.Visible = true;
        MenuCadastrado.Visible = false;
      }
    }

    protected void LoginButton_Click(object sender, EventArgs e)
    {
      try
      {
        IUsuarioServico servico = Resolver.GetImplementationOf<IUsuarioServico>();
        Usuario usuario = servico.ObterPorLogin(LoginTextBox.Text);

        if (usuario == null || !usuario.Ativo)
          throw new Exception("Usuário inválido ou inativo.");

        var autenticado = servico.VerificarAutenticidadeSenha(usuario, SenhaTextBox.Text);
        if (!autenticado)
          throw new Exception(mensagemErroLogin);

        LoginValidoPanel.Visible = true;
        UsuarioButton.Text = LoginTextBox.Text;
        LoginPanel.Visible = false;
        string adm;
        if (usuario.Admin)
          adm = "adm";
        else        
          adm = "user";

        FormsAuthentication.SetAuthCookie(LoginTextBox.Text + "/" + adm, false);
        ValidaUsuario();
        Response.Redirect(FormsAuthentication.DefaultUrl, true);
      }
      catch (Exception ex)
      {
        ErrorSummary.AddError(this.Page, ex.Message);
      }
    }

    protected void SairButton_Click(object sender, EventArgs e)
    {
      FormsAuthentication.SignOut();
      ValidaUsuario();
      Response.Redirect(FormsAuthentication.DefaultUrl, true);

    }
  }
}