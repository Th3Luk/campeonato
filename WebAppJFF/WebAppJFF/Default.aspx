﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteCampeonato.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebAppJFF.Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img class="first-slide" src="../img/img-bootstrap/dota.jpg" alt="First slide">
                <div class="container">
                    <div class="carousel-caption">
                        <p><a class="btn btn-lg btn-primary pull-right" href="#" role="button">Inscreva-se</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <img class="second-slide" src="../img/img-bootstrap/lol.png" alt="Second slide">
                <div class="container">
                    <div class="carousel-caption">
                        <p><a class="btn btn-lg btn-primary pull-right" href="#" role="button">Inscreva-se</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <img class="third-slide" src="../img/img-bootstrap/cs.jpg" alt="Third slide">
                <div class="container">
                    <div class="carousel-caption">
                        <p><a class="btn btn-lg btn-primary pull-right" href="#" role="button">Inscreva-se</a></p>
                    </div>
                </div>
            </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- /.carousel -->

    <!-- START THE FEATURETTES -->

    <div class="container marketing">
        <!-- Three columns of text below the carousel -->
        <div class="row">
            <div class="col-lg-4">
                <img class="img-circle" src="../img/img-bootstrap/poop.jpeg" alt="Generic placeholder image" width="140" height="140">
                <h2>Idéia</h2>
                <p>Quem sabe ter esse painel com algum destaque, como por exemplo algum time que venceu ou algum jogador revelação, etc.</p>
                <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
                <h2>Heading</h2>
                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
                <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
                <h2>Heading</h2>
                <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
                <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
            </div>
            <!-- /.col-lg-4 -->
        </div>
        <!-- /.row -->

<%--        <hr class="featurette-divider">--%>

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Propaganda. <span class="text-muted">Idéia para deixar um espaço reservado a anuncios.</span></h2>
                <p class="lead">Algum texto descritivo sobre o anunciante.</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" alt="Generic placeholder image" src="../img/img-bootstrap/IG.jpg">
            </div>
        </div>
    </div>

</asp:Content>
