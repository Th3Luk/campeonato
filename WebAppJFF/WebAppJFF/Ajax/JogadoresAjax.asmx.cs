﻿using Classes;
using Interfaces.Servicos;
using MPS.Runtime.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace WebAppJFF.Ajax {
  /// <summary>
  /// Summary description for JogadoresAjax
  /// </summary>
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [System.ComponentModel.ToolboxItem(false)]
  // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
  [System.Web.Script.Services.ScriptService]
  public class JogadoresAjax : System.Web.Services.WebService {

    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    [WebMethod]
    public JogadoresView obterPorId(int id) {
      ITimeServico servico = Resolver.GetImplementationOf<ITimeServico>();
      Time time = servico.ObterPorId(id);

      JogadoresView pv = new JogadoresView();
  
      return pv;
    }

    public class JogadoresView {
      public string Id { get; set; }
      public string Numero { get; set; }
      public string Assunto { get; set; }
      public string Observacao { get; set; }
      public string DataAutuacao { get; set; }
      public string LocalAtual { get; set; }
      public string Situacao { get; set; }
      public IList<InteressadoView> Interessados { get; set; }
      public bool Urgente { get; set; }
      public bool Virtual { get; set; }
      public bool Sigiloso { get; set; }
      public string Lembretes { get; set; }
      public string Prazos { get; set; }
      public string Avisos { get; set; }
      public string Impedimentos { get; set; }
    }

    public class InteressadoView {
      public string Qualificacao { get; set; }
      public string Nome { get; set; }
    }
  }
}