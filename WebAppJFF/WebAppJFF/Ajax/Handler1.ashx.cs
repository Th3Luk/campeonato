﻿using MPS.Runtime.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Interfaces.Servicos;

namespace WebAppJFF.Ajax
{
  /// <summary>
  /// Summary description for Handler1
  /// </summary>
  public class Handler1 : IHttpHandler
  {

    public void ProcessRequest(HttpContext context)
    {
      string idUsuario = context.Request.QueryString["idUsuario"];
      if (string.IsNullOrEmpty(idUsuario))
        throw new ArgumentNullException("idUsuario");
      IPerfilJogadorServico perfilServico = Resolver.GetImplementationOf<IPerfilJogadorServico>();
      byte[] imagem = perfilServico.ObterPorId(Int32.Parse(idUsuario)).Imagem;
      if (imagem != null)
      {
        context.Response.ContentType = "application/octet-stream";
        context.Response.OutputStream.Write(imagem, 0, imagem.Length);
      }
    }

    public bool IsReusable
    {
      get
      {
        return false;
      }
    }
  }
}