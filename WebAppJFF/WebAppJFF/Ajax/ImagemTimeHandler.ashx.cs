﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MPS.Runtime.Dependency;
using Interfaces.Servicos;
namespace WebAppJFF.Ajax {
  /// <summary>
  /// Summary description for $codebehindclassname$
  /// </summary>
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  public class ImagemTimeHandler : IHttpHandler
  {

    public void ProcessRequest(HttpContext context)
    {
      string idTime = context.Request.QueryString["idTime"];
      ITimeServico timeServico = Resolver.GetImplementationOf<ITimeServico>();
      byte[] imagem = timeServico.ObterPorId(Int32.Parse(idTime)).Logo;
      if (imagem != null) {
        context.Response.ContentType = "application/octet-stream";
        context.Response.OutputStream.Write(imagem, 0, imagem.Length);
      }
    }

    public bool IsReusable
    {
      get
      {
        return false;
      }
    }
  }
}
