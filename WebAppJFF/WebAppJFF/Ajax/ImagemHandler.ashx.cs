﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MPS.Runtime.Dependency;
using Interfaces.Servicos;

namespace MPS.GED.Portal.Ajax {
  /// <summary>
  /// Summary description for $codebehindclassname$
  /// </summary>
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  public class ImagemHandler : IHttpHandler {

    public void ProcessRequest(HttpContext context) {
      string idUsuario = context.Request.QueryString["idUsuario"];
      if (string.IsNullOrEmpty(idUsuario))
        throw new ArgumentNullException("idUsuario");
      IPerfilJogadorServico perfilServico = Resolver.GetImplementationOf<IPerfilJogadorServico>();
      byte[] imagem = perfilServico.ObterPorId(Int32.Parse(idUsuario)).Imagem;
      if (imagem != null) {
        context.Response.ContentType = "application/octet-stream";
        context.Response.OutputStream.Write(imagem, 0, imagem.Length);
      }
    }

    public bool IsReusable {
      get {
        return false;
      }
    }
  }
}
