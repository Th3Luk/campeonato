﻿using MPS.Runtime.Dependency;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Classes;
using Interfaces.Servicos;


namespace MPS.GED.WebSite.Ajax {
  /// <summary>
  /// Summary description for FileUploadHandler
  /// </summary>
  public class FileUploadHandler : IHttpHandler {

    public void ProcessRequest(HttpContext context) {
      if (context.Request.Files.Count > 0) {
        string path = context.Server.MapPath("~/Temp");
        if (!Directory.Exists(path))
          Directory.CreateDirectory(path);

        var file = context.Request.Files[0];
        string fileName = Path.Combine(path, file.FileName);
        file.SaveAs(fileName);
        IArqTempServico servico = Resolver.GetImplementationOf<IArqTempServico>();

        ArqTemp arq = new ArqTemp();
        arq.Imagem = File.ReadAllBytes(fileName);
        servico.Inserir(arq);
        File.Delete(fileName);

        context.Response.ContentType = "text/plain";
        var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        var result = new { Id = arq.Id };
        context.Response.Write(serializer.Serialize(result));
      }
    }

    public bool IsReusable {
      get {
        return false;
      }
    }
  }
}