﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using MPS.Runtime.Dependency;
using NHibernate.Cfg;
using FluentNHibernate;
using FluentNHibernate.Conventions.Helpers;



namespace WebAppJFF
{
  public class Global : System.Web.HttpApplication
  {

    protected void Application_Start(object sender, EventArgs e)
    {

      // Configura o Service Locator
      List<Type> interfaces = new List<Type>();
      List<Type> servicos = new List<Type>();

      // carrega os contratos
      Assembly[] interfacesAssemblies = new Assembly[] {
        Assembly.Load(new AssemblyName("Interfaces"))     };
      foreach (Assembly assembly in interfacesAssemblies)
      {
        foreach (Type tipo in assembly.GetTypes())
          if (tipo.IsInterface)
            interfaces.Add(tipo);
      }

      // carrega as classes de 
      Assembly[] servicosAssemblies = new Assembly[] {
          Assembly.Load(new AssemblyName("Servicos")),
          Assembly.Load(new AssemblyName("DAO"))
      };
      try
      {
        foreach (Assembly assembly in servicosAssemblies)
        {
          foreach (Type tipo in assembly.GetTypes())
            if (!tipo.IsAbstract && tipo.GetConstructor(Type.EmptyTypes) != null && tipo.GetInterfaces().Length > 0)
              servicos.Add(tipo);
        }
      }
      catch (ReflectionTypeLoadException exc)
      {
        foreach (Exception ex1 in exc.LoaderExceptions)
          System.Diagnostics.Debug.WriteLine("\n--Loader Exception: " + ex1.Message);
      }


      // registra as 
      SimpleDependencyResolver resolver = new SimpleDependencyResolver();
      foreach (Type contrato in interfaces)
      {
        foreach (Type servico in servicos)
        {
          foreach (Type intf in servico.GetInterfaces())
          {
            if (intf == contrato)
            {
              resolver.RegisterImplementationOf(contrato, Activator.CreateInstance(servico));
              break;
            }
          }
        }
      }

      // registra o provedor para  de dependencias
      Resolver.AddResolver(resolver);
    }

    protected void Application_End(object sender, EventArgs e)
    {

    }

    protected void Application_Error(object sender, EventArgs e)
    {
      //Exception exception = Server.GetLastError();
      //int codigoErro = 0;

      //if (exception.Message.Contains("Unable to connect to the remote server")) {
      //  codigoErro = 1;
      //} else {
      //  //testar antes se eh um erro http
      //  if (exception is HttpException) {
      //    HttpException httpException = (HttpException)exception;
      //    codigoErro = httpException.GetHttpCode();
      //  }
      //}

      //Response.Redirect("~/ErroPadrao.aspx?erro=" + codigoErro.ToString());
    }
  }
}