﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NoticiaResumidaControle.ascx.cs" Inherits="WebAppJFF.Controle.NoticiaResumidaControle" %>
<div class="divNoticiaResumida">
    <asp:Image runat="server" ID="imgImportante" ImageUrl="~/img/importante.png" CssClass="imagemImportante" ToolTip="Noticia importante!" />
    <asp:Label runat="server" ID="lblTituloNoticia" CssClass="labelTituloNoticiaResumida"></asp:Label>
    <br />
    <div class="divTextoNoticiaResumida">
        <asp:Label runat="server" ID="lblTextoNoticia" CssClass="textoNoticia"></asp:Label>
    </div>
</div>
