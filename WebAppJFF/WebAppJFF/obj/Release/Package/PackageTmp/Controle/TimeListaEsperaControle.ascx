﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimeListaEsperaControle.ascx.cs" Inherits="WebAppJFF.Controle.TimeListaEsperaControle" %>
<div class="divControleListaEspera">
    <table>
        <tr>
            <td class="colunaTimeEspera">
                <asp:Label runat="server" ID="lblNomeTime" CssClass="labelTime"></asp:Label>
            </td>
            <td class="colunaTimeEspera">
                <asp:Label runat="server" ID="Label1" CssClass="labelCapitao" Text="| Capitão: "></asp:Label><asp:Label runat="server" ID="lblNomeCapitao" CssClass="labelCapitao"></asp:Label>
                <a runat="server" id="linkCapitao" class="linkCapitao" target="_blank">
                    <asp:Image runat="server" ID="imgLogoSteam" ImageUrl="~/img/logoSteam.png" CssClass="logoSteamCapitaoEspera" />
                </a>
            </td>
        </tr>
    </table>
</div>
