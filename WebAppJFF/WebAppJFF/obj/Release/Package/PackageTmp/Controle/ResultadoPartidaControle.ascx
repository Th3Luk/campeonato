﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResultadoPartidaControle.ascx.cs" Inherits="WebAppJFF.Controle.ResultadoPartidaControle" %>
<div>
    <table style="width:100%;">
        <tr>
            <td style="width: 200px;">
                <asp:Label runat="server" ID="Label1" Text="ID da partida" CssClass="labelLBLResutladoPartida"></asp:Label>
                <br />
                <asp:Label runat="server" ID="lblIdPartida" CssClass="labelIDPartidaResutladoPartida"></asp:Label>
            </td>
            <td style="text-align:center;">
                <asp:Label runat="server" ID="Label5" Text="Vitória: " CssClass="labelVitoriaResutladoPartida"></asp:Label>
                <asp:Label runat="server" ID="lblEquipeVencedora" Text="Radiantes" CssClass="labelTimeVencedorResutladoPartida"></asp:Label>
            </td>
            <td style="text-align:right; width: 200px;">
                <div class="fb-like" data-href="http://localhost:58501/ResultadoPartida.aspx" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="Label2" Text="Data" CssClass="labelLBLResutladoPartida"></asp:Label>
                <br />
                <asp:Label runat="server" ID="lblHorarioPartida" Text="21/05/2014 - 20:00" CssClass="labelDataPartidaResutladoPartida"></asp:Label>
            </td>
            <td></td>
        </tr>
    </table>
    <br />
    <asp:Image runat="server" ID="imgLogoTime1" Width="45" Height="25" ImageUrl="~/img/TimeSemLogo.png" />
    <asp:Label runat="server" ID="lblTime1" Text="Radiantes" CssClass="labelTimeResutladoPartidaRAD"></asp:Label>
    <table style="font-size: 15px; background-color: #444444;">
        <tr style="background-color: #5b5a5a; font-weight: bold;">
            <td style="width: 47px;">
                <asp:Label runat="server" ID="lblRADHeroi" Text="Herói" ToolTip="Herói escolhido"></asp:Label></td>
            <td style="width: 200px; min-width: 200px;">
                <asp:Label runat="server" ID="lblRADJogador" Text="Jogador" ToolTip="Nick do jogador"></asp:Label></td>
            <td style="width: 27px;"></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADNivel" Text="Nível" ToolTip="Nível do jogador ao final da partida"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADKills" Text="K" ToolTip="Número de heróis mortos pelo jogador na partida"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADDeaths" Text="D" ToolTip="Número de vezes que o jogador morreu na partida"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADAssistencia" Text="A" ToolTip="Total de assistências realizados pelo jogador"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADLastHit" Text="LH" ToolTip="Total de ultimo hits (Last Hit) executados pelo jogador"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADDeny" Text="Deny" ToolTip="Total de negações(deny) executados pelo jogador"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADOuroPorMinuto" Text="GPM" ToolTip="Média de ouro obtida pelo jogador por minuto"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADOuroTotal" Text="GT" ToolTip="Ouro total obtido pelo jogador"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADXPPorMinuto" Text="XPM" ToolTip="Média de experiência obtida por minuto"></asp:Label></td>
            <td style="width: 230px; min-width: 230px;">
                <asp:Label runat="server" ID="lblRADItens" Text="Itens" ToolTip="Itens do jogador ao final da partida"></asp:Label></td>
        </tr>
        <tr style="background-color: #757575; height: 25px;">
            <td>
                <asp:Image runat="server" ID="imgRADHeroiJogador1" CssClass="imagemHeroi" /></td>
            <td>
                <asp:Label runat="server" ID="lblRADNomeJogador1"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgRADFotoJogador1" CssClass="imagemFotoJogador" /></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADNivelJogador1"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADKillsJogador1"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADDeathsJogador1"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADAssistenciaJogador1"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADLastHitJogador1"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADDenyJogador1"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADOuroPorMinutoJogador1"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADOuroTotalJogador1"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADXPPorMinutoJogador1"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgRADItem1Jogador1" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem2Jogador1" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem3Jogador1" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem4Jogador1" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem5Jogador1" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem6Jogador1" CssClass="imagemItemHeroi" />
            </td>
        </tr>
        <tr style="background-color: #929292; height: 25px;">
            <td>
                <asp:Image runat="server" ID="imgRADHeroiJogador2" CssClass="imagemHeroi" /></td>
            <td>
                <asp:Label runat="server" ID="lblRADNomeJogador2"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgRADFotoJogador2" CssClass="imagemFotoJogador" /></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADNivelJogador2"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADKillsJogador2"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADDeathsJogador2"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADAssistenciaJogador2"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADLastHitJogador2"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADDenyJogador2"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADOuroPorMinutoJogador2"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADOuroTotalJogador2"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADXPPorMinutoJogador2"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgRADItem1Jogador2" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem2Jogador2" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem3Jogador2" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem4Jogador2" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem5Jogador2" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem6Jogador2" CssClass="imagemItemHeroi" />
            </td>
        </tr>
        <tr style="background-color: #757575; height: 25px;">
            <td>
                <asp:Image runat="server" ID="imgRADHeroiJogador3" CssClass="imagemHeroi" /></td>
            <td>
                <asp:Label runat="server" ID="lblRADNomeJogador3"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgRADFotoJogador3" CssClass="imagemFotoJogador" /></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADNivelJogador3"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADKillsJogador3"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADDeathsJogador3"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADAssistenciaJogador3"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADLastHitJogador3"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADDenyJogador3"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADOuroPorMinutoJogador3"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADOuroTotalJogador3"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADXPPorMinutoJogador3"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgRADItem1Jogador3" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem2Jogador3" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem3Jogador3" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem4Jogador3" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem5Jogador3" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem6Jogador3" CssClass="imagemItemHeroi" />
            </td>
        </tr>
        <tr style="background-color: #929292; height: 25px;">
            <td>
                <asp:Image runat="server" ID="imgRADHeroiJogador4" CssClass="imagemHeroi" /></td>
            <td>
                <asp:Label runat="server" ID="lblRADNomeJogador4"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgRADFotoJogador4" CssClass="imagemFotoJogador" /></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADNivelJogador4"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADKillsJogador4"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADDeathsJogador4"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADAssistenciaJogador4"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADLastHitJogador4"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADDenyJogador4"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADOuroPorMinutoJogador4"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADOuroTotalJogador4"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADXPPorMinutoJogador4"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgRADItem1Jogador4" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem2Jogador4" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem3Jogador4" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem4Jogador4" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem5Jogador4" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem6Jogador4" CssClass="imagemItemHeroi" />
            </td>
        </tr>
        <tr style="background-color: #757575; height: 25px;">
            <td>
                <asp:Image runat="server" ID="imgRADHeroiJogador5" CssClass="imagemHeroi" /></td>
            <td>
                <asp:Label runat="server" ID="lblRADNomeJogador5"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgRADFotoJogador5" CssClass="imagemFotoJogador" /></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADNivelJogador5"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADKillsJogador5"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADDeathsJogador5"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADAssistenciaJogador5"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADLastHitJogador5"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADDenyJogador5"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADOuroPorMinutoJogador5"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADOuroTotalJogador5"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADXPPorMinutoJogador5"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgRADItem1Jogador5" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem2Jogador5" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem3Jogador5" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem4Jogador5" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem5Jogador5" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgRADItem6Jogador5" CssClass="imagemItemHeroi" />
            </td>
        </tr>
        <tr style="background-color: #5b5a5a; height: 25px;">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADNivelTotal"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADKillsJogadorTotal"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADDeathsJogadorTotal"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADAssistenciaJogadorTotal"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADLastHitJogadorTotal"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblRADDenyJogadorTotal"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADOuroPorMinutoJogadorTotal"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADOuroTotalJogadorTotal"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblRADXPPorMinutoJogadorTotal"></asp:Label></td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <br />
    <asp:Image runat="server" ID="imgLogoTime2" Width="45" Height="25" ImageUrl="~/img/TimeSemLogo.png" />
    <asp:Label runat="server" ID="lblTime2" Text="Temidos" CssClass="labelTimeResutladoPartidaTEM"></asp:Label>
    <table style="font-size: 15px; background-color: #444444;">
        <tr style="background-color: #5b5a5a; font-weight: bold;">
            <td style="width: 47px;">
                <asp:Label runat="server" ID="lblTEMHeroi" Text="Herói" ToolTip="Herói escolhido"></asp:Label></td>
            <td style="width: 200px; min-width: 200px;">
                <asp:Label runat="server" ID="lblTEMJogador" Text="Jogador" ToolTip="Nick do jogador"></asp:Label></td>
            <td style="width: 27px;"></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMNivel" Text="Nível" ToolTip="Nível do jogador ao final da partida"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMKills" Text="K" ToolTip="Número de heróis mortos pelo jogador na partida"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMDeaths" Text="D" ToolTip="Número de vezes que o jogador morreu na partida"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMAssistencia" Text="A" ToolTip="Total de assistências realizados pelo jogador"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMLastHit" Text="LH" ToolTip="Total de ultimo hits (Last Hit) executados pelo jogador"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMDeny" Text="Deny" ToolTip="Total de negações(deny) executados pelo jogador"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMOuroPorMinuto" Text="GPM" ToolTip="Média de ouro obtida pelo jogador por minuto"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMOuroTotal" Text="GT" ToolTip="Ouro total obtido pelo jogador"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMXPPorMinuto" Text="XPM" ToolTip="Média de experiência obtida por minuto"></asp:Label></td>
            <td style="width: 230px; min-width: 230px;">
                <asp:Label runat="server" ID="lblTEMItens" Text="Itens" ToolTip="Itens do jogador ao final da partida"></asp:Label></td>
        </tr>
        <tr style="background-color: #757575; height: 25px;">
            <td>
                <asp:Image runat="server" ID="imgTEMHeroiJogador1" CssClass="imagemHeroi" /></td>
            <td>
                <asp:Label runat="server" ID="lblTEMNomeJogador1" Text="lololol"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgTEMFotoJogador1" CssClass="imagemFotoJogador" /></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMNivelJogador1" Text="25"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMKillsJogador1" Text="40"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMDeathsJogador1" Text="2"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMAssistenciaJogador1" Text="5"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMLastHitJogador1" Text="200"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMDenyJogador1" Text="15"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMOuroPorMinutoJogador1" Text="800"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMOuroTotalJogador1" Text="20000"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMXPPorMinutoJogador1" Text="900"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgTEMItem1Jogador1" CssClass="imagemItemHeroi" ImageUrl="~/img/Itens/1.png" />
                <asp:Image runat="server" ID="imgTEMItem2Jogador1" CssClass="imagemItemHeroi" ImageUrl="~/img/Itens/48.png" />
                <asp:Image runat="server" ID="imgTEMItem3Jogador1" CssClass="imagemItemHeroi" ImageUrl="~/img/Itens/156.png" />
                <asp:Image runat="server" ID="imgTEMItem4Jogador1" CssClass="imagemItemHeroi" ImageUrl="~/img/Itens/208.png" />
                <asp:Image runat="server" ID="imgTEMItem5Jogador1" CssClass="imagemItemHeroi" ImageUrl="~/img/Itens/141.png" />
                <asp:Image runat="server" ID="imgTEMItem6Jogador1" CssClass="imagemItemHeroi" ImageUrl="~/img/Itens/133.png" />
            </td>
        </tr>
        <tr style="background-color: #929292; height: 25px;">
            <td>
                <asp:Image runat="server" ID="imgTEMHeroiJogador2" CssClass="imagemHeroi" /></td>
            <td>
                <asp:Label runat="server" ID="lblTEMNomeJogador2"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgTEMFotoJogador2" CssClass="imagemFotoJogador" /></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMNivelJogador2"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMKillsJogador2"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMDeathsJogador2"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMAssistenciaJogador2"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMLastHitJogador2"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMDenyJogador2"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMOuroPorMinutoJogador2"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMOuroTotalJogador2"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMXPPorMinutoJogador2"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgTEMItem1Jogador2" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem2Jogador2" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem3Jogador2" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem4Jogador2" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem5Jogador2" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem6Jogador2" CssClass="imagemItemHeroi" />
            </td>
        </tr>
        <tr style="background-color: #757575; height: 25px;">
            <td>
                <asp:Image runat="server" ID="imgTEMHeroiJogador3" CssClass="imagemHeroi" /></td>
            <td>
                <asp:Label runat="server" ID="lblTEMNomeJogador3"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgTEMFotoJogador3" CssClass="imagemFotoJogador" /></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMNivelJogador3"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMKillsJogador3"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMDeathsJogador3"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMAssistenciaJogador3"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMLastHitJogador3"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMDenyJogador3"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMOuroPorMinutoJogador3"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMOuroTotalJogador3"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMXPPorMinutoJogador3"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgTEMItem1Jogador3" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem2Jogador3" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem3Jogador3" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem4Jogador3" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem5Jogador3" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem6Jogador3" CssClass="imagemItemHeroi" />
            </td>
        </tr>
        <tr style="background-color: #929292; height: 25px;">
            <td>
                <asp:Image runat="server" ID="imgTEMHeroiJogador4" CssClass="imagemHeroi" /></td>
            <td>
                <asp:Label runat="server" ID="lblTEMNomeJogador4"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgTEMFotoJogador4" CssClass="imagemFotoJogador" /></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMNivelJogador4"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMKillsJogador4"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMDeathsJogador4"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMAssistenciaJogador4"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMLastHitJogador4"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMDenyJogador4"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMOuroPorMinutoJogador4"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMOuroTotalJogador4"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMXPPorMinutoJogador4"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgTEMItem1Jogador4" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem2Jogador4" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem3Jogador4" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem4Jogador4" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem5Jogador4" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem6Jogador4" CssClass="imagemItemHeroi" />
            </td>
        </tr>
        <tr style="background-color: #757575; height: 25px;">
            <td>
                <asp:Image runat="server" ID="imgTEMHeroiJogador5" CssClass="imagemHeroi" /></td>
            <td>
                <asp:Label runat="server" ID="lblTEMNomeJogador5"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgTEMFotoJogador5" CssClass="imagemFotoJogador" /></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMNivelJogador5"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMKillsJogador5"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMDeathsJogador5"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMAssistenciaJogador5"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMLastHitJogador5"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMDenyJogador5"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMOuroPorMinutoJogador5"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMOuroTotalJogador5"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMXPPorMinutoJogador5"></asp:Label></td>
            <td>
                <asp:Image runat="server" ID="imgTEMItem1Jogador5" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem2Jogador5" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem3Jogador5" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem4Jogador5" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem5Jogador5" CssClass="imagemItemHeroi" />
                <asp:Image runat="server" ID="imgTEMItem6Jogador5" CssClass="imagemItemHeroi" />
            </td>
        </tr>
        <tr style="background-color: #5b5a5a; height: 25px;">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMNivelTotal"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMKillsJogadorTotal"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMDeathsJogadorTotal"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMAssistenciaJogadorTotal"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMLastHitJogadorTotal"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada">
                <asp:Label runat="server" ID="lblTEMDenyJogadorTotal"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMOuroPorMinutoJogadorTotal"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMOuroTotalJogadorTotal"></asp:Label></td>
            <td class="colunaTabelaResultadoCentralizada40">
                <asp:Label runat="server" ID="lblTEMXPPorMinutoJogadorTotal"></asp:Label></td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <br />
    <asp:ImageButton runat="server" ID="btnVoltar" OnClientClick="javascript:history.back(-1);return false;" ImageUrl="~/img/BotaoVoltar.png" /> 
    <center>
        <asp:Image runat="server" ID="imgEstatisticas" ImageUrl="~/img/ImagemEstatisticas.jpg" Width="400" />
        <table>
            <tr>
                <td class="colunaEstatistica">Mais matou</td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="colunaEstatistica">Menos matou</td>
            </tr>
            <tr>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiJogadorMaiorkill" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/1.png" />
                    <asp:Label runat="server" ID="lblValorMaiorkill" CssClass="labelEstatisticaValor" Text="17"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoJogadorMaiorkill" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/10.png" />
                    <asp:Label runat="server" ID="lblJogadorMaiorkill" CssClass="labelEstatisticaNomeJogador" Text="Jogador"></asp:Label>
                </td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiJogadorMenorkill" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/20.png" />
                    <asp:Label runat="server" ID="lblValorMenorkill" CssClass="labelEstatisticaValorNegativo" Text="1"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoJogadorMenorkill" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/23.png" />
                    <asp:Label runat="server" ID="lblJogadorMenorkill" CssClass="labelEstatisticaNomeJogador" Text="Teste 123"></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td class="colunaEstatistica">Menos morreu</td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="colunaEstatistica">Mais morreu</td>
            </tr>
            <tr>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiJogadorMenosMorreu" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/1.png" />
                    <asp:Label runat="server" ID="lblValorMenosMorte" CssClass="labelEstatisticaValor" Text="17"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoJogadorMenosMorreu" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/10.png" />
                    <asp:Label runat="server" ID="lblJogadorMenosMorreu" CssClass="labelEstatisticaNomeJogador" Text="Jogador"></asp:Label>
                </td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiJogadorMaisMorreu" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/20.png" />
                    <asp:Label runat="server" ID="lblValorMaisMorte" CssClass="labelEstatisticaValorNegativo" Text="1"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoJogadorMaisMorreu" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/23.png" />
                    <asp:Label runat="server" ID="lblJogadorMaisMorreu" CssClass="labelEstatisticaNomeJogador" Text="Teste 123"></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td class="colunaEstatistica">Mais assistências</td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="colunaEstatistica">Menos assistências</td>
            </tr>
            <tr>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiJogadorMaiorAssist" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/1.png" />
                    <asp:Label runat="server" ID="lblValorMaiorAssist" CssClass="labelEstatisticaValor" Text="17"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoJogadorMaiorAssist" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/10.png" />
                    <asp:Label runat="server" ID="lblJogadorMaiorAssist" CssClass="labelEstatisticaNomeJogador" Text="Jogador"></asp:Label>
                </td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiJogadorMenorAssist" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/20.png" />
                    <asp:Label runat="server" ID="lblValorMenorAssist" CssClass="labelEstatisticaValorNegativo" Text="1"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoJogadorMenorAssist" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/23.png" />
                    <asp:Label runat="server" ID="lblJogadorMenorAssist" CssClass="labelEstatisticaNomeJogador" Text="Teste 123"></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td class="colunaEstatistica">Mais last hits</td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="colunaEstatistica">Menos last hits</td>
            </tr>
            <tr>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiJogadorMaiorLastHit" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/1.png" />
                    <asp:Label runat="server" ID="lblValorMaiorLastHit" CssClass="labelEstatisticaValor" Text="17"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoJogadorMaiorLastHit" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/10.png" />
                    <asp:Label runat="server" ID="lblJogadorMaiorLastHit" CssClass="labelEstatisticaNomeJogador" Text="Jogador"></asp:Label>
                </td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiJogadorMenorLastHit" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/20.png" />
                    <asp:Label runat="server" ID="lblValorMenorLastHit" CssClass="labelEstatisticaValorNegativo" Text="1"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoJogadorMenorLastHit" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/23.png" />
                    <asp:Label runat="server" ID="lblJogadorMenorLastHit" CssClass="labelEstatisticaNomeJogador" Text="Teste 123"></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td class="colunaEstatistica">Mais denies</td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="colunaEstatistica">Menos denies</td>
            </tr>
            <tr>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiJogadorMaiorDeny" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/1.png" />
                    <asp:Label runat="server" ID="lblValorMaiorDeny" CssClass="labelEstatisticaValor" Text="17"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoJogadorMaiorDeny" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/10.png" />
                    <asp:Label runat="server" ID="lblJogadorMaiorDeny" CssClass="labelEstatisticaNomeJogador" Text="Jogador"></asp:Label>
                </td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiJogadorMenorDeny" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/20.png" />
                    <asp:Label runat="server" ID="lblValorMenorDeny" CssClass="labelEstatisticaValorNegativo" Text="1"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoJogadorMenorDeny" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/23.png" />
                    <asp:Label runat="server" ID="lblJogadorMenorDeny" CssClass="labelEstatisticaNomeJogador" Text="Teste 123"></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td class="colunaEstatistica">Maior XP por minuto</td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="colunaEstatistica">Menor XP por minuto</td>
            </tr>
            <tr>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiJogadorMaiorXpMin" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/1.png" />
                    <asp:Label runat="server" ID="lblValorMaiorXpMin" CssClass="labelEstatisticaValor" Text="17"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoJogadorMaiorXpMin" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/10.png" />
                    <asp:Label runat="server" ID="lblJogadorMaiorXpMin" CssClass="labelEstatisticaNomeJogador" Text="Jogador"></asp:Label>
                </td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiJogadorMenorXpMin" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/20.png" />
                    <asp:Label runat="server" ID="lblValorMenorXpMin" CssClass="labelEstatisticaValorNegativo" Text="1"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoJogadorMenorXpMin" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/23.png" />
                    <asp:Label runat="server" ID="lblJogadorMenorXpMin" CssClass="labelEstatisticaNomeJogador" Text="Teste 123"></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td class="colunaEstatistica">Mais rico</td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="colunaEstatistica">Mais pobre</td>
            </tr>
            <tr>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiJogadorMaisRico" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/1.png" />
                    <asp:Label runat="server" ID="lblValorMaisRico" CssClass="labelEstatisticaValor" Text="17"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoJogadorMaisRico" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/10.png" />
                    <asp:Label runat="server" ID="lblJogadorMaisRico" CssClass="labelEstatisticaNomeJogador" Text="Jogador"></asp:Label>
                </td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiJogadorMaisPobre" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/20.png" />
                    <asp:Label runat="server" ID="lblValorMaisPobre" CssClass="labelEstatisticaValorNegativo" Text="1"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoJogadorMaisPobre" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/23.png" />
                    <asp:Label runat="server" ID="lblJogadorMaisPobre" CssClass="labelEstatisticaNomeJogador" Text="Teste 123"></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td class="colunaEstatistica">Maior dano causado em heróis</td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="colunaEstatistica">Maior dano causado em torres</td>
            </tr>
            <tr>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiJogadorMaisDanoCausadoEmHeroi" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/1.png" />
                    <asp:Label runat="server" ID="lblValorMaisDanoCausadoEmHeroi" CssClass="labelEstatisticaValor" Text="17"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoJogadorMaisDanoCausadoEmHeroi" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/10.png" />
                    <asp:Label runat="server" ID="lblJogadorMaisDanoCausadoEmHeroi" CssClass="labelEstatisticaNomeJogador" Text="Jogador"></asp:Label>
                </td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiJogadorMaisDanoCausadoEmTorre" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/20.png" />
                    <asp:Label runat="server" ID="lblValorMaisDanoCausadoEmTorre" CssClass="labelEstatisticaValorNegativo" Text="1"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoJogadorMaisDanoCausadoEmTorre" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/23.png" />
                    <asp:Label runat="server" ID="lblJogadorMaisDanoCausadoEmTorre" CssClass="labelEstatisticaNomeJogador" Text="Teste 123"></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td class="colunaEstatistica">Melhor Jogador</td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="colunaEstatistica">Pior Jogador</td>
            </tr>
            <tr>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiMelhorJogador" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/1.png" />
                    <asp:Label runat="server" ID="Label3" CssClass="labelEstatisticaValor" Text="******"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoMelhorJogador" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/10.png" />
                    <asp:Label runat="server" ID="lblMelhorJogador" CssClass="labelEstatisticaNomeJogador" Text="Jogador"></asp:Label>
                </td>
                <td class="colunaEstatisticaSeparador"></td>
                <td class="celulaEstatistica">
                    <asp:Image runat="server" ID="imgFotoHeroiPiorJogador" CssClass="imagemEstatisticaHeroi" ImageUrl="~/img/Herois/20.png" />
                    <asp:Label runat="server" ID="Label4" CssClass="labelEstatisticaValorNegativo" Text="******"></asp:Label><br />
                    <asp:Image runat="server" ID="imgFotoPiorJogador" CssClass="imagemEstatisticaJogador" ImageUrl="~/img/Herois/23.png" />
                    <asp:Label runat="server" ID="lblPiorJogador" CssClass="labelEstatisticaNomeJogador" Text="Teste 123"></asp:Label><br />
                </td>
            </tr>
        </table>
    </center>
</div>
