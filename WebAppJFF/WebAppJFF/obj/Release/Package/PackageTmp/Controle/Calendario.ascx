﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Calendario.ascx.cs" Inherits="WebAppJFF.Controle.Calendario" %>

<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<div class="form-group">
    <asp:Label runat="server" ID="ControleLabel" CssClass="control-label" Visible="false" Text="Data/Hora"></asp:Label>
    <sup runat="server" id="ObrigatorioIcon" visible="false" class="icone-obrigatorio text-danger" title="Campo obrigatório">
        <small>
            <span class="ionicons ion-ios-medical fa-fw" aria-hidden="true"></span>
        </small>
    </sup>
    <div class="input-group">
        <asp:TextBox ID="DataTxt" runat="server" CssClass="form-control"></asp:TextBox>

        <span id="CalendarioSpan" runat="server" class="input-group-btn">
            <button id="calendarioImagem" class="btn btn-default" type="button" runat="server" causesvalidation="false">
                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
            </button>
        </span>
    </div>
    
    <ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" DisplayMoney="Left" Mask="99/99/9999" MaskType="Date"
        MessageValidatorTip="False" AutoComplete="false" TargetControlID="DataTxt" CultureName="pt-BR" UserDateFormat="DayMonthYear">
    </ajax:MaskedEditExtender>

    <ajax:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" PopupButtonID="calendarioImagem"></ajax:CalendarExtender>

    <%--
        Display="Dynamic" muda a renderização de visibility:none para display:none
        Fonte: http://www.genuinescope.com/blog/asp-net-visibilitynone-style-of-field-validator-change-into-displaynone/    
    --%>
    <asp:RequiredFieldValidator ID="DataRequiredFieldValidator"
        EnableClientScript="true" runat="server" Display="Dynamic" ForeColor="" CssClass="text-danger" Enabled="false" Visible="false">
    </asp:RequiredFieldValidator>

    <asp:Label runat="server" ID="mensagemLabel" ForeColor="" Text="Data inválida." CssClass="text-danger" Style="display: none;"></asp:Label>
</div>