﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GridControl.ascx.cs" Inherits="WebAppJFF.Controle.GridControl" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Panel ID="GridPanel" runat="server" Visible="false">
            <asp:Panel runat="server" ID="ImprimirPanel" CssClass="clearfix" Visible="false">
                <div class="grid-toolbar pull-right">
                    <!-- Botões de Ações da Grid -->
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            Exportar&nbsp;&nbsp;<span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <li>
                                <asp:HyperLink ID="ImprimirLink" runat="server" EnableViewState="true" Target="_blank" Visible="True">
                                <span class="fa fa-file-pdf-o fa-fw fa-2x" aria-hidden="true"></span>&nbsp;PDF
                                </asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="ExportarExelLink" runat="server" EnableViewState="true" Target="_blank">
                                <span class="fa fa-file-excel-o fa-fw fa-2x" aria-hidden="true"></span>&nbsp;Planilha
                                </asp:HyperLink>
                            </li>
                        </ul>
                    </div>
                </div>
            </asp:Panel>

            <div class="table-responsive">
                <table id="<%=this.ClientID%>" class="table table-bordered table-striped grid">
                    <thead>
                        <tr>
                            <asp:PlaceHolder ID="HeadPlaceHolder" OnPreRender="HeadPlaceHolderPreRender" runat="server" />
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="BodyRepeater" runat="server" OnInit="BodyRepeater_Init" OnItemDataBound="BodyRepeater_ItemDataBound">
                            <ItemTemplate>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <asp:Panel ID="ExternalPagingPanel" runat="server" CssClass="row" Visible="false">
                <asp:Panel ID="PageCounter" runat="server" CssClass="col-xs-12 col-sm-4 PageCounter">

                    <p class="form-control-static">
                        Página&nbsp;<asp:TextBox ID="IrParaTextBox" runat="server" CssClass="form-control go-to-page select-all-text" AutoPostBack="true" OnTextChanged="IrParaTextBox_TextChanged" />&nbsp;de&nbsp;<asp:Label ID="TotalPaginasLabel" runat="server"></asp:Label>
                        <span class="fa fa-exclamation-circle infoconsulta" data-toggle="popover" data-html="true" data-content="<%= InformacoesConsulta.ToString() %>"></span>
                    </p>

                </asp:Panel>
                <asp:Panel ID="Panel2" CssClass="col-xs-12 col-sm-8 PagePager" runat="server">
                    <asp:LinkButton ID="FirstBtn" runat="server" CssClass="btn btn-link" CausesValidation="false" OnClick="FirstBtn_OnClick" ToolTip="Primeira página">
                            <span class="fa fa-backward fa-fw" aria-hidden="true"></span>&nbsp;Primeira
                    </asp:LinkButton>
                    <asp:LinkButton ID="PreviousBtn" runat="server" CssClass="btn btn-link" CausesValidation="false" OnClick="PreviousBtn_OnClick" ToolTip="Página anterior">
                            <span class="fa fa-play fa-rotate-180 fa-fw" aria-hidden="true"></span>&nbsp;Anterior
                    </asp:LinkButton>
                    <asp:LinkButton ID="NextBtn" runat="server" CssClass="btn btn-link" CausesValidation="false" OnClick="NextBtn_OnClick" ToolTip="Próxima página">
                            Próxima&nbsp;<span class="fa fa-play fa-fw" aria-hidden="true"></span>
                    </asp:LinkButton>
                    <asp:LinkButton ID="LastBtn" runat="server" CssClass="btn btn-link" CausesValidation="false" OnClick="LastBtn_OnClick" ToolTip="Última página">
                            Última&nbsp;<span class="fa fa-forward fa-fw" aria-hidden="true"></span>
                    </asp:LinkButton>
                </asp:Panel>
            </asp:Panel>

            <%-- Botao para Ordernar a grid --%>
            <asp:Button ID="OrdenarGrid" runat="server" CssClass="hide" />
        </asp:Panel>

        <%-- Painel de Grid sem Registros --%>
        <asp:Panel ID="VazioPanel" runat="server" CssClass="row grid-vazia">
            <div class="col-xs-12">
                <div class="alert alert-info">
                    <%= MensagemGridVazia %>
                </div>
            </div>
        </asp:Panel>
        
    </ContentTemplate>
</asp:UpdatePanel>

<script>
    function CarregarPaginacao() {
        $('[data-sortexpression]', '#' + '<%=this.ClientID%>' + ' thead tr').addClass('order-link');
        $('#' + '<%=this.ClientID%>' + ' thead tr').on('click', '[data-sortexpression]', function () {
            var sortExpression = $(this).data('sortexpression');
            __doPostBack('<%=OrdenarGrid.ClientID%>', sortExpression);
        });
    }
    function CarregarDetalhes() {
        //if ('ctl00_ContentPlaceHolder1_GridControl' == '<%=this.ClientID%>')
        //    console.log('The grid has been planted [ <%=this.ClientID%> ]');
        var s = '<%=ScriptDetalhes%>';
        if (s.length > 0) {
            carregarDetalhes('<%=this.ClientID%>', s);
        }
    }
    //Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(CarregarPaginacao);
    //Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(CarregarDetalhes);
</script>
