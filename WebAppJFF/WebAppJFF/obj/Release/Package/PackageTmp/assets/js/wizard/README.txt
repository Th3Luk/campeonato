# Bootstrap Wizard
Add Wizard functionalities to Bootstrap Tab

@version 1.0
@date 2015.03.03
@author Vinicius Garcia
@requires jquery 1.10 or higher

## How to use

<script>
	$(document).ready(function () {
		BootstrapWizard($('#Wizard1'));
	});
</script>

<div id="Wizard1" class="wizard-controller row">
	<!-- Lista de Abas -->
	<div class="col-md-2 col-sm-12">
		<ul role="tablist" class="tab-list nav nav-pills nav-stacked">
			<li class="active"><a data-toggle="tab" href="#interessados">Interessados</a></li>
			<li><a data-toggle="tab" href="#processo">Processo</a></li>
			<li class="disabled"><a data-toggle="tab" href="#infcomplementares">Informa��es Complementares</a></li>
			<li><a data-toggle="tab" href="#docoriginal">Documento Original</a></li>
		</ul>
	</div>
	<!-- Conteudo das Abas -->
	<div class="tab-content col-md-10 col-sm-12">
		<!-- Paginador de Abas -->
		<div class="row">
			<div class="col-sm-5 col-sm-offset-7">
				<ul class="pager wizard">
					<li class="previous">
						<button type="button" class="btn btn-default" autocomplete="off">
							<span class="glyphicon glyphicon-chevron-left fa-fw" aria-hidden="true"></span>&nbsp;Anterior
						</button>
					</li>
					<li class="next">
						<button type="button" class="btn btn-default" autocomplete="off">
							Pr�ximo&nbsp;<span class="glyphicon glyphicon-chevron-right fa-fw" aria-hidden="true"></span>
						</button>
					</li>
				</ul>
			</div>
		</div>
		<hr />
		<!-- Abas de Conteudo -->
		<div role="tabpanel" class="tab-pane active" id="interessados">
		</div>
		<div role="tabpanel" class="tab-pane" id="processo">
		</div>
		<div role="tabpanel" class="tab-pane" id="infcomplementares">
		</div>
		<div role="tabpanel" class="tab-pane" id="docoriginal">
		</div>
		<hr />
		<!-- Paginador de Abas -->
		<div class="row">
			<div class="col-sm-5 col-sm-offset-7">
				<ul class="pager wizard">
					<li class="previous">
						<button type="button" class="btn btn-default" autocomplete="off">
							<span class="glyphicon glyphicon-chevron-left fa-fw" aria-hidden="true"></span>&nbsp;Anterior
						</button>
					</li>
					<li class="next">
						<button type="button" class="btn btn-default" autocomplete="off">
							Pr�ximo&nbsp;<span class="glyphicon glyphicon-chevron-right fa-fw" aria-hidden="true"></span>
						</button>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>