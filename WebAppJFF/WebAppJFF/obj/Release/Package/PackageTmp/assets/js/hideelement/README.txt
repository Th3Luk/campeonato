# Hide Element
Hide element on click

@version 1.0
@date 2015.03.03
@author Vinicius Garcia
@requires jquery 1.10 or higher
@requires jquery-cookie

## How to use
<script>
	// Hide Banner
	$(function () {
		// Inicializa o javascript de esconder o cabecalho
		var hideOnStart = false;
		if (HideElement !== undefined)
			HideElement('bannerTop', hideOnStart);
	});
</script>

<div id="bannerTop" class="header row text-center" role="header">
	<div class="col-md-12">
		<a class="header-brand" href="#">
			<img class="ged-banner" src="./img/bannerTop.png" alt="Banner"/>
		</a>
	</div>
</div>