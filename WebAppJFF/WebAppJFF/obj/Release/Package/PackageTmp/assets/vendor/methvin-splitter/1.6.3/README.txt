jQuery Splitter Plugin
Fonte: https://github.com/e1ven/jQuery-Splitter

Esta versão foi corrigida para suportar as versões mais recentes do Jquery.

Functions

	// trigger toggleDock
	$('#MySplitter').trigger('toggleDock')
	// trigger dock
	$('#MySplitter').trigger('dock')
	// trigger undock
	$('#MySplitter').trigger('undock')
	// trigger destroy
	$('#MySplitter').trigger('destroy')

Getting Started

    <script src="/assets/vendor/methvin-splitter/1.6.3/splitter.js"></script>

    <style>
        .ui-state-default { background-color: #aaa; }
        .ui-state-hover { background-color: #da8; }
        .ui-state-highlight { background-color: #add; }
        .ui-state-error { background-color: #eaa; }

        .splitter {
            background: none repeat scroll 0 0 #aaa;
            border: 2px solid #aaa;
            height: 200px;
        }
        .splitter .splitter-pane
        {
            border: none;
            padding: 0;
            color: #ffffff;
            height: 200px;
        }
        .splitter .files-panel
        {
            overflow: auto;
        }
        .splitter-bar-vertical {
            background-image: url("img/vgrabber.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #aaa;
            width: 6px;
        }
        .splitter-bar-vertical-docked {
            background-image: url("img/vdockbar-trans.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #aaa;
            width: 10px;
        }
        .splitter-bar.ui-state-highlight {
            opacity: 0.7;
            background-color: #da8;
        }
        .splitter-iframe-hide {
            visibility: hidden;
        }
    </style>

    <script>
        $(document).ready(function () {
            $(".pdf-viewer-splitter").splitter({
                type: "v",
                outline: true,
                resizeToWidth: true,
                dock: "left",
                dockSpeed: 200,
                cookie: 'docksplitter'
            });
        });
    </script>
	
    <div class="row margin-bottom-row">
        <div class="col-md-12">
            <div class="pdf-viewer-splitter">
                <div class="files-panel" style="background-color: green;">
                    <div class="row no-margin-left no-margin-right">
                        <div style="background-color: purple;" class="esq col-xs-6">
                            esq
                        </div>
                        <div style="background-color: red;" class="dir col-xs-6">
                            dir
                        </div>
                    </div>
                </div>
                <div class="pdf-panel" style="background-color: blue;">
                    <div class="row no-margin-left no-margin-right">
                        <div class="col-xs-12">
                            pdf
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>