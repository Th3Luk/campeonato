﻿function DesmarcaVias(senderCheckBox) {
  var inputs = $('INPUT', '#GridViewViasSpan');
  var i;
  var input;
  for (i = 0; i < inputs.length; i++) {
    input = inputs[i];
    if (input.type == "checkbox")
      if (senderCheckBox.id != input.id)
      input.checked = false;
  }
}