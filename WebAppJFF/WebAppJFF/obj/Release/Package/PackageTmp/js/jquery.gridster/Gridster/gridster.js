/* Extensao para a funçao format substituir {} por texto */
String.prototype.format = function() {
  var args = arguments[0];
  return this.replace(/{([0-9 a-z A-Z]+)}/g, function (match, code) {
	return typeof args[code] != 'undefined' ? args[code] : '&nbsp;'; // escape(match)
  });
};
/**
 * Manipulaçao do Gridster
	var gridster = $(".gridster ul").gridster().data('gridster');
	gridster.add_widget('<li class="widget">The HTML of the widget...</li>', 1, 1); // Adicionar widget
	gridster.remove_widget($('.gridster li').eq(10)); // Remover widget
	gridster.serialize(); // Serializar widgets
	gridster.disable(); // Desabilitar Gridster
	gridster.enable(); // Habilitar Gridster
*/

// Adiciona um Widget no Gridster
adicionarWidget = function(gridster, widgetObject){
	// Adiciona o conteudo do Widget ao seus templates, de acordo com o tipo do conteudo
	var widgetConteudo = '';
	$(widgetObject.conteudo).each(function (i, v) {
	  var tmplConteudo = $('#tmpl_' + v.tipo).html();
	  widgetConteudo += tmplConteudo.format(v);
	});

	// Inclui no Widget o conteudo formatado em templates
	widgetObject.conteudoHtml = widgetConteudo;

	// Adicionar widget...
	var tmplWidget = $('#tmpl_widget-' + widgetObject.size_x + 'x' + widgetObject.size_y).html();
	var widget = '<li id="'+ widgetObject.id +'" class="widget widget-' + widgetObject.size_x + 'x' + widgetObject.size_y + ' ' + widgetObject.cor + '">' + tmplWidget.format(widgetObject) + '</li>';
	gridster.add_widget(widget, widgetObject.size_x, widgetObject.size_y, widgetObject.col, widgetObject.row);
}
$(function () {
	// Inicializa o Gridster
	$(".gridster ul").gridster({
		widget_margins: [10, 10]
		, widget_base_dimensions: [140, 140]
		, draggable: {
			// Iniciando movimentaçao do elemento
			start: function (event, ui) {
				// Previne o click de links dentro dos widgets
				$('.container .content a', ui.helper).addClass('noClick');
			},
			// Movimentando o elemento
			drag: function (event, ui) {
				// ...
			},
			// Fim da movimentaçao do elemento
			stop: function (event, ui) {
				var thisGrister = $(event.target).closest('.gridster');
				// Informa que o gridster sofreu alteraçoes
				if(!thisGrister.hasClass('wdg-pagina-alterada'))
					thisGrister.addClass('wdg-pagina-alterada');
					
				// Se os links possuirem a classe 'noClick', evitamos que eles sejam ativados...
				$('.widget .container .content a',thisGrister).off('click').on('click', function () {
					if ($(this).hasClass('noClick')) {
						$(this).removeClass('noClick');
						return false;
					}else{
						if((desejaSair == undefined) || (!desejaSair()))
							return false;
					}
				});
			}
		}
		, serialize_params: function ($w, wgd) {
			// $w: the jQuery wrapped HTMLElement
			// wgd: the grid coords object with keys col, row, size_x and size_y
			//return { obj: $w, col: wgd.col, row: wgd.row, size_x: wgd.size_x, size_y: wgd.size_y }

			return 'id:' + $w.attr('id') + ',col:' + wgd.col + ',row:' + wgd.row + '';
		}
	});
});