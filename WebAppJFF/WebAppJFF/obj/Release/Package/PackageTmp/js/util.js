function numbersonly(e) {
    var unicode = e.charCode ? e.charCode : e.keyCode
    if (unicode != 8) { //if the key isn't the backspace key (which we should allow)
        if (unicode < 48 || unicode > 57) //if not a number
            return false //disable key press
    }
}

function swapImage(id, img) {
    var str = new Array();
    str = img.split("/");
    var gif = str[str.length - 1];
    id.src = "img/over_" + gif;
}

function swapImageOver(id, img) {
    var str = new Array();
    str = img.split("_");
    id.src = "img/" + str[1];
}
function LimparCamposTexto(nomeDiv) {
    $("input", document.getElementById(nomeDiv)).val("");
    $("textarea", document.getElementById(nomeDiv)).val("");
}
function ReiniciarRadioButtonList(nomeDiv) {
    var list = $('INPUT', document.getElementById(nomeDiv));
    var i;
    for (i = 0; i <= list.length - 1; i++)
        list[i].checked = false;
}

function LimpaCampoTextCadastroFerias(DivCampo) {
    var arrayInput = $('INPUT', document.getElementById(DivCampo));
    arrayInput.val("");
    i = 0;
    while (arrayInput[i].type == "hidden")
        i++;
    $('INPUT', document.getElementById(DivCampo))[i].focus();
}


function LimparCampos(nomeDiv) {
    campos = $("input", document.getElementById(nomeDiv)).val("");
    selects = $("select", document.getElementById(nomeDiv));

    if (selects != null)
        for (i = 0; i < selects.length; i++) {
            for (option in selects[i].options) {
                selects[i].remove(option);
            }
        }
    return false;
}
function BuscarObjetoPorDivId(nomeDiv, tagObjeto) {
    var div = document.getElementById(nomeDiv);
    if (div == null)
        return null;
    else
        return BuscarObjeto(div.firstChild, tagObjeto);
}

function BuscarObjeto(objeto, nome) {
    while (objeto != null && objeto.nodeName != nome)
        objeto = objeto.nextSibling;
    if (objeto == null || objeto.nodeName != nome)
        return null;
    else
        return objeto;
}
var tam = 13;

function mudaFonte(tipo, elemento) {
    if (tipo == "mais") {
        if (tam < 16) tam += 1;
        createCookie('fonte', tam, 365);
    } else {
        if (tam > 10)
            tam -= 1;
        createCookie('fonte', tam, 365);
    }
    document.getElementById('mudaFonte').style.fontSize = tam + 'px';
    // document.getElementById('mudaFoto').style.fontSize = tam+'px';
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function FormataData(Campo, teclapres, format) {
    var tecla = teclapres.keyCode;
    var vr = new String(Campo.value);
    vr = vr.replace("/", "");
    vr = vr.replace("/", "");
    tam = vr.length + 1;
    format = (format == undefined) ? 'dd/mm/yyyy' : format;

    if (tecla != 9 && tecla != 8) {
        if (tam > 2 && tam < 5 && tecla != 47)
            Campo.value = vr.substr(0, 2) + '/' + vr.substr(2, tam);
        if (tam >= 5 && tam <= 10 && tecla != 47 && format == 'dd/mm/yyyy')
            Campo.value = vr.substr(0, 2) + '/' + vr.substr(2, 2) + '/' + vr.substr(4, 4);
    }
}

function FormataHora(Campo, teclapres) {
    var tecla = teclapres.keyCode;
    var vr = new String(Campo.value);
    vr = vr.replace(":", "");
    tam = vr.length + 1;

    if (tecla != 9 && tecla != 8) {
        if (tam > 2 && tam < 5 && tecla != 58)
            Campo.value = vr.substr(0, 2) + ':' + vr.substr(2, 2);
    }
}

function abrirJanelaModal(iframeSrc, objId) {
    var modal, frame, back;
    if ((frame = document.getElementById('iframe1')) == undefined)
        return false;
    else if ((modal = document.getElementById(objId)) == undefined)
        return false;
    if ((back = document.getElementById('backgroundJanelaModal')) != undefined) {
        back.style.top = 0;
        back.style.left = 0;
        back.style.display = 'block';
    }
    frame.src = iframeSrc;
    modal.style.top = parseInt((document.documentElement.clientHeight - frame.height) / 2) + 'px';
    modal.style.left = parseInt((document.documentElement.clientWidth - frame.width) / 2) + 'px';
    modal.style.display = 'block';
    return true;
}

function fecharJanelaModal(objId) {
    var modal, back;
    if ((modal = top.document.getElementById(objId)) == undefined)
        return false;
    if ((back = top.document.getElementById('backgroundJanelaModal')) != undefined) {
        back.style.top = '100%';
        back.style.left = '100%';
        back.style.display = 'none';
    }
    modal.style.display = 'none';
    return true;
}

function PulaCampo(obj, Campo2) {
    if (obj.value.length == 6)
        document.getElementById(Campo2).focus();
}
//usado nas telas de pedido de f�rias. 	
function ContaDias(dtInicio, dtFim, nrDias) {
    var dias = document.getElementById(nrDias).value;
    var data = document.getElementById(dtInicio).value;

    if (data != "") {
        var dia = data.substr(0, 2);
        var mes = (data.substr(3, 2) - 1);
        var ano = data.substr(6, 4);

        //inicia com dataInicial
        var dataReferencia = new Date(ano, mes, dia, 1, 1, 1);
        //soma os dias 
        dataReferencia.setDate((dataReferencia.getDate() + (dias - 1)));
        //pega dados da dataFinal
        dia = dataReferencia.getDate().toString();
        mes = (dataReferencia.getMonth() + 1).toString();
        ano = dataReferencia.getFullYear().toString();

        if (dia.length == 1) {
            dia = "0" + dia;
        }

        if (mes.length == 1) {
            mes = "0" + mes;
        }
        document.getElementById(dtFim).value = dia + '/' + mes + '/' + ano;
    }
}

function CopiaHora(HoraIniTxt, horaFim) {
    var HoraFimTxt = document.getElementById(horaFim);
    HoraFimTxt.value = HoraIniTxt.value;
    HoraFimTxt.focus();
}
/**** FUN��ES QUE DEVER�O ESTAR NO FRAMEWORK - SAMPAIO DHTML MODAL POPUP v2.0 ****/

function PegaCampo(form, NomeCampo) {
    theForm = form;
    for (i = 0; i < theForm.length; i++) {
        if (theForm.elements[i].name != undefined) {
            if (theForm.elements[i].name.indexOf(NomeCampo) > -1) {
                return theForm.elements[i];
                break;
            }
        }
    }
}

//fun��o que "injeta" um evento em um controle (por exemplo, coloca um onclick em um bot�o)
function addEvent(obj, evType, fn) {
    if (obj.addEventListener)
        obj.addEventListener(evType, fn, true)
    if (obj.attachEvent)
        obj.attachEvent("on" + evType, fn)
}

function AbrirModal(display, url, hiddenFieldId, textBoxId, args, width, height) {
    BackgroundJanelaModal(display);
    JanelaModal(display, url, hiddenFieldId, textBoxId, args, width, height);
}

//Respons�vel por criar e exibir o fundo semi-transparente
function BackgroundJanelaModal(display) {
    var divBackground = document.getElementById('backgroundJanelaModal');
    var body = document.getElementsByTagName('body')[0];

    if (divBackground == null) {
        divBackground = document.createElement('div');
        divBackground.setAttribute('id', 'backgroundJanelaModal');
        body.appendChild(divBackground);
    }

    divBackground.style.display = display;
}

var onChangeModal;
var onCloseModal;
function addOnCloseModalHandler(handler) {
    onCloseModal = handler;
}

function FecharModal() {
    AbrirModal('none', null);
    if (onCloseModal != '' && onCloseModal != null)
        eval(onCloseModal);
}

//Respons�vel por criar a janela modal
function JanelaModal(display, url, hiddenFieldId, textBoxId, args, width, height) {
    var iframe = document.getElementById('FrameModal');
    var divModal = document.getElementById('JanelaModal');
    var body = document.getElementsByTagName('body')[0];

    // tamanho da coluna
    var colsize = 'col-md-12';

    if (divModal == null) {
        // <div id="JanelaModal" class="modalPopup container">
        divModal = document.createElement('div');
        divModal.setAttribute('id', 'JanelaModal');
        divModal.className = "modalPopup container";

        // <div class="modalContent row">
        var divContent = document.createElement('div');
        divContent.className = "modalContent row";

        // <div class="col-*-*">
        var divFullContent = document.createElement('div');
        divFullContent.className = colsize;


        // <div class="modalHeader row"><a class="close pull-right"><span aria-hidden="true">�</span></a></div>
        var divHeader = document.createElement('div');
        divHeader.className = "modalHeader row";
        var fechar = document.createElement('a');
        fechar.className = "close pull-right";
        fechar.innerHTML = '<span aria-hidden="true">&times;</span>';
        addEvent(fechar, 'click', FecharModal);

        // <div class="col-*-*">
        var divFullFechar = document.createElement('div');
        divFullFechar.className = colsize;
        divFullFechar.appendChild(fechar);

        divHeader.appendChild(divFullFechar);

        // <div class="modalBody row">
        var divBody = document.createElement('div');
        divBody.className = "modalBody row";

        // <div class="col-*-*">
        var divFullBody = document.createElement('div');
        divFullBody.className = colsize;
        divBody.appendChild(divFullBody);

        divFullContent.appendChild(divHeader);
        divFullContent.appendChild(divBody);
        divContent.appendChild(divFullContent);
        divModal.appendChild(divContent);
        body.appendChild(divModal);
    }

    // Alinha e posiciona a modal
    $('#JanelaModal').css({
        'top': ($(window).scrollTop() + 100) + 'px',
        'left': (($(window).width() - $('#JanelaModal').width()) / 2) + 'px',
    });
    if (width != null)
        $('.modalPopup').css('width', width);
    if (height != null)
        $('.modalPopup').css('height', height);

    if (iframe == null) {
        iframe = document.createElement('iframe');
        iframe.setAttribute('id', 'FrameModal');
        iframe.setAttribute('frameBorder', '0');
        divFullBody.appendChild(iframe);
    }

    if (url != null) {
        if (args != null && args.toString() != 'NaN') {
            url = MontaURL(url, args);
            url = url + '&hid=' + hiddenFieldId + '&tid=' + textBoxId;
        } else {
            url = url + '?hid=' + hiddenFieldId + '&tid=' + textBoxId;
        }
        iframe.setAttribute('src', url);
    }
    else
        iframe.setAttribute('src', '');

    divModal.style.display = display;
}

//monta a url com a p�gina e o conjunto de parametros existentes
function MontaURL(url, args) {
    var path = url;
    var myArgs = args.split('&');
    for (i = 0; i < myArgs.length; i++) {
        if (i == 0)
            path += '?';
        path += ('arg' + i + '=');
        var field = document.getElementById(myArgs[i]);
        if (field != null)
            path += field.value;
        else
            path += myArgs[i];
        path += '&';
    }
    return path;
}

function CentralizaModal(div) {
    if ($('#' + div).length) {
        $('#' + div).css('top', ($(window).scrollTop() + 100) + 'px');
    }
}

function SetaValorModal(id, descricao) {
    //usa biblioteca Querystring.js
    var qs = new Querystring();
    cod = top.document.getElementById(qs.get('hid'));
    if (cod != null)
        cod.value = id;

    if (cod.onchange != null) {
        cod.onchange();
    }

    descricaoTxt = top.document.getElementById(qs.get('tid'));
    if (descricaoTxt != null) {
        descricaoTxt.value = descricao;
    }
    if (descricaoTxt.onchange != null) {
        descricaoTxt.onchange();
    }
    top.document.getElementById('backgroundJanelaModal').style.display = 'none';
    top.document.getElementById('JanelaModal').style.display = 'none';
    //dispara evento "onchange" ao setar o valor a partir do modal
    if (onChangeModal != null && onChangeModal != "") {
        eval('top.' + onChangeModal);
    }
    FecharModal();
}

addEvent(window, "resize", function () { CentralizaModal('JanelaModal') })


// M�todos para m�scaras

function getPosicaoCursor(o) {
    if (o.createTextRange) {
        var r = document.selection.createRange().duplicate()
        r.moveEnd('character', o.value.length)
        if (r.text == '') return o.value.length
        return o.value.lastIndexOf(r.text)
    } else {
        return o.selectionStart
    }
}

function setPosicaoCursor(o, posicao) {

    if (o != null) {
        if (o.createTextRange) {
            var range = o.createTextRange();
            range.move('character', posicao);
            range.select();
        } else {
            if (o.selectionStart) {
                o.focus();
                o.setSelectionRange(posicao, posicao);
            } else {
                o.focus();
            }
        }
    }
}

function formatar(valorTextBox, e, mascara) {
    {
        //var array = new Array();
        //array=mascara.split(';');    
        var caracteresEspeciais = new Array();
        var j = 0;
        for (var i = 0; i < mascara.length; i++) {
            if (mascara.charAt(i) != '_') {
                caracteresEspeciais[j] = mascara.charAt(i);
                j++;
            }
        }
        if (window.event) {
            if ((window.event.ctrlKey) || (window.event.shiftKey) || (window.event.altKey) || (window.event.altGrKey)) {
                isAuxiliar = true;
                return false;
            } else {
                isAuxiliar = false;
            }
        } else {
            if ((e.ctrlKey) || (e.shiftKey) || (e.altKey) || (e.altGrKey)) {
                isAuxiliar = true;
                return false;
            } else {
                isAuxiliar = false;
            }
        }
        var keyChar;
        var keyNum;
        if (window.event) { // IE
            keyNum = e.keyCode;
        }
        else if (e.which) { // Netscape/Firefox/Opera
            keyNum = e.which;
        }
        keyChar = String.fromCharCode(keyNum);

        if (isAuxiliar) {
            return false;
        }
        if (keyNum == 8) { //backspace
            setPosicaoCursor(valorTextBox, 0);
            return true;
        }
        if (keyNum == 46) { //delete
            var pos = getPosicaoCursor(valorTextBox);
            var nextChar = valorTextBox.value.substring(pos, pos + 1);
            if (caracteresEspeciais.toString().search(nextChar) == 0) {
                valorTextBox.value = valorTextBox.value.substring(0, pos) + nextChar + valorTextBox.value.substring(pos, valorTextBox.value.length);
                setPosicaoCursor(valorTextBox, pos + 1);
            } else {
                if (nextChar != '') {
                    valorTextBox.value = valorTextBox.value.substring(0, pos) + '_' + valorTextBox.value.substring(pos, valorTextBox.value.length);
                    setPosicaoCursor(valorTextBox, pos + 1);
                }
            }
            return true;
        }
        if ((keyNum >= 48 && keyNum <= 57) || (keyNum >= 96 && keyNum <= 105)) {//numero
            if (getPosicaoCursor(valorTextBox) > mascara.length - 1) {
                return false;
            }
            var pos = getPosicaoCursor(valorTextBox);
            var nextChar = valorTextBox.value.substring(pos, pos + 1);
            if (caracteresEspeciais.toString().search(nextChar) == 0) {
                setPosicaoCursor(valorTextBox, pos + 1);
                formatar(valorTextBox, e, mascara);
            } else {
                var tail = new String(valorTextBox.value.substring(pos + 1, valorTextBox.value.length));
                if (tail.search("_") > -1) {
                    var i = 0;
                    var aux;
                    var prox = nextChar;
                    while (i < tail.length) {
                        aux = tail.charAt(i);
                        if (caracteresEspeciais.toString().search(aux) != 0) {
                            tail = tail.substring(0, i) + prox + tail.substring(i + 1, tail.length);
                            prox = aux;
                        }
                        i++;
                    }
                }
                valorTextBox.value = valorTextBox.value.substring(0, pos) + tail;
                setPosicaoCursor(valorTextBox, pos, mascara);
            }
            return true;
        }
        if ((keyNum == 37) || (keyNum == 39) || (keyNum == 9) || (keyNum == 13)) {//setas direita/esquerda, enter e tabulacao
            return true;
        }
        return false;
    }
    return true;
}

function verificarFormatacao(valorTextBox) {
    if (valorTextBox.value != "__:__") {
        var hora = valorTextBox.value.split(':')[0];
        var minutos = valorTextBox.value.split(':')[1];
        if (hora < 24 && minutos < 60) {
            $('#' + validationSummaryId)[0].style.display = 'none';
        }
        else {
            var erro = document.createElement("ul");
            erro.innerHTML = "Campo Hora inv�lido. Formato correto: HH:MM.";
            var validationSummary = $('#' + validationSummaryId);
            validationSummary[0].innerHTML = "";
            validationSummary[0].style.color = "#C6150D";
            validationSummary.append(erro);
            validationSummary.css("display", "");
        }
    }
}
function preencheBarras(comp) {
    var arrDt = comp.value.split('/');
    if (arrDt.length == 1) {
        if (arrDt[0].length == 4)
            comp.value = arrDt[0].substring(0, 1) + '/' + arrDt[0].substring(1, 2) + '/' + arrDt[0].substring(2, 4);
        if (arrDt[0].length == 5) {
            if (Math.abs(arrDt[0].substring(0, 1)) > 3)
                comp.value = arrDt[0].substring(0, 1) + '/' + arrDt[0].substring(1, 3) + '/' + arrDt[0].substring(3, 5);
            else
                comp.value = arrDt[0].substring(0, 2) + '/' + arrDt[0].substring(2, 3) + '/' + arrDt[0].substring(3, 5);
        }
        if (arrDt[0].length == 6) {
            if (Math.abs(arrDt[0].substring(2, 4)) > 12) {
                comp.value = arrDt[0].substring(0, 1) + '/' + arrDt[0].substring(1, 2) + '/' + arrDt[0].substring(2, 6);
            } else
                comp.value = arrDt[0].substring(0, 2) + '/' + arrDt[0].substring(2, 4) + '/' + arrDt[0].substring(4, 6);
        }
        if (arrDt[0].length == 7) {
            if (Math.abs(arrDt[0].substring(0, 1)) > 3)
                comp.value = arrDt[0].substring(0, 1) + '/' + arrDt[0].substring(1, 3) + '/' + arrDt[0].substring(3, 7);
            else
                comp.value = arrDt[0].substring(0, 2) + '/' + arrDt[0].substring(2, 3) + '/' + arrDt[0].substring(3, 7);
        }
        if (arrDt[0].length == 8)
            comp.value = arrDt[0].substring(0, 2) + '/' + arrDt[0].substring(2, 4) + '/' + arrDt[0].substring(4, 8);
    } else if (arrDt.length == 2) {
        if (arrDt[0].length < 3 && arrDt[1].length > 4) {
            if (arrDt[1].length == 5)
                comp.value = arrDt[0] + '/' + arrDt[1].substring(0, 1) + '/' + arrDt[1].substring(1, 5);
            if (arrDt[1].length == 6)
                comp.value = arrDt[0] + '/' + arrDt[1].substring(0, 2) + '/' + arrDt[1].substring(2, 6);
        }
        if (arrDt[0].length == 2 && arrDt[1].length == 4) {
            if (Math.abs(arrDt[1].substring(0, 2)) > 12)
                comp.value = arrDt[0].substring(0, 1) + '/' + arrDt[0].substring(1, 2) + '/' + arrDt[1];
            else
                comp.value = arrDt[0] + '/' + arrDt[1].substring(0, 2) + '/' + arrDt[1].substring(2, 4);
        }
        if (arrDt[0].length == 2 && arrDt[1].length == 2)
            comp.value = arrDt[0].substring(0, 1) + '/' + arrDt[0].substring(1, 2) + '/' + arrDt[1];
        if (arrDt[0].length == 3) {
            if (Math.abs(arrDt[0].substring(0, 1)) > 3)
                comp.value = arrDt[0].substring(0, 1) + '/' + arrDt[0].substring(1, 3) + '/' + arrDt[1];
            else
                comp.value = arrDt[0].substring(0, 2) + '/' + arrDt[0].substring(2, 3) + '/' + arrDt[1];
        }
        if (arrDt[0].length == 4)
            comp.value = arrDt[0].substring(0, 2) + '/' + arrDt[0].substring(2, 4) + '/' + arrDt[1];
    }
}

function completarAno(comp) {
    var arrDt = comp.value.split('/');
    if ((arrDt.length == 3) && (arrDt[2].length == 2)) {
        ano = Math.abs(arrDt[2]);
        if (ano > 50) { seculo = '19'; } else { seculo = '20'; }
        comp.value = arrDt[0] + '/' + arrDt[1] + '/' + seculo + arrDt[2];
    }
}

//Para abrir a p�gina em nova aba
function abrirPaginaConferencia(pagina) {
    var rootURLSite = $('#rootURLSite').val();
    window.open(rootURLSite + 'VerificaAutenticidadeDocumento.aspx', '_blank');
}

function abrirManual() {
    var rootURLSite = $('#rootURLSite').val();
    window.open(rootURLSite + 'Manual.pdf', '_blank');
}

function abrirPerguntasFrequentes() {
    $('#linkAjudaMasterPage')[0].click();
}

function AbrirModalArquivosDigitalizados() {
    $('[id$="AbrirModalButton"]').click();
}
function MostraUpLoad() {
    $('A', '#LinkParaUpLoadSpan')[0].style.display = 'none';
    $('A', '#LinkParaUpLoadSpan')[1].style.display = 'none';
    $('INPUT', '#ArquivoUpLoadSpan')[0].style.display = 'inline';
}
function ArquivoUpload_Changed(fileUpload) {
    //var id = fileUpload.id.replace('ArquivoUpLoad', 'EnviarArquivoButton');
    //var btn = document.getElementById(id);
    //btn.click();
}