﻿var mostraValor = true;
function MostrarValores(linha, id){
  if (mostraValor){
    var valoresGrid = document.getElementById("ValoresGrid");
    if (valoresGrid != null){
      var l = $(linha);
      var position = l.offset();
      if (navigator.appName == 'Microsoft Internet Explorer')
        valoresGrid.style.top = position.top + l.height() - 25 + 'px';
      else
        valoresGrid.style.top = position.top + l.height() + 10 + 'px';
      valoresGrid.style.display = "block";
      document.getElementById("ConteudoValoresGrid").innerHTML = "";
      MostrarCarregando(true);
      BuscarValores(id);
    }
  }
  else
    mostraValor = true;
}

function MostrarValoresConfiguravel(linha, id, titulo, mensagemVazia){
  if (mostraValor){
    var valoresGrid = document.getElementById("ValoresGrid");
    if (valoresGrid != null){
      var l = $(linha);
      var position = l.offset();
      if (navigator.appName == 'Microsoft Internet Explorer')
        valoresGrid.style.top = position.top + l.height() - 25 + 'px';
      else
        valoresGrid.style.top = position.top + l.height() + 10 + 'px';
      valoresGrid.style.display = "block";
      document.getElementById("ConteudoValoresGrid").innerHTML = "";
      MostrarCarregando(true);
      BuscarValoresConfiguravel(id, titulo, mensagemVazia);
    }
  }
  else
    mostraValor = true;
}

function FecharValores(){
  var valoresGrid = document.getElementById("ValoresGrid");
  if (valoresGrid != null){
    valoresGrid.style.display = "none";
  }
}

function onCompleteBuscarValores(response){
  MostrarCarregando(false);
  document.getElementById("ConteudoValoresGrid").innerHTML = response;
}
function onCompleteBuscarValoresConfiguravel(response){
  MostrarCarregando(false);
  document.getElementById("ConteudoValoresGrid").innerHTML = response;
}

function onErrorBuscarValores(response){
  MostrarCarregando(false);
  var warning = "<div class=\"warning\"><ul>";
  warning += "<li>Erro ao buscar os valores.</li>";
  warning += "<li>" + response._message + "</li>";
  warning += "</ul></div>";
  document.getElementById("ConteudoValoresGrid").innerHTML = warning;
}
function onErrorBuscarValoresConfiguravel(response){
  MostrarCarregando(false);
  var warning = "<div class=\"warning\"><ul>";
  warning += "<li>Erro ao buscar os valores.</li>";
  warning += "<li>" + response._message + "</li>";
  warning += "</ul></div>";
  document.getElementById("ConteudoValoresGrid").innerHTML = warning;
}

function onTimeOutBuscarValores(response){
  MostrarCarregando(false);
  var warning = "<div class=\"warning\"><ul>";
  warning += "<li>Tempo Expirado.</li>";
  warning += "<li>" + response._message + "</li>";
  warning += "</ul></div>";
  document.getElementById("ConteudoValoresGrid").innerHTML = warning;
}
function onTimeOutBuscarValoresConfiguravel(response){
  MostrarCarregando(false);
  var warning = "<div class=\"warning\"><ul>";
  warning += "<li>Tempo Expirado.</li>";
  warning += "<li>" + response._message + "</li>";
  warning += "</ul></div>";
  document.getElementById("ConteudoValoresGrid").innerHTML = warning;
}

function MostrarCarregando(mostra){
  var carregandoDiv = document.getElementById("CarregandoDiv");
  if (mostra){
    carregandoDiv.style.visibility = 'visible';
    carregandoDiv.style.display = 'block';
  }
  else {
    carregandoDiv.style.visibility = '';
    carregandoDiv.style.display = '';
  }
}

function BuscarValoresConfiguravel(id, titulo, mensagemVazia) {
  var resultado = 
  "<table rules=\"all\" cellspacing=\"0\" border=\"1\" style=\"background: #ffffff; width: 100%; border-collapse: collapse; margin-right: 0px; font-size:11px;\" id=\"tabelaValores\">" +
  "<tbody><tr align=\"center\" style=\"color: White; background-color: Gray;\">" +
  "<th align=\"center\" scope=\"col\" class=\"espacoEsquerdo\" width=\"150px\">"+titulo+"</th>" +
  "</tr>" +
  "<tr class=\"RowStyle\">" +
  "<td align=\"left\" class=\"espacoEsquerdo\">";
  if (id == null || id == "") {
    if (mensagemVazia == null) 
      resultado = resultado + "Motivo não informado.</td>"
    else
      resultado = resultado + mensagemVazia +"</td>";
  }
  else
   resultado = resultado + id + "</td>"; 
  onCompleteBuscarValores(resultado);
}  
