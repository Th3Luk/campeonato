// Script de inicializacao
function init(evt) {
  // Inicialização de SVGPan.js
  root = document.documentElement;
  setupHandlers(root);
  // Id eh obrigatoriamente 'viewport' para funcionamento do SVGPan.js
  $('#graph0').attr('id', 'viewport');
  
  // Seta um tamanho minimo para a viewBox do Fluxograma
  $('#viewport').parent().attr('height', '1500px');
  $('#viewport').parent().attr('width', '1500px');
  $('#viewport').parent().attr('viewBox', '0.00 0.00 1500.00 1500.00');

  // MouseOver MouseOut Effects - Deixa colorido o elemento que esta sendo selecionado
  var strokeColor = '';
  var strokeWidh = '';
  $('#viewport g[class~="node"]').mouseover(function (evt) {
    strokeColor = $('polygon:last', $(this)).attr('stroke');
    strokeWidh = $('polygon:last', $(this)).attr('stroke-width');
    $('polygon:last', $(this)).attr('stroke', '#1E90FF');
    $('polygon:last', $(this)).attr('stroke-width', '1px');
  });
  $('#viewport g[class~="node"]').mouseout(function (evt) {
    $('polygon:last', $(this)).attr('stroke', strokeColor);
    $('polygon:last', $(this)).attr('stroke-width', strokeWidh);
  });

  // Afasta o fluxograma da area do navigation.svg, para que o fluxo nao fique escondido
  var precisa_corrigir = false;
  var nav_area = $('#viewport').attr('transform').replace(/scale\(.*\) rotate\(.*\) translate\(/g, '').replace(')', '').split(' ');
  $('#viewport image').each(function () {
    var link = $(this).attr('xlink:href');
    if (link.split('/')[link.split('/').length - 1] == 'inicio.png')
      if ((parseInt(nav_area[1]) - (-1 * parseInt($(this).attr('y')))) < 120) // 120x120 eh o tamanho do navigation.svg
        precisa_corrigir = true;
  });
  if (precisa_corrigir)
    $('#viewport').attr('transform', 'scale(1 1) rotate(0) translate(' + nav_area[0] + ' ' + (parseInt(nav_area[1]) + 120) + ')');
}

// Forca a execucao do script de inicializacao 'init()'
window.addEventListener('load', init, false);







