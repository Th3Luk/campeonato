// Sobreescreve o _resizeMenu do autocomplete para forcar que o menu tenha o tamanho do input que o chama
jQuery.ui.autocomplete.prototype._resizeMenu = function () {
	var ul = this.menu.element;
	ul.outerWidth(this.element.outerWidth());
};

// Seta o autocomplete
var cache = {};
function setAutoComplete(textFieldId, hiddenFieldId, sourceUrl, fntData, afterSelectCallback, optDelay, optMinLength, optMaxResults, optUseCache) {
	// Aguardar antes de buscar pelo termo
	if(optDelay == undefined)
		optDelay = 300;
	
	// Inicia a busca somente com um numero minimo de caracteres - setar 0 para procurar com qualquer qntidade
	if(optMinLength == undefined)
		optMinLength = 1;
	
	// Exibe um maximo de ocorrencias - setar 0 para exibir todas
	if(optMaxResults == undefined)
		optMaxResults = 10;
		
	// Usar ou nao o cache de resultados
	if(optUseCache == undefined)
		optUseCache = true;

	// Caso o numero de ocorrencias a serem exibidas for menor que o numero de resultados, exibir esta mensagem
	var MaxResultsMsg = '';

	// Funcao que adiciona uma mensagem informando as qtdes de resultados da pesquisa
	attachMensage = function(menu,display) {
		// Adicionar a mensagem apos a ul, caso exista
		var ul_id = menu.attr('id') + '-msg';
		if($('#'+ul_id).length == 0){ 
			var msg = $('<ul class="ui-autocomplete ui-front ui-menu ui-widget ui-widget-content ui-corner-all"></ul>')
						.css('display','none').attr('id',ul_id);
			menu.after(msg);
		}
		// Remove as mensagens anteriores, caso existam
		$('#'+ul_id+' li').remove();
		
		// Define a posicao do novo elemento
		var left = menu.css('left');
		var width = menu.css('width');
		var height = parseInt(menu.css('height').replace('px','')) - 1; // @vinicius.garcia : -1 para o Bootstrap; +7 para o GED antigo
		var top = (parseInt(menu.css('top').replace('px','')) + height) + 'px';
		$('#'+ul_id).css('top',top).css('left',left).css('width',width).css('display',display);
		
		// Inclui a mensagem atual
		$('#'+ul_id).append('<li class="ui-autocomplete-msg">' + MaxResultsMsg + '</li>');
	};
	
	$('#' + textFieldId).autocomplete({
		  delay: optDelay
		, minLength: optMinLength
		, source: function(request, response) {
			// Procura o termo no cache
			if((optUseCache) && (cache[textFieldId] != undefined) && (request.term in cache[textFieldId])) {
				MaxResultsMsg = cache[textFieldId][request.term].msg;
				response(cache[textFieldId][request.term].req);
			}else{
				// Caso nao tenha encontrado, pesquisa no servidor
				var data = fntData(request.term);
				$.ajax({
					url: sourceUrl,
					data: data,
					dataType: "json",
					type: "POST",
					contentType: "application/json; charset=utf-8",
					dataFilter: function(data) {
						return data;
					},
					success: function(data) {
						// Formata os dados recebidos
						var results = $.map(data.d, function(item) {
						return {
							label: item.Description,
							value: item.PrimaryKey + ' | ' + item.Description
						}
						});

						// Exibe um maximo de ocorrencias
						var numOcorrenciasTotal = results.length;
						if((optMaxResults > 0) && (results.length > optMaxResults)){
							results = results.slice(0, optMaxResults);
						}
						var numOcorrenciasExibicao = results.length;

						// Adicionar mensagem indicando que o usuario pode nao esta visualizando todas as opcoes possiveis
						MaxResultsMsg = 'Visualizando ' + numOcorrenciasExibicao + ' resultado(s) de ' + numOcorrenciasTotal;
						
						// Guardando resultado no cache
						if(optUseCache){
							if(cache[textFieldId] == undefined)
								cache[textFieldId] = {};
							cache[textFieldId][request.term] = { req: results, msg: MaxResultsMsg };
						}
						response(results);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						alert('Erro ao buscar informações');
					}
				});
			}
		}
		, close: function (event, ui) {
			// Esconde o footer do menu
			attachMensage($(this).data( 'ui-autocomplete' ).menu.element,'none');
		
			// Id e Texto sao separados por um ' | '
			var id = '', text = $(this).val();
			var data = text.split(' | ');
			if (data.length > 1) {
				id = data[0];
				text = data[1];
			}

			$('#' + hiddenFieldId).val(id);
			$('#' + textFieldId).val(text);

			// Executa uma operacao adicional com os dados
			if (typeof (afterSelectCallback) == "function")
				afterSelectCallback(id, text);

			$('#' + textFieldId).change();
		}
		, change: function (event, ui) {
			// Esconde o footer do menu
			attachMensage($(this).data( 'ui-autocomplete' ).menu.element,'none');
			
			if (ui.item == null) {
				$('#' + hiddenFieldId).val('');
				$('#' + textFieldId).val('');

				// Executa uma operacao adicional com os dados
				if (typeof (afterSelectCallback) == "function")
					afterSelectCallback('', $(this).val());
			}
		}
		, focus: function (event, ui) {
			// Id e Texto sao separados por um ' | '
			var id = '', text = ui.item.value;
			var data = text.split(' | ');
			if (data.length > 1) {
				id = data[0];
				text = data[1];
			}

			//$('#' + hiddenFieldId).val(id);
			$('#' + textFieldId).val(text);

			// Executa uma operacao adicional com os dados
			if (typeof (afterSelectCallback) == "function")
				afterSelectCallback(id, text);

			return false;
		}
		, open: function (event, ui) {
			// Mostra o footer do menu
			attachMensage($(this).data( 'ui-autocomplete' ).menu.element,'block');
		}
	});
};
//function XeretaValidator()
//correção do bug ASP 
//http://connect.microsoft.com/VisualStudio/feedback/details/471224/asp-net-validators-validatoronchange-event-errors-under-certain-circumstances
function ValidatorOnChange(event) {
  if (!event) {
    event = window.event;
  }
  Page_InvalidControlToBeFocused = null;
  var targetedControl;
  if ((typeof (event.srcElement) != "undefined") && (event.srcElement != null)) {
    targetedControl = event.srcElement;
  }
  else {
    targetedControl = event.target;
  }
  var vals;
  if (typeof (targetedControl.Validators) != "undefined") {
    vals = targetedControl.Validators;
  }
  else {
    if (targetedControl.tagName.toLowerCase() == "label") {
      targetedControl = document.getElementById(targetedControl.htmlFor);
      vals = targetedControl.Validators;
    }
  }
  if (vals) {
    var i;
    for (i = 0; i < vals.length; i++) {
      ValidatorValidate(vals[i], null, event);
    }
  }
  ValidatorUpdateIsValid();
} 