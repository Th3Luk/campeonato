﻿function MascaraCNPJ(cnpj){    
    formataCampo(cnpj, '00.000.000/0000-00', event);
}

function MascaraCPF(cpf){    
    formataCampo(cpf, '000.000.000-00', event);
}

function MascaraCEP(cep){
    formataCampo(cep, '00.000-000', event);
}

function MascaraFONE(tel){        
    formataCampo(tel, '(00) 0000-0000', event);
}

function MascaraMOEDA(valor){        
    formataValor(valor);
}

function MascaraPROCESSO(processo){        
    return FormataNumeroProcesso(processo, event);
}
function MascaraPROTOCOLO(protocolo){
  return FormataNumeroProtocolo(protocolo, event);
}

function CompletaPROTOCOLO(protocolo){
    CompletaNumeroProtocolo(protocolo);
}
function CompletaPROCESSO(processo){
    CompletaNumeroProcesso(processo);
}

function formataValor(campo) {
    exp = /\.|\,| /g
    temp = campo.value;
    campo.value = campo.value.toString().replace( exp, "" );
	vr = campo.value;
	tam = vr.length;	

	if ( tam <= 2 ){ 
 		campo.value = vr ; }
 	else if ( (tam > 2) && (tam <= 5) ){
 		campo.value = vr.substr( 0, tam - 2 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	else if ( (tam >= 6) && (tam <= 8) ){
 		campo.value = vr.substr( 0, tam - 5 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	else if ( (tam >= 9) && (tam <= 11) ){
 		campo.value = vr.substr( 0, tam - 8 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	else if ( (tam >= 12) && (tam <= 14) ){
 		campo.value = vr.substr( 0, tam - 11 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	else if ( tam > 14){
 	    //testa se atingiu o tamanho maximo e descarta ultimo caractere digitado;
        //implementado para evitar usar MaxLength do TextBox que invalida o evento de postback TextBoxChanged;
 	    campo.value = temp.substr(0,temp.length-1);} 	 		
}

 function formataCampo(campo, Mascara, evento) {
    var booleanoMascara;
    
    var digitado = evento.keyCode;
    exp = /\-|\.|\/|\(|\)| /g
    campoSoNumeros = campo.value.toString().replace( exp, "" );  
    var posicaoCampo = 0;    
    var NovoValorCampo="";
    var TamanhoMascara = campoSoNumeros.length;
    
    if (digitado != 8) { // backspace
        //testa se atingiu o tamanho maximo e descarta ultimo caractere digitado;
        //implementado para evitar usar MaxLength do TextBox que invalida o evento de postback TextBoxChanged;
        temp = campo.value;  
        if(Mascara.length == temp.length){            
            campo.value = temp.substr(0,temp.length-1);
            return true;
        }
        for(i=0; i<= TamanhoMascara; i++) {
            booleanoMascara  = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".")
                                || (Mascara.charAt(i) == "/"))
            booleanoMascara  = booleanoMascara || ((Mascara.charAt(i) == "(")
                                || (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " "))            
            if (booleanoMascara) {
                NovoValorCampo += Mascara.charAt(i);
                  TamanhoMascara++;
            }else {
                NovoValorCampo += campoSoNumeros.charAt(posicaoCampo);
                posicaoCampo++;
              }           
          }    
        campo.value = NovoValorCampo;
          return true;
    }else {
        return true;
    }
}

// protocolo 
var Id;
function SetarFocus(id){
 try {
 Id=id;
 setTimeout('FocusParaId()',1000);
  }
catch(err) {
      
    }
}
function FocusParaId(){
  if($('#'+Id)[0]!=undefined && !$('#'+Id)[0].disabled )
     $('#'+Id)[0].focus();
}

function FormataNumeroProtocolo(Campo, teclapres) {
  var tecla = teclapres.keyCode;
  var vr = new String(Campo.value);
  vr = vr.replace(".","");
  tam = vr.length + 1;
  if (tam == 5 && tecla != 46) 
    Campo.value = vr.substr(0,4) + '.';
  if (tam > 5 && tam <= 11)
    Campo.value = vr.substr(0, 4) + '.' + vr.substr(4, tam - 5);
  if (tecla == 13) {
    if(tam == 11)
      return true;
    else{
      CompletaNumeroProtocolo(Campo);
      return false;
    }
  }
  return true;
}

function CompletaNumeroProtocolo(Campo) {
  vlr = new String(Campo.value)
  if (vlr.length > 0) {
    if (vlr.length >= 4) {
      if (vlr.length == 4)
        vlr = vlr + ".";
      posicoesVazias = 11 - vlr.length;
      vlrAposPonto = vlr.substr(5, vlr.length - 5);
      vlr = vlr.substr(0, 5);
      for (i = 1; i <= posicoesVazias; i++)
        vlr = vlr + "0";
      vlr = vlr + vlrAposPonto;
      Campo.value = vlr;
    } else {
      var d = new Date().getFullYear();
      posicoesVazias = 6 - vlr.length;
      for (i = 1; i <= posicoesVazias; i++)
        vlr = "0" + vlr;
      vlr = d + "." + vlr;
      Campo.value = vlr;
    }
  }
}

// fim protocolo


// Processo

function FormataNumeroProcesso(Campo, teclapres) {
  var tecla = teclapres.keyCode;
  var vr = new String(Campo.value);
  var temBarra = (vr.indexOf('/',0) != -1);
  vr = vr.replace("/","");
  tam = vr.length + 1;
  if (tam == 9 && tecla != 47 && !temBarra) 
    Campo.value = vr.substr(0,8) + '/';
  if (tam > 9 && tam <= 13 & !temBarra)
    Campo.value = vr.substr(0, 8) + '/' + vr.substr(8, tam - 8);
  if (tecla == 13) {
    if (tam == 13)
      return true;
    else {
      CompletaNumeroProcesso(Campo);
      return false;
    }
  }
  return true;
}

function CompletaNumeroProcesso(Campo) { 
 vlr = new String(Campo.value)
  if (vlr.length > 0) {
    var ano = new Date().getFullYear();
    if(vlr.indexOf('/',0) != -1){
        ano = vlr.split('/')[1];
        vlr = vlr.split('/')[0];        
    }
    if (vlr.length < 8) {
      var vazias = 8 - vlr.length;
      var i;
      for (i = 1;i<=vazias;i++)
        vlr = "0" + vlr;
    } 
    if (vlr.length < 13) {
      vlr = vlr.substr(0,8);      
      vlr = vlr + '/' + ano;
    }
    Campo.value = vlr;
  }
}
//fim processo