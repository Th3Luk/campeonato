﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes.Base;

namespace Classes
{
  public class Usuario : BaseNomeEntidade
  {
    private PerfilJogador perfilJogador;
    public virtual PerfilJogador PerfilJogador {
      get { return perfilJogador; }
      set { perfilJogador = value; }
    }

    private string login;
    public virtual string Login
    {
      get { return login; }
      set { login = value; }
    }

    private byte[] senha;
    public virtual byte[] Senha
    {
      get { return senha; }
      set { senha = value; }
    }

    private DateTime dataCadastro;
    public virtual DateTime DataCadastro
    {
      get { return dataCadastro; }
      set { dataCadastro = value; }
    }

    private bool admin;
    public virtual bool Admin
    {
      get { return admin; }
      set { admin = value; }
    }
  }
}