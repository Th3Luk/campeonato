﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes.Base;

namespace Classes
{
  public class Time : BaseNomeEntidade 
  {
    private PerfilJogo capitao;
    public virtual PerfilJogo Capitao
    {
      get { return capitao; }
      set { capitao = value; }
    }

    private PerfilJogo criador;
    public virtual PerfilJogo Criador
    {
      get { return criador; }
      set { criador = value; }
    }

    private byte[] logo; //verificar a melhor maneira de mapear imagem
    public virtual byte[] Logo
    {
      get { return logo; }
      set { logo = value; }
    }

    private string tag;
    public virtual string Tag
    {
      get { return tag; }
      set { tag = value; }
    }
  }
}