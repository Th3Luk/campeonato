﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes.Base;

namespace Classes
{
  public class Jogo : BaseNomeEntidade
  {
    private string sigla;
    public virtual string Sigla
    {
      get { return sigla; }
      set { sigla = value; }
    }

    private byte[] logo;
    public virtual byte[] Logo
    {
      get { return logo; }
      set { logo = value; }
    }
  }
}