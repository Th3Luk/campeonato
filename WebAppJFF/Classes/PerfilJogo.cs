﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes.Base;
using Classes;

namespace Classes
{
  public class PerfilJogo : BaseNomeEntidade
  {
    private PerfilJogador perfilJogador;
    public virtual PerfilJogador PerfilJogador
    {
      get { return perfilJogador; }
      set { perfilJogador = value; }
    }

    private int idJogo;
    public virtual int IdJogo
    {
      get { return idJogo; }
      set { idJogo = value; }
    }

    private Time time;
    public virtual Time Time
    {
      get { return time; }
      set { time = value; }
    }

    private int tempoJogo;
    public virtual int TempoJogo
    {
      get { return tempoJogo; }
      set { tempoJogo = value; }
    }

    private string descricao;
    public virtual string Descricao
    {
      get { return descricao; }
      set { descricao = value; }
    }

    private int idPosicao;
    public virtual int IdPosicao
    {
      get { return idPosicao; }
      set { idPosicao = value; }
    }
  }
}