﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes.Base;

namespace Classes
{
  public class Posicao : BaseNomeEntidade 
  {
    private Jogo jogo;
    public virtual Jogo Jogo
    {
      get { return jogo; }
      set { jogo = value; }
    }

    private string descricao;
    public virtual string Descricao
    {
      get { return descricao; }
      set { descricao = value; }
    }
  }
}