﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Classes.Base
{
  public abstract class BaseNomeEntidade : BaseAtivoEntidade, IComparable {
    private string nome;

    public virtual string Nome {
      get { return nome; }
      set { nome = value; }
    }

    public override string ToString() {
      return this.nome;
    }

    #region IComparable Members

    public virtual int CompareTo(object obj) {
      return nome.CompareTo(((BaseNomeEntidade)obj).Nome);
    }

    #endregion
  }
}