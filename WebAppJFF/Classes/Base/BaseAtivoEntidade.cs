﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Classes.Base{
  public class BaseAtivoEntidade : BaseEntidade {
    private bool ativo = true;

    public virtual bool Ativo {
      get { return ativo; }
      set { ativo = value; }
    }
  }
}