﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Classes.Base{
  [Serializable]
  public abstract class BaseEntidade {
    private int id;

    public virtual int Id {
      get { return id; }
      set { id = value; }
    }

    protected internal virtual BaseEntidade Unproxy() {
      return this;
    }
  }

  public static class EntidadeExtensions {
    public static TType As<TType>(this BaseEntidade entity) where TType : BaseEntidade {
      if (entity == null)
        return null;
      return entity.Unproxy() as TType;
    }

    public static TType CastTo<TType>(this BaseEntidade entity) where TType : BaseEntidade {
      if (entity == null)
        return null;
      return (TType)entity.Unproxy();
    }

    public static bool Is<TType>(this BaseEntidade entity) where TType : BaseEntidade {
      if (entity == null)
        return false;
      return entity.Unproxy() is TType;
    }
  }
  
}