﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Classes
{
  public enum TipoCriptografia
  {
    [Description("Simétrica")]
    Simetrica = 'S',
    [Description("Assimétrica")]
    Assimetrica = 'A'
  }

  public enum JogoEnum
  {
    [Description("Dota")]
    Dota = 1,
    [Description("League of Legends")]
    Lol = 2,
    [Description("CS")]
    Cs = 3
  }

  public enum PosicaoDota
  {
    [Description("Hard Carrier")]
    HC = 1,
    [Description("Support 1")]
    Sup1 = 2,
    [Description("Support 2")]
    Sup2 = 3,
    [Description("Jungle")]
    Jungle = 4,
    [Description("Off Lane")]
    Off = 5,
    [Description("Manager")]
    Manager = 6
  }

  public enum PosicaoLol
  {
    [Description("Hard Carrier")]
    HC = 1,
    [Description("Support 1")]
    Sup1 = 2,
    [Description("Support 2")]
    Sup2 = 3,
    [Description("Jungle")]
    Jungle = 4,
    [Description("Off Lane")]
    Off = 5,
    [Description("Manager")]
    Manager = 6
  }

  public enum PosicaoCs
  {
    [Description("Sniper")]
    HC = 1,
    [Description("Tank")]
    Sup1 = 2,
    [Description("Bomb")]
    Sup2 = 3,
    [Description("Manager")]
    Manager = 4
  }

  public enum FaculdadeEnum
  {
    [Description("Universidade Tecnológica Federal do Paraná - Ponta Grossa")]
    UTFPRPG = 1,
    [Description("Universidade Tecnológica Federal do Paraná - Curitiba")]
    UTFPRCU = 2,
    [Description("Universidade Estadual de Ponta Grossa")]
    UEPG = 3,
    [Description("Universidade Federal do Paraná")]
    UFPR = 4
  }

  public static class EnumHelper
  {
    public static string ObterDescricaoEnum(object valor)
    {
      Type type = valor.GetType();
      MemberInfo[] memInfo = type.GetMember(valor.ToString());
      if (memInfo != null && memInfo.Length > 0)
      {
        object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
        if (attrs != null && attrs.Length > 0)
          return ((DescriptionAttribute)attrs[0]).Description;
      }
      return valor.ToString() == "0" ? "Não cadastrado" : valor.ToString();
    }

    public static string ObterValorEnum(object valor)
    {
      if (valor != null)
        return Convert.ToInt32(valor).ToString();
      return null;
    }
  }
}