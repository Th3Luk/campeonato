﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Classes.Base;

namespace Classes
{
  public class ArqTemp : BaseEntidade 
  {
    private byte[] imagem;
    public virtual byte[] Imagem
    {
      get { return imagem; }
      set { imagem = value; }
    }
  }
}
