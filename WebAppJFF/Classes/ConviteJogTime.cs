﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Classes;
using Classes.Base;

namespace Classes
{
  public class ConviteJogTime  : BaseEntidade 
  {
    private Time time;
    public virtual Time Time
    {
      get { return time; }
      set { time = value; }
    }

    private PerfilJogo jogador;
    public virtual PerfilJogo Jogador
    {
      get { return jogador; }
      set { jogador = value; }
    }

    private DateTime dataConvite;
    public virtual DateTime DataConvite
    {
      get { return dataConvite; }
      set { dataConvite = value; }
    }

  }
}
