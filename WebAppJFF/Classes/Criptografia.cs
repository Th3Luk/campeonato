﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using Classes.Base;

namespace Classes
{
  public class Criptografia : BaseNomeEntidade
  {
    public TipoCriptografia TpCriptografia
    {
      get;
      protected set;
    }

    #region Construtores

    public Criptografia()
    {
      this.TpCriptografia = TipoCriptografiaPadrao;
    }

    public Criptografia(char tipoCriptografia)
    {
      this.TpCriptografia = ObterTipoCriptografia(tipoCriptografia);
    }

    public Criptografia(TipoCriptografia tipoCriptografia)
    {
      this.TpCriptografia = tipoCriptografia;
    }

    #endregion

    byte[] resultCript;

    public byte[] MontarCriptografia(byte[] conteudo)
    {
      if (conteudo == null)
        return null;
      if (TpCriptografia == TipoCriptografia.Assimetrica)
        resultCript = CripAssimetrica(conteudo);
      else
        resultCript = CripSimetrica(conteudo);
      return resultCript;
    }

    public byte[] Descriptografar(byte[] conteudo)
    {
      if (TpCriptografia == TipoCriptografia.Assimetrica)
        resultCript = DesCripAssimetrica(conteudo);
      else
        resultCript = DesCripSimetrica(conteudo);
      return resultCript;
    }

    /***** Elementos estáticos *******/

    static public byte[] Descriptografar(char tipo, byte[] conteudo)
    {
      return Descriptografar(ObterTipoCriptografia(tipo), conteudo);
    }

    static public byte[] Descriptografar(TipoCriptografia tipo, byte[] conteudo)
    {
      if (conteudo == null)
        return null;
      if (tipo == TipoCriptografia.Assimetrica)
        return DesCripAssimetrica(conteudo);
      if (tipo == TipoCriptografia.Simetrica)
        return DesCripSimetrica(conteudo);
      throw new NotImplementedException("Não há método para descriptografar este tipo de criptografia.");
    }

    static public byte[] MontarCriptografia(char tipo, byte[] conteudo)
    {
      return MontarCriptografia(ObterTipoCriptografia(tipo), conteudo);
    }

    static public byte[] MontarCriptografia(TipoCriptografia tipo, byte[] conteudo)
    {
      if (conteudo == null)
        return null;
      if (tipo == TipoCriptografia.Assimetrica)
        return CripAssimetrica(conteudo);
      if (tipo == TipoCriptografia.Simetrica)
        return CripSimetrica(conteudo);
      throw new NotImplementedException("Não há método para criptografar com este tipo de criptografia.");
    }

    static public TipoCriptografia ObterTipoCriptografia(char tipo)
    {
      if (tipo == 'A')
        return TipoCriptografia.Assimetrica;
      if (tipo == 'S')
        return TipoCriptografia.Simetrica;
      throw new Exception("Caracter inválido para tipo de criptografia.");
    }

    static public TipoCriptografia TipoCriptografiaPadrao
    {
      get
      {
        char tipo = Convert.ToChar("S");
        return ObterTipoCriptografia(tipo);
      }
    }

    /***** Assimetrica *****/

    private static string strIV = "abcdefghijklmnmo";
    private static string strKey = "abcdefghijklmnmoabcdefghijklmnmo";

    public static byte[] CripAssimetrica(byte[] conteudo)
    {
      return Encryptor(conteudo);
    }

    public static byte[] DesCripAssimetrica(byte[] conteudo)
    {
      return Decryptor(conteudo);
    }

    private static byte[] Encryptor(byte[] TextToEncrypt)
    {
      //Turn the plaintext into a byte array.
      //byte[] PlainTextBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(TextToEncrypt);

      //Setup the AES providor for our purposes.
      AesCryptoServiceProvider aesProvider = new AesCryptoServiceProvider();
      aesProvider.BlockSize = 128;
      aesProvider.KeySize = 256;
      aesProvider.Key = System.Text.Encoding.ASCII.GetBytes(strKey);
      aesProvider.IV = System.Text.Encoding.ASCII.GetBytes(strIV);
      aesProvider.Padding = PaddingMode.PKCS7;
      aesProvider.Mode = CipherMode.CBC;

      ICryptoTransform cryptoTransform = aesProvider.CreateEncryptor(aesProvider.Key, aesProvider.IV);
      byte[] EncryptedBytes = cryptoTransform.TransformFinalBlock(TextToEncrypt, 0, TextToEncrypt.Length);
      return EncryptedBytes;
    }

    private static byte[] Decryptor(byte[] TextToDecrypt)
    {

      //Setup the AES provider for decrypting.            
      AesCryptoServiceProvider aesProvider = new AesCryptoServiceProvider();
      aesProvider.BlockSize = 128;
      aesProvider.KeySize = 256;
      aesProvider.Key = System.Text.Encoding.ASCII.GetBytes(strKey);
      aesProvider.IV = System.Text.Encoding.ASCII.GetBytes(strIV);
      aesProvider.Padding = PaddingMode.PKCS7;
      aesProvider.Mode = CipherMode.CBC;

      ICryptoTransform cryptoTransform = aesProvider.CreateDecryptor(aesProvider.Key, aesProvider.IV);
      byte[] DecryptedBytes = cryptoTransform.TransformFinalBlock(TextToDecrypt, 0, TextToDecrypt.Length);
      return DecryptedBytes;
    }


    /***** Fim da Assimetrica *****/

    /***** Inicia dados da Simetrica *****/
    private static byte[] Key = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };
    private static byte[] IV = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };

    public static byte[] CripSimetrica(byte[] conteudo)
    {
      return EncryptString(conteudo, Key, IV);
    }

    public static byte[] DesCripSimetrica(byte[] conteudo)
    {
      return DecryptString(conteudo, Key, IV);
    }

    public static byte[] EncryptString(byte[] clearData, byte[] Key, byte[] IV)
    {
      MemoryStream ms = new MemoryStream();
      Rijndael alg = Rijndael.Create();
      alg.Key = Key;
      alg.IV = IV;
      CryptoStream cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write);
      cs.Write(clearData, 0, clearData.Length);

      cs.Close();
      byte[] encryptedData = ms.ToArray();
      return encryptedData;
    }

    public static byte[] DecryptString(byte[] cipherData, byte[] Key, byte[] IV)
    {
      MemoryStream ms = new MemoryStream();
      Rijndael alg = Rijndael.Create();
      alg.Key = Key;
      alg.IV = IV;
      CryptoStream cs = new CryptoStream(ms, alg.CreateDecryptor(), CryptoStreamMode.Write);
      cs.Write(cipherData, 0, cipherData.Length);
      cs.Close();
      byte[] decryptedData = ms.ToArray();
      return decryptedData;
    }

    /***** Fim da Simetrica *****/


  }
}