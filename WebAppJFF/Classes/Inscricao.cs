﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Classes.Base;

namespace Classes
{
  public class Inscricao : BaseAtivoEntidade
  {
    private Time time;
    public virtual Time Time
    {
      get { return time; }
      set { time = value; }
    }

    private Campeonato campeonato;
    public virtual Campeonato Campeonato
    {
      get { return campeonato; }
      set { campeonato = value; }
    }

    private DateTime dataInscricao;
    public virtual DateTime DataInscricao
    {
      get { return dataInscricao; }
      set { dataInscricao = value; }
    }

    private DateTime dataConfirmacao;
    public virtual DateTime DataConfirmacao
    {
      get { return dataConfirmacao; }
      set { dataConfirmacao = value; }
    }

    private Usuario usuarioAdm;
    public virtual Usuario UsuarioAdm
    {
      get { return usuarioAdm; }
      set { usuarioAdm = value; }
    }
  }
}
