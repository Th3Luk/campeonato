﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Classes;
using Classes.Base;

namespace Classes
{
  public class Campeonato : BaseNomeEntidade 
  {
    private int idJogo;
    public virtual int IdJogo
    {
      get { return idJogo; }
      set { idJogo = value; }
    }

    private double valorInscricao;
    public virtual double ValorInscricao
    {
      get { return valorInscricao; }
      set { valorInscricao = value; }
    }

    private double valorPremio;
    public virtual double ValorPremio
    {
      get { return valorPremio; }
      set { valorPremio = value; }
    }

    private Usuario adm;
    public virtual Usuario Adm
    {
      get { return adm; }
      set { adm = value; }
    }

    private DateTime dataInscricaoInicio;
    public virtual DateTime DataInscricaoInicio
    {
      get { return dataInscricaoInicio; }
      set { dataInscricaoInicio = value; }
    }

    private DateTime dataInscricaoFinal;
    public virtual DateTime DataInscricaoFinal
    {
      get { return dataInscricaoFinal; }
      set { dataInscricaoFinal = value; }
    }

    private DateTime dataInicioCamp;
    public virtual DateTime DataInicioCamp
    {
      get { return dataInicioCamp; }
      set { dataInicioCamp = value; }
    }

    private DateTime dataFinalCamp;
    public virtual DateTime DataFinalCamp
    {
      get { return dataFinalCamp; }
      set { dataFinalCamp = value; }
    }

    private int numeroMaximoTimes;
    public virtual int NumeroMaximoTimes
    {
      get { return numeroMaximoTimes; }
      set { numeroMaximoTimes = value; }
    }

    private byte[] logo;
    public virtual byte[] Logo
    {
      get { return logo; }
      set { logo = value; }
    }

    private string descricaoResumida;
    public virtual string DescricaoResumida
    {
      get { return descricaoResumida; }
      set { descricaoResumida = value; }
    }


  }
}
