﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes.Base;
using Classes;

namespace Classes
{
  public class PerfilJogador : BaseNomeEntidade
  {
    private string ra;
    public virtual string RA
    {
      get { return ra; }
      set { ra = value; }
    }

    private string email;
    public virtual string Email
    {
      get { return email; }
      set { email = value; }
    }

    private int idFaculdade;
    public virtual int IdFaculdade
    {
      get { return idFaculdade; }
      set { idFaculdade = value; }
    }

    private string twitch;
    public virtual string Twitch
    {
      get { return twitch; }
      set { twitch = value; }
    }

    private byte[] imagem; //verificar a melhor maneira de mapear imagem
    public virtual byte[] Imagem
    {
      get { return imagem; }
      set { imagem = value; }
    }
  }
}