﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;
using DAO.Base;
using Interfaces.DAL;


namespace DAO {
  public class TimeDAO : BaseDescricaoDAO<Time>, ITimeDAO {
    public virtual void SelecionarPorParametros(int? idPerfilJogador, int idJogo, string nome, int startRowIndex, int maximumRows, string sortOrder, out int Total, out IList<Time> resultado) {
      try {

        Selecionar("p", startRowIndex, maximumRows, sortOrder, out Total, out resultado,
      criteria => {
        DetachedCriteria detachedVia = DetachedCriteria.For<PerfilJogo>("perfil").
          Add(Restrictions.EqProperty("perfil.Time.Id", "Time.Id")).
          SetProjection(Projections.Id());
        detachedVia.Add(Expression.Eq("perfil.IdJogo", idJogo));
        criteria.Add(Subqueries.Exists(detachedVia));

        if (idPerfilJogador != null)
          criteria.Add(Expression.Eq("Capitao.Id", idPerfilJogador));
        if (nome != null)
          criteria.Add(Expression.InsensitiveLike("Nome", nome, MatchMode.Anywhere));
      });
      } catch (Exception) {
        throw;
      }
    }
  }
}