﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;
using DAO.Base;
using Interfaces.DAL;

namespace DAO
{
  public class PerfilJogoDAO : BaseDescricaoDAO<PerfilJogo>, IPerfilJogoDAO
  {
    public virtual PerfilJogo SelecionarPorParametros(int idJogador, int idJogo)
    {
      try
      {
        return Obter(criteria =>
        {
          criteria.Add(Expression.Eq("PerfilJogador.Id", idJogador));
          if (idJogo != 0)
            criteria.Add(Expression.Eq("IdJogo", idJogo));
        });
      }
      catch (Exception ex)
      {
        throw;
      }
    }

    public void SelecionarParametrosPaginacao(string login, int? idFaculdade, int? idPosicao, int? idJogo, int? idTime, bool somenteSemTime, int startRowIndex, int maximumRows, string sortOrder, out int Total, out IList<PerfilJogo> resultado)
    {
      Selecionar("p", startRowIndex, maximumRows, sortOrder, out Total, out resultado,
        criteria =>
        {
          if (idFaculdade.HasValue)
          {
            DetachedCriteria detachedAssunto = criteria.CreateCriteria("PerfilJogador", "PerfilJogador");
            if (idFaculdade.HasValue)
              detachedAssunto.Add(Expression.Eq("IdFaculdade", idFaculdade));
          }
          if (!string.IsNullOrEmpty(login))
            criteria.Add(Expression.InsensitiveLike("Nome", login, MatchMode.Anywhere));

          if (idPosicao != null)
            criteria.Add(Expression.Eq("IdPosicao", idPosicao));

          if (idJogo != null)
            criteria.Add(Expression.Eq("IdJogo", idJogo));

          if (idTime != null)
            criteria.Add(Expression.Eq("Time.Id", idTime));   
          if (somenteSemTime)
            criteria.Add(Expression.IsNull("Time.Id")); 
        });
    }

  }
}