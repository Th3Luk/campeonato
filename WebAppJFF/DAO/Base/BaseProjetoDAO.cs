﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;
using Classes.Base;
using Interfaces.Base;


namespace DAO.Base
{
  public class BaseProjetoDAO<T> : DAO<T>, IBaseDAO<T>, IBaseProjetoDAO<T>
    where T : BaseEntidade {
    public BaseEntidade Base {
      get;
      set;
    }
  }
}