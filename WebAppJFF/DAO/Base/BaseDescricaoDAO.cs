﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes.Base;
using NHibernate.Criterion;
using Interfaces.Base;

namespace DAO.Base
{
  public class BaseDescricaoDAO<T> : BaseAtivoDAO<T>, IBaseDescricaoDAO<T>
    where T : BaseEntidade {

    #region IBaseDescricaoDAO<T> Members

      public virtual T ObterPorNome(string nome)
      {
          try
          {
              T resultado = ObterPorCriterios(Expression.Eq("Nome", nome));
              return resultado;
          }
          catch (Exception)
          {
              throw;
          }
      }

    public virtual IList<T> SelecionarPorDescricao(string descricao) {
      try {
        IList<T> resultado = SelecionarPorCriterios(Expression.Eq("Descricao", descricao));
        return resultado;
      } catch (Exception) {
        throw;
      }
    }

    public virtual IList<T> SelecionarPorDescricaoAutoComplete(string descricao) {
      try {
        IList<T> resultado = SelecionarPorCriterios(Expression.InsensitiveLike("Descricao", descricao, MatchMode.Anywhere));
        return resultado;
      } catch (Exception) {
        throw;
      }
    }

    public virtual IList<T> SelecionarAtivosPorEdicao(int edicao) {
        IList<T> Resultado;
        int Total;

        Selecionar(0, 1000, "Nome", out Total, out Resultado, criteria =>
        {
                criteria.Add(Expression.Eq("Edicao.Id", edicao));
                criteria.Add(Expression.Eq("Ativo", true));
        });
        return Resultado; 
    }

    public virtual IList<T> SelecionarAtivosPorDescricaoAutoComplete(string descricao) {
        try
        {
            IList<T> resultado = SelecionarPorCriterios(Expression.Eq("Eliminatoria.Id", Convert.ToInt32(descricao)), Expression.Eq("Ativo", true));
            return resultado;
        }
        catch (Exception)
        {
            throw;
        }
    }

    #endregion
  }
}