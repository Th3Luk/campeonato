﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate.Criterion;
using NHibernate;
using NHibernate.Transform;
using NHibernate.Impl;
using System.ComponentModel;
using Classes.Base;
using NHibernate.SqlCommand;
using Interfaces.Base;

namespace DAO.Base
{
  public static class OrderHelper {
    public static Order GetOrderFromString(string order) {
      Order o;
      if (order.EndsWith(" DESC", StringComparison.InvariantCultureIgnoreCase))
        o = FromListDirection(order.Remove(order.Length - " DESC".Length), ListSortDirection.Descending);
      else
        if (order.EndsWith(" ASC", StringComparison.InvariantCultureIgnoreCase))
          o = FromListDirection(order.Remove(order.Length - " ASC".Length), ListSortDirection.Ascending);
        else
          o = FromListDirection(order, ListSortDirection.Ascending);
      return o;
    }


    public static Order[] FromString(string properties) {
      string[] parts = properties.Split(',');
      Order[] result = new Order[parts.Length];
      for (int i = 0; i < parts.Length; i++) {
        string property = parts[i];
        if (property.EndsWith(" DESC", StringComparison.InvariantCultureIgnoreCase))
          result[i] = FromListDirection(property.Substring(0, property.Length - " DESC".Length).Trim(), ListSortDirection.Descending);
        else if (property.EndsWith(" ASC", StringComparison.InvariantCultureIgnoreCase))
          result[i] = FromListDirection(property.Substring(0, property.Length - " ASC".Length).Trim(), ListSortDirection.Ascending);
        else
          result[i] = FromListDirection(property, ListSortDirection.Ascending);
      }
      return result;
    }

    public static Order FromListDirection(string propertyName, ListSortDirection direction) {
      switch (direction) {
        case ListSortDirection.Ascending:
          return Order.Asc(propertyName);
        case ListSortDirection.Descending:
          return Order.Desc(propertyName);
        default:
          throw new ArgumentOutOfRangeException("direction");
      }
    }

    public static bool IsOrderConstraints(ref Order order, out string[] classes) {
      classes = null;
      string property = order.ToString();
      string propertyName;
      ListSortDirection direction = ListSortDirection.Ascending;
      if (property.EndsWith(" DESC", StringComparison.InvariantCultureIgnoreCase)) {
        propertyName = property.Substring(0, property.Length - " DESC".Length).Trim();
        direction = ListSortDirection.Descending;
      }
      else if (property.EndsWith(" ASC", StringComparison.InvariantCultureIgnoreCase))
        propertyName = property.Substring(0, property.Length - " ASC".Length).Trim();
      else
        propertyName = property;
      if (!propertyName.Contains("."))
        return false;
      string[] partes = propertyName.Split('.');
      order = FromListDirection(partes[partes.Length - 1], direction);
      classes = partes.Take(partes.Length - 1).ToArray();
      return true;
    }
  }


  public abstract class DAO<T> : MarshalByRefObject, IBaseDAO<T> where T : BaseEntidade {

    #region Metodos auxiliares
    public int ObterTotal() {
      try {
        ISession session = NHibernateHelper.OpenSession();
        ICriteria criteria = session.CreateCriteria(typeof(T), typeof(T).Name);
        int total = criteria.SetProjection(Projections.RowCount()).UniqueResult<int>();
        return total;
      }
      catch (Exception) {
        throw;
      }
    }

    public virtual int ObterTotal(Action<DetachedCriteria> configurar) {
      if (configurar == null)
        throw new ArgumentNullException("configurar");
      try {
        DetachedCriteria query = DetachedCriteria.For<T>(typeof(T).Name);
        configurar(query);
        ISession session = NHibernateHelper.OpenSession(); 
        ICriteria criteria = query.GetExecutableCriteria(session);
        return criteria.SetProjection(Projections.CountDistinct("Id")).UniqueResult<int>();

      }
      catch (Exception) {
        throw;
      }
    }

    protected ICriteria ObterSubCriteria(CriteriaImpl criteria, ICriteria criteriaParent, string classe) {
      foreach (CriteriaImpl.Subcriteria subCriteria in criteria.IterateSubcriteria())
        if (subCriteria.Path == classe && subCriteria.Parent == criteriaParent)
          return subCriteria;
      return criteriaParent.CreateCriteria(classe, classe);
    }

    protected void CriarAliasSeNecessario(ICriteria criteria, string associationPath, string alias) {
      bool necessario = true;
      CriteriaImpl impl = criteria as CriteriaImpl;
      foreach (CriteriaImpl.Subcriteria subCriteria in impl.IterateSubcriteria()) {
        if (subCriteria.Alias == alias) {
          necessario = false;
          break;
        }
      }
      if (necessario)
        criteria.CreateAlias(associationPath, alias, JoinType.LeftOuterJoin);
    }

    protected virtual void OnAntesObterPorCriterios(ISession session, ICriteria criteria) {
    }

    protected virtual void OnAposObterPorCriterios(ISession session, ICriteria criteria, T resultado) {
    }

    protected virtual void OnAposSelecionarCriteria(ISession session, ICriteria criteria, IList<T> resultado) {
    }

    /// <summary>
    /// Método para evitar o cache de objetos recem inseridos, evitando erro nas grids por misturar o tipo da classe com o proxy gerado pelo nhibernate.
    /// </summary>
    public void LimparSession() {
      //SessionHelper.Current.Clear();
    }
    #endregion

    #region Metodos de criação, atualização e exclusão
    public virtual void Inserir(T item) {
      if (item == null)
        throw new ArgumentNullException("item");
    
      using (ISession session = NHibernateHelper.OpenSession())
      using (ITransaction transaction = session.BeginTransaction()) {
        session.Save(item);
        transaction.Commit();
        
      }

    }

    public virtual void Atualizar(T item)
    {
        if (item == null)
            throw new ArgumentNullException("item");
        using (ISession session = NHibernateHelper.OpenSession())
        using (ITransaction transaction = session.BeginTransaction())
        {
            {
                session.Update(item);
                transaction.Commit();
            }
        }
    }

    public virtual void Excluir(int id) {
      using (ISession session = NHibernateHelper.OpenSession())
      using (ITransaction transaction = session.BeginTransaction()) {
        T item = session.Get<T>(id);
        if (item != null)
          session.Delete(item);
        transaction.Commit();
       
      }
    }
    #endregion

    #region Metodos para recuperar uma instancia.
    public virtual T ObterPorId(int id) {
      try {
        T resultado = ObterPorCriterios(Expression.Eq("Id", id));
        return resultado;
      }
      catch (Exception) {
        throw;
      }
    }

    protected T ObterPorCriterios(params ICriterion[] criterios) {
      T resultado;
      try
      {
          using (ISession session = NHibernateHelper.OpenSession())
          {
              ICriteria criteria = session.CreateCriteria(typeof(T), typeof(T).Name);
              if (criterios != null)
              {
                  foreach (ICriterion criterio in criterios)
                      criteria.Add(criterio);
              }
              OnAntesObterPorCriterios(session, criteria);
              resultado = criteria.UniqueResult<T>();
              OnAposObterPorCriterios(session, criteria, resultado);
          }
      }
      catch (Exception)
      {
          throw;
      }
      return resultado;
    }

    protected T Obter(Action<DetachedCriteria> configurar) {
      if (configurar == null)
        throw new ArgumentNullException("configurar");
      T resultado;
      try {
        DetachedCriteria query = DetachedCriteria.For<T>(typeof(T).Name);
        configurar(query);
        using (ISession session = NHibernateHelper.OpenSession())
        {
            ICriteria criteria = query.GetExecutableCriteria(session);
            OnAntesObterPorCriterios(session, criteria);
            resultado = criteria.UniqueResult<T>();
            OnAposObterPorCriterios(session, criteria, resultado);
        }
      }
      catch (Exception) {
        throw;
      }
      return resultado;
    }
    #endregion

    #region Metodos de selecao
    public virtual IList<T> SelecionarTodos() {
      return SelecionarPorCriterios(null);
    }

    public void Selecionar(int startRowIndex, int maximumRows, string sortOrder, out int total, out IList<T> result) {
      try {
        Selecionar(startRowIndex, maximumRows, sortOrder, out total, out result, (ICriterion[])null);
      }
      catch (Exception) {
        throw;
      }
    }

    protected virtual void Selecionar(int startRowIndex, int maximumRows, string sortOrder, out int total, out IList<T> result, params ICriterion[] criterios) {
      try {
          using (ISession session = NHibernateHelper.OpenSession())
          {
              ICriteria criteria;
              criteria = session.CreateCriteria(typeof(T), typeof(T).Name);

              if (criterios != null)
              {
                  foreach (ICriterion criterio in criterios)
                      criteria.Add(criterio);
              }

              ICriteria criteriaTotal = (ICriteria)criteria.Clone();
              total = criteriaTotal.SetProjection(Projections.RowCount()).UniqueResult<int>();

              criteria.SetResultTransformer(Transformers.RootEntity).SetFirstResult(startRowIndex).SetMaxResults(maximumRows);
              AplicarOrdenacao(sortOrder, criteria);

              result = criteria.List<T>();
          } 
     } catch (Exception) {
        throw;
      }
    }

    protected void AplicarOrdenacao(string sortOrder, ICriteria criteria) {
      if (!string.IsNullOrEmpty(sortOrder)) {
        foreach (Order order in OrderHelper.FromString(sortOrder)) {
          ICriteria criteriaAuxiliar = criteria;
          string[] classes;
          Order newOrder = order;
          if (OrderHelper.IsOrderConstraints(ref newOrder, out classes)) {
            foreach (string classe in classes)
              criteriaAuxiliar = ObterSubCriteria((CriteriaImpl)criteria, criteriaAuxiliar, classe);
          }
          criteriaAuxiliar.AddOrder(newOrder);
        }
      }
    }

    protected virtual IList<T> SelecionarPorCriterios(params ICriterion[] criterios) {
      IList<T> resultado;
      try
      {
          using (ISession session = NHibernateHelper.OpenSession())
          {
              ICriteria criteria = session.CreateCriteria(typeof(T), typeof(T).Name);
              if (criterios != null)
              {
                  foreach (ICriterion criterio in criterios)
                      criteria.Add(criterio);
              }
              resultado = criteria.List<T>();
              OnAposSelecionarCriteria(session, criteria, resultado);
          }
      }
      catch (Exception)
      {
          throw;
      }
      return resultado;
    }

    public IList<T> SelecionarPorIds(int[] ids) {
      IList<T> resultado;
      try
      {
          using (ISession session = NHibernateHelper.OpenSession())
          {
              ICriteria criteria = session.CreateCriteria(typeof(T), typeof(T).Name);
              criteria.Add(Expression.In("Id", ids));
              resultado = criteria.List<T>();
              OnAposSelecionarCriteria(session, criteria, resultado);
          }
      }
      catch (Exception)
      {
          throw;
      }
      return resultado;
    }

    protected virtual IList<T> Selecionar(Action<DetachedCriteria> configurar) {
      if (configurar == null)
        throw new ArgumentNullException("configurar");
      IList<T> resultado;
      try
      {
          DetachedCriteria query = DetachedCriteria.For<T>(typeof(T).Name);
          configurar(query);

          
            ISession session = NHibernateHelper.OpenSession();
              ICriteria criteria = query.GetExecutableCriteria(session);
              resultado = criteria.List<T>();
          
      }
      catch (Exception)
      {
          throw;
      }
      return resultado;
    }

    protected virtual IEnumerable<int> SelecionarIds(Action<DetachedCriteria> configurar) {
      if (configurar == null)
        throw new ArgumentNullException("configurar");
      IList<int> resultado;
      try
      {
          DetachedCriteria query = DetachedCriteria.For<T>();
          configurar(query);
          using (ISession session = NHibernateHelper.OpenSession())
          {
              ICriteria criteria = query.GetExecutableCriteria(session);
              criteria.SetProjection(Projections.Property("Id"));
              resultado = criteria.List<int>();
          }
      }
      catch (Exception)
      {
          throw;
      }
      return resultado.ToArray();
    }

    protected virtual void SelecionarDistintos(int startRowIndex, int maximumRows, string sortOrder, out int total, out IList<T> resultado, Action<DetachedCriteria> configurar) {
      if (string.IsNullOrEmpty(sortOrder))
        sortOrder = "id";
      if (configurar == null)
        throw new ArgumentNullException("configurar");
      try
      {
          DetachedCriteria query;
          ICriteria criteria;
          using (ISession session = NHibernateHelper.OpenSession())
          {
              query = DetachedCriteria.For<T>(typeof(T).Name);
              configurar(query);

              criteria = query.GetExecutableCriteria(session);
              ICriteria criteriaTotal = (ICriteria)criteria.Clone();
              total = criteriaTotal.SetProjection(Projections.CountDistinct("Id")).UniqueResult<int>();

              ProjectionList projList = Projections.ProjectionList();
              projList.Add(Projections.Distinct(Projections.Id()));
              projList.Add(Projections.Property(sortOrder.Replace(" DESC", "")));
              criteria.SetProjection(projList).SetFirstResult(startRowIndex).SetMaxResults(maximumRows);

              AplicarOrdenacaoCompleta(sortOrder, criteria);
              IList<object[]> distinctResult = criteria.List<object[]>();
              if (distinctResult.Any())
              {
                  IList<object> ids = distinctResult.Select(p => p[0]).ToList();
                  criteria = (ICriteria)session.CreateCriteria(typeof(T)).Clone();
                  criteria.Add(Expression.In("Id", ids.ToArray()));
                  resultado = criteria.List<T>();
              }
              else
                  resultado = new List<T>();
              OnAposSelecionarCriteria(session, criteria, resultado);
          }
      }
      catch (Exception)
      {
          throw;
      }
    }

    protected virtual void Selecionar(int startRowIndex, int maximumRows, string sortOrder, out int total, out IList<T> resultado, Action<DetachedCriteria> configurar) {
      if (string.IsNullOrEmpty(sortOrder))
        sortOrder = "id";
      if (configurar == null)
        throw new ArgumentNullException("configurar");
      try
      {
          DetachedCriteria query;
          ICriteria criteria;
          using (ISession session = NHibernateHelper.OpenSession())
          {
              query = DetachedCriteria.For<T>(typeof(T).Name);
              configurar(query);
              criteria = query.GetExecutableCriteria(session);
              ICriteria criteriaTotal = (ICriteria)criteria.Clone();
              total = criteriaTotal.SetProjection(Projections.RowCount()).UniqueResult<int>();
              criteria.SetResultTransformer(Transformers.RootEntity).SetFirstResult(startRowIndex).SetMaxResults(maximumRows);
              AplicarOrdenacaoCompleta(sortOrder, criteria);
              resultado = criteria.List<T>();

              OnAposSelecionarCriteria(session, criteria, resultado);
          }
      }
      catch (Exception)
      {
          throw;
      }
    }

    protected virtual void Selecionar(string rootAlias, int startRowIndex, int maximumRows, string sortOrder, out int total, out IList<T> resultado, Action<DetachedCriteria> configurar) {
      if (string.IsNullOrEmpty(sortOrder))
        sortOrder = "id";
      if (configurar == null)
        throw new ArgumentNullException("configurar");
      try {
        DetachedCriteria query;
        ICriteria criteria;
        using (ISession session = NHibernateHelper.OpenSession())
        {
            query = DetachedCriteria.For<T>(rootAlias);
            configurar(query);
            criteria = query.GetExecutableCriteria(session);
            ICriteria criteriaTotal = (ICriteria)criteria.Clone();
            total = criteriaTotal.SetProjection(Projections.RowCount()).UniqueResult<int>();
            criteria.SetResultTransformer(Transformers.RootEntity).SetFirstResult(startRowIndex).SetMaxResults(maximumRows);

            AplicarOrdenacaoCompleta(sortOrder, criteria);
            resultado = criteria.List<T>();
            OnAposSelecionarCriteria(session, criteria, resultado);
        }
      } catch (Exception) {
        throw;
      }
    }

    protected void AplicarOrdenacaoCompleta(string sortOrder, ICriteria criteria) {
      if (!string.IsNullOrEmpty(sortOrder)) {
        for (int j = 0; j < sortOrder.Split(',').Length; j++) {

          string[] ordemArray = sortOrder.Split(',')[j].Split('.');
          string ordemAlias = "";
          string sentido = " ASC";
          if (ordemArray[ordemArray.Length - 1].EndsWith(" DESC", StringComparison.InvariantCultureIgnoreCase))
            sentido = " DESC";
          for (int i = 0; i < ordemArray.Length; i++) {
            string ordem = ordemArray[i];
            if ((i == 0) && (ordemArray.Length != 1)) {//se é o primeiro e não unico
              CriarAliasSeNecessario(criteria, ordem, ordem);//se já tiver um alias usar o existente
              ordemAlias = ordem + ".";
            }
            else
              if (i != (ordemArray.Length - 1)) {//se nao é primeiro nem ultimo
                CriarAliasSeNecessario(criteria, ordemAlias + ordem, ordem);//se já tiver um alias usar o existente
                ordemAlias = ordem + ".";
              }
            if (i == (ordemArray.Length - 1)) {//se é o ultimo
              if (!ordem.EndsWith(sentido, StringComparison.InvariantCultureIgnoreCase))
                ordem += sentido;
              criteria.AddOrder(OrderHelper.GetOrderFromString(ordemAlias + ordem));
            }
          }
        }
      }
    }
    #endregion
  }
}