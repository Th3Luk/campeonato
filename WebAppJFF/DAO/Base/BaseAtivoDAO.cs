﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes.Base;
using NHibernate.Criterion;
using Interfaces.Base;

namespace DAO.Base
{
  public abstract class BaseAtivoDAO<T> : DAO<T>, IBaseAtivoDAO<T>
    where T : BaseEntidade {
    public virtual IList<T> SelecionarAtivos() {
      try {
        IList<T> resultado = SelecionarPorCriterios(Expression.Eq("Ativo", true));
        return resultado;
      } catch (Exception) {
        throw;
      }
    }
    public virtual void SelecionarAtivos(int startRowIndex, int maximumRows, string sortOrder, out int total, out IList<T> resultado) {
      try {
        Selecionar(startRowIndex, maximumRows, sortOrder, out total, out resultado, Expression.Eq("Ativo", true));
      } catch (Exception) {
        throw;
      }
    }
  }
}