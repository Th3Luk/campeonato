﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate;
using FluentNHibernate;
using System.Reflection;
using FluentNHibernate.Conventions.Helpers;
using System;
using NHibernate.Cfg;

namespace DAO{
  public class NHibernateHelper {
    private static ISessionFactory _sessionFactory;
    private static ISessionFactory sessionFactory {
      get {
        if (_sessionFactory == null) {
          var configuration = new Configuration();
          configuration.Configure();
          PersistenceModel model = new PersistenceModel();
          model.AddMappingsFromAssembly(Assembly.Load("WebAppJFF"));
          model.Conventions.Add(ForeignKey.Format((prop, type) => String.Concat("id", type.Name)));
          model.Configure(configuration);
          _sessionFactory = configuration.BuildSessionFactory();
        }
        return _sessionFactory;
      }
    }
    public static ISession OpenSession() {
      return sessionFactory.OpenSession();

    }

    public static void CloseSession()
    {
      sessionFactory.Close();
    }
  }

}