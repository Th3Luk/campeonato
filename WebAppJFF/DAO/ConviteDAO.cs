﻿using Interfaces.DAL;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAO.Base;
using Classes;

namespace DAO
{
  public class ConviteDAO : DAO<ConviteJogTime>, IConviteDAO
  {


    public void SelecionarParametrosPaginacao(int? idJogador, int? idTime, int startRowIndex, int maximumRows, string sortOrder, out int Total, out IList<ConviteJogTime> resultado)
    {
      Selecionar(startRowIndex, maximumRows, sortOrder, out Total, out resultado,
        criteria =>
        {
          if (idJogador != null)
            criteria.Add(Expression.Eq("Jogador.Id", idJogador));

          if (idTime != null)
            criteria.Add(Expression.Eq("Time.Id", idTime));
        });
    }
  }
}
