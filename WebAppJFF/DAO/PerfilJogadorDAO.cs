﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;
using DAO.Base;
using Interfaces.DAL;

namespace DAO
{
  public class PerfilJogadorDAO : BaseDescricaoDAO<PerfilJogador>, IPerfilJogadorDAO
  {
    
    public virtual PerfilJogador SelecionarPorParametros(int idUsuario)
    {
      try
      {
        PerfilJogador resultado = ObterPorCriterios(Expression.Eq("Usuario.Id", idUsuario));
        return resultado;
      }
      catch (Exception)
      {
        throw;
      }
    }

  }
}