﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;
using DAO.Base;
using Interfaces.DAL;

namespace DAO
{
  public class UsuarioDAO : BaseDescricaoDAO<Usuario>, IUsuarioDAO
  {
    public Usuario ObterPorLogin(string login)
    {
      return Obter(c =>
      {
        c.Add(Expression.Eq("Login", login));
      });
    }
  }
}