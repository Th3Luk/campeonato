﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;
using Classes.Base;

namespace Interfaces.Base
{
  public interface IBaseProjetoDAO<T> : IBaseDAO<T>
    where T : BaseEntidade {
    BaseEntidade Base { get; set; }
  }
}