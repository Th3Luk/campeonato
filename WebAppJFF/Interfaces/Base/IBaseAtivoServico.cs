﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes.Base;

namespace Interfaces.Base
{
  public interface IBaseAtivoServico<T> : IBaseServico<T> where T : BaseAtivoEntidade {
    IList<T> SelecionarAtivos();
    IList<T> SelecionarAtivos(int startRowIndex, int maximumRows, string sortOrder);
  }
}