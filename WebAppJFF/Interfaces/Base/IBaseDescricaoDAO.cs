﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes.Base;

namespace Interfaces.Base
{
  public interface IBaseDescricaoDAO<T> : IBaseAtivoDAO<T>
    where T : BaseEntidade {
    IList<T> SelecionarPorDescricao(string descricao);
    IList<T> SelecionarPorDescricaoAutoComplete(string descricao);
    IList<T> SelecionarAtivosPorEdicao(int edicao);
    IList<T> SelecionarAtivosPorDescricaoAutoComplete(string descricao);
    T ObterPorNome(string nome);
  }
}