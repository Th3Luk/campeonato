﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;
using Classes.Base;

namespace Interfaces.Base
{
  public interface IBaseProjetoServico<T> : IBaseServico<T> where T : BaseEntidade {
    BaseEntidade baseEnt { get; set; }
  }
}