﻿using Interfaces.DAL.InterfaceHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Classes.Base;

namespace Interfaces.DAL.InterfaceHelper {
  public static class Servico {

    private static IDictionary<Type, IList<object>> contratos = new Dictionary<Type, IList<object>>();

    private static bool carregado = false;

    private static T BuscarImlementacao<T>(Type key) {
      if (!contratos.Keys.Contains(key))
        return default(T);

      IList<object> implementacoes = contratos[key];
      if (implementacoes.Count > 1)
        throw new AmbiguousMatchException();

      object obj = implementacoes[0];
      if (!typeof(T).IsAssignableFrom(obj.GetType()))
        return default(T);
      return (T)obj;
    }

    public static T Obter<T>() {
      return BuscarImlementacao<T>(typeof(T));
    }

    public static T Obter<T>(BaseEntidade o) {
      if (o == null)
        throw new ArgumentNullException("o");
      return BuscarImlementacao<T>(o.As<BaseEntidade>().GetType());
    }

    public static void CarregarImplementacoes(Assembly[] assembliesInterfaces, Assembly[] assembliesImplementacoes) {
      lock (contratos) {

        if (carregado)
          throw new Exception("As implementações já foram carregadas.");
        if (assembliesInterfaces == null)
          throw new ArgumentNullException("assembliesInterfaces");
        if (assembliesImplementacoes == null)
          throw new ArgumentNullException("assembliesImplementacoes");

        carregado = true;

        List<Type> interfaces = new List<Type>();
        foreach (Assembly assembly in assembliesInterfaces) {
          foreach (Type tipo in assembly.GetTypes())
            if (tipo.IsInterface)
              interfaces.Add(tipo);
        }

        List<Type> implemetacoes = new List<Type>();
        foreach (Assembly assembly in assembliesImplementacoes) {
          foreach (Type tipo in assembly.GetTypes())
            if (!tipo.IsAbstract && tipo.GetConstructor(Type.EmptyTypes) != null && tipo.GetInterfaces().Length > 0)
              implemetacoes.Add(tipo);
        }

        foreach (Type interf in interfaces) {
          foreach (Type implementacao in implemetacoes) {
            foreach (Type interfImplementada in implementacao.GetInterfaces()) {
              if (interfImplementada == interf) {
                object objImpl = Activator.CreateInstance(implementacao);
                if (!contratos.Keys.Contains(interf))
                  contratos.Add(interf, new List<object>());
                contratos[interf].Add(objImpl);
                InterfaceAttribute servicoAttr = Attribute.GetCustomAttribute(interf, typeof(InterfaceAttribute)) as InterfaceAttribute;
                if (servicoAttr != null && servicoAttr.ClasseEspecifica != null) {
                  if (!contratos.Keys.Contains(servicoAttr.ClasseEspecifica))
                    contratos.Add(servicoAttr.ClasseEspecifica, new List<object>());
                  contratos[servicoAttr.ClasseEspecifica].Add(objImpl);
                }
              }
            }
          }
        }

      }
    }

  }
}
