﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces.DAL.InterfaceHelper {

  [AttributeUsage(AttributeTargets.Interface)]
  public class InterfaceAttribute : Attribute {
    private Type classeEspecifica = null;
    public InterfaceAttribute(Type classeEspecifica) {
      this.classeEspecifica = classeEspecifica;
    }

    public Type ClasseEspecifica {
      get {
        return classeEspecifica;
      }
    }
  }
}
