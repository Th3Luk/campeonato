﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;
using Interfaces.Base;

namespace Interfaces.DAL
{
  public interface IUsuarioDAO : IBaseDescricaoDAO<Usuario>
  {
    Usuario ObterPorLogin(string login);
  }
}