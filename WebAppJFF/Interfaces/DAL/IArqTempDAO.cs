﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Classes;
using Interfaces.Base;

namespace Interfaces.DAL
{
  public interface IArqTempDAO : IBaseDAO<ArqTemp>
  {
  }
}
