﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;
using Interfaces.Base;

namespace Interfaces.DAL
{
  public interface IPerfilJogadorDAO : IBaseDescricaoDAO<PerfilJogador>
  {
    PerfilJogador SelecionarPorParametros(int idUsuario);
  }
}