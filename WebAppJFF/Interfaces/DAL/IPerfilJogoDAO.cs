﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;
using Interfaces.Base;

namespace Interfaces.DAL
{
  public interface IPerfilJogoDAO : IBaseDescricaoDAO<PerfilJogo>
  {
    PerfilJogo SelecionarPorParametros(int idJogador, int idJogo);
    void SelecionarParametrosPaginacao(string login, int? idFaculdade, int? idPosicao, int? idJogo, int? idTime, bool somenteSemTime, int startRowIndex, int maximumRows, string sortOrder, out int Total, out IList<PerfilJogo> resultado);
  }
}