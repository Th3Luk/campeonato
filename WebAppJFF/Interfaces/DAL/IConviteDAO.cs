﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Classes;
using Interfaces.Base;

namespace Interfaces.DAL
{
  public interface IConviteDAO : IBaseDAO<ConviteJogTime>
  {
    void SelecionarParametrosPaginacao(int? idJogador, int? idTime, int startRowIndex, int maximumRows, string sortOrder, out int Total, out IList<ConviteJogTime> resultado);
  }
}
