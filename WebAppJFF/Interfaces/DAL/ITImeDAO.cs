﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;
using Interfaces.Base;

namespace Interfaces.DAL
{
  public interface ITimeDAO : IBaseDescricaoDAO<Time>
  {
    void SelecionarPorParametros(int? idPerfilJogador, int idJogo, string nome, int startRowIndex, int maximumRows, string sortOrder, out int Total, out IList<Time> resultado);
  }
}