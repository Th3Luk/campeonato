﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;
using Interfaces.Base;

namespace Interfaces.Servicos
{
  public interface IUsuarioServico : IBaseDescricaoServico<Usuario>
  {
    Usuario ObterPorLogin(string login);
    bool VerificarAutenticidadeSenha(Usuario usuario, string senha);
    byte[] RetornaSenhaEncript(string senha);
  }
}