﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;
using Interfaces.Base;

namespace Interfaces.Servicos
{
  public interface IFaculdadeServico : IBaseDescricaoServico<Faculdade>
  {
  }
}