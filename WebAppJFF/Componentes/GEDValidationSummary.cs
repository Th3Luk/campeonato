﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Componentes
{
  /**
   * Esta classe se propõe a resolver o seguinte erro do ValidationSummary.
   * O bug afeta o GED criando conflitos javascript, principalmente na utilização
   * da AjaxModalPopupExtender. Outros erros também podem ter sido causados por esta
   * falha.
   * 
   * Veja o erro em "ValidationSummary javascript error" (http://stackoverflow.com/questions/9264846/validationsummary-javascript-error)
   * Transcrição da solução apresentada:
   * 
   *  "Turns out this is a known bug in the ajax control toolkit. They claim it's been fixed in the latest release, 
   *  but I don't think it has. The fix is to create a server control that inherits from the validation summary and 
   *  inserts the one missing semi-colon between the two javascript statements."
   *  
   * Esta solução sujere o código que foi aplicado neste documento.
   * 
   * Outra resposta, no mesmo link:
   * 
   *  "This bug is part of Validation controls in ASP.NET and not AJAX Toolkit. You can turn off client-side validation 
   *  from all validation controls on your page with EnableClientScript="false" and the errors will be gone."
   *  
   * Esta solução não foi considerada, pois causaria um impacto muito grande em todo o sistema.
   * 
   * Mais detalhes sobre o bug em: http://ajaxcontroltoolkit.codeplex.com/workitem/27024
   */
  [ToolboxData("")]
  public partial class GEDValidationSummary : ValidationSummary
  {

    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);
      ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), this.ClientID, ";", true);
    }

  }
}